package com.example.simcareerapp.Persistence.JsonEntities;

import android.util.Log;

import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.CircuitoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Gara;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GaraConJson extends Gara {
//##################################################################################################
//                                  LABEL GARA PER JSON
//##################################################################################################
    protected final String LABLE_SEQUENZA = "seq";
    protected final String LABLE_DATA     = "data";
    protected final String LABLE_CIRCUITO = "circuito";
//##################################################################################################
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
    public GaraConJson(Long id, Date data, String nomeeCircuito) {
        super(id, data, nomeeCircuito);
    }
    public GaraConJson(Gara gara) {
        super(gara.getId(), gara.getData(), gara.getCircuito().getNome());
    }
//--------------------------------------------------------------------------------------------------
    public GaraConJson(JSONObject jsonObject) {
        if(jsonObject!=null) {
            try {
                this.setId(    jsonObject.getLong(LABLE_SEQUENZA) );

                String dataTesto = jsonObject.getString(LABLE_DATA);
                this.setData(null);
                if( ( dataTesto!=null ) && ( !dataTesto.equals("") ) ){
                    try {
                        this.setData(new SimpleDateFormat("dd/MM/yyyy").parse(dataTesto) );
                    } catch (ParseException e) {
                        Log.d("ErrJsonData",e.getMessage());
                    }
                }
                this.setCircuito(null);
                String nomeCircuito =    jsonObject.getString(LABLE_CIRCUITO);
                if( (nomeCircuito!=null) && (!nomeCircuito.equals("")) ){
                    this.setCircuito(new CircuitoConImmagini(nomeCircuito));
                }
            } catch (JSONException e) {
                Log.d("ErrJson",e.getMessage());
            }
        }
    }
//==================================================================================================
//##################################################################################################
//                              GENERA JSON
//##################################################################################################
//==================================================================================================
//                              GENERA JSON GARA
//==================================================================================================
    private static String FORMATTAZIONE_DATE = "dd/MM/yyyy";
    public JSONObject getJSON(){
        JSONObject object = new JSONObject();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(FORMATTAZIONE_DATE);
            String data = sdf.format(this.getData());
            object.put(LABLE_SEQUENZA,  this.getId());
            object.put(LABLE_DATA,      data );
            object.put(LABLE_CIRCUITO,  this.getCircuito().getNome());

        }catch (JSONException e) {
            object=null;
            e.printStackTrace();
        }
        return object;
    }
//==================================================================================================
//##################################################################################################
}
