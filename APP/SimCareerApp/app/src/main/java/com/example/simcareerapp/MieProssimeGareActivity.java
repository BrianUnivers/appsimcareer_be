package com.example.simcareerapp;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.simcareerapp.AppCompatActivityWithMenu.AppCompatActivityWithMyPrimaryMenu;
import com.example.simcareerapp.GestioneJson.GestioneListeCampionati;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.GareDeiCampionatiRVAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface.OnClickClassificaDellaGaraListener;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Gara;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Persistence.JsonEntities.CampionatoConJson;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MieProssimeGareActivity extends AppCompatActivityWithMyPrimaryMenu
                                     implements OnClickClassificaDellaGaraListener {
//##################################################################################################
//                              DATI PASSATI TRAMITE UN INTENT
//##################################################################################################
    private Utente utenteAttivo         = null;
    private Boolean mantieniAttivoUtente= null;
    private List<Campionato> listaMieiCampionatiProssimi  = new LinkedList<Campionato>();
//==================================================================================================
//                              METODO DI RECUPERO DI TALI VALORI DAL INTET CHE HA CHAMATO LA PAGINA
//==================================================================================================
    /**
     * Questo metodo serve per prendere dall'Activity chiamante le informazioni della sessione che
     * di obbligatori sono <<utenteAttivo>> e <<mantieniAttivoUtente>>.
     */
    private void initValoriDiSessioneDellAPP(){
        Intent i =getIntent();
        utenteAttivo            = MyIntent.getUtenteExtra(i,MyIntent.I_UTENTE_ATTIVO);
        mantieniAttivoUtente    = i.getBooleanExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,false);
    }
//==================================================================================================
//==================================================================================================
//                              IMPOSTAZIONI PARAMETRI DA PASSARE PER GLI INTENT DEL MENU LATERALE PRINCIPALE
//==================================================================================================
    @Override
    protected void setParamiter(MyIntent intent) {
        intent.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utenteAttivo);
        intent.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
    }
//==================================================================================================

//==================================================================================================
//                              APERTURA DELLA PAGINA DELLE CLASSIFICHE I VALORI DI SESSIONE
//==================================================================================================
    /**
     * Questo metodo serve per inviare le informazioni della sessione che sono <<utenteAttivo>> e
     * <<mantieniAttivoUtente>> quelli obbligatori e  <<idCampiojnato>> così da visualizarlo.
     * N.B.
     * Da questà activiti è possibile solo andare alla Activity ClassificaActivity senza apportare
     * modifiche.
     */
    private void apriPaginaClassificaCampionato(Long id){
        MyIntent i = new MyIntent(MieProssimeGareActivity.this, ClassificaActivity.class);
        i.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utenteAttivo);
        i.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
        i.putExtra(MyIntent.I_ID_CAMPIONATO_SELEZIONATO,id);
        startActivity(i);
    }
//==================================================================================================
// ##################################################################################################


//##################################################################################################
//                                  CREAZIONE DEL LAYOUT
//##################################################################################################
//==================================================================================================
//                                  CREAZIONE DELL'ACTITVITY
//==================================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  GET DATI DAL INTENT
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        initValoriDiSessioneDellAPP();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        setContentView(R.layout.activity_mie_prossime_gare);
        super.initToolBar(R.menu.bar_menu_vuoto);
        super.impostaPaginaComeHome();
        //Visualizza gare dei campionati
        RecyclerView listaGarePerCampionati = findViewById(R.id.rcl_ListaGarePerCampionato);
       List<CampionatoConJson> listaCampionati = GestioneListeCampionati.letturaECreazioneListaCampionati(this,R.raw.campionati);
        listaMieiCampionatiProssimi = selezionaCampionatiAttiviPerPilota(listaCampionati,utenteAttivo);
        GareDeiCampionatiRVAdapter gareRVAdapter = new GareDeiCampionatiRVAdapter(this,this,this,listaMieiCampionatiProssimi,utenteAttivo);;
        if(gareRVAdapter!=null){
            listaGarePerCampionati.setAdapter(gareRVAdapter);
        }
        listaGarePerCampionati.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false));
    }
//##################################################################################################
//##################################################################################################
//                                  GESTIONE CAMPIONATI
//##################################################################################################
//==================================================================================================
//                                  SELEZIONA CAMPIONATI ATTIVI A CUI SONO ISCRITTO
//==================================================================================================
    private List<Campionato> selezionaCampionatiAttiviPerPilota(List<CampionatoConJson> listaCampionati, Utente utenteAttivo) {
        List<Campionato> ris = new LinkedList<Campionato>();
        Iterator<CampionatoConJson> i = listaCampionati.iterator();
        Date inizio = new Date();
        Date fine = new Date();
        inizio.setTime(inizio.getTime()+(1000*60*60*24*7));//Sette giorni d'anticipo
        while (i.hasNext()){
            Campionato campionato = i.next();
            if((inizio.after(campionato.getDataInizioCampionato()))&&(fine.before(campionato.getDataFineCampionato()))){
                if(campionato.utenteIscritto(utenteAttivo)){
                    ris.add(campionato);
                }
            }
        }

        return ris;
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                                  METODI PER COMUNICATRE CON OnClickClassificaDellaGaraListener
//##################################################################################################
//==================================================================================================
//                                  BOTTONE CLASSIFICA CAMPIONATO
//==================================================================================================
    @Override
    public void visualizzaClassificaDelCampionato(Campionato campionato, Gara gara) {
        apriPaginaClassificaCampionato(campionato.getId());
    }
//==================================================================================================
//##################################################################################################

}