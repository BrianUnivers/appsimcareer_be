package com.example.simcareerapp.GestioneJson.Generale;

import android.content.Context;
import android.util.Log;
import android.util.Pair;

import com.example.simcareerapp.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class GestioneGenerale {
    private static boolean LETTURA = false;
    private static boolean SCRITTURA = true;

//##################################################################################################
//                                  APPOGGIO PER BLOCCARE LA LETTURA E SCRITTUA CONTEMPORANEA
//##################################################################################################
    /*
    * Questa funzione fa da collo di bottiglia così da fare una solo operazione sia in lettura che in
    * scrittura su di un file allo stetto mometo.
    *
    * E' stato costuita così per semplitictà così da non gestire con semafori nel backend l'acceso allo stesso file
    * dato che se ci fosse un server il gestore delle notifice andrebbe a interrogare il server per
    * eventuali modifiche in maniera indipendente da quelle che vengono fatta dalla aplicazione quando
    * si caricano le attivity che richiedono tali informazioni.
    * */
    private synchronized static Pair<String,Boolean> scritturaLetturaFileJsonSynchronized(Context context , int idFile, String cotenuto,Boolean operazione) {
        Pair<String,Boolean> ris;
        if(operazione){
            //STIAMO PER FARE UN OPERAZIONE DI SCRITTURA
            ris = new Pair<String,Boolean>(
                                    null,
                                    scritturaFileJsonSynchronized(context , idFile, cotenuto));
        }else{
            //STIAMO PER FARE UN OPERAZIONE DI LETTURA
            ris = new Pair<String,Boolean>(
                    leturaFileJsonSynchronized(context , idFile)
                    ,null);
        }
        return ris;
    }
//##################################################################################################
//##################################################################################################
//                                  APERTURA E LETTURA FILE                                         <<synchronized>>
//##################################################################################################
    /**
     * Operazione di lettura da un file json peresente nelle risorse della aplicazione.
     */
    private synchronized static String leturaFileJsonSynchronized(Context context , int idFile) {
        String nomeFile = context.getResources().getResourceName(idFile);
        nomeFile = nomeFile.split("/")[1]+"RW.json";

        InputStream inputStream = null;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        FileOutputStream outputStreamCopiaFile = null;//File Se NON c'è
//##################################################################################################
//                                      ATTENZIONE
//##################################################################################################
            /**
             * Questa parte di codice serve per cerare alla prima lettura del file json nelle risorse
             * una copia in cui le succesive letture verrano fatte, poichè solo in quest'ultimo
             * potrà contenere eventuali modifichi(cioè sovrascrizioni) in un secondo momento.
             */
        byte buf[] = new byte[1024];
        int len;
        try {
            File file = new File(context.getFilesDir(),nomeFile);
            if(file.exists()){
                inputStream = new FileInputStream(file);
            }else{
                if (file.createNewFile()) {
                    outputStreamCopiaFile = new FileOutputStream(file);//dove copiare alla prima lettura
                    inputStream = context.getResources().openRawResource(idFile);// originale
                }
            }
//##################################################################################################
            while ((len = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, len);
                if(outputStreamCopiaFile!=null){
                    outputStreamCopiaFile.write(buf,0,len);
                }
            }
            if(outputStreamCopiaFile!=null){
                outputStreamCopiaFile.close();
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.d("ErrLetturaFile",e.getMessage());
        }
        return outputStream.toString();
    }
//##################################################################################################
//##################################################################################################
//                                  APERTURA E SCRITTURA FILE                                       <<synchronized>>
//##################################################################################################
    /**
     * Operazione di scrittura da un file json peresente nelle risorse della aplicazione.
     */
    private synchronized static Boolean scritturaFileJsonSynchronized(Context context , int idFile,String cotenuto) {
        Boolean ris = false;
        String nomeFile = context.getResources().getResourceName(idFile);
        nomeFile = nomeFile.split("/")[1]+"RW.json";

        FileOutputStream outputStreamCopiaFile =null;

        try {
            File file = new File(context.getFilesDir(),nomeFile);
            if(file.exists()){
                outputStreamCopiaFile = new FileOutputStream(file);
            }else{
                if (file.createNewFile()) {
                    outputStreamCopiaFile = new FileOutputStream(file);//dove copiare alla prima lettura
                }
            }
            if(outputStreamCopiaFile!=null){
                outputStreamCopiaFile.write(cotenuto.getBytes());
                outputStreamCopiaFile.flush();
                outputStreamCopiaFile.close();
            }
            ris = true;
        } catch (IOException e) {
            Log.d("ErrScritturaFile",e.getMessage());
        }
        return ris;
    }
//##################################################################################################
//==================================================================================================
//                                  WRAPPER LETTURA
//==================================================================================================
    public synchronized static String leturaFileJson(Context context,int idFile){
        Pair<String,Boolean> ris = scritturaLetturaFileJsonSynchronized(context,idFile,null,LETTURA);
        return ris.first;
    }
//==================================================================================================
//==================================================================================================
//                                  WRAPPER SCRITTURA
//==================================================================================================
    public synchronized static Boolean scritturaFileJson(Context context,int idFile,String cotenuto){
        Pair<String,Boolean> ris = scritturaLetturaFileJsonSynchronized(context,idFile,cotenuto,SCRITTURA);
        return ris.second;
    }
//==================================================================================================
}
