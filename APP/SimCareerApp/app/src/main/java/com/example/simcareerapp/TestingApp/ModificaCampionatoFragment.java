package com.example.simcareerapp.TestingApp;

import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MyChipAdapter.MyChipAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinnerAdapter.ConsumiSpinnerAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyView.MyGridView;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySeparatoriPagine.SeparatorePaginaConTesto;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ModificaCampionatoFragment} factory method to
 * create an instance of this fragment.
 */
public class ModificaCampionatoFragment extends Fragment {

    //private OnFragmentInfoCampionatoFragmentEventListener listener = null;
//##################################################################################################
//                              DATI PASSATI
//##################################################################################################
    private Campionato campionato   = null;
//==================================================================================================
//                                  COSTRUTORI
//==================================================================================================
    public ModificaCampionatoFragment(Campionato campionato) {
        this.campionato   = campionato;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  FRAGMENT
//##################################################################################################
    private View    viewGroupLayout           = null;
    private MyGridView gridViewAiutiAllaGuida = null;
    private MyGridView gridViewBandiere       = null;
    private Spinner    viewSpinnerCarburante  = null;
    private Spinner    viewSpinnerGomme       = null;
//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
//==================================================================================================
//==================================================================================================
//                                  DEFINIZIONE DEL LAYOUT DEL FRAGMENT
//==================================================================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);//<<<-------------------------------------ATTEZIONE
        //Scelta del layout usato per fragment.
        viewGroupLayout = inflater.inflate(R.layout.fragment_modifica_campionato, container, false);

        //Inizializazione contenuti impitatati
        setLayoutDeiInclude();
        //Inizializza layout con i dati
        setLayoutPerCampionato();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  METODO CHIAMATO SULLA SELEZIONE DEI CONSUMI CARBURANTE
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        viewSpinnerCarburante.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                campionato.setConsumiCarburante((String) adapterView.getItemAtPosition(i));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  METODO CHIAMATO SULLA SELEZIONE DEI CONSUMI GOMME
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        viewSpinnerGomme.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                campionato.setConsumiGomme((String) adapterView.getItemAtPosition(i));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  PEREMERE SUI GRIDVIEW
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //BANDIERE
        gridViewBandiere.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,int position, long id) {
                Pair<String, Boolean> elemeto=(Pair<String, Boolean>)gridViewBandiere.getItemAtPosition(position);
                Pair<String, Boolean>  nuovoElemeto = new Pair<>(elemeto.first,!elemeto.second);
                campionato.setStatoDelleBandiera(nuovoElemeto);
                MyChipAdapter adapterBandiere=new MyChipAdapter(getActivity(),campionato.getStatoDelleBandiere(),true);
                gridViewBandiere.setAdapter(adapterBandiere);
            }
        });
//--------------------------------------------------------------------------------------------------
        //AIUTI ALLA GUIDA
       gridViewAiutiAllaGuida.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,int position, long id) {
                Pair<String, Boolean> elemeto=(Pair<String, Boolean>)gridViewAiutiAllaGuida.getItemAtPosition(position);
                Pair<String, Boolean>  nuovoElemeto = new Pair<>(elemeto.first,!elemeto.second);
                campionato.setStatoDellAiutiAllaGuida(nuovoElemeto);
                MyChipAdapter adapterAiutiAllaGuida=new MyChipAdapter(getActivity(),campionato.getStatoDegliAiutiAllaGuida(),true);
                gridViewAiutiAllaGuida.setAdapter(adapterAiutiAllaGuida);
            }
        });

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return viewGroupLayout;
    }

//==================================================================================================

//##################################################################################################
//##################################################################################################
//                                 ADATTA IL CONTENUTO DEL LAYOUT
//##################################################################################################
//==================================================================================================
//                                 IMPOSTAZIONI DELLE VARIANTI DI BLOCHI INCLUSI
//==================================================================================================
    /**
     * Questo metodo imposta il layout dei bolochi neutri che vengono impotati per il loro riutilizzo.
     */
    private void setLayoutDeiInclude() {
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude((ViewGroup) viewGroupLayout,
                R.id.include_dvd_DivisoreAiutiAllaGuida,
                R.string.lbl_AiutiAllaGuida,
                R.drawable.ic_baseline_car_steering_wheel_24_normal_text);
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude((ViewGroup)  viewGroupLayout,
                R.id.include_dvd_DivisoreBandiere,
                R.string.lbl_Bandiera,
                R.drawable.ic_baseline_flag_24_normal_text);
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude((ViewGroup)  viewGroupLayout,
                R.id.include_dvd_DivisoreConsumi,
                R.string.lbl_Consumi,
                R.drawable.ic_baseline_gas_pump_24_normal_text);
    }
//==================================================================================================
//==================================================================================================
//                                 IMPOSTAZIONI IL LAYOUTO IN BASE AL CAMPIONATO
//==================================================================================================
    /**
     * Questo metodo imposta il layout in base al campionato scelto
     */
    private void setLayoutPerCampionato(){
        //Imposta Aiuti alla guida
        gridViewAiutiAllaGuida= (MyGridView) viewGroupLayout.findViewById(R.id.grd_AiutiAlleGuida);
        MyChipAdapter adapterAiutiAllaGuida=new MyChipAdapter(getActivity(),campionato.getStatoDegliAiutiAllaGuida(),true);
        gridViewAiutiAllaGuida.setAdapter(adapterAiutiAllaGuida);

        //Imposta Bandiere
        gridViewBandiere= (MyGridView) viewGroupLayout.findViewById(R.id.grd_Banndiere);
        MyChipAdapter adapterBandiere=new MyChipAdapter(getActivity(),campionato.getStatoDelleBandiere(),true);
        gridViewBandiere.setAdapter(adapterBandiere);


        //Imposta Consumi Carburante
        viewSpinnerCarburante  = viewGroupLayout.findViewById(R.id.spn_InputCarburante);
        ArrayAdapter adapterCarburante = new ConsumiSpinnerAdapter(getContext(),
                                        ConsumiSpinnerAdapter.generareListaConsumiConDefault(
                                                campionato.getConsumiCarburante()
                                        ));
        viewSpinnerCarburante.setAdapter(adapterCarburante);

        //Imposta Consumi Gomme
        viewSpinnerGomme  = viewGroupLayout.findViewById(R.id.spn_InputGomme);
        ArrayAdapter adapterGomme  = new ConsumiSpinnerAdapter(getContext(),
                                        ConsumiSpinnerAdapter.generareListaConsumiConDefault(
                                                campionato.getConsumiGomme()
                                        ));
        viewSpinnerGomme.setAdapter(adapterGomme);
    }
//==================================================================================================
//##################################################################################################



//##################################################################################################
//                                  COMUNICAZIONE CON L'ACTIVITY
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY
//==================================================================================================
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Crea il link con l'activity che contiene il fragment.
        //listener = (OnFragmentInfoCampionatoFragmentEventListener) activity;
    }
//==================================================================================================
//##################################################################################################
}