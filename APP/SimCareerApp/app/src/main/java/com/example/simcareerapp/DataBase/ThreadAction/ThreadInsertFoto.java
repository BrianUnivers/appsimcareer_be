package com.example.simcareerapp.DataBase.ThreadAction;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;

import com.example.simcareerapp.DataBase.Management.SimpleDbHelper;
import com.example.simcareerapp.DataBase.Query.FotoQuery;
import com.example.simcareerapp.Persistence.Entities.Foto;

public class ThreadInsertFoto extends AsyncTask<Pair<Foto,Long>,Integer, Foto> {
    private SimpleDbHelper database;
    private Context context;
//==================================================================================================
//                              COSTRUTORE                                                          <<Processo Principale>>
//==================================================================================================
    /**
     * Per avere la posibilità di accedere alla Accivity e instauro un collegamento col server.
     */
    public ThreadInsertFoto(Context context) {
        //Crazione DataBase o Associazione con uno già creato
        this.database = new SimpleDbHelper(context);
        this.context = context;
    }
//==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE PRIMA DELLA CHAMATA DEL THREAD                     <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
//==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DOPO DELLA CHAMATA DEL THREAD                      <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onPostExecute(Foto foto) {
        super.onPostExecute(foto);
    }
    //==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DURANTE DELLA CHAMATA DEL THREAD                   <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }
//==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DAL THREAD                                         <<Processo Seconario>>
//==================================================================================================
    /**
     * Effetua la richiesta al database della foto profilo del utente con id passato, se presente allora
     * il thread retiutuirà l'oggeto Foto altrimenti restiuirà null;
     */
    @Override
    protected Foto doInBackground(Pair<Foto,Long>... item) {
        Foto foto       = item[0].first;
        Long idUtente   = item[0].second;
        long id = FotoQuery.insertFotoNuovaFotoAttivaPerUtente(database,foto,idUtente);
        if(id<0){
            foto = null;
        }
        return foto;
    }
//==================================================================================================
}
