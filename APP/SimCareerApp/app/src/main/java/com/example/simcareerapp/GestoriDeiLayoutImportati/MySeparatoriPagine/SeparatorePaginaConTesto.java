package com.example.simcareerapp.GestoriDeiLayoutImportati.MySeparatoriPagine;

import android.content.Intent;
import android.media.Image;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.simcareerapp.R;

public class SeparatorePaginaConTesto {
//==================================================================================================
//                                  IMPOSTAZIONI DELLE VARIANTI DI BLOCHI INCLUSI
//==================================================================================================
    /**
     * Questi metodi permettono di impostatre i separatori delle pagine con un  testo e una icona
     */
    static public void setSeparatorePaginaConTestoDeiInclude(AppCompatActivity context, int idImpost, int idText, int idIcon) {
        setSeparatorePaginaConTestoDeiIncludeGenerico(context,idImpost,idText,idIcon);
    }
    static public void setSeparatorePaginaConTestoDeiIncludeLable(AppCompatActivity context, int idImpost, int idText) {
        setSeparatorePaginaConTestoDeiIncludeGenerico(context,idImpost,idText,null);
    }
    static public void setSeparatorePaginaConTestoDeiIncludeIcon(AppCompatActivity context, int idImpost, int idIcon) {
        setSeparatorePaginaConTestoDeiIncludeGenerico(context,idImpost,null,idIcon);
    }
    //Per il LAYOUT
    static public void setSeparatorePaginaConTestoDeiInclude(ViewGroup layout, int idImpost, int idText, int idIcon) {
        setSeparatorePaginaConTestoDeiIncludeGenerico(layout,idImpost,idText,idIcon);
    }
    static public void setSeparatorePaginaConTestoDeiIncludeLable(ViewGroup layout, int idImpost, int idText) {
        setSeparatorePaginaConTestoDeiIncludeGenerico(layout,idImpost,idText,null);
    }
    static public void setSeparatorePaginaConTestoDeiIncludeIcon(ViewGroup layout, int idImpost, int idIcon) {
        setSeparatorePaginaConTestoDeiIncludeGenerico(layout,idImpost,null,idIcon);
    }
//==================================================================================================
    /**
     * Questi metodo effutua per tutte le casistiche l'operazione  di impostatre i separatori delle
     * pagine con un  testo e una icona se presenti.
     */
    static private void setSeparatorePaginaConTestoDeiIncludeGenerico(AppCompatActivity context, int idImpost, Integer idText, Integer idIcon) {
        ViewGroup barraPreferito = (ViewGroup) context.findViewById(idImpost);
        if(idText!=null){
            TextView label = (TextView) barraPreferito.findViewById(R.id.txt_LabelDivisore);
            label.setText(idText);
        }
        if(idIcon!=null){
            ImageView icon = (ImageView) barraPreferito.findViewById(R.id.img_IconaDivisore);
            icon.setImageDrawable(context.getResources().getDrawable(idIcon));
        }
    }
    /**
     * Questi metodo effutua per tutte le casistiche l'operazione  di impostatre i separatori delle
     * pagine con un  testo e una icona se presenti.
     */
    static private void setSeparatorePaginaConTestoDeiIncludeGenerico(ViewGroup layout, int idImpost, Integer idText, Integer idIcon) {
        ViewGroup barraPreferito = (ViewGroup) layout.findViewById(idImpost);
        if(idText!=null){
            TextView label = (TextView) barraPreferito.findViewById(R.id.txt_LabelDivisore);
            label.setText(idText);
        }
        if(idIcon!=null){
            ImageView icon = (ImageView) barraPreferito.findViewById(R.id.img_IconaDivisore);
            icon.setImageDrawable(layout.getResources().getDrawable(idIcon));
        }
    }

}
