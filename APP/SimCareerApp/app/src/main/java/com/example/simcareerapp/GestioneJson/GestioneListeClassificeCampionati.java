package com.example.simcareerapp.GestioneJson;

import android.content.Context;

import com.example.simcareerapp.GestioneJson.Generale.GestioneGenerale;
import com.example.simcareerapp.Persistence.JsonEntities.CampionatoConJson;
import com.example.simcareerapp.Persistence.JsonEntities.ClassificaCampionatiConJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class GestioneListeClassificeCampionati extends GestioneGenerale {
//##################################################################################################
//                                  ETICHETTE PER LA GESTIONE DEL JSON
//##################################################################################################
    private static final String LABLE_CAMPIONATI= "campionati";
//##################################################################################################
//==================================================================================================
//                              LETTURA E CREAZIONE LISTA DEI CAMPIONATI
//==================================================================================================
    public static List<ClassificaCampionatiConJson> letturaECreazioneListaClassificaCampionati(Context context , int idFile){
        String contenutoFile = GestioneGenerale.leturaFileJson(context,idFile);
        List<ClassificaCampionatiConJson> ris = new LinkedList<ClassificaCampionatiConJson>();
        try {
            JSONObject jsonObject = new JSONObject(contenutoFile);

            JSONArray jsonArrayGare = jsonObject.getJSONArray(LABLE_CAMPIONATI);
            for(int i=0; i<jsonArrayGare.length(); i++){
                ClassificaCampionatiConJson campionato= new ClassificaCampionatiConJson(jsonArrayGare.getJSONObject(i));
                if(!campionato.isNull()){
                    ris.add(campionato);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ris;
    }
//==================================================================================================
}
