package com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp.Interface.OnAletDialogIscrizioneAlCampionatoEventListener;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinner.SpinnerConImmagine;
import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.R;

import java.util.ArrayList;

public class AlertDialogIscrizioneAlCampionato extends MyGenericAlertDialog {
    private Spinner viewTeam;
    // Selezione Auto
    private ViewGroup viewAuto;
    private SpinnerConImmagine autoSpinnerConImmagine;
//==================================================================================================
//                                  CREAZIONE DEL ALERTDIALOG
//==================================================================================================
    /**
     * Questo è il cortutore a che serve per sapiere l'utente e il campionato a cui si stà per iscrivere
     */
    public AlertDialogIscrizioneAlCampionato(Context context, Campionato campionato, Utente utenteAttivo, int idAlertDialog, int idIcon, int idTitle, int idBtnPrimario, int idBtnSecondario) {
        super(context, idAlertDialog, idIcon, idTitle, idBtnPrimario, idBtnSecondario);
        //Team view
        viewTeam  = view.findViewById(R.id.spn_InputTeam);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(),
                R.array.array_TeamName, R.layout.row_spinner);
        adapter.setDropDownViewResource(R.layout.row_spinner);
        viewTeam.setAdapter(adapter);
        //Inizializazione spinnere con immagine auto
        viewAuto = view.findViewById(R.id.include_Machina);
        autoSpinnerConImmagine =  new SpinnerConImmagine(this.getContext(),view,R.id.include_Machina,R.string.lbl_Auto);
        ArrayList<ValoreConImmaginePerSpinner> lista = new ArrayList<>();
        lista.addAll(campionato.getListaAuto());
        autoSpinnerConImmagine.setSpinnerConImmagineGenerico(lista);
    }
//==================================================================================================


//==================================================================================================
//                                  METODI CHIAMATI ALLA PRESIONE DEI BOTTONI
//==================================================================================================
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante Conferma del AlertDialoge
     * imposta l'iscrizione al campionato.
     */
    @Override
    protected void listenerButtonPrimario(View v){
        Spinner spinnerAuto = viewAuto.findViewById(R.id.spn_SpinnerDeiValori);

        String          nomeTeam = (String)          viewTeam.getSelectedItem();
        AutoConImmagini auto     = (AutoConImmagini) spinnerAuto.getSelectedItem();

        if( ((OnAletDialogIscrizioneAlCampionatoEventListener)context).informazioniPerNuovaIscrizioneAlCampionato(nomeTeam,auto) ){
            this.cancel();
        }
    }
//==================================================================================================


//##################################################################################################

}
