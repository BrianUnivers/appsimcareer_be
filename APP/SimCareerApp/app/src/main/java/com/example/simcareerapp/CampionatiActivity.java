package com.example.simcareerapp;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.simcareerapp.AppCompatActivityWithMenu.AppCompatActivityWithMyPrimaryMenu;
import com.example.simcareerapp.AppCompatActivityWithMenu.MyServiceDiNotifiche.MyServizioDiNotifiche;
import com.example.simcareerapp.GestioneJson.GestioneListeCampionati;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.CampionatoRVAdapter;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Persistence.JsonEntities.CampionatoConJson;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;


public class CampionatiActivity extends AppCompatActivityWithMyPrimaryMenu {
//##################################################################################################
//                              DATI PASSATI TRAMITE UN INTENT
//##################################################################################################
    private Utente utenteAttivo         = null;
    private Boolean mantieniAttivoUtente= null;
//==================================================================================================
//                              METODO DI RECUPERO DI TALI VALORI DAL INTET CHE HA CHAMATO LA PAGINA
//==================================================================================================
    /**
     * Questo metodo serve per prendere dall'Activity chiamante le informazioni della sessione che
     * di obbligatori sono <<utenteAttivo>> e <<mantieniAttivoUtente>>.
     */
    private void initValoriDiSessioneDellAPP(){
        Intent i =getIntent();
        utenteAttivo            = MyIntent.getUtenteExtra(i,MyIntent.I_UTENTE_ATTIVO);
        mantieniAttivoUtente    = i.getBooleanExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,false);
    }
//==================================================================================================
//==================================================================================================
//                              IMPOSTAZIONI PARAMETRI DA PASSARE PER GLI INTENT DEL MENU LATERALE PRINCIPALE
//==================================================================================================
    @Override
    public void setParamiter(MyIntent intent) {
        intent.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utenteAttivo);
        intent.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
    }
//==================================================================================================
//##################################################################################################


//##################################################################################################
//                                  CREAZIONE DEL LAYOUT
//##################################################################################################
    private List<Campionato> listaCampionatiPassati = new LinkedList<Campionato>();
    private List<Campionato> listaCampionatiInCorso = new LinkedList<Campionato>();
    private List<Campionato> listaCampionatiFuturi  = new LinkedList<Campionato>();
    private RecyclerView recyclerView;
//==================================================================================================
//                                  CREAZIONE DELL'ACTITVITY
//==================================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  GET DATI DAL INTENT
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        initValoriDiSessioneDellAPP();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        setContentView(R.layout.activity_campionati);
        super.initToolBar(R.menu.bar_menu_vuoto);
        super.impostaPaginaComeHome();
//------------------------------
        recyclerView = findViewById(R.id.rcl_ListaCampionati);
        List<CampionatoConJson> listaCampionati = GestioneListeCampionati.letturaECreazioneListaCampionati(this,R.raw.campionati);
        initListeCampionati(listaCampionati);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  BOTTONI DELLA TABAR IN BASSO
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        BottomNavigationView bottomTabarView = findViewById(R.id.tbb_TabListeCampionati);
        bottomTabarView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener(){
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return cambiaListaDeiCampionatiDaVisualizzare(menuItem);
            }
        } );
        bottomTabarView.setSelectedItemId(R.id.btn_tab_InCorso);//Selezione della prima lista visibile senza premere
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
//==================================================================================================

//==================================================================================================
//                                  GENERA LE TRE LISTE DEI GAMPINATI
//==================================================================================================
    /**
     * Questa funzione prende una lista di campionati e li separare in base al loro stato inserendoli
     * in una delle tre liste di campionati presenti.
     */
    private void initListeCampionati(List<CampionatoConJson> listaCampionati){
        listaCampionatiPassati =new LinkedList<Campionato>();
        listaCampionatiInCorso =new LinkedList<Campionato>();
        listaCampionatiFuturi  =new LinkedList<Campionato>();
        Date oggi = new Date();

        if(listaCampionati!=null){
            for(int i=0; i<listaCampionati.size(); i++){
                Campionato campionato =listaCampionati.get(i);
                Date dateInizio = campionato.getDataInizioCampionato();
                if(oggi.before(dateInizio)){
                    listaCampionatiFuturi.add(campionato);
                }else{
                    Date dateIFine = campionato.getDataFineCampionato();
                    if(oggi.before(dateIFine)){
                        listaCampionatiInCorso.add(campionato);
                    }else{
                        listaCampionatiPassati.add(campionato);
                    }
                }
            }
        }
    }
//==================================================================================================

//==================================================================================================
//                                  CAMBIA LA LISTA VISUALIZATA DEI CAMPIONATI
//==================================================================================================
    /**
     * Questo metodo vinene chiamato tramite il lissener,della tabbar quando viene premuto un dei tre
     * tab (e una volta in automatico alla creazione della activity) e serve per selezionare la lista
     * dei campionati che si vuole visualizare.
     */
    private boolean  cambiaListaDeiCampionatiDaVisualizzare(@NonNull MenuItem menuItem){
        CampionatoRVAdapter campionatoRVAdapter = null;
        switch (menuItem.getItemId()){
            case R.id.btn_tab_Passati:
                campionatoRVAdapter= new CampionatoRVAdapter(this,this,listaCampionatiPassati,utenteAttivo);
                break;
            case R.id.btn_tab_InCorso:
                campionatoRVAdapter= new CampionatoRVAdapter(this,this,listaCampionatiInCorso,utenteAttivo);
                break;
            case R.id.btn_tab_Futuri:
                campionatoRVAdapter= new CampionatoRVAdapter(this,this,listaCampionatiFuturi,utenteAttivo);
                break;
        }
        boolean ris = false;
        if(campionatoRVAdapter!=null) {
            recyclerView.setAdapter(campionatoRVAdapter);
            ris = true;
        }
        return ris;
    }
//==================================================================================================
}