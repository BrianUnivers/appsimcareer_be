package com.example.simcareerapp.DataBase.Query;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.simcareerapp.DataBase.Management.FeedReaderContract;
import com.example.simcareerapp.DataBase.Management.SimpleDbHelper;
import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.CircuitoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Foto;
import com.example.simcareerapp.Persistence.Entities.Indirizzo;
import com.example.simcareerapp.Persistence.Entities.Utente;

import java.sql.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Questa classe contiene i metodi inserire, selezionare e aggiornare i dati contenuti nella tabella Foto,
 * tramite le query al database locale.
 *
 * @author Brian Emanuel
 */
public class FotoQuery {
    /**
     * La costante "projectionAll" contiene in un array di stringhe tutti i nomi delle colonne della
     * tabella Foto del database locale.
     */
    private static String[] projectionAll = {
            FeedReaderContract.FeedFoto._ID,
            FeedReaderContract.FeedFoto.COLUMN_NAME_ID_UTENTE,
            FeedReaderContract.FeedFoto.COLUMN_NAME_TESTO_ALTERNATIVO,
            FeedReaderContract.FeedFoto.COLUMN_NAME_STATO_ATTIVO,
            FeedReaderContract.FeedFoto.COLUMN_NAME_URL
    };
//##################################################################################################
//                             GET
//##################################################################################################
    /**
     * Tutti i metodi GET possono tornare NULL se non ci sono risultati validi
     */
//==================================================================================================
//                             GET LISTA DI TUTTE LE FOTO
//==================================================================================================
    /**
     * Questo è il metodo che richede al database di restituire tutte le entry della tabella Foto.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @return
     *      Il rusultato di questo metodo è una lista di foto, se e solo se esistono dei risultati validi
     *      altrimenti restituirà null.
     */
    public static List<Foto> getAll(SimpleDbHelper database){
        return getAllConCondizioni(database,null,null);
    }
//==================================================================================================
//==================================================================================================
//                             GET FOTO BY ID
//==================================================================================================
    /**
     * Questo è il metodo che richede al database di restituire la entry della tabella Foto avente
     * uno specifico ID.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param id
     *      Il parametro passato contiene l'ID della foto che si stà richiedendo.
     * @return
     *      Il rusultato di questo metodo è un oggetto di tipo Foto, se e solo se esiste un elemento
     *      con tale id altrimenti null.
     */
    public static Foto getFoto(SimpleDbHelper database,Long id){
        String selection        = FeedReaderContract.FeedFoto._ID+" = ? ";
        String[] selectionArgs  = {id.toString()};
        List<Foto> listaRisultati = getAllConCondizioni(database,selection,selectionArgs);
        Foto ris =  null;
        if( (listaRisultati!=null) && (listaRisultati.size() == 1 )){
            ris = listaRisultati.get(0);
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                             GET FOTO BY ID_UTENTE
//==================================================================================================
    /**
     * Questo è il metodo che richede al database di restituire tutte le entry della tabella Foto che
     * hanno uno specifico ID Utente.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param idUtente
     *      Il parametro passato contiene ID dell'utente di cui si vogliono tutte le foto.
     * @return
     *      Il rusultato di questo metodo è una lista di foto, se e solo se esistono dei risultati validi
     *      altrimenti restituirà null.
     */
    public static List<Foto> getFotoByIdUtente(SimpleDbHelper database,Long idUtente){
        String selection        = FeedReaderContract.FeedFoto.COLUMN_NAME_ID_UTENTE+" = ? ";
        String[] selectionArgs  = {idUtente.toString()};
        return getAllConCondizioni(database,selection,selectionArgs);
    }
//==================================================================================================
//==================================================================================================
//                             GET FOTO BY ID_UTENTE ATTIVA
//==================================================================================================
    /**
     * Questo è il metodo che richede al database di restituire l'entry della Foto attiva che
     * ha uno specifico ID Utente.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param idUtente
     *      Il parametro passato contiene ID dell'utente di cui si vuole la foto attiva.
     * @return
     *      Il rusultato di questo metodo è la foto attiva, se e solo se esiste tale risultato valido
     *      altrimenti restituirà null.
     */
    public static Foto getFotoByIdUtenteAttiva(SimpleDbHelper database,Long idUtente){
        List<Foto> listaRisultati = getFotoByIdUtenteConStato(database,idUtente,true);
        Foto ris =  null;
        if( (listaRisultati!=null) && (listaRisultati.size() == 1 )){
            ris = listaRisultati.get(0);
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                             GET FOTO BY ID_UTENTE DISATTIVE
//==================================================================================================
    /**
     * Questo è il metodo che richede al database di restituire tutte le entry della tabella Foto che
     * hanno uno specifico ID Utente e non sono foto attive
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param idUtente
     *      Il parametro passato contiene ID dell'utente di cui si vuole tutte le foto non attiva.
     * @return
     *      Il rusultato di questo metodo è una lista di foto, se e solo se esistono dei risultati validi
     *      altrimenti restituirà null.
     */
    public static List<Foto> getFotoByIdUtenteDisattive(SimpleDbHelper database,Long idUtente){
        return getFotoByIdUtenteConStato(database,idUtente,false);
    }
//==================================================================================================
//==================================================================================================
//                             GET FOTO BY ID_UTENTE STATO GENERICO
//==================================================================================================
    /**
     * Questo è il metodo che richede al database di restituire tutte le entry della tabella Foto che
     * hanno uno specifico ID Utente e hanno uno stato specifico.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param idUtente
     *      Il parametro passato contiene ID dell'utente di cui si vuole tutte le foto con un certo stato.
     * @param statoAttivo
     *      Il parametro passato contiene lo stato attivo se true, o disattivo se fasle delle foto di
     *      un specifico utene.
     * @return
     *      Il rusultato di questo metodo è una lista di foto, se e solo se esistono dei risultati validi
     *      altrimenti restituirà null.
     */
    public static List<Foto> getFotoByIdUtenteConStato(SimpleDbHelper database,Long idUtente,Boolean statoAttivo){
        Integer statoAttivo_BD = FeedReaderContract.DB_FALSE;
        if(statoAttivo){
            statoAttivo_BD = FeedReaderContract.DB_TRUE;
        }
        String selection        =   FeedReaderContract.FeedFoto.COLUMN_NAME_ID_UTENTE       + " = ? AND "+
                                    FeedReaderContract.FeedFoto.COLUMN_NAME_STATO_ATTIVO    + " = ? ";
        String[] selectionArgs  = {idUtente.toString(),statoAttivo_BD.toString()};
        return getAllConCondizioni(database,selection,selectionArgs);
    }
//==================================================================================================
//==================================================================================================
//                             APPOGIO
//==================================================================================================
    /**
     * Questo è il metodo che richede al database di restituire tutte le entry della tabella Foto
     * che rispettano delle condizioni.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param selection 
     *      Il parametro passato contiene una stinga contenente le condizioni della query, senza i valori
     *      variabili rappresentati dal caratter '?' ed in oltre senza l'utilizzio all'inizio di WHERE.
     * @param selectionArgs 
     *      Il parametro passato contiene l'array di stinge contenti i valori da inserire, nello stesso 
     *      ardine, nella sringa delle condizioni.
     * @return
     *      Il rusultato di questo metodo è una lista di foto, se e solo se esistono dei risultati validi
     *      altrimenti restituirà null.
     *      
     * @see SQLiteDatabase#query(String, String[], String, String[], String, String, String) 
     */
    public static List<Foto> getAllConCondizioni(SimpleDbHelper database,String selection,String[] selectionArgs){
        SQLiteDatabase db = database.getReadableDatabase();

        Cursor cursor = db.query( FeedReaderContract.FeedFoto.TABLE_NAME,
                projectionAll,
                selection,
                selectionArgs,
                null,
                null,
                null);
        //Perndi risultato
        List<Foto> listaFoto = new LinkedList<Foto>();
        while(cursor.moveToNext()){
            Foto foto = new Foto();
            foto.setId(                 cursor.getLong(   cursor.getColumnIndexOrThrow(FeedReaderContract.FeedFoto._ID) ) );
            foto.setIdUtente(           cursor.getLong(   cursor.getColumnIndexOrThrow(FeedReaderContract.FeedFoto.COLUMN_NAME_ID_UTENTE) ) );
            foto.setTestoAlternativo(   cursor.getString( cursor.getColumnIndexOrThrow(FeedReaderContract.FeedFoto.COLUMN_NAME_TESTO_ALTERNATIVO) ) );
            foto.setUrlFile(            cursor.getString( cursor.getColumnIndexOrThrow(FeedReaderContract.FeedFoto.COLUMN_NAME_URL) ) );

            Boolean statoAttivo = false;
            if(cursor.getInt( cursor.getColumnIndexOrThrow( FeedReaderContract.FeedFoto.COLUMN_NAME_STATO_ATTIVO) ) == FeedReaderContract.DB_TRUE){
                statoAttivo = true;
            }
            foto.setStatoAttivo(statoAttivo);

            listaFoto.add(foto);
        }
        cursor.close();
        if(listaFoto.size()==0){
            listaFoto= null;
        }
        return listaFoto;
    }
//==================================================================================================
//##################################################################################################
//                             INSERT
//##################################################################################################
//==================================================================================================
//                             INSERT FOTO
//==================================================================================================
//--------------------------------------------------------------------------------------------------
//                              INSERT FOTO <<normale>>
//--------------------------------------------------------------------------------------------------
    /**
     * Il valore di ritorno è l'id del record appena inserito se però il valore è pari a -1 non è stato
     * possibile inserire la foto nel database.
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param foto
     *      Il parametro passato contiene l'oggetto di tipo Foto che deve essere inserito all'interno
     *      del database locale.
     * @return
     *      Il risultato i questo metodo è id della foto appena inserita se l'aggiunta al database
     *      è avvenuta con successo alrimenti -1.
     */
    public static Long insertFoto(SimpleDbHelper database,Foto foto){
        SQLiteDatabase db = database.getWritableDatabase();

        ContentValues values = new ContentValues();
        values = setValues(values,foto);
        Long id = db.insert(FeedReaderContract.FeedFoto.TABLE_NAME,null,values);
        if(id>=0){
            foto.setId(id);
        }
        return id;
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//                              INSERT FOTO sostituisci foto attiva con questa.
//--------------------------------------------------------------------------------------------------
    /**
     * Questo metodo permette d'inserire una foto per un utente specifico in modo tale che sia la
     * sua foto attiva, in oltre se persente già una attiva la disattiva mentenedola.
     * Il valore di ritorno è l'id del record appena inserito se però il valore è pari a -1 non è stato
     * possibile inserire la foto nel database.
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param foto
     *      Il parameto passato contienel'oggetto di tipo Foto che deve essere inserito all'interno
     *      del database locale.
     * @param idUtente
     *      Il parametro passato contiene l'identificativo dell'utente a cui si vuole aggiungere la
     *      nuova foto attiva.
     * @return
     *      Il risultato di questo metodo è id della foto appena inserita se l'aggiunta al database
     *      è avvenuta con successo alrimenti -1.
     */
    public static Long insertFotoNuovaFotoAttivaPerUtente(SimpleDbHelper database,Foto foto,Long idUtente){
        Long ris = null;
        foto.setIdUtente(idUtente);
        foto.setStatoAttivo(true);
        //Ho preso la foto che non dovra essere attva.
        Foto vechiaFotoAttiva = getFotoByIdUtenteAttiva(database,idUtente);
        Integer id =0;
        if(vechiaFotoAttiva!=null){
            vechiaFotoAttiva.setStatoAttivo(false);
            id = updateFoto(database,vechiaFotoAttiva,vechiaFotoAttiva.getId());
        }
        if( (id==1) || (vechiaFotoAttiva==null) ){//E' stata cambiata l'immagine con succeso.
            //Ora inserisco l'immagine
            ris = insertFoto(database,foto);
            if( (ris<0) && (vechiaFotoAttiva!= null) ){
                vechiaFotoAttiva.setStatoAttivo(true);
                updateFoto(database,vechiaFotoAttiva,vechiaFotoAttiva.getId());
            }
        }
        return ris;
    }
//--------------------------------------------------------------------------------------------------
//==================================================================================================
//==================================================================================================
//                             APPOGIO
//==================================================================================================

    /**
     * Questo metodo permette di inserire i dati delle foto nel insieme dei valori che si volgliono
     * aggiungere ad una query, nel seguente ordine id_utente,testo_altenativo,url, e stato della foto.
     *
     * @param values
     *      Questo parametro contiene l'oggetto pecedentemente creato che conterra i valori da inserire
     *      nella query, e dove verranno, in questo metodo, inseriti i dati che compongono l'oggetto
     *      Foto.
     * @param foto
     *      Questo parametro contiene l'oggetto foto che si vuole scomporre ed  inserire in una query.
     * @return
     *      Il risultato di questo metodo è un oggetto  di tipo ContentValues cantenente i dati della
     *      foto che si è passata.
     */
    private static ContentValues setValues(ContentValues values,Foto foto){
        values.put(FeedReaderContract.FeedFoto.COLUMN_NAME_ID_UTENTE,         foto.getIdUtente());
        values.put(FeedReaderContract.FeedFoto.COLUMN_NAME_TESTO_ALTERNATIVO, foto.getTestoAlternativo());
        values.put(FeedReaderContract.FeedFoto.COLUMN_NAME_URL,               foto.getUrlFile());
        Integer statoAttivo = FeedReaderContract.DB_FALSE;
        if(foto.getStatoAttivo()){
            statoAttivo = FeedReaderContract.DB_TRUE;
        }
        values.put(FeedReaderContract.FeedFoto.COLUMN_NAME_STATO_ATTIVO,         statoAttivo);
        return values;
    }
//==================================================================================================
//##################################################################################################
//                             UPDATE
//##################################################################################################
//==================================================================================================
//                             UPDATE FOTO
//==================================================================================================
    /**
     * Questo metodo aggiorna i valori delle foto con lo stesso con un id indicato, pendendo i nuovi
     * dati da un altro oggetto.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param foto
     *      Il parametro passato contiene tutti i dati di una foto che devono essere usati per sostiuire
     *      quelli selezionati.
     * @param id
     *      Il parametro passato contiene l'id delle foto che si vogliono modificare.
     * @return
     *    Il valore di ritorno è il numero di record della tabella foto che sono stati modificati.
     */
    public static Integer updateFoto(SimpleDbHelper database,Foto foto,long id){
        String selection = FeedReaderContract.FeedFoto._ID + " = ?";
        String[] selectionArgs = { new Long(id).toString() };
        return updateFotoConCondizioni(database,foto,selection,selectionArgs);
    }
//==================================================================================================
//                             APPOGIO
//==================================================================================================
    /**
     * Questo metodo che effetua tutte le possibili modifiche sugli elementi delle tabella foto indicati
     * dalle restrizioni presenti nelle stringa di selezione inserondo nella query i dati presenti nel
     * array di stringhe passato.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param foto
     *      Il parametro passato contiene tutti i dati di una foto che devono essere usati per sostiuire
     *      quelli selezionati.
     * @param selection
     *      Il parametro passato contiene una stinga contenente le condizioni della query, senza i valori
     *      variabili rappresentati dal caratter '?' ed in oltre senza l'utilizzio all'inizio di WHERE.
     * @param selectionArgs
     *      Il parametro passato contiene l'array di stinge contenti i valori da inserire, nello stesso
     *      ardine, nella sringa delle condizioni.
     * @return
     *      Il valore di ritorno è il numero di record della tabella foto che sono stati modificati.
     */
    private static Integer updateFotoConCondizioni(SimpleDbHelper database,Foto foto,String selection,String[] selectionArgs){
        SQLiteDatabase db = database.getWritableDatabase();
        ContentValues values = new ContentValues();
        values = setValues(values,foto);
        Integer id = db.update(FeedReaderContract.FeedFoto.TABLE_NAME,
                values,
                selection,
                selectionArgs);
        return id;
    }
//==================================================================================================
}
