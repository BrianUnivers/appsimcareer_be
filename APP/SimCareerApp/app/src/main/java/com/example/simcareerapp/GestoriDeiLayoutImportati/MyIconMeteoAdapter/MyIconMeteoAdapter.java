package com.example.simcareerapp.GestoriDeiLayoutImportati.MyIconMeteoAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.example.simcareerapp.R;
import com.google.android.material.chip.Chip;

import java.util.List;


public class MyIconMeteoAdapter extends ArrayAdapter<Integer> {
    private List<Integer> listaIconRisorse = null;
    private static int resourceLayout = R.layout.row_icon_meteo;
    private Context context = null;
//==================================================================================================
//                                  COSTRUTTRI
//==================================================================================================
    public MyIconMeteoAdapter(Context context, List<Integer> listaIconRisorse) {
        super(context, resourceLayout, listaIconRisorse);
        this.listaIconRisorse = listaIconRisorse;
        this.context = context;
    }
//==================================================================================================
//==================================================================================================
//                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE
//==================================================================================================
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getCount() {return listaIconRisorse.size();}
//==================================================================================================
//==================================================================================================
//                              RITORNA UN ELEMETO DELLA LISTA
//==================================================================================================
    /**
     * Questa funzione deve ritorna un elemto della lista data una posizione.
     */
    @Override
    public Integer getItem(int position) {return listaIconRisorse.get(position);}
//==================================================================================================
//==================================================================================================
//                              RITORNA UN IDENTIFICATORE DI UN ELEMETO
//==================================================================================================
    /**
     * Questa funzione deve ritorna un identificativo di un elemento della lista data una posizione.
     */
    @Override
    public long getItemId(int position) {return getItem(position).hashCode();}
//==================================================================================================
//==================================================================================================
//                              IMPOSTA IL LAYOUT
//==================================================================================================
@Override
    public View getView(int position, View v, ViewGroup vg){
        if (v == null){
            v = LayoutInflater.from(context).inflate(resourceLayout, null);
        }
        Integer idIconaMeteo = (Integer) getItem(position);
        ImageView icon = v.findViewById(R.id.img_IconaMeteo);
        icon.setImageResource(idIconaMeteo);

        return v;
    }
//==================================================================================================
}
