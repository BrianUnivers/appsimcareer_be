package com.example.simcareerapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.simcareerapp.GestioneJson.GestioneListeClassificeCampionati;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.ClassificaPilotiRVAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.ClassificaTeamRVAdapter;
import com.example.simcareerapp.Persistence.Entities.ClassificaCampionati;
import com.example.simcareerapp.Persistence.Entities.PilotaConPunti;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Persistence.JsonEntities.ClassificaCampionatiConJson;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ClassificaActivity extends AppCompatActivity {

//##################################################################################################
//                              DATI PASSATI TRAMITE UN INTENT
//##################################################################################################
    private Utente utenteAttivo             = null;
    private Boolean mantieniAttivoUtente    = null;
    private ClassificaCampionati campionato = null; // Non passato ma ricavato
    private List<ClassificaCampionatiConJson> listaClassifiche = null; // Serve per salvare le modifiche.
    private String NomeTeamUtenteAttivo     = "";
//==================================================================================================
//                              METODO DI RECUPERO DI TALI VALORI DAL INTET CHE HA CHAMATO LA PAGINA
//==================================================================================================
    /**
     * Questo metodo serve per prendere dall'Activity chiamante le informazioni della sessione che
     * di obbligatori sono <<utenteAttivo>>, <<mantieniAttivoUtente>> e <<ClassificaCampionati>>.
     * Quest'ultimo però viene ricavato dall'id che viene passato
     *
     * Se non dovesse trovare il campionato con quel'id si ritorna alla pagina dei campionati.
     */
    private void initValoriDiSessioneDellAPP(){
        Intent i =getIntent();
        utenteAttivo            = MyIntent.getUtenteExtra(i,MyIntent.I_UTENTE_ATTIVO);
        mantieniAttivoUtente    = i.getBooleanExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,false);
        Long idCampionato       = i.getLongExtra(MyIntent.I_ID_CAMPIONATO_SELEZIONATO,-1);
        //Verifico se nel dataBase non ci sono valori
        if(idCampionato==-1){
            apriPaginaListaCampionati();
        }else{
            ClassificaCampionati tempCampionato=null;
            listaClassifiche = GestioneListeClassificeCampionati.letturaECreazioneListaClassificaCampionati(this,R.raw.classifiche);
            if( (listaClassifiche!=null) && (listaClassifiche.size()!=0)){
                int j=-1;
                boolean campionatoTrovato=false;
                do {
                    j++;
                    tempCampionato = listaClassifiche.get(j);
                    if(tempCampionato.getId()==idCampionato){
                        campionato = tempCampionato;
                        campionatoTrovato = true;
                    }else{
                        if(j==listaClassifiche.size()-1){
                            campionatoTrovato = true;
                        }
                    }
                }while (!campionatoTrovato);
            }
        }
        if(campionato==null){
            apriPaginaListaCampionati();
        }else {

            Boolean ricercaTeamUtenteAttivoTerminata = false;
            Iterator<PilotaConPunti> pilotaIterator = campionato.getClassificaPiloti().iterator();
            while ((pilotaIterator.hasNext()) && (!ricercaTeamUtenteAttivoTerminata)) {
                PilotaConPunti pilota = pilotaIterator.next();
                if (utenteAttivo.equalsToPiloti(pilota)) {
                    NomeTeamUtenteAttivo = pilota.getNomeTeam();
                    ricercaTeamUtenteAttivoTerminata = true;
                }
            }
        }
    }
//==================================================================================================
//==================================================================================================
//                              APERTURA DELLA PAGINA DI LISTA CAMPIONATI PASSANDO I VALORI DI SESSIONE
//==================================================================================================
    /**
     * Questo metodo serve per inviare le informazioni della sessione che sono <<utenteAttivo>> e
     * <<mantieniAttivoUtente>> quelli obbligatori.
     * N.B.
     * Da questà activiti è possibile solo andare alla Activity CampionatiActivity senza apportare
     * modifiche.
     */
    private void apriPaginaListaCampionati(){
        MyIntent i = new MyIntent(ClassificaActivity.this, CampionatiActivity.class);
        i.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utenteAttivo);
        i.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
        startActivity(i);
    }
//==================================================================================================
//==================================================================================================
//                              APERTURA DELLA PAGINA DI LISTA CAMPIONATI PASSANDO I VALORI DI SESSIONE
//==================================================================================================
    /**
     * Questo metodo serve per inviare le informazioni della sessione che sono <<utenteAttivo>> e
     * <<mantieniAttivoUtente>> quelli obbligatori.
     * N.B.
     * Da questà activiti è possibile solo andare alla Activity SingoloCampionatoActivity senza apportare
     * modifiche.
     */
    private void apriPaginaPrecedete(){
        onBackPressed();//VAI ALL'ACTIVITY PRECEDENTE

        /*MyIntent i = new MyIntent(ClassificaActivity.this, SingoloCampionatoActivity.class);
        i.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utenteAttivo);
        i.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
        i.putExtra(MyIntent.I_ID_CAMPIONATO_SELEZIONATO,campionato.getId());
        startActivity(i);*/
    }
//==================================================================================================

//##################################################################################################



//##################################################################################################
//                              DATI PER GESTIRE IL LAYOUT
//##################################################################################################
    //Toolbar da implemetare
    private Toolbar toolbar     = null;
    private BottomNavigationView bottomTabarView = null;
    private RecyclerView listaClassificaView = null;
//##################################################################################################
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  GET DATI DAL INTENT
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        initValoriDiSessioneDellAPP();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        setContentView(R.layout.activity_classifica);
        //Implemetazione della toolbar
        toolbar =  findViewById(R.id.include_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24_white);

        //Imposta il sottotitolo
        if(campionato!=null) {
            Toolbar sottotitolo = findViewById(R.id.toolbar_subtitle);
            sottotitolo.setTitle(campionato.getName());
        }
        listaClassificaView = findViewById(R.id.rcl_ListaClassifica);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  LISTENER DEI BOTTONI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Listener del pulsante Annula modifiche
        if(toolbar!=null)
            toolbar.setNavigationOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    apriPaginaPrecedete();
                }
            });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  BOTTONI DELLA TABAR IN BASSO
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        bottomTabarView = findViewById(R.id.tbb_TabClassifica);
        bottomTabarView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener(){
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return cambiaClassificaDaVisualizare(menuItem);
            }
        } );
        if(campionato!=null) {
            bottomTabarView.setSelectedItemId(R.id.btn_tab_ClassificaPiloti);//Selezione della prima vita
        }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    }
//##################################################################################################



//##################################################################################################
//                                  GESTIONE DEL MENU' SULLA TOOLBAR
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE DEL MENU' (senza il tasto a sinistra)
//==================================================================================================
    /**
     * Metodo per definire il layout della toolbar definedo quali sono le sue voci.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bar_menu_vuoto,menu);
        return true;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  OPERAZIONI FATTE QUANDO SI PREME SU UNA TAB
//##################################################################################################
//==================================================================================================
//                                  CAMBIA LA SCHEDA DA VISUALIZATA DELE CAMPIONATO
//==================================================================================================
    /**
     * Questo metodo vinene chiamato tramite il lissener,della tabbar quando viene premuto un dei quatro
     * tab viene cambiato il contenuto del FrameLayout
     */
    private boolean  cambiaClassificaDaVisualizare(@NonNull MenuItem menuItem){
        RecyclerView.Adapter adapter = null;
        boolean ris = true;
        switch (menuItem.getItemId()){
            case R.id.btn_tab_ClassificaPiloti:
                Collections.sort(campionato.getClassificaPiloti());
                adapter = new ClassificaPilotiRVAdapter(this,this, campionato.getClassificaPiloti(),utenteAttivo);
                break;
            case R.id.btn_tab_ClassificaTeam:
                adapter = new ClassificaTeamRVAdapter(this,this, campionato.getClassificaTeam(),NomeTeamUtenteAttivo);
                break;
            default:
                ris=false;
        }
        if( (ris) && (adapter!=null) ){
            RecyclerView.Adapter old = listaClassificaView.getAdapter();
            if((old==null) || ( !old.getClass().equals(adapter.getClass()) ) ){
                listaClassificaView.setAdapter(adapter);
                listaClassificaView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false));
            }
        }
        return ris;
    }
//==================================================================================================
//##################################################################################################



}