package com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp.Interface;

import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Indirizzo;

//##################################################################################################
//                                  INTERFACCIA PER COMUNICARE
//##################################################################################################
public interface OnAletDialogIscrizioneAlCampionatoEventListener {
    /**
     * Ritorna true se i dati del team e del auto passati sono ritenuti validi altrimeti false
     */
    boolean informazioniPerNuovaIscrizioneAlCampionato(String nomeTeam, AutoConImmagini auto);
}
//##################################################################################################
//##################################################################################################