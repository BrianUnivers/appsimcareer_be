package com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.PilotaConPunti;
import com.example.simcareerapp.Persistence.Entities.TeamConPunti;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.R;

import java.util.List;

public class ClassificaTeamRVAdapter extends RecyclerView.Adapter<ClassificaTeamRVAdapter.MyViewHoder>  {
//##################################################################################################
//                              CLASSE INTERNA
//##################################################################################################
    /**
     * Questa classe è quella che permette alla RecyclingView Adapter di definire un layout custom
     * del singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public View v_mainLayout;
        //Informazioni generali
        public TextView  v_numTeamView;
        public TextView  v_punteggioTeamView;
        //Team, auto
        public TextView  v_nomeTeamView;
        public TextView  v_nomeAutoView;
        //Immagine
        public ImageView v_imagineTeamView;
        public ImageView v_imagineAutoView;
        //Divisore
        public View      v_divisoreView;
//==================================================================================================
//                              COSTRUTTORI
//==================================================================================================
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout           = itemView.findViewById(R.id.cly_RowClassificaPilota);
            //Informazioni generali
            v_numTeamView           = itemView.findViewById(R.id.txt_NumTeam);
            v_punteggioTeamView   = itemView.findViewById(R.id.txt_PunteggioTeam);
            //Pilota, team, auto
            v_nomeTeamView          = itemView.findViewById(R.id.txt_NomeTeam);
            v_nomeAutoView          = itemView.findViewById(R.id.txt_NomeAuto);
            //Immagine
            v_imagineTeamView       = itemView.findViewById(R.id.img_Team);
            v_imagineAutoView       = itemView.findViewById(R.id.img_Auto);
            //Divisore
            v_divisoreView          = itemView.findViewById(R.id.dvd_sottolinetatura);
        }
//==================================================================================================
    }
//##################################################################################################



//##################################################################################################
//                              CLASSE PRINCIPALE SoloGareRVAdapter
//##################################################################################################
    private Context comtext=null;
    private Activity activity=null;
    private List<TeamConPunti> listaTeam=null;
    private String nomeTeamDelUtenteAttivo=null;

//==================================================================================================
//                              COSTRUTTORI
//==================================================================================================
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public ClassificaTeamRVAdapter(Context comtext, Activity activity, List<TeamConPunti> listaTeam, String nomeTeamDelUtenteAttivo) {
        this.comtext = comtext;
        this.activity = activity;
        this.listaTeam = listaTeam;
        this.nomeTeamDelUtenteAttivo = nomeTeamDelUtenteAttivo;
    }
//==================================================================================================

//==================================================================================================
//                              CERAZIONE DEL SINGOLO ELEMETOTO
//==================================================================================================
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public ClassificaTeamRVAdapter.MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View bloccoElemeto = inflater.inflate(R.layout.row_classifica_team, parent,false);
        return new ClassificaTeamRVAdapter.MyViewHoder(bloccoElemeto);
    }
//==================================================================================================

//==================================================================================================
//                              INSERIRE L'ELEMETO NEL LAYOUT
//==================================================================================================
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore ClassificaTeamRVAdapter.
     */
    @Override
    public void onBindViewHolder(ClassificaTeamRVAdapter.MyViewHoder holder, int position) {
        final TeamConPunti team =listaTeam.get(position);
        final AutoConImmagini auto = team.getAuto();
        //Inserisci i dati nel layout
        //Informazioni generali
        holder.v_numTeamView.setText( ""+(position+1)+"°");
        holder.v_punteggioTeamView.setText(team.getPunti()+" punti");
        //Pilota, team, auto
        holder.v_nomeTeamView.setText(team.getNomeTeam());
        holder.v_nomeAutoView.setText(auto.getNome());
        if(nomeTeamDelUtenteAttivo.toLowerCase().equals(team.getNomeTeam().toLowerCase())){
            impostaPilotaAttivo(holder);
        }
    }
//==================================================================================================
//                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE
//==================================================================================================
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaTeam.size();
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                              IMPOSTA UTENTE ATTIVO
//##################################################################################################
    private void impostaPilotaAttivo(ClassificaTeamRVAdapter.MyViewHoder holder){
        holder.v_numTeamView.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
        holder.v_imagineAutoView.setColorFilter(activity.getResources().getColor(R.color.colorPrimary),   PorterDuff.Mode.MULTIPLY);
        holder.v_imagineTeamView.setColorFilter(activity.getResources().getColor(R.color.colorPrimary),   PorterDuff.Mode.MULTIPLY);
        holder.v_divisoreView.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));
        holder.v_divisoreView.setMinimumHeight(5);
    }
//##################################################################################################
}
