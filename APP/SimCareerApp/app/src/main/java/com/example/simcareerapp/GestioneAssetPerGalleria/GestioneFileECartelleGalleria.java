package com.example.simcareerapp.GestioneAssetPerGalleria;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Pair;

import com.example.simcareerapp.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

public class GestioneFileECartelleGalleria {

    private static String THUMBS = "thumbs";
    public static String PATH_GALLERIA = "Galleria";

    private Context context = null;
    private String root = null;
    private List<String> listaPathCartelle =null;
//##################################################################################################
//                                  COSTRUTTORI
//##################################################################################################
//==================================================================================================
//                                  COSTRUTTORI
//==================================================================================================
    public GestioneFileECartelleGalleria(Context context,String path) {
        this.context = context;
        this.root = path;
        this.listaPathCartelle=ceazioneListaPathDiCartellePerGalleria(path);
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  GET CARTELLE GALLERIA
//##################################################################################################
    /**
     * Questo metodo restituisce una lista di coppie di <<String>> e <<Bitmap>> patrendo dalla lista
     * delle catelle valide.
     */
    public List<Pair<String,Bitmap>> getListaCartelle(){
        List<Pair<String,Bitmap>> ris = new ArrayList<>();

        String pathFolder;
        String nomeFolder = null;
        Bitmap bitmapFoto = null;
        String[] temp;
        String[] tempFile;
        Iterator<String> iterator = this.listaPathCartelle.iterator();
        while (iterator.hasNext()){
            pathFolder = iterator.next();

            nomeFolder = null;
            bitmapFoto = null;
            try{

                tempFile = this.context.getAssets().list(pathFolder+File.separator+THUMBS);
                int i=-1;
                do{
                    i++;
                    if(accept(pathFolder+File.separator+THUMBS+File.separator+tempFile[i])){
                        temp = pathFolder.split(File.separator);
                        nomeFolder=temp[temp.length-1];
                        bitmapFoto= getFoto(pathFolder+File.separator+THUMBS,tempFile[i]);
                    }
                }while( (bitmapFoto==null) || (nomeFolder==null) );

            }catch (IOException e){
                nomeFolder = null;
                bitmapFoto = null;
            }
            if((bitmapFoto!=null) && (nomeFolder!=null)){
                ris.add(new Pair<String, Bitmap>(nomeFolder,bitmapFoto));
            }
        }
        return ris;
    }
//##################################################################################################

//##################################################################################################
//                                  GET IMMAGINE
//##################################################################################################
//==================================================================================================
//                                  GET BITMAP FOTO
//==================================================================================================
    /**
     * Questo metodo restituisce la foto in formato <<Bitmap>> se persente altrimeti null;
     */
    private Bitmap getFoto(String path, String nome){
        Bitmap bitmap = null;
        try {
            if(accept(path+File.separator+nome)){
                AssetFileDescriptor fileDescriptor = this.context.getAssets().openFd(path+File.separator+nome);
                bitmap = BitmapFactory.decodeStream(fileDescriptor.createInputStream());
            }
        } catch (IOException e) {
            bitmap = null;
        }
        return bitmap;
    }
//==================================================================================================
//==================================================================================================
//                                  GET FILE FOTO
//==================================================================================================
    /**
     * Questo metodo restituisce la foto in formato <<FIle>> se persente altrimeti null;
     */
    private File getFile(String path, String nome){
        File file =null;
        try {
            file = new File(this.context.getCacheDir(),this.context.getResources().getString(R.string.app_name)+"_"+nome);
            if(!file.exists()){
                file.createNewFile();
            }
            if (file.exists()){
                byte buf[] = new byte[1024];
                int len;
                FileOutputStream outputStream = new FileOutputStream(file);
                InputStream inputStream = this.context.getAssets().open(path+File.separator+nome);
                while ((len = inputStream.read(buf)) != -1) {
                    outputStream.write(buf, 0, len);
                }
                outputStream.close();
                inputStream.close();
            }else{
                file = null;
            }
        } catch (Exception e) {
            file=null;
        }
        return file;
    }
//##################################################################################################


//##################################################################################################
//                                  CREAZIONE DELLE CARTRELLE DELLA GALLERA
//##################################################################################################
//==================================================================================================
//                                  CREAZIONE LISTA DI TUTTE LE CARTELLE CHE CONTENGONO IMMAGINI
//==================================================================================================
    /**
     * Questo metodo crea la lista di tutte le cartelle in <<assets>> che contengono imagini per essere
     * usate.
     */
    private List<String> ceazioneListaPathDiCartellePerGalleria(String path) {
        List<String> ris= new LinkedList<>();
        String [] list;
        try {
            list = this.context.getAssets().list(path);
            for (int i=0; i<list.length;i++){
                String folder = list[i];
                if( verificaValiditaFolder(path+File.separator+folder) ){
                    ris.add(path+File.separator+folder);
                }
            }
        } catch (IOException e) {
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                                  VERIFICA SE LA CARTELLA HA UNA STRUTTURA VALIDA E HA FOTO
//==================================================================================================
    /**
     * Verifica se nella cartella c'è la catella <<thumbs>>
     */
    private boolean verificaValiditaFolder(String path){
        Boolean ris = false;
        Set<String> lista;
        try {
            lista = new TreeSet<String>(Arrays.asList(this.context.getAssets().list(path )));
            if( lista.contains(THUMBS) ){
                if(verificaPersenzaImmagine(path,THUMBS)){
                    ris = true;
                }
            }
        } catch (IOException e) {
        }
        return ris;
    }
//==================================================================================================

//==================================================================================================
//                                  VERIFICA PRESENZA IMMAGINE E IMAGINE PICCOLE
//==================================================================================================
    /**
     * Verifica se le immagini piccole hanno tutte il corrispetivo grande.
     */
    private boolean verificaPersenzaImmagine(String path,String folder){
        Boolean ris = true;
        Set<String> lista;
        String [] listaPiccola;
        try {
            listaPiccola = this.context.getAssets().list(path+File.separator+folder);
            lista = new TreeSet<String>(Arrays.asList(this.context.getAssets().list(path )));
            for (int i=0; i<listaPiccola.length;i++){
                String file = listaPiccola[i];
                if( accept(file) ){
                    if(!lista.contains(file)){
                        ris = false;
                    }
                }
            }
        } catch (IOException e) {}
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                                  VERIFICA SE FILE E' UN IMMAGINE
//==================================================================================================
    /**
     * Verifica se le immagini piccole hanno tutte il corrispetivo grande.
     */
    private boolean accept(String fileName) {
        List okFileExtensions = new ArrayList();
        okFileExtensions.add(".jpg");
        okFileExtensions.add(".png");
        okFileExtensions.add(".jpeg");
        boolean estensioneTrovata = false;
        boolean fileAccetato = false;
        Iterator<String> iterator = okFileExtensions.iterator();
        while ( (iterator.hasNext()) && (!estensioneTrovata) ){
            if(fileName.endsWith(iterator.next())){
                estensioneTrovata = true;
                fileAccetato = true;
            }
        }
        return fileAccetato;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  GET LISTA FOTO NELLA CATELLA
//##################################################################################################
    /**
     * Questo metodo restituisce una lista di <<Bitmap>> patrendo dalla lista delle cartelle e il nome
     * delle cartelle.
     */
    public List<Pair<String,Bitmap>> getListaFoto(String folder){
        List<Pair<String,Bitmap>> ris = new LinkedList<Pair<String,Bitmap>>();
        String[] lista;
        if(this.listaPathCartelle.contains(root+File.separator+folder)){
            try {
                lista = this.context.getAssets().list(root +File.separator+ folder +File.separator+ THUMBS);
                for (int i=0; i<lista.length;i++){
                    String file = lista[i];
                    if( accept(file) ){
                        ris.add(new Pair<String, Bitmap>(file,getFoto(root+File.separator+ folder +File.separator+ THUMBS,file)));
                    }
                }
            } catch (IOException e) {}
        }
        return ris;
    }

//##################################################################################################

//##################################################################################################
//                                  GET FOTO DALLA CATELLA e NOME FILE
//##################################################################################################
//==================================================================================================
//                                  GET BITMAP FOTO DALLA CATELLA e NOME FILE
//==================================================================================================
    /**
     * Questo metodo restituisce il <<Bitmap>> della foto selezionata o null se non esiste
     */
    public Bitmap getBitmapFoto(String folder,String fileNome){
       Bitmap ris = null;
        TreeSet<String> lista;
        if(this.listaPathCartelle.contains(root+File.separator+folder)){
            try {
                lista = new TreeSet<String>(Arrays.asList(this.context.getAssets().list(root+File.separator+folder)));
                if(lista.contains(fileNome)){
                    ris= getFoto(root+File.separator+folder ,fileNome);
                }
            } catch (IOException e) {
                ris = null;
            }
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                                  GET FILE FOTO DALLA CATELLA e NOME FILE
//==================================================================================================
    /**
     * Questo metodo restituisce il <<Bitmap>> della foto selezionata o null se non esiste
     */
    public File getFileFoto(String folder, String fileNome) {
        File ris = null;
        Bitmap bitmap = getBitmapFoto(folder,fileNome);
        if(bitmap!=null){
            try {
                //Nuovo DA API 22 A 29
                /*ris = new File(this.context.getExternalCacheDir()+File.separator+//Path
                        this.context.getResources().getString(R.string.app_name)+new Date().getTime() +".png");//File name
                FileOutputStream fileOutputStream= new FileOutputStream(ris);
                bitmap.compress(Bitmap.CompressFormat.PNG,100,fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();*/
                //Nuovo DA API 22 A 30
                File directoryFoto = this.context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                if(!directoryFoto.exists()){
                    directoryFoto.mkdirs();
                }
                ris = new File(directoryFoto,this.context.getResources().getString(R.string.app_name)+"_"+new Date().getTime() +".png");
                FileOutputStream fileOutputStream = new FileOutputStream(ris);
                bitmap.compress(Bitmap.CompressFormat.PNG,100,fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();

            }catch (Exception e) {
                ris = null;
            }
        }
        return ris;
    }
//==================================================================================================
//##################################################################################################
}
