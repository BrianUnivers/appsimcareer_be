package com.example.simcareerapp.AppCompatActivityWithMenu.MyServiceDiNotifiche;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Build;
import android.webkit.WebHistoryItem;
import android.widget.Toast;

import androidx.annotation.VisibleForTesting;
import androidx.core.app.NotificationCompat;

import com.example.simcareerapp.FotoGalleria.ListaFotoGalleriaFragment;
import com.example.simcareerapp.GestioneJson.GestioneListaMeteo;
import com.example.simcareerapp.GestioneJson.GestioneListeCampionati;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyPersistableBundle;
import com.example.simcareerapp.MieProssimeGareActivity;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Gara;
import com.example.simcareerapp.Persistence.Entities.Meteo;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Persistence.JsonEntities.CampionatoConJson;
import com.example.simcareerapp.Persistence.JsonEntities.MeteoConJson;
import com.example.simcareerapp.R;
import com.example.simcareerapp.SingoloCampionatoActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MyJobServiceNotificheUtente extends JobService {
//##################################################################################################
//                                  IMPORTANTE PER LE NOTIFICHE
//##################################################################################################
//==================================================================================================
//                                  ANITENI APERTE LE NOTIFICHE DOPO LA CHIUSURA DELL'APP
//==================================================================================================
    /**
     * Questo metodo permete a questo servizio di manternere attive notifiche anche se l'app viene chiusa
     * infatti viene richiamto a sua volta da un servio che si attiva alla sua chiusura e lo fa rimaniere
     * ativo.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return Service.START_STICKY;//STICKY NOTIFICATION
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  NUMERO NOTIFICHE
//##################################################################################################
    /**
     * Gestione dei ID per la creazione di notifiche sempre diverse.
     */
    private static int NEXT_ID = 0;
    public int getNextId(){
        return NEXT_ID++;
    }
//##################################################################################################

//##################################################################################################
//                                  VARIABILI CHE CONTENGONO LO STATO PRECEDENTE E QUELLO ATTUALE
//##################################################################################################
    /**
     * Variabili che contengono le informazioni principali per conoscere lo stato del meteo e dei
     * campionati rispetto all'ultima volta che si è verifificato un cambiamento.
     * Ed i dati del utente per capire di quali cambiamenti deve essere avvisato.
     */
    // Stato precedente è static per non leggerlo dal file ogni volta che viene eseguito il jobservice
    private static HashMap<String, MeteoConJson> meteoOld = null;
    private static List<CampionatoConJson>       campionatoOld = null;
    // Stato attuale
    private HashMap<String, MeteoConJson> meteoNew = null;
    private List<CampionatoConJson>       campionatoNew = null;
    // Dati utente
    private Utente  utenteAttivo = null;
    private Boolean mantieniAttivoUtente = null;
//##################################################################################################

//##################################################################################################
//                                  GSIONE RISORSE
//##################################################################################################
//==================================================================================================
//                                  LETTURA FILE SITUAZIONE CORRENTE E PASSATA ED EVENTUALE NOTIFICHE
//==================================================================================================
    /**
     * Questo metodo è qullo che prende le informazioni dai file e quelli utenti passatialla cereazione
     * del JobService così da verificare gli aggiornameti e se presenti aggiornare di conseguenza lo
     * stato attuale come l'ultimo notificato.
     * In più lo stato precedente viene letto dal file solo al primo controllo e viene sovreascritto
     * solo se c'è stato un cambiameto impotante.
     */
    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        //Estrazioni dai file locali dei dati dei stati del capionato e meteo sia attuali che passati.
        if(meteoOld==null){
            meteoOld = GestioneListaMeteo.letturaECreazioneListaMeteo(getApplicationContext(), R.raw.meteo_ultimo_controllo_per_notifiche);
        }
        if(campionatoOld==null){
            campionatoOld = GestioneListeCampionati.letturaECreazioneListaCampionati(getApplicationContext(), R.raw.campionati_ultimo_controllo_per_notifiche);
        }
        //Toast.makeText(getApplicationContext(), "OK 0 !", Toast.LENGTH_SHORT).show();
        meteoNew      = GestioneListaMeteo.letturaECreazioneListaMeteo(getApplicationContext(), R.raw.meteo);
        campionatoNew = GestioneListeCampionati.letturaECreazioneListaCampionati(getApplicationContext(), R.raw.campionati);

        //Estrazione dei parametri utente passati al servizio.
        MyPersistableBundle myBundle = new MyPersistableBundle(jobParameters.getExtras());
        utenteAttivo         = myBundle.getUtente(MyPersistableBundle.I_UTENTE_ATTIVO);
        mantieniAttivoUtente = myBundle.getBoolean(MyPersistableBundle.I_MANTIENI_ATTIVO_UTENTE);

        //Verifica se posso cotrollare i cambiameti.
        if((meteoOld.size()!=0) && (campionatoOld.size()!=0) && (utenteAttivo!=null) && (mantieniAttivoUtente!=null)){
            //Verifica cambiameti e invia le notifiche
            if(verificaCambiametiENotificali()){
                //Salva le modifificne già notificate
                GestioneListaMeteo.aggiornaListaMeteo(getApplicationContext(),R.raw.meteo_ultimo_controllo_per_notifiche,meteoNew);
                GestioneListeCampionati.aggiornaListaCampionati(getApplicationContext(),R.raw.campionati_ultimo_controllo_per_notifiche,campionatoNew);
                meteoOld      = meteoNew;     //Commeta per fare test così da non continuare a afare le stesse modifiche
                campionatoOld = campionatoNew;//Commeta per fare test così da non continuare a afare le stesse modifiche
            }
        }else{
            //Cambio lo stato iniziale con quello attuale perchè è la ptima lettura.
            meteoOld      = meteoNew;
            campionatoOld = campionatoNew;
        }

        //NotificationCompat.Builder d=creaNotifica(R.drawable.ic_baseline_gas_pump_24_normal_text,R.string.app_name,"Prova");
        //inviaNotifica(d);
        return false;
    }
//==================================================================================================
//==================================================================================================
//                                  SALVATAGGIO DELLO STATO CORRENTE COME ULTIMO STATO CONTROLLATO
//                                  SIA SU FILE CHE RUNTIME
//==================================================================================================
    /**
     * Questo metodo è quello che viene chiamato quando viene interroto il JobService che non fa altro
     * che salvare l'ultimo stato attuale non completamete notificato cioè che è stato interotto
     * come ultimo stato corretamete notificato.
     */
    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        GestioneListaMeteo.aggiornaListaMeteo(getApplicationContext(),R.raw.meteo_ultimo_controllo_per_notifiche,meteoOld);
        GestioneListeCampionati.aggiornaListaCampionati(getApplicationContext(),R.raw.campionati_ultimo_controllo_per_notifiche,campionatoOld);
        /*if(!mantieniAttivoUtente){//Utente non attivo
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
        }*/
        return true;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  CREAZIONE E INVIO NOTIFICHE
//##################################################################################################
//==================================================================================================
//                                  CRAZIONE NOTIFICA
//==================================================================================================
    /**
     * Questo metodo serve per creare una nuova notifica aggiungendo la sua icona un titolo e un
     * messaggio. Preò non verra inviata.
     */
    private NotificationCompat.Builder  creaNotifica(int idIcona,int idTitolo, String messagio) {
        String nomeChannel = "myChannel";//+NEXT_ID;
        //Per i sistemi operativi con API 26+ si deve creare un canale per poter inviare le notifiche.
        createNotificationChannel(nomeChannel);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, nomeChannel);

        builder.setSmallIcon(idIcona)
                .setContentTitle(this.getResources().getString(idTitolo))
                .setContentText(messagio)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messagio))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        //builder.setOngoing(true);//Mantieni aperte le notifiche dopo la chiusura del app.
        builder.setAutoCancel(true);
        return builder;
    }
//==================================================================================================
//==================================================================================================
//                                  INVIO NOTIFICA
//==================================================================================================
    /**
     * Questo metodo riceve una notifica corretametete creata e non fa altro che inviarla come nuova
     * notifica cioè con un id unico.
     */
    public void inviaNotifica(NotificationCompat.Builder builder){
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(getNextId(), builder.build());
    }
//==================================================================================================
//==================================================================================================
//                                  CREAZIONE DI UN CANALALE PER L'INVIO DELLE NOTIFICE PER API 26
//==================================================================================================
    /**
     * Per poter inviare le notifiche con i sistemi operativi con API 26+ si deve creare una canale
     * così da usarlo per la creazione della notifica.
     */
    private void createNotificationChannel(String nomeChannel) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(nomeChannel, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  INVIA NOTIFICA CAMBIAMENTO
//##################################################################################################
//==================================================================================================
//                                  INVIA NOTIFICA CAMBIAMENTO CAMPIONATO
//==================================================================================================
    /**
     * Metodo dato due campionati precedentemete dichiarati diversi cerca le differenze e genreara la
     * notifica adatta alle modifiche trovate. In oltre permette premendo su di essa aprire la
     * pagina del campionato a cui si riferisce.
     */
    private void inviaNotificaModificaCampionato(Campionato newCampionato,Campionato oldCampionato){
        String messagio = generaMessagioDelleModificheTrovateNelCampionato(newCampionato,oldCampionato);
        //Crea Notifica
        NotificationCompat.Builder builder = creaNotifica(R.drawable.ic_baseline_flag_checkered_solid_normal_text,
                R.string.ttl_CampionatoCambiato,messagio);

        Intent intent=null;
        if(mantieniAttivoUtente){
            /**
             * L'utente deve rimanere attivo anche se la app è chusa e quindi la notifica può senza
             * problemi di sicurezza aprire direttamete la pagina interessata.
             */
            // Create an explicit intent for an Activity in your app
            intent = new MyIntent(this, SingoloCampionatoActivity.class);
            ((MyIntent) intent).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            ((MyIntent) intent).putExtraUtente(MyIntent.I_UTENTE_ATTIVO,utenteAttivo);
            ((MyIntent) intent).putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
            ((MyIntent) intent).putExtra(MyIntent.I_ID_CAMPIONATO_SELEZIONATO,newCampionato.getId());
            PendingIntent pendingIntent = PendingIntent.getActivity(this, NEXT_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            // Set the intent that will fire when the user taps the notification
            builder.setContentIntent(pendingIntent);
        }else{
            /**
             * L'utente non è detto che abbia la sesione ancora attiva cioè può aver chiuso l'app
             * e perdendo così la certezza che permendo sulla notifica si vada ad aprire una sesione
             * già terminata. Quindi per sicurezza verrà inviato un messaggio di tipo toast che avvisa
             * l'utente della possibileacceso diretto ai dati solo se l'utente all'atto di login ha
             * selezionato la voce ricordami.
             */
            //TODO: EVENTUALI OPERAZIONEI
        }
        inviaNotifica(builder);
    }
//==================================================================================================
//==================================================================================================
//                                  INVIA NOTIFICA CAMBIAMENTO METEOROLOGICO
//==================================================================================================
    /**
     * Metodo dato una lista dei campionati con gare nei prossimi 7 gioni  che subiscono cambiameti
     * meteorologico genreara il correto messagio da inviare con le notifiche.
     * E se la lista è di un singolo campionato verra visualizata premendo sulla notifica la pagina
     * del campionato in questione. Altrimenti verrà aperta la pagina delle prossime gare del utente
     * e nella notifica si troverà quali gare di quali campinati c'è stato un cambiameto.
     */
    private void inviaNotificaCambiametoMeteorologico(List<Campionato> listaCampionati){
        if((listaCampionati!=null) && (!listaCampionati.isEmpty())){
            String messagio = generaMessagioCambiametoMeteorologico(listaCampionati);
            //Crea Notifica
            NotificationCompat.Builder builder = creaNotifica(R.drawable.ic_baseline_cloud_sun_solid_primary,
                    R.string.ttl_CampionatoPrevisioniMeteorologico,messagio);


            PendingIntent pendingIntent;
            Intent intent=null;
            if(mantieniAttivoUtente){
                /**
                 * L'utente deve rimanere attivo anche se la app è chusa e quindi la notifica può senza
                 * problemi di sicurezza aprire direttamete la pagina interessata.
                 */
                if(listaCampionati.size()==1){
                    // Create an explicit intent for an Activity in your app
                    intent = new MyIntent(this, SingoloCampionatoActivity.class);
                    ((MyIntent) intent).putExtra(MyIntent.I_ID_CAMPIONATO_SELEZIONATO,listaCampionati.get(0).getId());
                }else{
                    // Create an explicit intent for an Activity in your app
                    intent = new MyIntent(this, MieProssimeGareActivity.class);
                }
                ((MyIntent) intent).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                ((MyIntent) intent).putExtraUtente(MyIntent.I_UTENTE_ATTIVO,utenteAttivo);
                ((MyIntent) intent).putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
                pendingIntent = PendingIntent.getActivity(this, NEXT_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                // Set the intent that will fire when the user taps the notification
                builder.setContentIntent(pendingIntent);
            }else{
                /**
                 * L'utente non è detto che abbia la sesione ancora attiva cioè può aver chiuso l'app
                 * e perdendo così la certezza che permendo sulla notifica si vada ad aprire una sesione
                 * già terminata. Quindi per sicurezza verrà inviato un messaggio di tipo toast che avvisa
                 * l'utente della possibileacceso diretto ai dati solo se l'utente all'atto di login ha
                 * selezionato la voce ricordami.
                 */
                //TODO: EVENTUALI OPERAZIONEI
            }
            inviaNotifica(builder);
        }
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  VERIFICA CAMBIAMETI
//##################################################################################################
//==================================================================================================
//                                  VERIFICA CAMBIAMETI GENERERALE E INVIA NOTIFICHE
//==================================================================================================
    /**
     * Questo metodo verifica se ci sono cambiameti ed invia le eventuali notifiche.
     * Esso ritorna true se ci sono state modifiche, false altrimenti.
     */
    private boolean verificaCambiametiENotificali(){
        boolean ris=false;
        List<Campionato>         listaCampionati   = selezionaCampionatiNonConclusiPerPilota(campionatoNew,utenteAttivo);
        HashMap<Long,Campionato> hashMapCampionati = generaHashMapDaListaCampionato(campionatoOld);
        List<Campionato> CampionatiConVariazioniMeteo = new LinkedList<>();

        Iterator<Campionato> iterator = listaCampionati.iterator();
        while(iterator.hasNext()){
            Campionato newCampionato = iterator.next();
            Campionato oldCampionato=hashMapCampionati.get(newCampionato.getId());
            if(oldCampionato!=null){//Non è un nuovo campionato mai vito prima
                if(!newCampionato.equals(oldCampionato)){
                    ris = true;
                    inviaNotificaModificaCampionato(newCampionato,oldCampionato);
                }
               if(verificaCambiametoMeteoGare(newCampionato.getListaGara())){
                    ris = true;
                    CampionatiConVariazioniMeteo.add(newCampionato);
                }
            }
        }
        //Invia notifica che apre pagina del campionato se singolo o
        //quella delle mie prossime gare.
        inviaNotificaCambiametoMeteorologico(CampionatiConVariazioniMeteo);

        return ris;
    }
//==================================================================================================
//                                  VERIFICA CAMBIAMETI METEREOLOGICI
//==================================================================================================
    /**
     * Questo medodo serve per verificare se ci sono gare nella lista che si svolgono entro sette giorni
     * che hanno subito delle variazioni meteorologiche.
     */
    private Boolean verificaCambiametoMeteoGare(List<Gara> listGare){
        Boolean ris = false;
        if(listGare!=null){
            Iterator<Gara> iterator = listGare.iterator();
            while (iterator.hasNext()){
                Gara gara =iterator.next();
                Date oggi = new Date();
                Date settimanaProssima = new Date(oggi.getTime()+(1000*60*60*24*7));
                if( (oggi.before(gara.getData())) && (settimanaProssima.after(gara.getData()))){
                    Meteo newMeteo=this.meteoNew.get(gara.getCircuito().getNome().toLowerCase());
                    Meteo oldMeteo=this.meteoOld.get(gara.getCircuito().getNome().toLowerCase());
                    if( (newMeteo!=null) && (oldMeteo!=null) && (!newMeteo.equals(oldMeteo))){
                        ris=true;
                    }
                }
            }
        }
        return ris;
    }
//==================================================================================================
//##################################################################################################


//##################################################################################################
//                                  GESTIONE CAMPIONATI
//##################################################################################################
//==================================================================================================
//                                  SELEZIONA CAMPIONATI ATTIVI A CUI SONO ISCRITTO
//==================================================================================================
    /**
     * Questo metodo crea la lista dei campionati dei campionati che sono in corso o che iniziano entro
     * sette giorni da oggi a cui si è iscritto l'utente.
     */
    private List<Campionato> selezionaCampionatiNonConclusiPerPilota(List<CampionatoConJson> listaCampionati, Utente utenteAttivo) {
        List<Campionato> ris = new LinkedList<Campionato>();
        Iterator<CampionatoConJson> i = listaCampionati.iterator();
        Date inizio = new Date();
        Date oggi = new Date();
        inizio.setTime(inizio.getTime()+(1000*60*60*24*7));//Sette giorni d'anticipo
        while (i.hasNext()){
            Campionato campionato = i.next();
            if(oggi.before(campionato.getDataFineCampionato())){
                if(campionato.utenteIscritto(utenteAttivo)){
                    ris.add(campionato);
                }
            }
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                                  CREAZIONE HASHMAP LISTA CAMPIONATI VECCHI
//==================================================================================================
    /**
     * Questo metodo trasforma una lista di campionati in un hashMap dei campionati con chiave l'id
     * del capionato cosi da facilitare le ricerce succesive.
     */
    private HashMap<Long,Campionato> generaHashMapDaListaCampionato(List<CampionatoConJson> listaCampionati){
        HashMap<Long,Campionato> ris = new HashMap<>();
        Iterator<CampionatoConJson> iterator = listaCampionati.iterator();
        while (iterator.hasNext()){
            Campionato campionato = iterator.next();
            ris.put(campionato.getId(),campionato);
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                                  CREAZIONE HASHMAP LISTA GARE VECCHIE
//==================================================================================================
    /**
     * Questo metodo trasforma una lista di gare in un hashMap delle gare con chiave l'id
     * della gara cosi da facilitare le ricerce succesive.
     */
    private HashMap<Long,Gara> generaHashMapDaListaGare(List<Gara> listaGare){
        HashMap<Long,Gara> ris = new HashMap<>();
        Iterator<Gara> iterator = listaGare.iterator();
        while (iterator.hasNext()){
            Gara gara = iterator.next();
            ris.put(gara.getId(),gara);
        }
        return ris;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  GENERA MESSAGGI
//##################################################################################################
//==================================================================================================
//                                  GENERA MESSAGGI DI MODIFICA CAMPIONATO
//==================================================================================================
    /**
     * Questo metodo dato due campionati precedentemente catrolati che siano diversi, genera un messagio
     * elencando tutte le variazioni dal capionato allo stato precedente rispetto allo stato attuale.
     */
    private String generaMessagioDelleModificheTrovateNelCampionato(Campionato newCampionato,Campionato oldCampionato){
        int cabiameti = 0;
        String messagioCambiameti=new String();
        //Cambiameti Nome
        if(!newCampionato.getName().equals(oldCampionato.getName())){
            cabiameti++;
            messagioCambiameti +="\n- Ha cambiato nome da \""+oldCampionato.getName()+"\" a \""+newCampionato.getName()+"\".";
        }

        //Cambiameti Gare
        if(!newCampionato.getListaGara().equals(oldCampionato.getListaGara())){
            HashMap<Long,Gara> gareHashOldCampionato = generaHashMapDaListaGare(oldCampionato.getListaGara());
            Iterator<Gara> iterator = newCampionato.getListaGara().iterator();
            while (iterator.hasNext()){
                Gara newGara = iterator.next();
                Gara oldGara = gareHashOldCampionato.get(newGara.getId());
                if(oldGara==null){
                    cabiameti++;
                    messagioCambiameti +="\n- Si è aggiunito la gara di \""+newGara.getCircuito().getNome()+"\" il giorno "+formattaData(newGara.getData())+".";
                }else{
                    if(!newGara.equals(oldGara)){
                        if(!newGara.getCircuito().equals(oldGara.getCircuito())){
                            cabiameti++;
                            messagioCambiameti +="\n- La gara si terrà \""+newGara.getCircuito().getNome()+"\" al posto di \""+oldGara.getCircuito().getNome()+"\".";
                        }
                        if(!newGara.getData().equals(oldGara.getData())){
                            cabiameti++;
                            messagioCambiameti +="\n- La gara di \""+newGara.getCircuito().getNome()+"\" è stata spostata dal giono "+formattaData(oldGara.getData())+" al giono "+formattaData(newGara.getData())+".";
                        }
                    }
                }
            }
        }

        //Cambiamete le bandiere
        if(!newCampionato.equalsBandere(oldCampionato)){
            cabiameti++;
            messagioCambiameti +="\n- C'è stata una variazione nelle bandiere utilizate.";
        }
        //Cambiamete le aiuti alla guida
        if(!newCampionato.equalsAiutiAllaGuida(oldCampionato)){
            cabiameti++;
            messagioCambiameti +="\n- C'è stata una variazione nei aiuti alla guida permessi.";
        }
        //Cambiamete le aiuti alla guida
        if(!newCampionato.equalsConsumi(oldCampionato)){
            cabiameti++;
            messagioCambiameti +="\n- C'è stata una variazione nei consumi.";
        }

        String messagio="Il campionato "+newCampionato.getName()+ " ha subito ";
        if(cabiameti==1){
            messagio+=" il seguente cambiameto.";
        }else{
            messagio+=" i seguenti cambiameti.";
        }
        messagio+=messagioCambiameti;
        return messagio;
    }
//==================================================================================================
//==================================================================================================
//                                  GENERA MESSAGGI DI MODIFICA CAMPIONATO
//==================================================================================================
    /**
     * Questo metodo cerca le gare dei campionati passati già selezionati che hanno delle gare nei
     * prossimi sette gionri che hanno subito dei cambiameti meteorologici, generando così i messagi
     * che indicano il nome del campinato e la gara che ha subito tali cambiamenti.
     */
    private String generaMessagioCambiametoMeteorologico(List<Campionato> listaCampionati){
        int cabiameti = 0;
        String messagioCambiameti=new String();

        Iterator<Campionato> iterator = listaCampionati.iterator();
        while (iterator.hasNext()){
            cabiameti++;
            int cabiametiGare = 0;
            String messagioCambiametiGare=new String();
            //Cambiameti meteo
            Campionato campionato=iterator.next();
            Iterator<Gara> iteratorGare = campionato.getListaGara().iterator();
            while (iteratorGare.hasNext()){
                Gara gara =iteratorGare.next();
                Date oggi = new Date();
                Date settimanaProssima = new Date(oggi.getTime()+(1000*60*60*24*7));
                if( (oggi.before(gara.getData())) && (settimanaProssima.after(gara.getData()))){
                    Meteo newMeteo=this.meteoNew.get(gara.getCircuito().getNome().toLowerCase());
                    Meteo oldMeteo=this.meteoOld.get(gara.getCircuito().getNome().toLowerCase());
                    if( (newMeteo!=null) && (oldMeteo!=null) && (!newMeteo.equals(oldMeteo))){
                        cabiametiGare++;
                        messagioCambiametiGare+="\n   - Circuito di \""+gara.getCircuito().getNome()+"\".";
                    }
                }
            }
            //Prefisso per gere
            messagioCambiameti+="\n- Il campionato di \""+campionato.getName()+"\" ha subito cambiamenti per";
            if(cabiametiGare==1){
                messagioCambiameti+=" la seguente gara.";
            }else{
                messagioCambiameti+=" le seguenti gare.";
            }
            messagioCambiameti+=messagioCambiametiGare;
        }

        //Prefisso per campionato
        String messagio="Le previsioni meteo hanno subito dei cambiameti per";
        if(cabiameti==1){
            messagio+=" il seguente campionato.";
        }else{
            messagio+=" i seguenti campionati.";
        }
        messagio+=messagioCambiameti;
        return messagio;
    }
//==================================================================================================
//==================================================================================================
//                                  METODI DI APPOGGIO
//==================================================================================================
    /**
     * Questo metodo farmatta corretamete la data, generando una stringa.
     */
    //Formattazione
    private static String FORMATTAZIONE_DATE = "dd/MM/yyyy";
    private String formattaData(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat(FORMATTAZIONE_DATE);
        return sdf.format(date);
    }
//==================================================================================================
//##################################################################################################
}

