package com.example.simcareerapp.TestingApp;

import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinnerAdapter.CampionatoSpinnerAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinnerAdapter.GaraSpinnerAdapter;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Gara;
import com.example.simcareerapp.Persistence.Entities.Meteo;
import com.example.simcareerapp.Persistence.JsonEntities.MeteoConJson;
import com.example.simcareerapp.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TestAppModificaGaraFragment#} factory method to
 * create an instance of this fragment.
 */
public class TestAppModificaGaraFragment extends Fragment {
//private OnFragmentInfoCampionatoFragmentEventListener listener = null;
//##################################################################################################
//                              DATI PASSATI
//##################################################################################################
    private List<Campionato> listaCampionati   = null;
    private HashMap<String,MeteoConJson> meteo = null;
    //==================================================================================================
//                                  COSTRUTORI
//==================================================================================================
    public TestAppModificaGaraFragment(List<Campionato> listaCampionati, HashMap<String, MeteoConJson> meteo) {
        this.listaCampionati   = listaCampionati;
        this.meteo             = meteo;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  FRAGMENT
//##################################################################################################
    private View    viewGroupLayout = null;
    private Spinner viewSpinnerCampionato = null;
    private Spinner viewSpinnerGara = null;
//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
//==================================================================================================
//==================================================================================================
//                                  DEFINIZIONE DEL LAYOUT DEL FRAGMENT
//==================================================================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);//<<<-------------------------------------ATTEZIONE
        //Scelta del layout usato per fragment.
        viewGroupLayout = inflater.inflate(R.layout.fragment_test_app_modifica_gara, container, false);

        //Imposta lo spinner per la selezione del Campionato
        viewSpinnerCampionato  = viewGroupLayout.findViewById(R.id.spn_InputCampionato);
        viewSpinnerGara        = viewGroupLayout.findViewById(R.id.spn_InputGara);

        ArrayList<Campionato> listaCampionatiApp = new ArrayList<>();
        listaCampionatiApp.addAll(listaCampionati);
        ArrayAdapter adapter = new CampionatoSpinnerAdapter(getContext(),listaCampionatiApp);
        viewSpinnerCampionato.setAdapter(adapter);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  METODO CHIAMATO SULLA SELEZIONE DEL CAMPIONATO
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        viewSpinnerCampionato.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Campionato campionato = (Campionato) adapterView.getItemAtPosition(i);
                visualizaImpostazioniCampionato(campionato);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  METODO CHIAMATO SULLA SELEZIONE DELLA GARA
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        viewSpinnerGara.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Gara gara = (Gara) adapterView.getItemAtPosition(i);
                visualizaImpostazioniGara(gara,meteo.get(gara.getCircuito().getNome().toLowerCase()));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return viewGroupLayout;
    }

//==================================================================================================
//##################################################################################################



//##################################################################################################
//                                  COMUNICAZIONE CON L'ACTIVITY
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY
//==================================================================================================
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Crea il link con l'activity che contiene il fragment.
        //listener = (OnFragmentInfoCampionatoFragmentEventListener) activity;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  SELEZIONE DI UN NUOVO CAMPIONATO
//##################################################################################################
    private void visualizaImpostazioniCampionato(Campionato campionato){
        ArrayAdapter adapter = new GaraSpinnerAdapter(getContext(),selezionaGareFuture(campionato.getListaGara()));
        viewSpinnerGara.setAdapter(adapter);
    }
//==================================================================================================
//                                  SELEZIONA GARE DA EFFETUARE
//==================================================================================================
    private ArrayList<Gara> selezionaGareFuture(List<Gara> lista){
        ArrayList<Gara> ris= new ArrayList<Gara>();
        Date oggi = new Date();
        if(lista!=null){
            Iterator<Gara> iterator = lista.iterator();
            while (iterator.hasNext()){
                Gara gara = iterator.next();
                if(gara.getData().after(oggi)){
                    ris.add(gara);
                }
            }
        }
        return ris;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  SELEZIONE DI UN NUOVO CAMPIONATO
//##################################################################################################
    private void visualizaImpostazioniGara(Gara gara, Meteo meteo){
        Fragment frag = new ModificaGaraFragment(gara,meteo);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fly_ModificheAlGara, frag);
        ft.commit();
    }
//##################################################################################################
}