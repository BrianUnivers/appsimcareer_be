package com.example.simcareerapp.GestioneJson;

import android.content.Context;

import com.example.simcareerapp.GestioneJson.Generale.GestioneGenerale;
import com.example.simcareerapp.Persistence.JsonEntities.CampionatoConJson;
import com.example.simcareerapp.Persistence.JsonEntities.MeteoConJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyStore;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GestioneListaMeteo extends GestioneGenerale {
//##################################################################################################
//                                  ETICHETTE PER LA GESTIONE DEL JSON
//##################################################################################################
    private static final String LABLE_METEO= "meteo";
//##################################################################################################
//==================================================================================================
//                              LETTURA E CREAZIONE LISTA DEI CAMPIONATI
//==================================================================================================
    public static HashMap<String,MeteoConJson> letturaECreazioneListaMeteo(Context context , int idFile){
        String contenutoFile = GestioneGenerale.leturaFileJson(context,idFile);
        HashMap<String,MeteoConJson> ris = new HashMap<String,MeteoConJson>();
        try {
            JSONObject jsonObject = new JSONObject(contenutoFile);

            JSONArray jsonArrayGare = jsonObject.getJSONArray(LABLE_METEO);
            for(int i=0; i<jsonArrayGare.length(); i++){
                MeteoConJson meteo= new MeteoConJson(jsonArrayGare.getJSONObject(i));
                if(!meteo.isNull()){
                    ris.put(meteo.getLuogo().toLowerCase(),meteo);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                              AGGIORNA LISTA DEI CAMPIONATI
//==================================================================================================
public static Boolean aggiornaListaMeteo(Context context , int idFile,HashMap<String,MeteoConJson>  lista){
    String contenuto = "";

    try {
        JSONObject object = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        Iterator<Map.Entry<String,MeteoConJson>> iterator = lista.entrySet().iterator();
        int i=0;
        while (iterator.hasNext()){
            MeteoConJson meteo = iterator.next().getValue();
            JSONObject meteoJSON = meteo.getJSON();
            if(meteoJSON!=null){
                jsonArray.put(i,meteoJSON);
                i++;//Prossimo elemeto
            }
        }
        object.put(LABLE_METEO,jsonArray);
        contenuto = object.toString();
    } catch (JSONException e) {
        e.printStackTrace();
    }
    return GestioneGenerale.scritturaFileJson(context,idFile,contenuto);
}
//==================================================================================================
}
