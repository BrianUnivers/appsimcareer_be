package com.example.simcareerapp.Persistence.Entities;

import com.example.simcareerapp.Persistence.Entities.CircuitoConImmagini;

import java.util.Date;
import java.util.Objects;

public class Gara {
//##################################################################################################
//                                  INFORMAZIONI GARA
//##################################################################################################
    protected Long                id        = null;
    protected Date                data      = null;
    protected CircuitoConImmagini circuito  = null;
//##################################################################################################
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
    public Gara() {
    }
//--------------------------------------------------------------------------------------------------
    public Gara(Long id, Date data, String nomeeCircuito) {
        this.id = id;
        this.data = data;
        this.circuito = new CircuitoConImmagini(nomeeCircuito);
    }
//==================================================================================================
//==================================================================================================
//                              GETTER E SETTER
//==================================================================================================
    public Long getId() {
        return id;
    }
//------------------------------
    public void setId(Long id) {
        this.id = id;
    }
//--------------------------------------------------------------------------------------------------
    public Date getData() {
        return data;
    }
//------------------------------
    public void setData(Date data) {
        this.data = data;
    }
//--------------------------------------------------------------------------------------------------
    public CircuitoConImmagini getCircuito() {
        return circuito;
    }
//------------------------------
    public void setCircuito(CircuitoConImmagini circuito) {
        this.circuito = circuito;
    }
//==================================================================================================
//==================================================================================================
//                              VERIFICA CHE NON SIA UN OGGETO CON PARAMETRI NULL
//==================================================================================================
    public boolean isNull(){
        return !( (getCircuito()!=null) && (getData()!=null) && (getId()!=null) );
    }

//==================================================================================================
//==================================================================================================
//                              EQUALS E HASHCODE
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Gara)) return false;
        Gara gara = (Gara) o;
        return getId().equals(gara.getId()) &&
                getData().equals(gara.getData()) &&
                getCircuito().equals(gara.getCircuito());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getData(), getCircuito());
    }

//==================================================================================================
}
