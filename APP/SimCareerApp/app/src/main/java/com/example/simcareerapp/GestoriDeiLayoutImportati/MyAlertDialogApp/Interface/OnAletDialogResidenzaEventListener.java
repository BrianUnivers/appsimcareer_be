package com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp.Interface;

import java.io.File;

import com.example.simcareerapp.Persistence.Entities.Indirizzo;

//##################################################################################################
//                                  INTERFACCIA PER COMUNICARE
//##################################################################################################
public interface OnAletDialogResidenzaEventListener {
    /**
     * Ritorna true se l'indirizzo pasato è ritenuto valido altrimeti false
     */
    boolean impostaIndirizzoDiResidenza(Indirizzo indirizzo);
}
//##################################################################################################
//##################################################################################################