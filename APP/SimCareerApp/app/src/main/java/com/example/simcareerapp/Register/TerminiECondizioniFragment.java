package com.example.simcareerapp.Register;

import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.simcareerapp.R;
import com.example.simcareerapp.Register.Interface.OnFragmentTerminiECondizioniEventListener;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TerminiECondizioniFragment#} factory method to
 * create an instance of this fragment.
 */
public class TerminiECondizioniFragment extends Fragment {
    /**
     * listener è la variabile che permette di comunicare con l'Activity
     * View     è la variabile che permette di ricavaare le view dai id
     */
    private OnFragmentTerminiECondizioniEventListener listener = null;
    private View viewGroupLayout = null;
    private TextView terminiECondizioni=null;
    private ScrollView scrollView = null;
//==================================================================================================
//                                  COSTRUTORI
//==================================================================================================
    public TerminiECondizioniFragment() {
        // Required empty public constructor
    }
//==================================================================================================


//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
//==================================================================================================


//==================================================================================================
//                                  DEFINIZIONE DEL LAYOUT DEL FRAGMENT
//==================================================================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);//<<<-------------------------------------ATTEZIONE

        //Scelta del layout usato per fragment.
        viewGroupLayout = inflater.inflate(R.layout.fragment_termini_e_condizioni, container, false);
        terminiECondizioni = viewGroupLayout.findViewById(R.id.txt_TerminiECondizioni);
        scrollView = viewGroupLayout.findViewById(R.id.scl_TerminiECondizioni);
        /*if(terminiECondizioni.getHeight()<=scrollView.getHeight()) {
            Button btnAcceta = (Button) viewGroupLayout.findViewById(R.id.btn_Acceta);
            btnAcceta.setEnabled(true);
        }*/
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  LISTENER DEI BOTTONI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Listener del pulsante Annula
        Button btnAnnula = (Button) viewGroupLayout.findViewById(R.id.btn_Annula);
        btnAnnula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonAnnula(v);
            }
        });
//--------------------------------------------------------------------------------------------------
        // Listener del pulsante acceta
        Button btnAcceta = (Button) viewGroupLayout.findViewById(R.id.btn_Acceta);
        btnAcceta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonAcceta(v);
            }
        });
//--------------------------------------------------------------------------------------------------
        //Abilita il pulsante solo se si sono viste tutte le condizioni .
        final ScrollView sclTerminiECondizioni = (ScrollView) viewGroupLayout.findViewById(R.id.scl_TerminiECondizioni);
        sclTerminiECondizioni.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                scrollEndAbilitaAccetta(sclTerminiECondizioni);
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return viewGroupLayout;
    }
//==================================================================================================


//==================================================================================================
//                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY
//==================================================================================================
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Crea il link con l'activity che contiene il fragment.
        listener = (OnFragmentTerminiECondizioniEventListener) activity;
    }
//==================================================================================================


//==================================================================================================
//                                  METODI CHIAMATI ALLA PRESIONE DEI BOTTONI
//==================================================================================================

    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante annula che passera il
     * controllo all' activity che attiverà la procedura di anullamento
     */
    public void buttonAnnula(View v) {
        listener.anullaIscrizione();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante di accetazione dei termini che
     * passera il controllo all' activity che attiverà la procedura registazione
     */
    public void buttonAcceta(View v) {
        listener.accettaTerminiECondizioni();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Il metodo chiamato dal Lisener quando si raggiunge il fondo scrollando dei temini e condizioni
     * per abilitare il pulsante continua.
     */
    public void scrollEndAbilitaAccetta(ScrollView sclTerminiECondizioni) {
        int rootHeight = sclTerminiECondizioni.getHeight();
        int childHeight = sclTerminiECondizioni.getChildAt(0).getHeight();
        int scrollY = sclTerminiECondizioni.getScrollY();
        if ((childHeight - rootHeight == scrollY) ) {
            Button btnAcceta = (Button) viewGroupLayout.findViewById(R.id.btn_Acceta);
            btnAcceta.setEnabled(true);
        }
    }
//==================================================================================================
}