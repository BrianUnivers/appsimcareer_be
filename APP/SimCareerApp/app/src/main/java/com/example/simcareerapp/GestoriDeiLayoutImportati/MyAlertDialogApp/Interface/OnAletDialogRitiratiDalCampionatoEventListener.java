package com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp.Interface;

import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Pilota;

//##################################################################################################
//                                  INTERFACCIA PER COMUNICARE
//##################################################################################################
public interface OnAletDialogRitiratiDalCampionatoEventListener {
    /**
     * Ritorna true se i dati del pilota passati sono ritenuti validi altrimeti false
     */
    boolean informazioniPerAnnulareIscrizioneAlCampionato(Pilota pilota);
}
//##################################################################################################
//##################################################################################################