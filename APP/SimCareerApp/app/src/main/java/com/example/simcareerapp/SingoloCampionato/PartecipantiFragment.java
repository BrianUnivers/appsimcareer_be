package com.example.simcareerapp.SingoloCampionato;

import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.GareDeiCampionatiRVAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.PilotiRVAdapter;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Persistence.JsonEntities.CampionatoConJson;
import com.example.simcareerapp.R;
import com.example.simcareerapp.SingoloCampionato.Interface.OnFragmentInfoCampionatoFragmentEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PartecipantiFragment# } factory method to
 * create an instance of this fragment.
 */
public class PartecipantiFragment extends Fragment {
    /**
     * listener è la variabile che permette di comunicare con l'Activity
     * View     è la variabile che permette di ricavaare le view dai id
     */
    private OnFragmentInfoCampionatoFragmentEventListener listener = null;
    private View viewGroupLayout = null;
//##################################################################################################
//                              DATI PASSATI
//##################################################################################################
    private CampionatoConJson campionato   = null;
    private Utente utenteAttivo = null;
//==================================================================================================
//                                  COSTRUTORI
//==================================================================================================
    public PartecipantiFragment(CampionatoConJson campionato, Utente utenteAttivo) {
        this.campionato   = campionato;
        this.utenteAttivo = utenteAttivo;
    }
//==================================================================================================
//##################################################################################################


//##################################################################################################
//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
//==================================================================================================
//==================================================================================================
//                                  DEFINIZIONE DEL LAYOUT DEL FRAGMENT
//==================================================================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);//<<<-------------------------------------ATTEZIONE

        //Scelta del layout usato per fragment.
        viewGroupLayout = inflater.inflate(R.layout.fragment_partecipanti, container, false);
        //Imposta il sottotitolo
        Toolbar sottotitolo = viewGroupLayout.findViewById(R.id.toolbar_subtitle);
        sottotitolo.setTitle(campionato.getName());
        //Visualizza piloti
        RecyclerView listaPiloti = viewGroupLayout.findViewById(R.id.rcl_ListaPiloti);
        PilotiRVAdapter pilotiRVAdapter = new PilotiRVAdapter(getContext(),getActivity(),campionato.getListaPiloti(),utenteAttivo);;
        if(pilotiRVAdapter!=null){
            listaPiloti.setAdapter(pilotiRVAdapter);
        }
        listaPiloti.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL, false));

        return viewGroupLayout;
    }

//==================================================================================================
//##################################################################################################



//##################################################################################################
//                                  COMUNICAZIONE CON L'ACTIVITY
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY
//==================================================================================================
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Crea il link con l'activity che contiene il fragment.
        listener = (OnFragmentInfoCampionatoFragmentEventListener) activity;
    }
//==================================================================================================
//##################################################################################################

}