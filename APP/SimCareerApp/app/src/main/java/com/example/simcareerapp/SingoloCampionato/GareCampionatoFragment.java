package com.example.simcareerapp.SingoloCampionato;

import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.CampionatoRVAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.GareDeiCampionatiRVAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface.OnClickClassificaDellaGaraListener;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Persistence.JsonEntities.CampionatoConJson;
import com.example.simcareerapp.R;
import com.example.simcareerapp.SingoloCampionato.Interface.OnFragmentInfoCampionatoFragmentEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GareCampionatoFragment#} factory method to
 * create an instance of this fragment.
 */
public class GareCampionatoFragment extends Fragment {
    /**
     * listener è la variabile che permette di comunicare con l'Activity
     * View     è la variabile che permette di ricavaare le view dai id
     */
    private OnFragmentInfoCampionatoFragmentEventListener listener = null;
    private View viewGroupLayout = null;
//##################################################################################################
//                              DATI PASSATI
//##################################################################################################
    private CampionatoConJson campionato   = null;
    private Utente utenteAttivo = null;
    private OnClickClassificaDellaGaraListener actionOnClick =null;
//==================================================================================================
//                                  COSTRUTORI
//==================================================================================================
    public GareCampionatoFragment(CampionatoConJson campionato, Utente utenteAttivo,OnClickClassificaDellaGaraListener actionOnClick) {
        this.campionato   = campionato;
        this.utenteAttivo = utenteAttivo;
        this.actionOnClick=actionOnClick;
    }
//==================================================================================================
//##################################################################################################


//##################################################################################################
//                                  FRAGMENT
//##################################################################################################
//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
//==================================================================================================
//==================================================================================================
//                                  DEFINIZIONE DEL LAYOUT DEL FRAGMENT
//==================================================================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);//<<<-------------------------------------ATTEZIONE

        //Scelta del layout usato per fragment.
        viewGroupLayout = inflater.inflate(R.layout.fragment_gare_campionato, container, false);
        //Visualizza gare dei campionati
        RecyclerView listaGarePerCampionati = viewGroupLayout.findViewById(R.id.rcl_ListaGarePerCampionato);
        List<Campionato> listaGarePerCampionato = new ArrayList<Campionato>();
        listaGarePerCampionato.add(campionato);
        GareDeiCampionatiRVAdapter gareRVAdapter = new GareDeiCampionatiRVAdapter(getContext(),getActivity(),actionOnClick,listaGarePerCampionato,utenteAttivo);;
        if(gareRVAdapter!=null){
            listaGarePerCampionati.setAdapter(gareRVAdapter);
        }
        listaGarePerCampionati.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL, false));
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  LISTENER DEI BOTTONI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

//--------------------------------------------------------------------------------------------------

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return viewGroupLayout;
    }

//==================================================================================================
//##################################################################################################



//##################################################################################################
//                                  COMUNICAZIONE CON L'ACTIVITY
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY
//==================================================================================================
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Crea il link con l'activity che contiene il fragment.
        listener = (OnFragmentInfoCampionatoFragmentEventListener) activity;
    }
//==================================================================================================
//##################################################################################################

}