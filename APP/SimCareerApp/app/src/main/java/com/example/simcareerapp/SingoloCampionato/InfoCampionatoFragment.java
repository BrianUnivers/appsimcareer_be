package com.example.simcareerapp.SingoloCampionato;

import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MyChipAdapter.MyChipAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyView.MyGridView;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySeparatoriPagine.SeparatorePaginaConTesto;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Persistence.JsonEntities.CampionatoConJson;
import com.example.simcareerapp.R;
import com.example.simcareerapp.SingoloCampionato.Interface.OnFragmentInfoCampionatoFragmentEventListener;

import java.text.SimpleDateFormat;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InfoCampionatoFragment} factory method to
 * create an instance of this fragment.
 */
public class InfoCampionatoFragment extends Fragment {
    /**
     * listener è la variabile che permette di comunicare con l'Activity
     * View     è la variabile che permette di ricavaare le view dai id
     */
    private OnFragmentInfoCampionatoFragmentEventListener listener = null;
    private View viewGroupLayout = null;
//##################################################################################################
//                              DATI PASSATI
//##################################################################################################
    private CampionatoConJson campionato   = null;
    private Utente            utenteAttivo = null;
//==================================================================================================
//                                  COSTRUTORI
//==================================================================================================
    public InfoCampionatoFragment(CampionatoConJson campionato, Utente utenteAttivo) {
        this.campionato   = campionato;
        this.utenteAttivo = utenteAttivo;
    }
//==================================================================================================
//##################################################################################################


//##################################################################################################
    private Button btnViewIscriviti;
    private Button btnViewRitirati;
    private Button btnViewClassifica;
//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
}
//==================================================================================================
//==================================================================================================
//                                  DEFINIZIONE DEL LAYOUT DEL FRAGMENT
//==================================================================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);//<<<-------------------------------------ATTEZIONE

        //Scelta del layout usato per fragment.
        viewGroupLayout = inflater.inflate(R.layout.fragment_info_campionato, container, false);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  LISTENER DEI BOTTONI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        btnViewIscriviti = viewGroupLayout.findViewById(R.id.btn_Iscriviti);
        btnViewRitirati  = viewGroupLayout.findViewById(R.id.btn_Ritirati);
        btnViewClassifica= viewGroupLayout.findViewById(R.id.btn_Classifica);
//--------------------------------------------------------------------------------------------------
        btnViewIscriviti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonIscriviti(v);
            }
        });
//--------------------------------------------------------------------------------------------------
        btnViewRitirati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonRitirati(v);
            }
        });
//--------------------------------------------------------------------------------------------------
        btnViewClassifica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClassifica(v);
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        setLayoutDeiInclude();
        setLayoutConDati();
        return viewGroupLayout;
    }

//==================================================================================================
//##################################################################################################



//##################################################################################################
//                                  COMUNICAZIONE CON L'ACTIVITY
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY
//==================================================================================================
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Crea il link con l'activity che contiene il fragment.
        listener = (OnFragmentInfoCampionatoFragmentEventListener) activity;
    }
//==================================================================================================
//##################################################################################################



//##################################################################################################
//                                  METODI DEI PULSANTI
//##################################################################################################
//==================================================================================================
//                                  METODI CHIAMATI ALLA PRESIONE DEI BOTTONI
//==================================================================================================
    private void buttonIscriviti(View v){
        listener.creaNuovaIscrizioneAlCampionato();
    }
//--------------------------------------------------------------------------------------------------
    private void buttonRitirati(View v){
        listener.cancellaIscrizioneAlCampionato();
    }
//--------------------------------------------------------------------------------------------------
    private void buttonClassifica(View v){
        listener.classificaCampionato();
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                 ADATTA IL CONTENUTO DEL LAYOUT
//##################################################################################################
//==================================================================================================
//                                 IMPOSTAZIONI DELLE VARIANTI DI BLOCHI INCLUSI
//==================================================================================================
    /**
     * Questo metodo imposta il layout dei bolochi neutri che vengono impotati per il loro riutilizzo.
     */
    private void setLayoutDeiInclude() {
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude((ViewGroup) viewGroupLayout,
                                                                                R.id.include_dvd_DivisoreAiutiAllaGuida,
                                                                                R.string.lbl_AiutiAllaGuida,
                                                                                R.drawable.ic_baseline_car_steering_wheel_24_normal_text);
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude((ViewGroup)  viewGroupLayout,
                                                                                R.id.include_dvd_DivisoreBandiere,
                                                                                R.string.lbl_Bandiera,
                                                                                R.drawable.ic_baseline_flag_24_normal_text);
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude((ViewGroup)  viewGroupLayout,
                                                                                R.id.include_dvd_DivisoreConsumi,
                                                                                R.string.lbl_Consumi,
                                                                                R.drawable.ic_baseline_gas_pump_24_normal_text);
    }
//==================================================================================================
// ==================================================================================================
//                                 IMPOSTAZIONI IL LAYOUT CON I DATI
//==================================================================================================
    /**
     * Questo metodo imposta inserisce i dati nel layout.
     */
    //Formattazione
    private static String FORMATTAZIONE_DATE = "dd/MM/yyyy";
    private void setLayoutConDati() {

        //Imposta il sottotitolo
        Toolbar sottotitolo = viewGroupLayout.findViewById(R.id.toolbar_subtitle);
        sottotitolo.setTitle(campionato.getName());

        //Imposta inizio fine campionato
        SimpleDateFormat sdf = new SimpleDateFormat(FORMATTAZIONE_DATE);
        String inizioStr = sdf.format(campionato.getDataInizioCampionato());
        String fineStr = sdf.format(campionato.getDataFineCampionato());
        ((TextView) viewGroupLayout.findViewById(R.id.txt_DataInizioCampionato)).setText(inizioStr);
        ((TextView) viewGroupLayout.findViewById(R.id.txt_DataFineCampionato)).setText(fineStr);

        //Visualizzazione bottoni
        if(campionato.iscrizioneAperta()){
            if( (campionato.postiDisponibili()) && (!campionato.utenteIscritto(utenteAttivo))){// Iscrizioni aperte ma non iscritto
                btnViewIscriviti.setVisibility(View.VISIBLE);
            }else if(campionato.utenteIscritto(utenteAttivo)){// Iscrizioni aperte ma  iscritto
                btnViewRitirati.setVisibility(View.VISIBLE);
            }
        }else if(campionato.campionatoConcluso()){
            btnViewClassifica.setVisibility(View.VISIBLE);
        }

        //Imposta Aiuti alla guida
        MyChipAdapter adapterAiutiAllaGuida=new MyChipAdapter(getActivity(),campionato.getStatoDegliAiutiAllaGuida());
        MyGridView gridViewAiutiAllaGuida= (MyGridView) viewGroupLayout.findViewById(R.id.grd_AiutiAlleGuida);
        gridViewAiutiAllaGuida.setAdapter(adapterAiutiAllaGuida);

        //Imposta Bandiere
        MyChipAdapter adapterBandiere=new MyChipAdapter(getActivity(),campionato.getStatoDelleBandiere());
        MyGridView gridViewBandiere= (MyGridView) viewGroupLayout.findViewById(R.id.grd_Banndiere);
        gridViewBandiere.setAdapter(adapterBandiere);


        //Imposta Consumi
        TextView valoreConsumoCarburante = viewGroupLayout.findViewById(R.id.txt_ConsumoCarburante);
        TextView valoreConsumoGomma      = viewGroupLayout.findViewById(R.id.txt_ConsumoGomme);
        valoreConsumoCarburante.setText(    campionato.getConsumiCarburanteString());
        valoreConsumoGomma.setText(         campionato.getConsumiGommeString());
   }
//==================================================================================================
//##################################################################################################
}
