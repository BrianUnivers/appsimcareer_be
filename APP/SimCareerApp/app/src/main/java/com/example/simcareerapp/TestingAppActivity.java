package com.example.simcareerapp;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.simcareerapp.AppCompatActivityWithMenu.AppCompatActivityWithMyPrimaryMenu;
import com.example.simcareerapp.GestioneJson.GestioneListaMeteo;
import com.example.simcareerapp.GestioneJson.GestioneListeCampionati;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Persistence.JsonEntities.CampionatoConJson;
import com.example.simcareerapp.Persistence.JsonEntities.MeteoConJson;
import com.example.simcareerapp.TestingApp.TestAppModifcaCampionatoFragment;
import com.example.simcareerapp.TestingApp.TestAppModificaGaraFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class TestingAppActivity extends AppCompatActivityWithMyPrimaryMenu {
//##################################################################################################
//                              DATI PASSATI TRAMITE UN INTENT
//##################################################################################################
    private Utente utenteAttivo         = null;
    private Boolean mantieniAttivoUtente= null;
//==================================================================================================
//                              METODO DI RECUPERO DI TALI VALORI DAL INTET CHE HA CHAMATO LA PAGINA
//==================================================================================================
    /**
     * Questo metodo serve per prendere dall'Activity chiamante le informazioni della sessione che
     * di obbligatori sono <<utenteAttivo>> e <<mantieniAttivoUtente>>.
     */
    private void initValoriDiSessioneDellAPP(){
        Intent i =getIntent();
        utenteAttivo            = MyIntent.getUtenteExtra(i,MyIntent.I_UTENTE_ATTIVO);
        mantieniAttivoUtente    = i.getBooleanExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,false);
    }
//==================================================================================================
//==================================================================================================
//                              IMPOSTAZIONI PARAMETRI DA PASSARE PER GLI INTENT DEL MENU LATERALE PRINCIPALE
//==================================================================================================
    @Override
    protected void setParamiter(MyIntent intent) {
        intent.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utenteAttivo);
        intent.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                              DATI PER GESTIRE IL LAYOUT
//##################################################################################################
    private BottomNavigationView bottomTabarView = null;
//##################################################################################################

//##################################################################################################
//                                  CREAZIONE DEL LAYOUT
//##################################################################################################
    private List<CampionatoConJson> listaCampionati = new LinkedList<CampionatoConJson>();
    private HashMap<String,MeteoConJson> listaMeteo = new HashMap<String,MeteoConJson>();
//==================================================================================================
//                                  CREAZIONE DELL'ACTITVITY
//==================================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  GET DATI DAL INTENT
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        initValoriDiSessioneDellAPP();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        setContentView(R.layout.activity_testing_app);
        super.initToolBar(R.menu.bar_menu_modifica_annula);
        super.impostaPaginaComeHome();

        listaCampionati  = GestioneListeCampionati.letturaECreazioneListaCampionati(this,R.raw.campionati);
        listaMeteo       = GestioneListaMeteo.letturaECreazioneListaMeteo(this,R.raw.meteo);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  BOTTONI DELLA TABAR IN BASSO
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        bottomTabarView = findViewById(R.id.tbb_TabCampionato);
        bottomTabarView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener(){
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return cambiaSchedaDaVisualizare(menuItem);
            }
        } );
        bottomTabarView.setSelectedItemId(R.id.btn_tab_ModificaCampionato);//Selezione della prima vita
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  GESTIONE DEL MENU' SULLA TOOLBAR
//##################################################################################################
//==================================================================================================
//                                  GESTIONE DEI EVENTI DI CLICK SULLE VOCI DEL MENU'
//==================================================================================================
    /**
     * Metodo per distingere i tasti della toolbar e sapere si deve fare.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.btn_bar_Salva:
                salvaModifiche();
                break;
            case R.id.btn_bar_Annula:
                annullaModifiche();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
//==================================================================================================
//==================================================================================================
//                                  SALVA
//==================================================================================================
    /**
     * Metodo serve per salvare tutte le modifiche appotrate ai campionati.
     */
    private void salvaModifiche(){
        Boolean risScritturaCampionati = GestioneListeCampionati.aggiornaListaCampionati(this,R.raw.campionati,listaCampionati);
        Boolean risScritturaMETEO      = GestioneListaMeteo.aggiornaListaMeteo(this,R.raw.meteo,listaMeteo);

        if(risScritturaCampionati&&risScritturaMETEO){
            Toast.makeText(this, "Salvataggio completo.", Toast.LENGTH_SHORT).show();
        }else if(risScritturaCampionati){
            Toast.makeText(this, "Salvato solo le modifiche ai campionati.", Toast.LENGTH_SHORT).show();
        }else if(risScritturaMETEO){
            Toast.makeText(this, "Salvato solo le modifiche del meteo.", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Non è stato possibile salvare.", Toast.LENGTH_SHORT).show();
        }
    }
//==================================================================================================
//==================================================================================================
//                                  ANNULA
//==================================================================================================
    /**
     * Metodo serve per salvare tutte le modifiche appotrate ai campionati.
     */
    private void annullaModifiche(){
        listaCampionati = GestioneListeCampionati.letturaECreazioneListaCampionati(this,R.raw.campionati);
        listaMeteo       = GestioneListaMeteo.letturaECreazioneListaMeteo(this,R.raw.meteo);
        Toast.makeText(this, "Annulato le modifiche", Toast.LENGTH_SHORT).show();
        bottomTabarView.setSelectedItemId(R.id.btn_tab_ModificaCampionato);//Selezione della prima vita
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  OPERAZIONI FATTE QUANDO SI PREME SU UNA TAB
//##################################################################################################
//==================================================================================================
//                                  CAMBIA LA SCHEDA DA VISUALIZATA DELE CAMPIONATO
//==================================================================================================
    /**
     * Questo metodo vinene chiamato tramite il lissener,della tabbar quando viene premuto un dei quatro
     * tab viene cambiato il contenuto del FrameLayout
     */
    private boolean  cambiaSchedaDaVisualizare(@NonNull MenuItem menuItem){
        Fragment frag = null;

        boolean ris = true;
        switch (menuItem.getItemId()){
            case R.id.btn_tab_ModificaCampionato:
                frag = new TestAppModifcaCampionatoFragment(getListaCampionatiNonTerminati(listaCampionati));
                break;
            case R.id.btn_tab_ModificaGara:
                frag = new TestAppModificaGaraFragment(getListaCampionatiNonTerminati(listaCampionati),listaMeteo);
                break;
            default:
                ris=false;
        }
        if( (ris) && (frag!=null) ){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fly_ModificaCampionatoEGare, frag);
            ft.commit();
        }
        return ris;
    }
//==================================================================================================
//##################################################################################################


//##################################################################################################
//                                  FILTRA CAMPIONATI DA MODIFICHARE
//##################################################################################################
//==================================================================================================
//                                  LISTA CAMPIONATI NON TEMINATI
//==================================================================================================
    /**
     * Questo metodo restiuisce una lista di tutti i campionati non teminati cioè in corso e futuri
     * ai quali è possibile modificare i valori.
     */
    private List<Campionato> getListaCampionatiNonTerminati(List<CampionatoConJson> lista){
        List<Campionato> ris=new LinkedList<>();
        Iterator<CampionatoConJson> iterator = lista.iterator();
        while (iterator.hasNext()){
            Campionato campionato = iterator.next();
            if(!campionato.campionatoConcluso()){
                ris.add(campionato);
            }
        }
        return ris;
    }
//==================================================================================================
//##################################################################################################
}