package com.example.simcareerapp.GestoriDeiLayoutImportati.MyGPS;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.simcareerapp.R;

public class GPSTracker implements LocationListener {
    private int REQUEST_CODE =123;

    private Context context;
    public GPSTracker(Context context) {
        this.context=context;
    }

    /**
     * Effentua il controllo dei permessi per l'uso della geolocalizazione e restituisce le coordinate
     * attuali, se è possibile accedervi altrimenti null.
     * @return Ritorna un oggeto contenetre le coordinate della posizione attuale, oppure null se non
     * si ha i permessi o il GPS è spento.
     */
    public Location getPosizizione() {
        return chiediTuttiPermessi();
    }
//==================================================================================================
//                                  PRENDI POSIZIONE
//==================================================================================================
    /**
     * Crea e avvia un servizio che aggiorna ogni 5 minuti la posizione attuale del telefono, sottoforma
     * di coordinate GPS.
     * Questo metodo in oltre ritorna una volta avviato il servizio ( o usato se già attivo) le
     * coordina GPS oppure null, se si è verificato un errore nei permessi.
     */
    private Location prendiPosizizioneAttuale() {
        Location ris = null;
        //Permessi
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            //Ativazione del servizio che aggiona la posizione ogni 5 minuti con una distaza minima di 10m
            LocationManager lm = (LocationManager) context.getSystemService(context.LOCATION_SERVICE);
            boolean statoAbilitatoDelGPS = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (statoAbilitatoDelGPS) {
                //GPS Abilitato
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, this);
                //Prendo la posizione
                do {
                    ris = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }while(ris==null);
                Log.d("MyTAG","GPS trovato");
            } else {
                //GPS Spento
                Toast.makeText(context, "Abilitare il GPS", Toast.LENGTH_LONG).show();
            }
        }else{
            //Non ho il permesso per accedere al GPS.
            Toast.makeText(context, "Permetti l'utilizzo del GPS", Toast.LENGTH_SHORT).show();

        }
        return ris;
    }
//==================================================================================================



//==================================================================================================
//                                  RICHIEDI TUTTI I PERMESSI CHE POSSONO SERVIRE PER GPS
//==================================================================================================
    /**
     * Questo metodo richede i permessi di accedere al GPS cosi da poter accedere alle coodinate, e
     * se tale permesso è accosentito restituisce le coordinate, oppure null se c'è stato un errore.
     */
    private Location chiediTuttiPermessi(){
        Location ris = null;
        if( ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){

            //Quanfo i permessi non sono garantiti
            if( (ActivityCompat.shouldShowRequestPermissionRationale( (Activity)context,
                    Manifest.permission.ACCESS_FINE_LOCATION)) ){

                // Creazione del AlertDialog per richedere i permessi
                AlertDialog.Builder builder = new AlertDialog.Builder( context, R.style.AlertDialog );
                builder.setTitle(R.string.dlg_ttl_PermessiCambioImmagione);
                builder.setMessage(R.string.dlg_txt_PermessiGPS);
                builder.setPositiveButton(R.string.dlg_btn_Acceto, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(
                                (Activity)context ,new String[] {
                                        Manifest.permission.ACCESS_FINE_LOCATION
                                },REQUEST_CODE);
                    }
                });
                builder.setNegativeButton(R.string.dlg_btn_Nega,null);
                AlertDialog alertDialog = builder.create();

                alertDialog.show();

            }else{
                ActivityCompat.requestPermissions(
                        (Activity)context,new String[] {
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },REQUEST_CODE);
            }
        }else{
            //Quando i permessi sono ativi
            ris= prendiPosizizioneAttuale();
        }
        return ris;
    }
//==================================================================================================
    @Override
    public void onLocationChanged(Location location) {
    }
//==================================================================================================
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }
//==================================================================================================
    @Override
    public void onProviderEnabled(String s) {
    }
//==================================================================================================
    @Override
    public void onProviderDisabled(String s) {
    }
//==================================================================================================
}
