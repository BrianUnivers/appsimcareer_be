package com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.simcareerapp.GestioneJson.GestioneListaMeteo;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface.OnClickClassificaDellaGaraListener;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Persistence.JsonEntities.MeteoConJson;
import com.example.simcareerapp.R;

import java.util.HashMap;
import java.util.List;

public class GareDeiCampionatiRVAdapter extends RecyclerView.Adapter<GareDeiCampionatiRVAdapter.MyViewHoder>  {
//##################################################################################################
//                              CLASSE INTERNA
//##################################################################################################
    /**
     * Questa classe è quella che permette alla RecyclingView Adapter di definire un layout custom
     * del singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public View         v_mainLayout;
        //Informazioni generali
        public Toolbar      v_titoloView;
        public RecyclerView v_listaGareView;
//==================================================================================================
//                              COSTRUTTORI
//==================================================================================================
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout    = itemView.findViewById(R.id.cly_RowGarePerCampionato);
            v_titoloView    = itemView.findViewById(R.id.toolbar_subtitle);
            v_listaGareView = itemView.findViewById(R.id.rcl_Gare);
        }
//==================================================================================================
    }
//##################################################################################################



//##################################################################################################
//                              CLASSE PRINCIPALE GareDeiCampionatiRVAdapter
//##################################################################################################
    private Context context=null;
    private Activity activity=null;
    private List<Campionato> listaCampionati=null;
    private Utente utenteAttivo=null;
    private HashMap<String, MeteoConJson> previsioniMeteo=null;
    private OnClickClassificaDellaGaraListener actionOnClick = null;
//==================================================================================================
//                              COSTRUTTORI
//==================================================================================================
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public GareDeiCampionatiRVAdapter(Context context, Activity activity,OnClickClassificaDellaGaraListener actionOnClick, List<Campionato> listaCampionati, Utente utenteAttivo) {
        this.context = context;
        this.activity = activity;
        this.listaCampionati = listaCampionati;
        this.utenteAttivo = utenteAttivo;
        this.actionOnClick=actionOnClick;
        this.previsioniMeteo = GestioneListaMeteo.letturaECreazioneListaMeteo(context,R.raw.meteo);
    }
//==================================================================================================

//==================================================================================================
//                              CERAZIONE DEL SINGOLO ELEMETOTO
//==================================================================================================
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public GareDeiCampionatiRVAdapter.MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View bloccoElemeto = inflater.inflate(R.layout.row_list_gare_campionato, parent,false);
        return new GareDeiCampionatiRVAdapter.MyViewHoder(bloccoElemeto);
    }
//==================================================================================================

//==================================================================================================
//                              INSERIRE L'ELEMETO NEL LAYOUT
//==================================================================================================
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore GareDeiCampionatiRVAdapter.
     */
    @Override
    public void onBindViewHolder(@NonNull GareDeiCampionatiRVAdapter.MyViewHoder holder, int position) {
        final Campionato campionato =listaCampionati.get(position);
        //Inserisci i dati nel layout
        holder.v_titoloView.setTitle(campionato.getName());
        //LIsta di gare
        SoloGareRVAdapter gareRVAdapter = new SoloGareRVAdapter(context,activity,actionOnClick,campionato ,utenteAttivo,previsioniMeteo);;
        if(gareRVAdapter!=null){
            LinearLayoutManager linearLayoutManager=null;
            if(listaCampionati.size()!=1){
                linearLayoutManager = new LinearLayoutManager(
                    holder.v_listaGareView.getContext(),
                    LinearLayoutManager.HORIZONTAL,
                    false
                );
            }else{
                 linearLayoutManager = new LinearLayoutManager(
                        holder.v_listaGareView.getContext(),
                        LinearLayoutManager.VERTICAL,
                        false
                );
            }
            linearLayoutManager.setInitialPrefetchItemCount(campionato.getListaGara().size());

            holder.v_listaGareView.setLayoutManager(linearLayoutManager);
            holder.v_listaGareView.setAdapter(gareRVAdapter);
        }
    }
//==================================================================================================
//                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE
//==================================================================================================
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaCampionati.size();
    }
//==================================================================================================
//##################################################################################################
}
