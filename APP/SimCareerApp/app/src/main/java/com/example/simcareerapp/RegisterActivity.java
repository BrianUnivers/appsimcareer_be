package com.example.simcareerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.sql.Date;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import com.example.simcareerapp.DataBase.ThreadAction.ThreadRegistraUtenteParziale;
import com.example.simcareerapp.DataBase.ThreadAction.ThreadVerificaEmail;
import com.example.simcareerapp.DataBase.ThreadAction.ThreadVerificaUsername;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Register.Interface.*;
import com.example.simcareerapp.Register.*;

public class RegisterActivity extends AppCompatActivity implements  OnFragmentTerminiECondizioniEventListener ,
                                                                    OnFragmentRegisterEventListener  {
//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Set il primo fragment
        Fragment frag = new TerminiECondizioniFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fly_Page, frag);
        ft.commit();
    }
//==================================================================================================


//==================================================================================================
//                                  IMPLEMETAZIONI DEL FRAGMENT OnFragmentLoginEventListener
//==================================================================================================
    public void anullaIscrizione(){
        Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(i);
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Sovrascrive il tasto indietro fisico
     */
    @Override
    public void onBackPressed() {
        anullaIscrizione();
    }
//--------------------------------------------------------------------------------------------------
    public void accettaTerminiECondizioni(){
        //Cambio titolo
        TextView txtTile = (TextView) findViewById(R.id.txt_Title);
        txtTile.setText(R.string.ttl_CreaAccount);

        //Cambio fragment
        FrameLayout fra = (FrameLayout) findViewById(R.id.fly_Page);
        //txtTile.setText(R.string.ttl_CreaAccount);

        Fragment frag = new RegisterFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fly_Page, frag);
        ft.commit();

    }
//==================================================================================================
//                                  IMPLEMETAZIONI DEL FRAGMENT OnFragmentRegisterEventListener
//==================================================================================================
    /**
     * Effetua la registrazione dei dati parziali e se tali dati sono correti e vengono inseriti nel
     * database correttamente allora si passa alla pagina modifica profilo utente dove si inseriranno
     * gli ultimi dati. Se però non verra inserito non verrà eseguito nulla e si rimarrà nella pagina
     * attuale.
     */
    public void effetuaRegistrazione(String nome, String cognome, Calendar dataNascita, String email, String username, String password){
        Utente utente = new Utente();
        utente.setNome(nome);
        utente.setCognome(cognome);
        utente.setDataNascita(new Date(dataNascita.getTimeInMillis()));
        utente.setEmail(email);
        utente.setUsername(username);
        utente.setPassword(password);

        ThreadRegistraUtenteParziale threadRegistraUtenteParziale = new ThreadRegistraUtenteParziale(this);
        threadRegistraUtenteParziale.execute(utente);
        try {
            utente =threadRegistraUtenteParziale.get();
        } catch (ExecutionException e) {
            utente =null;
            e.printStackTrace();
        } catch (InterruptedException e) {
            utente =null;
            e.printStackTrace();
        }


        if(utente!=null){
            MyIntent i = new MyIntent(RegisterActivity.this, ModificaProfiloUtenteActivity.class);
            i.putExtraUtente(MyIntent.I_UTENTE_ATTIVO,utente);
            i.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,false);
            startActivity(i);
        }
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Verifica se è già presente nel database un utente con questo usename.
     * Ritorna vero se l'username non è mai stato usato, falso altrimenti.
     */
    public boolean usernameValido(String username){
        ThreadVerificaUsername threadVerificaUsername = new ThreadVerificaUsername(this);
        threadVerificaUsername.execute(username);
        Boolean ris =false;
        try {
            ris =threadVerificaUsername.get();
        } catch (ExecutionException e) {
            ris =false;
            e.printStackTrace();
        } catch (InterruptedException e) {
            ris =false;
            e.printStackTrace();
        }
        return ris;
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Verifica se è già presente nel database un utente con questo email.
     * Ritorna vero se l'email non è mai stato usato, falso altrimenti.
     */
    public boolean emailValido(String email){
        ThreadVerificaEmail threadVerificaEmail = new ThreadVerificaEmail(this);
        threadVerificaEmail.execute(email);
        Boolean ris =false;
        try {
            ris =threadVerificaEmail.get();
        } catch (ExecutionException e) {
            ris =false;
            e.printStackTrace();
        } catch (InterruptedException e) {
            ris =false;
            e.printStackTrace();
        }
        return ris;
    }
//==================================================================================================
}