package com.example.simcareerapp.ModificaProfiloUtente.Interface;


//##################################################################################################
//                                  INTERFACCIA PER COMUNICARE
//##################################################################################################

import java.io.File;

/**
 * Interfacca per interagire con questo fragment che deve essere implementato dall'Activity con
 * cui si vule collabolrare.
 */
public interface OnFragmentImmagineProfiloUtenteEventListener {
    File inizializazioneImmagineTramiteId();
    boolean cambioImmagine(File immagine);
}
//##################################################################################################
//##################################################################################################