package com.example.simcareerapp.GestioneRicordaSessione;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.Pair;

import com.example.simcareerapp.CampionatiActivity;
import com.example.simcareerapp.DataBase.ThreadAction.ThreadLogin;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.ModificaProfiloUtenteActivity;
import com.example.simcareerapp.Persistence.Entities.Utente;

import java.util.concurrent.ExecutionException;

public class GestioneSessione  {
    private static final String SESSIONE_UTENTE = "SESSIONE_UTENTE";
    private static final String USERNAME  = "USERNAME" ;
    private static final String PASSWORD  = "PASSWORD" ;
    private static final String RICORDAMI = "RICORDAMI";
//##################################################################################################
//                              GEESTIONE SESSIONE
//##################################################################################################
    private static SharedPreferences sharedPreferences=null;
//==================================================================================================
//                              CREAZIONE SESSIONE UTENTE REGISTRATO
//==================================================================================================
    public static void creaSessione(Context context, String username, String password, Boolean remember){
        sharedPreferences = context.getSharedPreferences(SESSIONE_UTENTE,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (remember){
            editor.putString(USERNAME,  username);
            editor.putString(PASSWORD,  password);
            editor.putBoolean(RICORDAMI,remember);
        }else{
            cancellaSessioneAperte(editor);
        }
        editor.apply();
    }
//==================================================================================================
//==================================================================================================
//                              CANCELLA SESSIONE
//==================================================================================================
    public static void cancellaSessioneAperte(Context context){
        sharedPreferences = context.getSharedPreferences(SESSIONE_UTENTE,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        cancellaSessioneAperte(editor);
        editor.apply();
    }
//--------------------------------------------------------------------------------------------------
    private static void cancellaSessioneAperte(SharedPreferences.Editor editor){
        editor.clear();
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                              ESISSTE SESSIONE
//##################################################################################################
//==================================================================================================
//                              ESISTE SESSIONE ATTIVA
//==================================================================================================
    public static boolean esisteSessioneApera(Context context){
        sharedPreferences = context.getSharedPreferences(SESSIONE_UTENTE,Context.MODE_PRIVATE);
        String  username = sharedPreferences.getString(USERNAME,null);
        String  password = sharedPreferences.getString(PASSWORD,null);
        return  ((password!=null) && (password!=null));
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                              GET INFORMATION SESION
//##################################################################################################
//==================================================================================================
//                              GET USERNAME
//==================================================================================================
    public static  String getUsername(Context context){
        sharedPreferences = context.getSharedPreferences(SESSIONE_UTENTE,Context.MODE_PRIVATE);
        return sharedPreferences.getString(USERNAME,null);
    }
//==================================================================================================
//==================================================================================================
//                              GET PASSWORD
//==================================================================================================
    public static  String getPassword(Context context){
        sharedPreferences = context.getSharedPreferences(SESSIONE_UTENTE,Context.MODE_PRIVATE);
        return sharedPreferences.getString(PASSWORD,null);
    }
//==================================================================================================
//==================================================================================================
//                              GET RICORDAMI
//==================================================================================================
    public static  boolean getRicordami(Context context){
        sharedPreferences = context.getSharedPreferences(SESSIONE_UTENTE,Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(RICORDAMI,false);
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                              LOGIN THROUGH SESSION IF EXIST
//##################################################################################################
//==================================================================================================
//                              GET RICORDAMI
//==================================================================================================
    public static void loginTramiteSessioneSePresente(Context context){
        if(esisteSessioneApera(context)){
            String  username   = GestioneSessione.getUsername(context);
            String  password  = GestioneSessione.getPassword(context);
            Boolean ricordami = GestioneSessione.getRicordami(context);

            Log.d("MyTag","Inizio controllo credenziali utente.");
            ThreadLogin threadLogin = new ThreadLogin(context);
            threadLogin.execute(new Pair<String, String>(username,password));
            Utente utente=null;
            try {
                utente =threadLogin.get();
            } catch (ExecutionException e) {
                utente=null;
                e.printStackTrace();
            } catch (InterruptedException e) {
                utente=null;
                e.printStackTrace();
            }
            if(utente != null){
                Log.d("MyTag","Utente valido");
                if(utente.getResidenza()!=null){
                    //Utente che ha concluso la registazione
                    apriPaginaCampionati(context,utente,ricordami);
                }else{
                    //Utente che NON ha concluso la registazione
                    apriPaginaConcludiRegistrazione(context,utente,ricordami);
                }
            }
        }
    }
//==================================================================================================
    /**
     * Il metodo che viene chamato aprire la pagina dei campinati passando come valori l'utente registrato
     * e il valore che serve per sapre se devo ricordarmi del utente.
     */
    private static void apriPaginaCampionati(Context context,Utente utente,Boolean ricordami){
        MyIntent i = new MyIntent(context, CampionatiActivity.class);
        i.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utente);
        i.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,ricordami);
        context.startActivity(i);
    }
//==================================================================================================
//==================================================================================================
    /**
     * Il metodo che viene chamato aprire la pagina dei per concludere la registrazione che è rismata
     * interrotta passando come valori l'utente registrato e il valore che serve per sapre se devo
     * ricordarmi del utente.
     */
    private static void apriPaginaConcludiRegistrazione(Context context,Utente utente,Boolean ricordami){
        MyIntent i = new MyIntent(context, ModificaProfiloUtenteActivity.class);
        i.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utente);
        i.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,ricordami);
        context.startActivity(i);
    }
//==================================================================================================
//##################################################################################################
}
