package com.example.simcareerapp.SingoloCampionato.Interface;
//##################################################################################################
//                                  INTERFACCIA PER COMUNICARE
//##################################################################################################
public interface OnFragmentInfoCampionatoFragmentEventListener {
    void creaNuovaIscrizioneAlCampionato();
    void cancellaIscrizioneAlCampionato();
    void classificaCampionato();
}
//##################################################################################################
//##################################################################################################