package com.example.simcareerapp.Persistence.Entities;

public class Indirizzo {
//==================================================================================================
//                                  ATTRIBUTI
//==================================================================================================
    private String stato;
    private String regione;
    private String citta;
    private String viaPiazza;
//==================================================================================================

//==================================================================================================
//                                  METODI COSTRUTTORI
//==================================================================================================
    public Indirizzo(String stato, String regione, String citta, String viaPiazza) {
        this.stato = stato;
        this.regione = regione;
        this.citta = citta;
        this.viaPiazza = viaPiazza;
    }
//==================================================================================================

//==================================================================================================
//                                  METODI GETTER E SETTER
//==================================================================================================
    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }
//------------------------------------------------
    public String getRegione() {
        return regione;
    }

    public void setRegione(String regione) {
        this.regione = regione;
    }
//------------------------------------------------
    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }
//------------------------------------------------
    public String getViaPiazza() {
        return viaPiazza;
    }

    public void setViaPiazza(String viaPiazza) {
        this.viaPiazza = viaPiazza;
    }
//==================================================================================================

//==================================================================================================
//                                  METODI STRING INDIRIZZI
//==================================================================================================
    public String getIndirizzoCompleto(){
        String ris = "";
        if(!stato.equals("") ){
            ris +=stato;
        }
        if(!regione.equals("") ){
            if(!ris.equals("")){
                ris+=", ";
            }
            ris +=regione;
        }
        if(!citta.equals("") ){
            if(!ris.equals("")){
                ris+=", \n";
            }
            ris +=citta;
        }
        if(!viaPiazza.equals("") ){
            if(!ris.equals("")){
                ris+=", ";
            }
            ris +=viaPiazza;
        }
        return ris;
    }
//==================================================================================================
}
