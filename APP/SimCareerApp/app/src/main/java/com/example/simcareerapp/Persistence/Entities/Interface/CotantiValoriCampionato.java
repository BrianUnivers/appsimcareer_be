package com.example.simcareerapp.Persistence.Entities.Interface;

import com.example.simcareerapp.R;



public interface CotantiValoriCampionato {
//##################################################################################################
//                                  BANDIERE
//##################################################################################################
//==================================================================================================
//                                  VALORI BANDIERE
//==================================================================================================
    public final String BANDIERA_NERA                 = "Nera";
    public final String BANDIERA_BLU                  = "Blu";
    public final String BANDIERA_GIALLA               = "Gialla";
    public final String BANDIERA_ROSSA                = "Rossa";
    public final String BANDIERA_VERDE                = "Verde";
    public final String BANDIERA_BIANCA_NERA          = "Bianca e Nera";
    public final String BANDIERA_BIANCA_DISCO_ARANCIO = "Bianca disco arancio";
    public final String BANDIERA_STRISCE_GIALLE_ROSE  = "Strisce gialle e rosse";
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                                  AIUTI
//##################################################################################################
//==================================================================================================
//                                  VALORI AIUTI
//==================================================================================================
    public final String AIUTO_ABS                  = "ABS";
    public final String AIUTO_AUTORETROMARCIA      = "Autoretromarcia";
    public final String AIUTO_CONTROLLO_TRAZIONE   = "Controllo trazione";
    public final String AIUTO_CORSIA_BOX           = "Aiuto corsia box";
    public final String AIUTO_BLOCCAGGIO_OPPOSTO   = "Bloccaggio opposto";
    public final String AIUTO_FRIZIONE_AUTOMATICA  = "Frizione automatica";
    public final String AIUTO_FRENATA              = "Aiuto fermata";
    public final String AIUTO_CAMBIO_AUTOMATICO    = "Cambio automatico";
    public final String AIUTO_INVULNERABILITA      = "Invulnerabilità";
    public final String AIUTO_STERZATA             = "Aiuto sterzata";
    public final String AIUTO_CONTROLLO_STABILITA  = "Controllo stabilità";
    public final String AIUTO_RIPRESA_SPIN         = "Ripresa spin";
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                                  CONSUMI
//##################################################################################################
//==================================================================================================
//                                  VALORI CONSUMI
//==================================================================================================
    public final String CONSUMI_BASSI   = "Bassi";
    public final String CONSUMI_NORMALI = "Normale";
    public final String CONSUMI_ELEVATI = "Elevati";
//==================================================================================================
//##################################################################################################
}
