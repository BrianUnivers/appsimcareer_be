package com.example.simcareerapp.GestioneMyIntentEPersistableBundle;



import android.app.job.JobParameters;
import android.content.Intent;
import android.os.PersistableBundle;

import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.Interface.CostantiInserireEscrarreDatiDaiInternt;
import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.CircuitoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Foto;
import com.example.simcareerapp.Persistence.Entities.Indirizzo;
import com.example.simcareerapp.Persistence.Entities.Utente;

import java.sql.Date;

public class MyPersistableBundle implements CostantiInserireEscrarreDatiDaiInternt {
    private static final int TRUE  = 1;
    private static final int FALSE = 0;


    private PersistableBundle persistableBundle = null;
//==================================================================================================
//                                  COSTRUTTORI
//==================================================================================================
    public MyPersistableBundle(PersistableBundle persistableBundle) {
        if(null!=persistableBundle){
            this.persistableBundle = persistableBundle;
        }else{
            this.persistableBundle = new PersistableBundle();
        }
    }
//--------------------------------------------------------------------------------------------------
    public MyPersistableBundle() {
        persistableBundle = new PersistableBundle();
    }
//==================================================================================================
//==================================================================================================
//                                  GET PersistableBundle
//==================================================================================================
    public PersistableBundle getPersistableBundle() {
        return persistableBundle;
    }
//==================================================================================================
//==================================================================================================
//                                  GET E SET DEI PARAMTRI COPLESSI DA UN INTENT
//==================================================================================================
//--------------------------------------------------------------------------------------------------
//                                  GET E SET UTENTE
//--------------------------------------------------------------------------------------------------
    /**
     * Questo metodo scompone l'oggeto utente e lo inserisce sottoforama di oggeti di base nei parametri
     * passati con gli PersistableBundle.
     * Si noti che c'è in conda ai identificativi delle vari identificativi una variabile cosi da
     * permettere eventualmete di inserire più oggeti di tipo utente.
     */
    public PersistableBundle putUtente(String nome, Utente utente){
        if(utente!=null){
            this.persistableBundle.putLong(I_UTENTE_ID                       +"_"+nome,  utente.getId());
            this.persistableBundle.putString(I_UTENTE_NOME                   +"_"+nome,  utente.getNome());
            this.persistableBundle.putString(I_UTENTE_COGNOME                +"_"+nome,  utente.getCognome());
            this.persistableBundle.putLong(I_UTENTE_DATA_NASCITA             +"_"+nome,  utente.getDataNascita().getTime());
            this.persistableBundle.putString(I_UTENTE_EMAIL                  +"_"+nome,  utente.getEmail());
            this.persistableBundle.putString(I_UTENTE_USERNAME               +"_"+nome,  utente.getUsername());
            this.persistableBundle.putString(I_UTENTE_SALT                   +"_"+nome,  utente.getSalt());
            this.persistableBundle.putString(I_UTENTE_PASSWORD               +"_"+nome,  utente.getPassword());
//--------------------------------
            Indirizzo indirizzo = utente.getResidenza();
            if(indirizzo!=null){
                this.persistableBundle.putString(I_UTENTE_RESIDENZA_STATO          +"_"+nome,  indirizzo.getStato());
                this.persistableBundle.putString(I_UTENTE_RESIDENZA_REGIONE        +"_"+nome,  indirizzo.getRegione());
                this.persistableBundle.putString(I_UTENTE_RESIDENZA_CITTA          +"_"+nome,  indirizzo.getCitta());
                this.persistableBundle.putString(I_UTENTE_RESIDENZA_VIA_PIAZZA     +"_"+nome,  indirizzo.getViaPiazza());
            }
//--------------------------------
            Integer numeroDiGara = utente.getNumeroDiGara();
            if(numeroDiGara!=null){
                this.persistableBundle.putInt(I_UTENTE_NUMERO_GARA              +"_"+nome, numeroDiGara );
            }
//--------------------------------
            AutoConImmagini autoPreferita = utente.getAutoPreferita();
            if(autoPreferita!=null){
                this.persistableBundle.putString(I_UTENTE_AUTO_PREF_NOME           +"_"+nome,  autoPreferita.getNome());
                this.persistableBundle.putInt(I_UTENTE_AUTO_PREF_ID_IMG            +"_"+nome,  autoPreferita.getIdImmagine());
            }
//--------------------------------
            CircuitoConImmagini circuitoPreferita = utente.getCircuitoPreferito();
            if(circuitoPreferita!=null) {
                this.persistableBundle.putString(I_UTENTE_CIRCUITO_PREF_NOME + "_" + nome, circuitoPreferita.getNome());
                this.persistableBundle.putInt(I_UTENTE_CIRCUITO_PREF_ID_IMG  + "_" + nome, circuitoPreferita.getIdImmagine());
            }
//--------------------------------
            CircuitoConImmagini circuitoOdiato = utente.getCircuitoOdiato();
            if(circuitoOdiato!=null) {
                this.persistableBundle.putString(I_UTENTE_CIRCUITO_ODIATO_NOME     +"_"+nome, circuitoOdiato.getNome());
                this.persistableBundle.putInt(I_UTENTE_CIRCUITO_ODIATO_ID_IMG      +"_"+nome, circuitoOdiato.getIdImmagine());
            }
        }
        return this.persistableBundle;
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Questo metodo ricompone l'oggeto utente e estraendo sottoforama di oggeti di base nei parametri
     * contenuti con gli JobParameters.
     * Si noti che c'è in conda ai identificativi delle vari identificativi una variabile cosi da
     * permettere eventualmete di riconoscere e distingere un oggeto di tipo utente da altri.
     */
    public Utente getUtente(String nome){
        Utente utente =null;
        if(this.persistableBundle.getString(I_UTENTE_NOME           +"_"+nome )!=null) {
            utente = new Utente();
//--------------------------------
            utente.setId(this.persistableBundle.getLong(                   I_UTENTE_ID + "_" + nome, -1));
            utente.setNome(this.persistableBundle.getString(               I_UTENTE_NOME + "_" + nome));
            utente.setCognome(this.persistableBundle.getString(            I_UTENTE_COGNOME + "_" + nome));
            utente.setDataNascita(new Date(this.persistableBundle.getLong( I_UTENTE_DATA_NASCITA + "_" + nome, 0)));
            utente.setUsername(this.persistableBundle.getString(           I_UTENTE_USERNAME + "_" + nome));
            utente.setEmail(this.persistableBundle.getString(              I_UTENTE_EMAIL + "_" + nome));
            utente.setSalt(this.persistableBundle.getString(               I_UTENTE_SALT + "_" + nome));
            utente.setPasswordHashed(this.persistableBundle.getString(     I_UTENTE_PASSWORD + "_" + nome));
//--------------------------------
            Indirizzo indirizzo = null;
            if(this.persistableBundle.getString(I_UTENTE_RESIDENZA_STATO + "_" + nome)!=null){
                indirizzo = new Indirizzo(
                        this.persistableBundle.getString(I_UTENTE_RESIDENZA_STATO + "_" + nome),
                        this.persistableBundle.getString(I_UTENTE_RESIDENZA_REGIONE + "_" + nome),
                        this.persistableBundle.getString(I_UTENTE_RESIDENZA_CITTA + "_" + nome),
                        this.persistableBundle.getString(I_UTENTE_RESIDENZA_VIA_PIAZZA + "_" + nome)
                );
            }
            utente.setResidenza(indirizzo);
//--------------------------------
            Integer numGara = this.persistableBundle.getInt(I_UTENTE_NUMERO_GARA + "_" + nome, -1);
            if(numGara<0){
                numGara= null;
            }
            utente.setNumeroDiGara(numGara);
//--------------------------------
            AutoConImmagini autoPreferita = null;
            if(this.persistableBundle.getString(I_UTENTE_AUTO_PREF_NOME + "_" + nome)!=null){
                autoPreferita = new AutoConImmagini(
                        this.persistableBundle.getString(I_UTENTE_AUTO_PREF_NOME + "_" + nome),
                        this.persistableBundle.getInt(I_UTENTE_AUTO_PREF_ID_IMG + "_" + nome, -1)
                );
            }
            utente.setAutoPreferita(autoPreferita);
//--------------------------------
            CircuitoConImmagini circuitoPreferita = null;
            if(this.persistableBundle.getString(I_UTENTE_CIRCUITO_PREF_NOME + "_" + nome)!=null){
                circuitoPreferita = new CircuitoConImmagini(
                        this.persistableBundle.getString(I_UTENTE_CIRCUITO_PREF_NOME + "_" + nome),
                        this.persistableBundle.getInt(I_UTENTE_CIRCUITO_PREF_ID_IMG + "_" + nome, -1)
                );
            }
            utente.setCircuitoPreferito(circuitoPreferita);
//--------------------------------
            CircuitoConImmagini circuitoOdiato = null;
            if(this.persistableBundle.getString(I_UTENTE_CIRCUITO_ODIATO_NOME + "_" + nome)!=null){
                circuitoOdiato = new CircuitoConImmagini(
                        this.persistableBundle.getString(I_UTENTE_CIRCUITO_ODIATO_NOME + "_" + nome),
                        this.persistableBundle.getInt(I_UTENTE_CIRCUITO_ODIATO_ID_IMG + "_" + nome, -1)
                );
            }
            utente.setCircuitoOdiato(circuitoOdiato);
        }
        return utente;
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//                                  GET E SET BOOLEANO
//--------------------------------------------------------------------------------------------------
    /**
     * Questo metodo scompone permette di inviare come valore un booleano.
     **/
    public PersistableBundle putBoolean(String nome, Boolean boleano){
        if((boleano==null)|| (boleano==false)){
            this.persistableBundle.putInt(nome,FALSE);
        }else{
            this.persistableBundle.putInt(nome,TRUE);
        }
        return this.persistableBundle;
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Questo metodo scompone permette di inviare come valore un booleano.
     **/
    public Boolean getBoolean(String nome){
        int stao = this.persistableBundle.getInt(nome);
        if(stao==TRUE){
            return true;
        }else{
            return false;
        }
    }
//--------------------------------------------------------------------------------------------------
//==================================================================================================

}
