package com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import com.example.simcareerapp.ModificaProfiloUtenteActivity;
import com.example.simcareerapp.ProfiloUtenteActivity;
import com.example.simcareerapp.R;

public class MyGenericAlertDialog extends AlertDialog {

    protected Button btnViewPrimario=null;// Acceta
    protected Button btnViewSecondario=null;// Annula
    protected View view=null;//Layout di tutto il AlertDialog
    protected Context context;
//==================================================================================================
//                                  CREAZIONE DEL ALERTDIALOG
//==================================================================================================
    /**
     * Metodo che crea un alertdialog con la struttura di base e le funzionalità di base.
     */
    public MyGenericAlertDialog(Context context, int idAlertDialog, int idIcon, int idTitle, int idBtnPrimario, int idBtnSecondario) {
        super(context);
        this.context = context;
        //Imposta il layout
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(idAlertDialog,null);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  IMPOSTAZIONE STRINGE E ICONE
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Imposta la barra del titolo
        Toolbar toolbarAlertDialog=(Toolbar) view.findViewById(R.id.toolbar);
        toolbarAlertDialog.setLogo(idIcon);
        toolbarAlertDialog.setTitle(idTitle);

        //Imposta i bottoni Primario
        btnViewPrimario= (Button)view.findViewById(R.id.btn_PrimarioAlertDialog);
        btnViewPrimario.setText(idBtnPrimario);

        //Imposta i bottoni Secondario
        btnViewSecondario= (Button)view.findViewById(R.id.btn_SecondarioAlertDialog);
        btnViewSecondario.setText(idBtnSecondario);

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  LISTENER DEI BOTTONI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Listener del pulsante primario
        if(btnViewPrimario!=null){
            btnViewPrimario.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    listenerButtonPrimario(v);
                }
            });
        }
//--------------------------------------------------------------------------------------------------
        // Listener del pulsante primario
        if(btnViewSecondario!=null){
            btnViewSecondario.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    listenerButtonSecondario(v);
                }
            });
        }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        this.setView(view);
    }
//==================================================================================================


//==================================================================================================
//                                  METODI CHIAMATI ALLA PRESIONE DEI BOTTONI
//==================================================================================================
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante primario del AlertDialoge
     * chiude il AlertDialog
     */
    protected void listenerButtonPrimario(View v){
        Log.d("MyTAG ", "Ho premuto il tasto dell'AlertDialog "+((Button) v).getText()+" e chiudo il AlertDialog." );
        this.cancel();
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante secondario del AlertDialog e
     * chiude il AlertDialog
     */
    protected void listenerButtonSecondario(View v){
        Log.d("MyTAG ", "Ho premuto il tasto dell'AlertDialog "+((Button) v).getText()+" e chiudo il AlertDialog." );
        this.cancel();
    }
//==================================================================================================
}
