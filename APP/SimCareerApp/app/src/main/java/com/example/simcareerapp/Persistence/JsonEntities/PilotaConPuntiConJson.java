package com.example.simcareerapp.Persistence.JsonEntities;

import android.util.Log;

import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Pilota;
import com.example.simcareerapp.Persistence.Entities.PilotaConPunti;

import org.json.JSONException;
import org.json.JSONObject;

public class PilotaConPuntiConJson extends PilotaConPunti {
//##################################################################################################
//                                  LABEL PILOTI PER JSON
//##################################################################################################
    protected final String LABLE_NOME = "nome";
    protected final String LABLE_TEAM = "team";
    protected final String LABLE_AUTO = "auto";
    protected final String LABLE_PUNTI= "punti";
//##################################################################################################
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
public PilotaConPuntiConJson(String nomeCognome, String nomeTeam, String nomeAuto,Integer punti) {
    super(nomeCognome, nomeTeam, nomeAuto,punti);
}
//-------------------------------------------------------------------------------------------------
    public PilotaConPuntiConJson(PilotaConPunti pilota) {
        super(pilota.getNomeCognome(), pilota.getNomeTeam(), pilota.getAuto().getNome(),pilota.getPunti());
    }
//--------------------------------------------------------------------------------------------------
    public PilotaConPuntiConJson(JSONObject jsonObject){
        if(jsonObject!=null) {
            try {
                this.setNomeCognome( jsonObject.getString(LABLE_NOME) );
                this.setNomeTeam(    jsonObject.getString(LABLE_TEAM) );
                this.setPunti(       jsonObject.getInt(LABLE_PUNTI) );

                this.setAuto(null);
                String nomeAuto =    jsonObject.getString(LABLE_AUTO);
                if( (nomeAuto!=null) && (!nomeAuto.equals("")) ){
                    this.setAuto(new AutoConImmagini(nomeAuto));
                }
            } catch (JSONException e) {
                Log.d("ErrJson",e.getMessage());
            }
        }
    }
//==================================================================================================
}
