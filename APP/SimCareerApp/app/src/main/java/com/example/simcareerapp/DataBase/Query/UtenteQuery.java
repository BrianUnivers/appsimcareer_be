package com.example.simcareerapp.DataBase.Query;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.simcareerapp.DataBase.Management.FeedReaderContract;
import com.example.simcareerapp.DataBase.Management.FeedReaderContract.FeedUtente;
import com.example.simcareerapp.DataBase.Management.HashPasswordWithSalt;
import com.example.simcareerapp.DataBase.Management.SimpleDbHelper;
import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.CircuitoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Foto;
import com.example.simcareerapp.Persistence.Entities.Indirizzo;
import com.example.simcareerapp.Persistence.Entities.Utente;

import java.sql.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Questa classe contiene i metodi inserire, selezionare e aggiornare i dati contenuti nella tabella Utente,
 * tramite le query al database locale.
 *
 * @author Brian Emanuel
 */
public class UtenteQuery {
    /**
     * La costante "projectionAll" contiene in un array di stringhe tutti i nomi delle colonne della
     * tabella Utente del database locale.
     */
    private static String[] projectionAll = {
            FeedUtente._ID,
            FeedUtente.COLUMN_NAME_NOME,
            FeedUtente.COLUMN_NAME_COGNOME,
            FeedUtente.COLUMN_NAME_DATA_NASCITA,
            FeedUtente.COLUMN_NAME_EMAIL,
            FeedUtente.COLUMN_NAME_USERNAME,
            FeedUtente.COLUMN_NAME_PASSWORD,
            FeedUtente.COLUMN_NAME_SALT,
            FeedUtente.COLUMN_NAME_RESIDENZA_STATO,
            FeedUtente.COLUMN_NAME_RESIDENZA_REGIONE,
            FeedUtente.COLUMN_NAME_RESIDENZA_CITTA,
            FeedUtente.COLUMN_NAME_RESIDENZA_VIA_PIAZZA,
            FeedUtente.COLUMN_NAME_NUMERO_DI_GARA,
            FeedUtente.COLUMN_NAME_AUTO_PREFERITA_NOME,
            FeedUtente.COLUMN_NAME_AUTO_PREFERITA_ID_IMMAGINE,
            FeedUtente.COLUMN_NAME_CIRCUITO_PREFERITA_NOME,
            FeedUtente.COLUMN_NAME_CIRCUITO_PREFERITA_ID_IMMAGINE,
            FeedUtente.COLUMN_NAME_CIRCUITO_ODIATO_NOME,
            FeedUtente.COLUMN_NAME_CIRCUITO_ODIATO_ID_IMMAGINE
        };
//##################################################################################################
//                             GET
//##################################################################################################
    /**
     * Tutti i metodi GET possono tornare NULL se non ci sono risultati validi
     */
//==================================================================================================
//                             GET LISTA DI TUTTI GLI UTENTI
//==================================================================================================

    /**
     * Questo è il metodo che richede al database di restituire tutte le entry della tabella Utente.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @return
     *      Il rusultato di questo metodo è una lista d'utenti, se e solo se esistono dei risultati validi
     *      altrimenti restituirà null.
     */
    public static List<Utente> getAll(SimpleDbHelper database){
        SQLiteDatabase db = database.getReadableDatabase();

        Cursor cursor = db.query( FeedUtente.TABLE_NAME,
                                  projectionAll,
                         null,
                      null,
                          null,
                           null,
                          null);
        //Perndi risultato
        List<Utente> listaUtenti = new LinkedList<Utente>();
        while(cursor.moveToNext()){
            Utente utente = new Utente();
            utente.setId(               cursor.getLong(   cursor.getColumnIndexOrThrow(FeedUtente._ID) ) );
            utente.setNome(             cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_NOME) ) );
            utente.setCognome(          cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_COGNOME) ) );
            utente.setDataNascita(      new Date(cursor.getLong( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_DATA_NASCITA) ) ));
            utente.setEmail(            cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_EMAIL) ) );
            utente.setUsername(         cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_USERNAME) ) );
            utente.setPasswordHashed(   cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_PASSWORD) ) );
            utente.setSalt(             cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_SALT) ) );
            //Resisenza
            utente.setResidenza( new Indirizzo(
                                        cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_RESIDENZA_STATO)),
                                        cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_RESIDENZA_REGIONE)),
                                        cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_RESIDENZA_CITTA)),
                                        cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_RESIDENZA_VIA_PIAZZA))
                                    ) );
            if(     ( utente.getResidenza().getStato()      ==null ) ||
                    ( utente.getResidenza().getRegione()    ==null ) ||
                    ( utente.getResidenza().getCitta()      ==null ) ||
                    ( utente.getResidenza().getViaPiazza()  ==null )  ){
                utente.setResidenza(null);
            }
            //Numero di Gara
            utente.setNumeroDiGara( cursor.getInt( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_NUMERO_DI_GARA) ) );
            //Auto Preferita
            utente.setAutoPreferita( new AutoConImmagini(
                                        cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_AUTO_PREFERITA_NOME)),
                                        cursor.getInt(    cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_AUTO_PREFERITA_ID_IMMAGINE))
                                    ) );
            if(     ( utente.getAutoPreferita().getNome()      ==null ) ||
                    ( utente.getAutoPreferita().getIdImmagine()==null )  ){
                utente.setAutoPreferita(null);
            }
            //Circuito Preferita
            utente.setCircuitoPreferito( new CircuitoConImmagini(
                                        cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_CIRCUITO_PREFERITA_NOME)),
                                        cursor.getInt(    cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_CIRCUITO_PREFERITA_ID_IMMAGINE))
                                    ) );
            if(     ( utente.getCircuitoPreferito().getNome()      ==null ) ||
                    ( utente.getCircuitoPreferito().getIdImmagine()==null )  ){
                utente.setCircuitoPreferito(null);
            }
            //Circuito Odiata
            utente.setCircuitoOdiato( new CircuitoConImmagini(
                                        cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_CIRCUITO_ODIATO_NOME)),
                                        cursor.getInt(    cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_CIRCUITO_ODIATO_ID_IMMAGINE))
                                    ) );
            if(     ( utente.getCircuitoOdiato().getNome()      ==null ) ||
                    ( utente.getCircuitoOdiato().getIdImmagine()==null )  ){
                utente.setCircuitoOdiato(null);
            }

            listaUtenti.add(utente);
        }
        cursor.close();
        if(listaUtenti.size()==0){
            listaUtenti= null;
        }
        return listaUtenti;
    }
//==================================================================================================
//==================================================================================================
//                             GET UTENTE BY ID
//==================================================================================================
    /**
     * Questo è il metodo che richede al database di restituire la entry della tabella Utenti avente
     * uno specifico ID.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param id
     *      Il parametro passato contiene l'ID dell'utente che si stà richiedendo.
     * @return
     *      Il rusultato di questo metodo è l'utenti con l'id indicato, se e solo se esiste altrimenti
     *      restituirà null.
     */
    public static Utente getUtente(SimpleDbHelper database,Long id){
        String selection        = FeedUtente._ID+" = ? ";
        String[] selectionArgs  = {id.toString()};
        return getUtentiConCondizioni(database,selection,selectionArgs);
    }
//==================================================================================================
//==================================================================================================
//                             GET SALT
//==================================================================================================
//--------------------------------------------------------------------------------------------------
//                             GET SALT BY ID
//--------------------------------------------------------------------------------------------------
    /**
     * Questo è il metodo che richede al database di restituire la stonga che rappresenta il salt usata
     * per l'hash della password di un'utente identificato dall'ID.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param id
     *      Il parametro passato contiene l'ID dell'utente che si stà richiedendo.
     * @return
     *      Il rusultato di questo metodo è la stringa che rappresenta il salt della password dell'utentente
     *      indicato dall'id, se e solo se esiste altrimenti restituirà null.
     */
    public static String getSaltById(SimpleDbHelper database,Long id){
        String selection        = FeedUtente._ID+" = ? ";
        String[] selectionArgs  = {id.toString()};
        return getSaltConCondizioni(database,selection,selectionArgs);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//                             GET SALT BY USERNAME
//--------------------------------------------------------------------------------------------------
    /**
     * Questo è il metodo che richede al database di restituire la stonga che rappresenta il salt usata
     * per l'hash della password di un'utente identificato dall'username.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param username
     *      Il parametro passato contiene username usato dall'utente che si stà richiedendo.
     * @return
     *      Il rusultato di questo metodo è la stringa che rappresenta il salt della password dell'utentente
     *      indicato dall'username, se e solo se esiste altrimenti restituirà null.
     */
    public static String getSaltByUsername(SimpleDbHelper database,String username ){
        String selection        = FeedUtente.COLUMN_NAME_USERNAME+" = ? ";
        String[] selectionArgs  = {username};
        return getSaltConCondizioni(database,selection,selectionArgs);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//                             GET SALT BY EMAIL
//--------------------------------------------------------------------------------------------------
    /**
     * Questo è il metodo che richede al database di restituire la stonga che rappresenta il salt usata
     * per l'hash della password di un'utente identificato dal email.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param email
     *      Il parametro passato contiene l'email usato dall'utente che si stà richiedendo.
     * @return
     *      Il rusultato di questo metodo è la stringa che rappresenta il salt della password dell'utentente
     *      indicato dal email, se e solo se esiste altrimenti restituirà null.
     */
    public static String getSaltByEmail(SimpleDbHelper database,String email){
        String selection        = FeedUtente.COLUMN_NAME_EMAIL+" = ? ";
        String[] selectionArgs  = {email};
        return getSaltConCondizioni(database,selection,selectionArgs);
    }
//==================================================================================================
//==================================================================================================
//                             GET UTENTE AUTENTICATO
//==================================================================================================
//--------------------------------------------------------------------------------------------------
//                             GET UTENTE AUTENTICATO BY USERNAME e PASSWORD
//--------------------------------------------------------------------------------------------------
    /**
     * Questo è il metodo che richede al database di restituire l'utente identificato dalla coppia
     * username e password a cui è stato  precedentemente aggiuto il salt e applicato l'algoritmo di hash.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param username
     *      Il parametro passato contiene username usato dall'utente che si stà richiedendo.
     * @param password
     *      Il parametro passato contiene la password che deve avere l'utente nel database per essere
     *      ritenuta valida l'autenicazione e quindi la restituzione di tutti i dati di quell'utente.
     * @return
     *      Il rusultato di questo metodo è un oggetto utente con tutti i dati dell'utente che è
     *      identificato dall'username avente la password specificata, se non esiste verrà restituito null.
     */
    public static Utente getUtenteAutenticatoByUsernamePassword(SimpleDbHelper database,String username,String password){
        String selection        = FeedUtente.COLUMN_NAME_USERNAME+" = ? AND "+ FeedUtente.COLUMN_NAME_PASSWORD+" = ? ";
        String[] selectionArgs  = {username,password};
        return getUtentiConCondizioni(database,selection,selectionArgs);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//                             GET UTENTE AUTENTICATO BY EMAIL e PASSWORD
//--------------------------------------------------------------------------------------------------
    /**
     * Questo è il metodo che richede al database di restituire l'utente identificato dalla coppia
     * email e password a cui è stato  precedentemente aggiuto il salt e applicato l'algoritmo di hash.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param email
     *      Il parametro passato contiene l'email usato dall'utente che si stà richiedendo.
     * @param password
     *      Il parametro passato contiene la password che deve avere l'utente nel database per essere
     *      ritenuta valida l'autenicazione e quindi la restituzione di tutti i dati di quell'utente.
     * @return
     *      Il rusultato di questo metodo è un oggetto utente con tutti i dati dell'utente che è
     *      identificato dal email avente la password specificata, se non esiste verrà restituito null.
     *
     */
    public static Utente getUtenteAutenticatoByEmailPassword(SimpleDbHelper database,String email,String password){
        String selection        = FeedUtente.COLUMN_NAME_EMAIL+" = ? AND "+ FeedUtente.COLUMN_NAME_PASSWORD+" = ? ";
        String[] selectionArgs  = {email,password};
        return getUtentiConCondizioni(database,selection,selectionArgs);
    }
//==================================================================================================
//==================================================================================================
//                             APPOGIO
//==================================================================================================
    /**
     * Questo è il metodo che richede al database di restituire la entry della tabella Uenente che
     *  rispetta delle condizioni.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param selection
     *      Il parametro passato contiene una stinga contenente le condizioni della query, senza i valori
     *      variabili rappresentati dal caratter '?' ed in oltre senza l'utilizzio all'inizio di WHERE.
     * @param selectionArgs
     *      Il parametro passato contiene l'array di stinge contenti i valori da inserire, nello stesso
     *      ardine, nella sringa delle condizioni
     * @return
     *      Il rusultato di questo metodo è un utente,se esiste un risultato valido altrimenti restituirà
     *      null.
     */
    private static Utente getUtentiConCondizioni(SimpleDbHelper database,String selection,String[] selectionArgs ){
        SQLiteDatabase db = database.getReadableDatabase();

        Cursor cursor = db.query( FeedUtente.TABLE_NAME,
                projectionAll,
                selection,
                selectionArgs,
                null,
                null,
                null);
        //Perndi risultato
        Utente utente = null;
        if (cursor.moveToNext()){
            utente = new Utente();
            utente.setId(               cursor.getLong(   cursor.getColumnIndexOrThrow(FeedUtente._ID) ) );
            utente.setNome(             cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_NOME) ) );
            utente.setCognome(          cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_COGNOME) ) );
            utente.setDataNascita(new Date(cursor.getLong( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_DATA_NASCITA) ) ));
            utente.setEmail(            cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_EMAIL) ) );
            utente.setUsername(         cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_USERNAME) ) );
            utente.setPasswordHashed(   cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_PASSWORD) ) );
            utente.setSalt(             cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_SALT) ) );
            //Resisenza
            utente.setResidenza( new Indirizzo(
                    cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_RESIDENZA_STATO)),
                    cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_RESIDENZA_REGIONE)),
                    cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_RESIDENZA_CITTA)),
                    cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_RESIDENZA_VIA_PIAZZA))
            ) );
            if(     ( utente.getResidenza().getStato()      ==null ) ||
                    ( utente.getResidenza().getRegione()    ==null ) ||
                    ( utente.getResidenza().getCitta()      ==null ) ||
                    ( utente.getResidenza().getViaPiazza()  ==null )  ){
                utente.setResidenza(null);
            }
            //Numero di Gara
            utente.setNumeroDiGara( cursor.getInt( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_NUMERO_DI_GARA) ) );
            //Auto Preferita
            utente.setAutoPreferita( new AutoConImmagini(
                    cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_AUTO_PREFERITA_NOME)),
                    cursor.getInt(    cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_AUTO_PREFERITA_ID_IMMAGINE))
            ) );
            if(     ( utente.getAutoPreferita().getNome()      ==null ) ||
                    ( utente.getAutoPreferita().getIdImmagine()==null )  ){
                utente.setAutoPreferita(null);
            }
            //Circuito Preferita
            utente.setCircuitoPreferito( new CircuitoConImmagini(
                    cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_CIRCUITO_PREFERITA_NOME)),
                    cursor.getInt(    cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_CIRCUITO_PREFERITA_ID_IMMAGINE))
            ) );
            if(     ( utente.getCircuitoPreferito().getNome()      ==null ) ||
                    ( utente.getCircuitoPreferito().getIdImmagine()==null )  ){
                utente.setCircuitoPreferito(null);
            }
            //Circuito Odiata
            utente.setCircuitoOdiato( new CircuitoConImmagini(
                    cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_CIRCUITO_ODIATO_NOME)),
                    cursor.getInt(    cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_CIRCUITO_ODIATO_ID_IMMAGINE))
            ) );
            if(     ( utente.getCircuitoOdiato().getNome()      ==null ) ||
                    ( utente.getCircuitoOdiato().getIdImmagine()==null )  ){
                utente.setCircuitoOdiato(null);
            }
        }
        cursor.close();
        return utente;
    }
//--------------------------------------------------------------------------------------------------
    /**
     * TODO: Questo è il metodo che richede al database di restituire############################### <-- Qui
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param selection
     * @param selectionArgs
     * @return
     */
    private static String getSaltConCondizioni(SimpleDbHelper database,String selection,String[] selectionArgs ){
        SQLiteDatabase db = database.getReadableDatabase();
        String[] projection = {FeedUtente.COLUMN_NAME_SALT};
        Cursor cursor = db.query( FeedUtente.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null);
        //Perndi risultato
        String salt = null;
        if (cursor.moveToNext()){
            salt = cursor.getString( cursor.getColumnIndexOrThrow(FeedUtente.COLUMN_NAME_SALT) );
        }
        cursor.close();
        return salt;
    }
//==================================================================================================
//##################################################################################################
//                             INSERT
//##################################################################################################
//==================================================================================================
//                             INSERT UTENTE
//==================================================================================================
//--------------------------------------------------------------------------------------------------
//                             INSERT UTENTE CON DATI PARZIALI
//--------------------------------------------------------------------------------------------------
    /**
     * Questo è il metodo che richede al database di restituire
     * Il valore di ritorno è l'id del record appena inserito se però il valore è pari a -1 non è stato
     * possibile inserire l'utente nel database.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param utente
     * @return
     */
    public static Long insertUtenteDatiParziali(SimpleDbHelper database,Utente utente){
        SQLiteDatabase db = database.getWritableDatabase();

        ContentValues values = new ContentValues();
        values = setValuesMinimiObbligatori(values,utente);
        Long id = db.insert(FeedUtente.TABLE_NAME,null,values);
        if(id>=0){
            utente.setId(id);
        }
        return id;
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//                             INSERT UTENTE CON TUTTI I DATI
//--------------------------------------------------------------------------------------------------
    /**
     * Questo è il metodo che richede al database di restituire
     * Il valore di ritorno è l'id del record appena inserito se però il valore è pari a -1 non è stato
     * possibile inserire l'utente nel database.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param utente
     * @return
     */
    public static Long insertUtente(SimpleDbHelper database,Utente utente){
        SQLiteDatabase db = database.getWritableDatabase();

        ContentValues values = new ContentValues();
        values = setValuesCompleto(values,utente);
        Long id = db.insert(FeedUtente.TABLE_NAME,null,values);
        if(id>=0){
            utente.setId(id);
        }
        return id;
    }
//--------------------------------------------------------------------------------------------------
//==================================================================================================
//==================================================================================================
//                             APPOGIO
//==================================================================================================
    /**
     *
     * @param values
     * @param utente
     * @return
     */
    private static ContentValues setValuesMinimiObbligatori(ContentValues values,Utente utente){
        values.put(FeedUtente.COLUMN_NAME_NOME,             utente.getNome());
        values.put(FeedUtente.COLUMN_NAME_COGNOME,          utente.getCognome());
        values.put(FeedUtente.COLUMN_NAME_DATA_NASCITA,     utente.getDataNascita().getTime());
        values.put(FeedUtente.COLUMN_NAME_EMAIL,            utente.getEmail());
        values.put(FeedUtente.COLUMN_NAME_USERNAME,         utente.getUsername());
        values.put(FeedUtente.COLUMN_NAME_SALT,             utente.getSalt());
        values.put(FeedUtente.COLUMN_NAME_PASSWORD,         utente.getPassword());
        return values;
    }
//--------------------------------------------------------------------------------------------------
    /**
     *
     * @param values
     * @param utente
     * @return
     */
    private static ContentValues setValuesCompleto(ContentValues values,Utente utente) {
        values = setValuesMinimiObbligatori(values, utente);
        //Residenza
        Indirizzo indirizzo = utente.getResidenza();
        if (indirizzo != null){
            values.put(FeedUtente.COLUMN_NAME_RESIDENZA_STATO, indirizzo.getStato());
            values.put(FeedUtente.COLUMN_NAME_RESIDENZA_REGIONE, indirizzo.getRegione());
            values.put(FeedUtente.COLUMN_NAME_RESIDENZA_CITTA, indirizzo.getCitta());
            values.put(FeedUtente.COLUMN_NAME_RESIDENZA_VIA_PIAZZA, indirizzo.getViaPiazza());
        }
        //Numero di gara
        if(utente.getNumeroDiGara()!=null){
            values.put(FeedUtente.COLUMN_NAME_NUMERO_DI_GARA,       utente.getNumeroDiGara());
        }
        //Auto Preferita
        AutoConImmagini autoPreferita = utente.getAutoPreferita();
        if(autoPreferita!=null) {
            values.put(FeedUtente.COLUMN_NAME_AUTO_PREFERITA_NOME, autoPreferita.getNome());
            values.put(FeedUtente.COLUMN_NAME_AUTO_PREFERITA_ID_IMMAGINE, autoPreferita.getIdImmagine());
        }
        //Circuito Preferita
        CircuitoConImmagini circuitoPreferita = utente.getCircuitoPreferito();
        if(circuitoPreferita!=null){
            values.put(FeedUtente.COLUMN_NAME_CIRCUITO_PREFERITA_NOME,       circuitoPreferita.getNome());
            values.put(FeedUtente.COLUMN_NAME_CIRCUITO_PREFERITA_ID_IMMAGINE,circuitoPreferita.getIdImmagine());
        }
        //Circuito Odiata
        CircuitoConImmagini circuitoOdiato = utente.getCircuitoOdiato();
        if(circuitoOdiato!=null){
            values.put(FeedUtente.COLUMN_NAME_CIRCUITO_ODIATO_NOME,         circuitoOdiato.getNome());
            values.put(FeedUtente.COLUMN_NAME_CIRCUITO_ODIATO_ID_IMMAGINE,  circuitoOdiato.getIdImmagine());
        }
        return values;
    }
//==================================================================================================
//##################################################################################################
//                             UPDATE
//##################################################################################################
//==================================================================================================
//                             UPDATE UTENTE
//==================================================================================================
    /**
     * Questo è il metodo che richede al database di restituire
     * Questo metodo aggiorna lo stesso con un id con un altro oggeto
     * Il valore di ritorno è il numero di record che sono stati modificati.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param utente
     * @param id
     * @return
     */
    public static Integer updateUtente(SimpleDbHelper database, Utente utente, long id){
        String selection = FeedReaderContract.FeedFoto._ID + " = ?";
        String[] selectionArgs = { new Long(id).toString() };
        return updateUtenteConCondizioni(database,utente,selection,selectionArgs);
    }
//==================================================================================================
//                             UTENTE
//==================================================================================================
    /**
     * Questo è il metodo che richede al database di restituire
     *  Questo metodo effetua le modifiche.
     *
     * @param database
     *      Il parametro passato contiene il riferimento al database al quale si vuole chiedere la
     *      query.
     * @param utente
     * @param selection
     * @param selectionArgs
     * @return
     */
    private static Integer updateUtenteConCondizioni(SimpleDbHelper database,Utente utente,String selection,String[] selectionArgs){
        SQLiteDatabase db = database.getWritableDatabase();
        ContentValues values = new ContentValues();
        values = setValuesCompleto(values,utente);
        Integer id = db.update(FeedReaderContract.FeedUtente.TABLE_NAME,
                values,
                selection,
                selectionArgs);
        return id;
    }
//==================================================================================================
}
