package com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface;
//##################################################################################################
//                                  INTERFACCIA PER COMUNICARE
//##################################################################################################
/**
 * Interfacca per interagire con questo FotoGalleriaRVAdapter che deve essere implementato dall'Activity con
 * cui si vule collabolrare. Per sapere cosa fare quando premo sul cartella della galleria.
 */
public interface OnClickFotoGalleriaListener {
    void apriFotoGalleria(String fileNome);
}
//##################################################################################################
//##################################################################################################