package com.example.simcareerapp.Persistence.Entities;

import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;
import com.example.simcareerapp.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class CircuitoConImmagini extends ValoreConImmaginePerSpinner {
//==================================================================================================
//                                  Costrottore
//==================================================================================================
    public CircuitoConImmagini(String nome) {
        super(nome);
        this.idImmagine = AssociazioneNomeIdImmagine.getIdImmagineCircuito(nome);
        if(this.idImmagine==null){
            this.idImmagine= R.drawable.userdefault;
        }
    }
    public CircuitoConImmagini(String nome,Integer idImmagine) {
        super(nome);
        this.idImmagine = idImmagine;
    }
//==================================================================================================

//==================================================================================================
//                                  Get List Imagine
//==================================================================================================
    static public ArrayList<ValoreConImmaginePerSpinner> getListaCircuitoDaListaNomi(List<String> lisataNomi){
        ArrayList<ValoreConImmaginePerSpinner> ris=new ArrayList<>();
        Iterator iterator = lisataNomi.iterator();
        while (iterator.hasNext()){
            ris.add(new CircuitoConImmagini((String)iterator.next()));
        }
        return ris;
    }
//==================================================================================================

//==================================================================================================
//                                  Get List Imagine
//==================================================================================================
    static public ArrayList<ValoreConImmaginePerSpinner> getAllListaCircuito(){
        ArrayList<String> listaNomi=new ArrayList<>();
        listaNomi.add("Adria Karting Raceway");
        listaNomi.add("Alaharma");
        listaNomi.add("Atlanta Motorsports Park");
        listaNomi.add("Berlin E-Prix");
        listaNomi.add("Botniaring");
        listaNomi.add("Brianza");
        listaNomi.add("Circuito d'Azur");
        listaNomi.add("Guapore");
        listaNomi.add("Hong Kong E-Prix");
        listaNomi.add("Ibarra");
        listaNomi.add("Imola");
        listaNomi.add("Indianapolis");
        listaNomi.add("Kristianstad Karting");
        listaNomi.add("Le Mans 24H");
        listaNomi.add("Lester Special Edition");
        listaNomi.add("Lime Rock Park");
        listaNomi.add("Longford");
        listaNomi.add("Malaysia");
        listaNomi.add("Monaco E-Prix");
        listaNomi.add("Monte Carlo");
        listaNomi.add("New York E-Prix");
        listaNomi.add("NOLA Motorsports Park");
        listaNomi.add("Nurburgring Norschliefe e Nurburgring GP");
        listaNomi.add("Palm Beach international Raceway");
        listaNomi.add("Portland International Raceway");
        listaNomi.add("Portugal International");
        listaNomi.add("Sau Paulo");
        listaNomi.add("International Raceway");
        listaNomi.add("Silverstone");
        listaNomi.add("Virginia International Raceway");
        listaNomi.add("Zandvoort");
        listaNomi.add("Sepang");
        listaNomi.add("Buriram");
        listaNomi.add("Suzuka");

        listaNomi.add("Jerez");
        listaNomi.add("Barcellona");
        listaNomi.add("Valencia");
        listaNomi.add("Portimao");
        listaNomi.add("Zhejiang");
        listaNomi.add("Adria");
        listaNomi.add("Hungaroring");


        Collections.sort(listaNomi);
        return getListaCircuitoDaListaNomi(listaNomi);
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//##################################################################################################
//                                  Classe per la associazione nome circuito to id
//##################################################################################################
    static class AssociazioneNomeIdImmagine{
        private static HashMap<String,Integer> associazioneNomeId=null;
//--------------------------------------------------------------------------------------------------
//                                  Inizializzazione unica per tutta l'esecuzione del'app.
//--------------------------------------------------------------------------------------------------
        private static void init(){
            associazioneNomeId = new HashMap();
            associazioneNomeId.put("adria karting raceway",                         R.drawable.track_adria_karting_raceway);
            associazioneNomeId.put("alaharma",                                      R.drawable.track_alaharma);
            associazioneNomeId.put("atlanta motorsports park",                      R.drawable.track_atlanta_motorsports_park);
            associazioneNomeId.put("berlin e-prix",                                 R.drawable.track_berlin_e_prix);
            associazioneNomeId.put("botniaring",                                    R.drawable.track_botniaring);
            associazioneNomeId.put("brianza",                                       R.drawable.track_brianza);
            associazioneNomeId.put("circuito d'azur",                               R.drawable.track_circuit_d_azur);
            associazioneNomeId.put("guapore",                                       R.drawable.track_guapore);
            associazioneNomeId.put("hong kong e-prix",                              R.drawable.track_hong_kong_e_prix);
            associazioneNomeId.put("ibarra",                                        R.drawable.track_ibarra);
            associazioneNomeId.put("imola",                                         R.drawable.track_imola);
            associazioneNomeId.put("indianapolis",                                  R.drawable.track_indianapolis);
            associazioneNomeId.put("kristianstad karting",                          R.drawable.track_kristianstad_karting);
            associazioneNomeId.put("le mans 24h",                                   R.drawable.track_le_mans_24h);
            associazioneNomeId.put("lester special edition",                        R.drawable.track_lester_special_edition);
            associazioneNomeId.put("lime rock park",                                R.drawable.track_lime_rock_park);
            associazioneNomeId.put("longford",                                      R.drawable.track_longford);
            associazioneNomeId.put("malaysia",                                      R.drawable.track_malaysia);
            associazioneNomeId.put("monaco e-prix",                                 R.drawable.track_monaco_e_prix);
            associazioneNomeId.put("monte carlo",                                   R.drawable.track_monte_carlo);
            associazioneNomeId.put("new york e-prix",                               R.drawable.track_new_york_e_prix);
            associazioneNomeId.put("nola motorsports park",                         R.drawable.track_nola_motorsports_park);
            associazioneNomeId.put("nurburgring norschliefe e nurburgring gp",      R.drawable.track_nurburgring_norschliefe_and_nurburgring_gp);
            associazioneNomeId.put("palm beach international raceway",              R.drawable.track_palm_beach_international_raceway);
            associazioneNomeId.put("portland international raceway",                R.drawable.track_portland_international_raceway);
            associazioneNomeId.put("portugal international",                        R.drawable.track_portugal_international);
            associazioneNomeId.put("sau paulo",                                     R.drawable.track_sau_paulo);
            associazioneNomeId.put("international raceway",                         R.drawable.track_sebring_international_raceway);
            associazioneNomeId.put("silverstone",                                   R.drawable.track_silverstone);
            associazioneNomeId.put("virginia international raceway",                R.drawable.track_virginia_international_raceway);
            associazioneNomeId.put("zandvoort",                                     R.drawable.track_zandvoort);
            associazioneNomeId.put("sepang",                                        R.drawable.track_sepang);
            associazioneNomeId.put("buriram",                                       R.drawable.track_buriram);
            associazioneNomeId.put("suzuka",                                        R.drawable.track_suzuka);

            associazioneNomeId.put("jerez",                                         R.drawable.track_circuito_jerez);
            associazioneNomeId.put("barcellona",                                    R.drawable.track_circuito_barcellona);
            associazioneNomeId.put("valencia",                                      R.drawable.track_circuito_valencia);
            associazioneNomeId.put("portimao",                                      R.drawable.track_portimao);
            associazioneNomeId.put("zhejiang",                                      R.drawable.track_zhejiang);
            associazioneNomeId.put("adria",                                         R.drawable.track_circuito_adria);
            associazioneNomeId.put("hungaroring",                                   R.drawable.track_hungaroring);

        }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//                                  Restituisce l'id associato al nomepassato
//--------------------------------------------------------------------------------------------------
        /**Restituisce il valore a cui è mappata la chiave specificata o nullse questa mappa non contiene
         * alcuna mappatura per la chiave
         */
        public static Integer getIdImmagineCircuito(String nome){
            if(associazioneNomeId==null){
                init();
            }
            return associazioneNomeId.get(nome.toLowerCase());
        }
    }
//###################################################################################################
}