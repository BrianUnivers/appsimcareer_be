package com.example.simcareerapp.AppCompatActivityWithMenu.MyServiceDiNotifiche;

import android.app.NotificationManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;
import android.widget.Toast;

import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyPersistableBundle;
import com.example.simcareerapp.Persistence.Entities.Utente;

import java.util.Iterator;
import java.util.List;

import static android.content.Context.NOTIFICATION_SERVICE;

public class MyServizioDiNotifiche {
//##################################################################################################
//                                  NUMERO DEL SERZIO CHE GESTISCE LE NOTIFICHE
//##################################################################################################
    private static int ID_JOB_NOTIFICHE = 1;
//##################################################################################################

//##################################################################################################
//                                  CHIUSURA SERVIZIO
//##################################################################################################
//==================================================================================================
//                                  CANCELLAZIONE DELLE NOTIFICHE NON CHIUSE E DEL SERVIZIO
//==================================================================================================
    /**
     *
     */
    public static void chiusuraServizioDiNotificheENotifiche(Context context){
        JobScheduler scheduler = (JobScheduler) context.getSystemService(context.JOB_SCHEDULER_SERVICE);
        scheduler.cancel(ID_JOB_NOTIFICHE);
        //Cancella le notifiche fatte
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }
//==================================================================================================
//##################################################################################################
    public static void avviaServizioDiNotifiche(Context context, Utente utenteAttivo,Boolean mantieniUtenteAttivo){

        // creazione del JobScheduler
        JobScheduler scheduler = (JobScheduler) context.getSystemService(context.JOB_SCHEDULER_SERVICE);
        if(!isJobServiceOn(scheduler,ID_JOB_NOTIFICHE)) {

            // il Builder richiede la componente relativa al Service
            JobInfo.Builder builder = new JobInfo.Builder(ID_JOB_NOTIFICHE,
                    new ComponentName(context.getPackageName(),
                            MyJobServiceNotificheUtente.class.getName()));
            // impostazione delle condizioni
            builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED);
            builder.setPeriodic(1000*60*5);


            //Passa i parametri
            MyPersistableBundle myBundle = new MyPersistableBundle();
            myBundle.putUtente(MyPersistableBundle.I_UTENTE_ATTIVO,utenteAttivo);
            myBundle.putBoolean(MyPersistableBundle.I_MANTIENI_ATTIVO_UTENTE, mantieniUtenteAttivo);
            builder.setExtras(myBundle.getPersistableBundle());

            // creazione del JobInfo
            JobInfo jobinfo = builder.build();

            // schedulazione del JobInfo
            int res = scheduler.schedule(jobinfo);
            // valutazione del risultato
            if (res == JobScheduler.RESULT_SUCCESS) {
                // attività da svolgere in caso di successo
                //Toast.makeText(context, "Ciao a tutti OK :-) !", Toast.LENGTH_SHORT).show();
            } else {
                // attività da svolgere in caso di errore
              //  Toast.makeText(context, "Ciao a tutti KO :-( !", Toast.LENGTH_SHORT).show();
            }
        }else{
            //Toast.makeText(context, "KO!", Toast.LENGTH_SHORT).show();
        }
    }


    public static boolean isJobServiceOn( Context context,int jobId ) {
        JobScheduler scheduler = (JobScheduler) context.getSystemService( Context.JOB_SCHEDULER_SERVICE ) ;
        return isJobServiceOn(scheduler,jobId);
    }

    private static boolean isJobServiceOn( JobScheduler scheduler,int jobId ){
        boolean hasBeenScheduled = false ;
        for ( JobInfo jobInfo : scheduler.getAllPendingJobs() ) {
            if ( (jobInfo.getId() == jobId)  ) {
                hasBeenScheduled = true ;
                break ;
            }
        }
        return hasBeenScheduled ;
    }
}
