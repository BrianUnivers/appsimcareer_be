package com.example.simcareerapp.GestioneJson;

import android.content.Context;

import com.example.simcareerapp.GestioneJson.Generale.GestioneGenerale;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Gara;
import com.example.simcareerapp.Persistence.JsonEntities.CampionatoConJson;
import com.example.simcareerapp.Persistence.JsonEntities.GaraConJson;
import com.example.simcareerapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class GestioneListeCampionati extends GestioneGenerale {
//##################################################################################################
//                                  ETICHETTE PER LA GESTIONE DEL JSON
//##################################################################################################
    private static final String LABLE_CAMPIONATI= "campionati";
//##################################################################################################
//==================================================================================================
//                              LETTURA E CREAZIONE LISTA DEI CAMPIONATI
//==================================================================================================
    public static List<CampionatoConJson> letturaECreazioneListaCampionati(Context context , int idFile){
        String contenutoFile = GestioneGenerale.leturaFileJson(context,idFile);
        List<CampionatoConJson> ris = new LinkedList<CampionatoConJson>();
        try {
            JSONObject jsonObject = new JSONObject(contenutoFile);

            JSONArray jsonArrayGare = jsonObject.getJSONArray(LABLE_CAMPIONATI);
            for(int i=0; i<jsonArrayGare.length(); i++){
                CampionatoConJson campionato= new CampionatoConJson(jsonArrayGare.getJSONObject(i));
                if(!campionato.isNull()){
                    ris.add(campionato);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                              AGGIORNA LISTA DEI CAMPIONATI
//==================================================================================================
    public static Boolean aggiornaListaCampionati(Context context , int idFile,List<CampionatoConJson> lista){
        String contenuto = "";

        try {
            JSONObject object = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            Iterator<CampionatoConJson> iterator = lista.iterator();
            int i=0;
            while (iterator.hasNext()){
                CampionatoConJson campionato = iterator.next();
                JSONObject campionatoJSON = campionato.getJSON();
                if(campionatoJSON!=null){
                    jsonArray.put(i,campionatoJSON);
                    i++;//Prossimo elemeto
                }
            }
            object.put(LABLE_CAMPIONATI,jsonArray);
            contenuto = object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return GestioneGenerale.scritturaFileJson(context,idFile,contenuto);
    }
//==================================================================================================
}
