package com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.simcareerapp.CampionatiActivity;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.R;
import com.example.simcareerapp.SingoloCampionatoActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CampionatoRVAdapter extends RecyclerView.Adapter<CampionatoRVAdapter.MyViewHoder> {
//##################################################################################################
//                              CLASSE INTERNA
//##################################################################################################
    /**
     * Questa classe è quella che permette alla RecyclingView Adapter di definire un layout custom
     * del singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public View      v_mainLayout;
        //Informazioni generali
        public TextView  v_titoloView;
        public ImageView v_imageView;
        //Informazioni specifiche
        public TextView  v_value1View;
        public TextView  v_value2View;
        public TextView  v_value3View;
        public TextView  v_lable4View;
        public ImageView v_iconaView;
//==================================================================================================
//                              COSTRUTTORI
//==================================================================================================
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout    = itemView.findViewById(R.id.lly_BloccoCampionato);

            v_titoloView    = itemView.findViewById(R.id.txt_TitoloBlocco);
            v_imageView     = itemView.findViewById(R.id.img_ImmagineBlocco);
            //Riga 1
            v_value1View    = itemView.findViewById(R.id.txt_Value_1);
            //Riga 2
            v_value2View    = itemView.findViewById(R.id.txt_Value_2);
            //Riga 3
            v_value3View    = itemView.findViewById(R.id.txt_Value_3);
            //Riga 4
            v_lable4View    = itemView.findViewById(R.id.txt_Lable4);
            //Icona
            v_iconaView     = itemView.findViewById(R.id.img_Icona);
        }
//==================================================================================================
    }
//##################################################################################################



//##################################################################################################
//                              CLASSE PRINCIPALE CampionatoRVAdapter
//##################################################################################################
    private Context comtext=null;
    private Activity activity=null;
    private List<Campionato> listaCampionati=null;
    private Utente utenteAttivo=null;
    //Formattazione
    private static String FORMATTAZIONE_DATE = "dd/MM/yyyy";
//==================================================================================================
//                              COSTRUTTORI
//==================================================================================================
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public CampionatoRVAdapter(Context comtext,Activity activity, List<Campionato> listaCampionati,Utente utenteAttivo) {
        this.comtext = comtext;
        this.activity = activity;
        this.listaCampionati = listaCampionati;
        this.utenteAttivo = utenteAttivo;
    }
//==================================================================================================

//==================================================================================================
//                              CERAZIONE DEL SINGOLO ELEMETOTO
//==================================================================================================
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(comtext);
        View bloccoElemeto = inflater.inflate(R.layout.row_list_campinati_layout, parent,false);
        return new MyViewHoder(bloccoElemeto);
    }
//==================================================================================================

//==================================================================================================
//                              INSERIRE L'ELEMETO NEL LAYOUT
//==================================================================================================
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore CampionatoRVAdapter.
     * Inoltre aggiunge la possibilità di premere sul singolo oggeto così da aprire un altra activity.
     * <<SingoloCampionatoActivity>>
     */
    @Override
    public void onBindViewHolder(@NonNull MyViewHoder holder, int position) {
        final Campionato campionato =listaCampionati.get(position);
        //Inserisci i dati nel layout
        inserisciCampionatoNelLayout(holder, campionato);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  IMPOSTA COME BOTTONE IL CAMPIONATO
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        holder.v_mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyIntent intent = new MyIntent(comtext, SingoloCampionatoActivity.class);
                intent.putExtra(MyIntent.I_ID_CAMPIONATO_SELEZIONATO,campionato.getId());
                if(activity instanceof CampionatiActivity){
                    //Imposta come dati passati anche <<UtenteAttivo>> e <<MantieniAttivoUtente>>
                    ((CampionatiActivity) activity).setParamiter(intent);
                    comtext.startActivity(intent);
                }
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
//==================================================================================================
//                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE
//==================================================================================================
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaCampionati.size();
    }
//==================================================================================================


//==================================================================================================
//                              INSERISCI I DATI NEL LAYOUT
//==================================================================================================
    /**
     * Questo metodo si occupa di inserire i dati del campinato nel layout del singolo elemeto.
     */
    private void inserisciCampionatoNelLayout(@NonNull MyViewHoder holder, Campionato campionato){
        // Formattzazione delle date.
        SimpleDateFormat sdf = new SimpleDateFormat(FORMATTAZIONE_DATE);
        //Imposta Titolo
        holder.v_titoloView.setText(campionato.getName());
        //Imposta Immagine
        String nomeFileEsteso = campionato.getLogoURL();
        String nomeFile = (nomeFileEsteso.substring(0, (nomeFileEsteso).lastIndexOf('.')));//Toglie l'estensione
        Uri path = Uri.parse("android.resource://com.example.simcareerapp/raw/"+nomeFile);
        holder.v_imageView.setImageURI(path);
        //Imposta Tabella Riga 1
        Date inizio = campionato.getDataInizioCampionato();
        if (inizio!=null){
            String inizioStr = sdf.format(inizio);
            holder.v_value1View.setText(inizioStr);
        }else{
            holder.v_value1View.setText("");
        }
        //Imposta Tabella Riga 2
        Date fine = campionato.getDataFineCampionato();
        if (fine!=null){
            String fineStr = sdf.format(fine);
            holder.v_value2View.setText(fineStr);
        }else{
            holder.v_value2View.setText("");
        }
        //Imposta Tabella Riga 3
        holder.v_value3View.setText(""+(campionato.getListaGara().size()));
        //Imposta Tabella Riga 4
        if(campionato.iscrizioneAperta()){
            if(campionato.postiDisponibili()){
                String testo = comtext.getResources().getString(R.string.lbl_NumeroIscrizioni);
                testo += " "+campionato.getListaPiloti().size()+"/"+campionato.MAX_NUM_ISCRITTI;
                holder.v_lable4View.setText(testo);
            }else{
                holder.v_lable4View.setText(R.string.lbl_IscrizioniComplete);
            }
        }else{
            holder.v_lable4View.setText(R.string.lbl_IscrizioniChiuse);
        }
        //Imposta Layout e icona
        if(campionato.utenteIscritto(utenteAttivo)){
            holder.v_iconaView.setVisibility(View.VISIBLE);
            holder.v_lable4View.setTextColor(comtext.getResources().getColor(R.color.colorPrimary));
        }else{
            holder.v_iconaView.setVisibility(View.INVISIBLE);
            holder.v_lable4View.setTextColor(comtext.getResources().getColor(R.color.colorNormalText));
        }
    }
//==================================================================================================
//##################################################################################################
}
