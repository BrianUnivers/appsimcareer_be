package com.example.simcareerapp.Persistence.Entities;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;
import com.example.simcareerapp.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class AutoConImmagini extends ValoreConImmaginePerSpinner {
//==================================================================================================
//                                  Costrottore
//==================================================================================================
    public AutoConImmagini(String nome) {
        super(nome);
        this.idImmagine = AssociazioneNomeIdImmagine.getIdImmagineMacchina(nome);
        if(this.idImmagine==null){
            this.idImmagine=R.drawable.userdefault;
        }
    }
    public AutoConImmagini(String nome,Integer idImmagine) {
        super(nome);
        this.idImmagine = idImmagine;
    }
//==================================================================================================



//==================================================================================================
//                                  Get List Imagine
//==================================================================================================
    static public ArrayList<ValoreConImmaginePerSpinner> getListaMacchinaDaListaNomi(List<String> lisataNomi){
        ArrayList<ValoreConImmaginePerSpinner> ris=new ArrayList<>();

        Iterator iterator = lisataNomi.iterator();
        while (iterator.hasNext()){
            ris.add(new AutoConImmagini((String)iterator.next()));
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                                  Get List Imagine
//==================================================================================================
    static public ArrayList<ValoreConImmaginePerSpinner> getAllListaMacchina(){
        ArrayList<String> listaNomi=new ArrayList<>();
        listaNomi.add("Seat Leon");
        listaNomi.add("Honda Civic");
        listaNomi.add("Peugeot 308");
        listaNomi.add("VW Golf");
        listaNomi.add("Alfa Romeo Giulietta");
        listaNomi.add("AC 427 SC");
        listaNomi.add("Aston Martin Vantage GT3");
        listaNomi.add("Aston Martin Vantage GTE");
        listaNomi.add("Audi LMS GT3");
        listaNomi.add("Bentley Continental GT3");
        listaNomi.add("BMW M2 CS Racing");
        listaNomi.add("BMW M6 GT3");
        listaNomi.add("BMW M8 GTE");
        listaNomi.add("Brabham BT20");
        listaNomi.add("Brabham BT44B");
        listaNomi.add("Callaway Corvette C7 GT3-R");
        listaNomi.add("Chevrolet Camaro GT3 2012");
        listaNomi.add("Chevrolet Corvette C6R GT2");
        listaNomi.add("Chevrolet Corvette C7R");
        listaNomi.add("Dallara DW12");
        listaNomi.add("Ferrari 488 GTE");
        listaNomi.add("Formula E 2018");
        listaNomi.add("Formula E Gen 2 2019-2020");
        listaNomi.add("Formula ISI 2012");
        listaNomi.add("Formula Renault35");
        listaNomi.add("Formula Vee");
        listaNomi.add("Honda Civic BTCC");
        listaNomi.add("Kartcup");
        listaNomi.add("Marussia MR01 2012");
        listaNomi.add("McLaren 650S GT3");
        listaNomi.add("McLaren 720S GT3");
        listaNomi.add("McLaren Senna GTR");
        listaNomi.add("McLaren M23");
        listaNomi.add("McLaren MP4/8");
        listaNomi.add("McLaren MP4/13");
        listaNomi.add("Mercedes AMG GT3");
        listaNomi.add("Nissan GT500");
        listaNomi.add("Nissan GTR");
        listaNomi.add("Norma M30 MLP3");
        listaNomi.add("Oreca 07 LMP2");
        listaNomi.add("Panoz AIV Roadster");
        listaNomi.add("Porsche 911 GT3");
        listaNomi.add("Porsche 911 RSR");
        listaNomi.add("Radical SR3-RXX");
        listaNomi.add("Renault Clio");
        listaNomi.add("Stockcar 2015");
        listaNomi.add("Tatuus USF-17");
        listaNomi.add("Tatuus pm-18");
        listaNomi.add("Tatuus f1-T014");
        listaNomi.add("Tatuus f3-T318");
        listaNomi.add("USF2000");
        listaNomi.add("Hyundai i30n");
        Collections.sort(listaNomi);
        return getListaMacchinaDaListaNomi(listaNomi);
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//##################################################################################################
//                                  Classe per la associazione nome auto to id
//##################################################################################################
    static class AssociazioneNomeIdImmagine{
        private static HashMap<String,Integer> associazioneNomeId=null;
//--------------------------------------------------------------------------------------------------
//                                  Inizializzazione unica per tutta l'esecuzione del'app.
//--------------------------------------------------------------------------------------------------
        private static void init(){
            associazioneNomeId = new HashMap();
            associazioneNomeId.put("seat leon",                     R.drawable.car_seat_leon);
            associazioneNomeId.put("honda civic",                   R.drawable.car_honda_civic);
            associazioneNomeId.put("peugeot 308",                   R.drawable.car_peugeot_308);
            associazioneNomeId.put("vw golf",                       R.drawable.car_vw_golf);
            associazioneNomeId.put("alfa romeo giulietta",          R.drawable.car_alfa_romeo_giulietta);
            associazioneNomeId.put("ac 427 sc",                     R.drawable.car_ac_427_sc);
            associazioneNomeId.put("aston martin vantage gt3",      R.drawable.car_aston_martin_vantage_gt3);
            associazioneNomeId.put("aston martin vantage gte",      R.drawable.car_aston_martin_vantage_gte);
            associazioneNomeId.put("audi lms gt3",                  R.drawable.car_audi_lms_gt3);
            associazioneNomeId.put("bentley continental gt3",       R.drawable.car_bentley_continental_gt3);
            associazioneNomeId.put("bmw m2 cs racing",              R.drawable.car_bmw_m2_cs_racing);
            associazioneNomeId.put("bmw m6 gt3",                    R.drawable.car_bmw_m6_gt3);
            associazioneNomeId.put("bmw m8 gte",                    R.drawable.car_bmw_m8_gte);
            associazioneNomeId.put("brabham bt20",                  R.drawable.car_brabham_bt20);
            associazioneNomeId.put("brabham bt44b",                 R.drawable.car_brabham_bt44b);
            associazioneNomeId.put("callaway corvette c7 gt3-r",    R.drawable.car_callaway_corvette_c7_gt3_r);
            associazioneNomeId.put("chevrolet camaro gt3 2012",     R.drawable.car_chevrolet_camaro_gt3_2012);
            associazioneNomeId.put("chevrolet corvette c6r gt2",    R.drawable.car_chevrolet_corvette_c6r_gt2);
            associazioneNomeId.put("chevrolet corvette c7r",        R.drawable.car_chevrolet_corvette_c7r);
            associazioneNomeId.put("dallara dw12",                  R.drawable.car_dallara_dw12);
            associazioneNomeId.put("ferrari 488 gte",               R.drawable.car_ferrari_488_gte);
            associazioneNomeId.put("formula e 2018",                R.drawable.car_formula_e_2018);
            associazioneNomeId.put("formula e gen 2 2019-2020",     R.drawable.car_formula_e_gen_2_2019_2020);
            associazioneNomeId.put("formula isi 2012",              R.drawable.car_formula_isi_2012);
            associazioneNomeId.put("formula renault35",             R.drawable.car_formula_renault35);
            associazioneNomeId.put("formula vee",                   R.drawable.car_formula_vee);
            associazioneNomeId.put("honda civic btcc",              R.drawable.car_honda_civic_btcc);
            associazioneNomeId.put("kartcup",                       R.drawable.car_kartcup);
            associazioneNomeId.put("marussia mr01 2012",            R.drawable.car_marussia_mr01_2012);
            associazioneNomeId.put("mclaren 650s gt3",               R.drawable.car_mclaren_650s_gt3);
            associazioneNomeId.put("mclaren 720s gt3",              R.drawable.car_mclaren_720s_gt3);
            associazioneNomeId.put("mclaren senna gtr",             R.drawable.car_mclaren_senna_gtr);
            associazioneNomeId.put("mclaren m23",                   R.drawable.car_mclaren_m23);
            associazioneNomeId.put("mclaren mp4/8",                 R.drawable.car_mclaren_mp4_8);
            associazioneNomeId.put("mclaren mp4/13",                R.drawable.car_mclaren_mp4_13);
            associazioneNomeId.put("mercedes amg gt3",              R.drawable.car_mercedes_amg_gt3);
            associazioneNomeId.put("nissan gt500",                  R.drawable.car_nissan_gt500);
            associazioneNomeId.put("nissan gtr",                    R.drawable.car_nissan_gtr);
            associazioneNomeId.put("norma m30 mlp3",                R.drawable.car_norma_m30_mlp3);
            associazioneNomeId.put("oreca 07 lmp2",                 R.drawable.car_oreca_07_lmp2);
            associazioneNomeId.put("panoz aiv roadster",            R.drawable.car_panoz_aiv_roadster);
            associazioneNomeId.put("porsche 911 gt3",               R.drawable.car_porsche_911_gt3);
            associazioneNomeId.put("porsche 911 rsr",               R.drawable.car_porsche_911_rsr);
            associazioneNomeId.put("radical sr3-rxx",               R.drawable.car_radical_sr3_rxx);
            associazioneNomeId.put("renault clio",                  R.drawable.car_renault_clio);
            associazioneNomeId.put("stockcar 2015",                 R.drawable.car_stockcar_2015);
            associazioneNomeId.put("tatuus usf-17",                 R.drawable.car_tatuus_usf_17);
            associazioneNomeId.put("tatuus pm-18",                  R.drawable.car_tatuus_pm_18);
            associazioneNomeId.put("tatuus f1-t014",                R.drawable.car_tatuus_f1_t014);
            associazioneNomeId.put("tatuus f3-t318",                R.drawable.car_tatuus_f3_t318);
            associazioneNomeId.put("usf2000",                       R.drawable.car_usf2000);
            associazioneNomeId.put("hyundai i30n",                  R.drawable.car_hyundai_i30n);
        }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//                                  Restituisce l'id associato al nomepassato
//--------------------------------------------------------------------------------------------------
        /**Restituisce il valore a cui è mappata la chiave specificata o nullse questa mappa non contiene
         * alcuna mappatura per la chiave
         */
        public static Integer getIdImmagineMacchina(String nome){
            if(associazioneNomeId==null){
                init();
            }
            return associazioneNomeId.get(nome.toLowerCase());
        }
    }
//##################################################################################################
}
