package com.example.simcareerapp.GestioneMyIntentEPersistableBundle;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.Interface.CostantiInserireEscrarreDatiDaiInternt;
import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.CircuitoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Foto;
import com.example.simcareerapp.Persistence.Entities.Indirizzo;
import com.example.simcareerapp.Persistence.Entities.Utente;

import java.sql.Date;

public class MyIntent extends Intent implements CostantiInserireEscrarreDatiDaiInternt {
//==================================================================================================
//                                  COSTRUTTORI
//==================================================================================================
    public MyIntent() {
    }
//--------------------------------------------------------------------------------------------------
    public MyIntent(Intent o) {
        super(o);
    }
//--------------------------------------------------------------------------------------------------
    public MyIntent(String action) {
        super(action);
    }
//--------------------------------------------------------------------------------------------------
    public MyIntent(String action, Uri uri) {
        super(action, uri);
    }
//--------------------------------------------------------------------------------------------------
    public MyIntent(Context packageContext, Class<?> cls) {
        super(packageContext, cls);
    }
//--------------------------------------------------------------------------------------------------
    public MyIntent(String action, Uri uri, Context packageContext, Class<?> cls) {
        super(action, uri, packageContext, cls);
    }
//==================================================================================================
//==================================================================================================
//                                  GET E SET DEI PARAMTRI COPLESSI DA UN INTENT
//==================================================================================================
//--------------------------------------------------------------------------------------------------
//                                  GET E SET UTENTE
//--------------------------------------------------------------------------------------------------
    /**
     * Questo metodo scompone l'oggeto utente e lo inserisce sottoforama di oggeti di base nei parametri
     * passati con gli intent.
     * Si noti che c'è in conda ai identificativi delle vari identificativi una variabile cosi da
     * permettere eventualmete di inserire più oggeti di tipo utente.
     */
    public MyIntent putExtraUtente(String nome,Utente utente){
        if(utente!=null){
            this.putExtra(I_UTENTE_ID                       +"_"+nome,  utente.getId());
            this.putExtra(I_UTENTE_NOME                     +"_"+nome,  utente.getNome());
            this.putExtra(I_UTENTE_COGNOME                  +"_"+nome,  utente.getCognome());
            this.putExtra(I_UTENTE_DATA_NASCITA             +"_"+nome,  utente.getDataNascita().getTime());
            this.putExtra(I_UTENTE_EMAIL                    +"_"+nome,  utente.getEmail());
            this.putExtra(I_UTENTE_USERNAME                 +"_"+nome,  utente.getUsername());
            this.putExtra(I_UTENTE_SALT                     +"_"+nome,  utente.getSalt());
            this.putExtra(I_UTENTE_PASSWORD                 +"_"+nome,  utente.getPassword());
//--------------------------------
            Indirizzo indirizzo = utente.getResidenza();
            if(indirizzo!=null){
                this.putExtra(I_UTENTE_RESIDENZA_STATO          +"_"+nome,  indirizzo.getStato());
                this.putExtra(I_UTENTE_RESIDENZA_REGIONE        +"_"+nome,  indirizzo.getRegione());
                this.putExtra(I_UTENTE_RESIDENZA_CITTA          +"_"+nome,  indirizzo.getCitta());
                this.putExtra(I_UTENTE_RESIDENZA_VIA_PIAZZA     +"_"+nome,  indirizzo.getViaPiazza());
            }
//--------------------------------
            Integer numeroDiGara = utente.getNumeroDiGara();
            if(numeroDiGara!=null){
                this.putExtra(I_UTENTE_NUMERO_GARA              +"_"+nome, numeroDiGara );
            }
//--------------------------------
            AutoConImmagini autoPreferita = utente.getAutoPreferita();
            if(autoPreferita!=null){
                this.putExtra(I_UTENTE_AUTO_PREF_NOME           +"_"+nome,  autoPreferita.getNome());
                this.putExtra(I_UTENTE_AUTO_PREF_ID_IMG         +"_"+nome,  autoPreferita.getIdImmagine());
            }
//--------------------------------
            CircuitoConImmagini circuitoPreferita = utente.getCircuitoPreferito();
            if(circuitoPreferita!=null) {
                this.putExtra(I_UTENTE_CIRCUITO_PREF_NOME + "_" + nome, circuitoPreferita.getNome());
                this.putExtra(I_UTENTE_CIRCUITO_PREF_ID_IMG + "_" + nome, circuitoPreferita.getIdImmagine());
            }
//--------------------------------
            CircuitoConImmagini circuitoOdiato = utente.getCircuitoOdiato();
            if(circuitoOdiato!=null) {
                this.putExtra(I_UTENTE_CIRCUITO_ODIATO_NOME     +"_"+nome, circuitoOdiato.getNome());
                this.putExtra(I_UTENTE_CIRCUITO_ODIATO_ID_IMG   +"_"+nome, circuitoOdiato.getIdImmagine());
            }
        }
        return this;
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Questo metodo ricompone l'oggeto utente e estraendo sottoforama di oggeti di base nei parametri
     * contenuti con gli intent.
     * Si noti che c'è in conda ai identificativi delle vari identificativi una variabile cosi da
     * permettere eventualmete di riconoscere e distingere un oggeto di tipo utente da altri.
     */
    public static Utente getUtenteExtra(Intent intent,String nome){
        Utente utente =null;
        if(intent.getStringExtra(I_UTENTE_NOME           +"_"+nome )!=null) {
            utente = new Utente();
//--------------------------------
            utente.setId(intent.getLongExtra(                   I_UTENTE_ID + "_" + nome, -1));
            utente.setNome(intent.getStringExtra(               I_UTENTE_NOME + "_" + nome));
            utente.setCognome(intent.getStringExtra(            I_UTENTE_COGNOME + "_" + nome));
            utente.setDataNascita(new Date(intent.getLongExtra( I_UTENTE_DATA_NASCITA + "_" + nome, 0)));
            utente.setUsername(intent.getStringExtra(           I_UTENTE_USERNAME + "_" + nome));
            utente.setEmail(intent.getStringExtra(              I_UTENTE_EMAIL + "_" + nome));
            utente.setSalt(intent.getStringExtra(               I_UTENTE_SALT + "_" + nome));
            utente.setPasswordHashed(intent.getStringExtra(     I_UTENTE_PASSWORD + "_" + nome));
//--------------------------------
            Indirizzo indirizzo = null;
            if(intent.getStringExtra(I_UTENTE_RESIDENZA_STATO + "_" + nome)!=null){
                indirizzo = new Indirizzo(
                        intent.getStringExtra(I_UTENTE_RESIDENZA_STATO + "_" + nome),
                        intent.getStringExtra(I_UTENTE_RESIDENZA_REGIONE + "_" + nome),
                        intent.getStringExtra(I_UTENTE_RESIDENZA_CITTA + "_" + nome),
                        intent.getStringExtra(I_UTENTE_RESIDENZA_VIA_PIAZZA + "_" + nome)
                );
            }
            utente.setResidenza(indirizzo);
//--------------------------------
            Integer numGara = intent.getIntExtra(I_UTENTE_NUMERO_GARA + "_" + nome, -1);
            if(numGara<0){
                numGara= null;
            }
            utente.setNumeroDiGara(numGara);
//--------------------------------
            AutoConImmagini autoPreferita = null;
            if(intent.getStringExtra(I_UTENTE_AUTO_PREF_NOME + "_" + nome)!=null){
                autoPreferita = new AutoConImmagini(
                    intent.getStringExtra(I_UTENTE_AUTO_PREF_NOME + "_" + nome),
                    intent.getIntExtra(I_UTENTE_AUTO_PREF_ID_IMG + "_" + nome, -1)
                );
            }
            utente.setAutoPreferita(autoPreferita);
//--------------------------------
            CircuitoConImmagini circuitoPreferita = null;
            if(intent.getStringExtra(I_UTENTE_CIRCUITO_PREF_NOME + "_" + nome)!=null){
                 circuitoPreferita = new CircuitoConImmagini(
                        intent.getStringExtra(I_UTENTE_CIRCUITO_PREF_NOME + "_" + nome),
                        intent.getIntExtra(I_UTENTE_CIRCUITO_PREF_ID_IMG + "_" + nome, -1)
                );
            }
            utente.setCircuitoPreferito(circuitoPreferita);
//--------------------------------
            CircuitoConImmagini circuitoOdiato = null;
            if(intent.getStringExtra(I_UTENTE_CIRCUITO_ODIATO_NOME + "_" + nome)!=null){
                 circuitoOdiato = new CircuitoConImmagini(
                        intent.getStringExtra(I_UTENTE_CIRCUITO_ODIATO_NOME + "_" + nome),
                        intent.getIntExtra(I_UTENTE_CIRCUITO_ODIATO_ID_IMG + "_" + nome, -1)
                );
            }
            utente.setCircuitoOdiato(circuitoOdiato);
        }
        return utente;
    }
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//                                  GET E SET FOTO
//--------------------------------------------------------------------------------------------------
    /**
     * Questo metodo scompone l'oggeto foto e lo inserisce sottoforama di oggeti di base nei parametri
     * passati con gli intent.
     * Si noti che c'è in conda ai identificativi delle vari identificativi una variabile cosi da
     * permettere eventualmete di inserire più oggeti di tipo foto.
     */
    public MyIntent putExtraFoto(String nome, Foto foto){
        if(foto!=null) {
            this.putExtra(I_FOTO_ID + "_" + nome, foto.getId());
            this.putExtra(I_FOTO_ID_UTENTE + "_" + nome, foto.getIdUtente());
            this.putExtra(I_FOTO_TESTO_ALTERNATIVO + "_" + nome, foto.getTestoAlternativo());
            this.putExtra(I_FOTO_STATO_ATTIVO + "_" + nome, foto.getStatoAttivo());
            this.putExtra(I_FOTO_URL_FILE + "_" + nome, foto.getUrlFile());
        }
        return this;
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Questo metodo ricompone l'oggeto Foto e estraendo sottoforama di oggeti di base nei parametri
     * contenuti con gli intent.
     * Si noti che c'è in conda ai identificativi delle vari identificativi una variabile cosi da
     * permettere eventualmete di riconoscere e distingere un oggeto di tipo Foto da altri.
     */
    public static Foto getFotoExtra(Intent intent,String nome){
        Foto foto =null;
        if(     (intent.getStringExtra(I_FOTO_URL_FILE    +"_"+nome )!=null)            &&
                (intent.getLongExtra(I_FOTO_ID    +"_"+nome ,-1)!=-1)       &&
                (intent.getLongExtra(I_FOTO_ID_UTENTE    +"_"+nome,-1)!=-1)  ) {
            foto = new Foto();
//--------------------------------
            foto.setId(                 intent.getLongExtra(    I_FOTO_ID                   + "_" + nome,-1));
            foto.setIdUtente(           intent.getLongExtra(    I_FOTO_ID_UTENTE            + "_" + nome,-1));
//--------------------------------
            foto.setTestoAlternativo(   intent.getStringExtra(  I_FOTO_TESTO_ALTERNATIVO    + "_" + nome));
            foto.setStatoAttivo(        intent.getBooleanExtra( I_FOTO_STATO_ATTIVO         + "_" + nome, false));
            foto.setUrlFile(            intent.getStringExtra(  I_FOTO_URL_FILE             + "_" + nome));
//--------------------------------
        }
        return foto;
    }
//--------------------------------------------------------------------------------------------------
//==================================================================================================
}
