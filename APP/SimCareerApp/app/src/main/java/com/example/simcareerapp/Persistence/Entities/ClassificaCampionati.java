package com.example.simcareerapp.Persistence.Entities;

import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;

import java.util.List;

public class ClassificaCampionati {
//##################################################################################################
//                                  INFORMAZIONI CLASSIFICA
//##################################################################################################
    protected Long    id      = null;
    protected String  name    = null;
    protected String  logoURL = null;
//##################################################################################################
//##################################################################################################
//                                  CLASSIFICA PILOTA
//##################################################################################################
    protected List<PilotaConPunti> classificaPiloti = null;
//##################################################################################################
//##################################################################################################
//                                  CLASSIFICA TEAM
//##################################################################################################
    protected List<TeamConPunti> classificaTeam   = null;
//##################################################################################################
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
    public ClassificaCampionati() {
    }
//--------------------------------------------------------------------------------------------------
    public ClassificaCampionati(Long id,
                                String name,
                                String logoURL,
                                List<PilotaConPunti> classificaPiloti,
                                List<TeamConPunti> classificaTeam) {
        this.id = id;
        this.name = name;
        this.logoURL = logoURL;
        this.classificaPiloti = classificaPiloti;
        this.classificaTeam = classificaTeam;
    }
//==================================================================================================



//==================================================================================================
//                              GETTER E SETTER
//==================================================================================================
    public Long getId() {
        return id;
    }
//--------------------------------------
    public void setId(Long id) {
        this.id = id;
    }
//--------------------------------------------------------------------------------------------------
    public String getName() {
        return name;
    }
//--------------------------------------
    public void setName(String name) {
        this.name = name;
    }
//--------------------------------------------------------------------------------------------------
    public String getLogoURL() {
        return logoURL;
    }
//--------------------------------------
    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }
//--------------------------------------------------------------------------------------------------
    public List<PilotaConPunti> getClassificaPiloti() {
        return classificaPiloti;
    }
//--------------------------------------
    public void setClassificaPiloti(List<PilotaConPunti> classificaPiloti) {
        this.classificaPiloti = classificaPiloti;
    }
//--------------------------------------------------------------------------------------------------
    public List<TeamConPunti> getClassificaTeam() {
        return classificaTeam;
    }
//--------------------------------------
    public void setClassificaTeam(List<TeamConPunti> classificaTeam) {
        this.classificaTeam = classificaTeam;
    }
//==================================================================================================
//==================================================================================================
//                              VERIFICA CHE NON SIA UN OGGETO CON PARAMETRI NULL
//==================================================================================================
public boolean isNull(){
    return !(   (getId()!=null) &&
            (getName()!=null) &&
            (getLogoURL()!=null) &&
            (getClassificaPiloti()!=null) &&
            (getClassificaPiloti().size()!=0)&&
            (getClassificaTeam()!=null) &&
            (getClassificaTeam().size()!=0)  );
}
//==================================================================================================
}
