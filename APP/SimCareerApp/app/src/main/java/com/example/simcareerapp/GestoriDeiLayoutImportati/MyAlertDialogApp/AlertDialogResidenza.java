package com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;


import java.util.List;
import java.util.Locale;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp.Interface.OnAletDialogResidenzaEventListener;
import com.example.simcareerapp.R;

import com.example.simcareerapp.Persistence.Entities.Indirizzo;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyGPS.GPSTracker;


public class AlertDialogResidenza extends MyGenericAlertDialog {
    private EditText viewStato;
    private EditText viewRegione;
    private EditText viewCitta;
    private EditText viewViaPiazza;
    private ProgressBar viewLoading;
//==================================================================================================
//                                  CREAZIONE DEL ALERTDIALOG
//==================================================================================================
    /**
     * Questo è il cortutore a deve avere un oggeto indirizzo per inizializare
     */
    public AlertDialogResidenza( Context context, Indirizzo indirizzo, int idAlertDialog, int idIcon, int idTitle, int idBtnPrimario, int idBtnSecondario) {
        super(context, idAlertDialog, idIcon, idTitle, idBtnPrimario, idBtnSecondario);
        viewLoading = view.findViewById(R.id.prb_Loading);
        //Dove inserire l'indirizzo
        viewStato       = view.findViewById(R.id.txt_InputStato);
        viewRegione     = view.findViewById(R.id.txt_InputRegione);
        viewCitta       = view.findViewById(R.id.txt_InputCitta);
        viewViaPiazza   = view.findViewById(R.id.txt_InputViaPiazza);
        if(indirizzo!=null){
            impostaValoriInputDaIndirizzo(indirizzo);
        }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  LISTENER DEI BOTTONI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Button btnMiaPosizione= (Button) super.view.findViewById(R.id.btn_MiaPosizione);
        btnMiaPosizione.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trovaIlMioIndirizzoDaGPS(v);
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
//==================================================================================================


//==================================================================================================
//                                  METODI CHIAMATI ALLA PRESIONE DEI BOTTONI
//==================================================================================================
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante Conferma del AlertDialoge
     * imposta l'indirizzo
     */
    @Override
    protected void listenerButtonPrimario(View v){
        String stato     = ((EditText) view.findViewById(R.id.txt_InputStato)).getText().toString();
        String regione   = ((EditText) view.findViewById(R.id.txt_InputRegione)).getText().toString();
        String citta     = ((EditText) view.findViewById(R.id.txt_InputCitta)).getText().toString();
        String viaPiazza = ((EditText) view.findViewById(R.id.txt_InputViaPiazza)).getText().toString();
        Indirizzo indirizzo = new Indirizzo(stato,regione,citta,viaPiazza);
        if( ((OnAletDialogResidenzaEventListener)context).impostaIndirizzoDiResidenza(indirizzo) ){
            this.cancel();
        }
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante mia posizione del AlertDialog e
     * recupera il mio indirizo dal gps.
     */
    protected void trovaIlMioIndirizzoDaGPS(View v){
        viewLoading.setVisibility(View.VISIBLE);
        GPSTracker g = new GPSTracker(context);
        Location posizione=g.getPosizizione();
        if(posizione!=null){
            Indirizzo indirizzo = getIndirizzoDaCordinateGPS(posizione.getLatitude(),posizione.getLongitude());
            if(indirizzo!=null){
                impostaValoriInputDaIndirizzo(indirizzo);
            }
        }
        viewLoading.setVisibility(View.GONE);
    }
//==================================================================================================



//==================================================================================================
//                                  METODI PER IL RECUPERO DEL INDIRIZZAO DAL GPS
//==================================================================================================
    /**
     * Questo metodo dato le cordinate GPS restituisce, un oggeto indirizzo che contiene l'indirizzo
     * della posizione attuale, oppure restituisce null se non riesce a trovarlo.
     */
    Indirizzo getIndirizzoDaCordinateGPS(double latitude,double longitude){
        Indirizzo ris;
        Geocoder geocoder;
        List<Address> addresses;
        Address address;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String stato = addresses.get(0).getCountryName();
            String regione = addresses.get(0).getAdminArea();
            String citta = addresses.get(0).getLocality();
            //String postalCode = addresses.get(0).getPostalCode();
            String viaPiazza =addresses.get(0).getThoroughfare();
            viaPiazza +=" "+  addresses.get(0).getFeatureName();

            ris = new Indirizzo(stato,regione,citta,viaPiazza);
            Log.d("MyTAG ", "ris= "+ris.getIndirizzoCompleto());
        }catch (Exception e){
            Log.d("MyTAG ", "Lat= "+latitude+" , Lon="+longitude+" /n "+e.toString() );
            ris = null;
        }
        return ris;
    }
//==================================================================================================



//==================================================================================================
//                                  METODO PER VISUALIZZARE L'INDIRIZZO
//==================================================================================================

    /**
     * Metodo che imposta i valori già inseriti una volta, nei capi di text editor per poterli eventualmete
     * modificarli, così da evitare di riscriverli.
     */
    private void impostaValoriInputDaIndirizzo(Indirizzo indirizzo){
        viewStato.setText(      indirizzo.getStato());
        viewRegione.setText(    indirizzo.getRegione());
        viewCitta.setText(      indirizzo.getCitta());
        viewViaPiazza.setText(  indirizzo.getViaPiazza());
    }
//==================================================================================================


//==================================================================================================
//                                  METODI PER IL RECUPERO DEL GPS
//==================================================================================================

//==================================================================================================
}

