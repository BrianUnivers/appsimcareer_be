package com.example.simcareerapp.FotoGalleria;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.simcareerapp.FotoGalleria.ZoomMove.MyOnTouchListener;
import com.example.simcareerapp.FotoGalleria.ZoomMove.ZoomListener;
import com.example.simcareerapp.GestioneAssetPerGalleria.GestioneFileECartelleGalleria;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface.OnClickFotoGalleriaListener;
import com.example.simcareerapp.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FotoGalleriaFragment#} factory method to
 * create an instance of this fragment.
 */
public class FotoGalleriaFragment extends Fragment {
    /**
     * listener è la variabile che permette di comunicare con l'Activity
     * View     è la variabile che permette di ricavaare le view dai id
     */
    private OnClickFotoGalleriaListener listener = null;
    private View viewGroupLayout = null;
    private ImageView imageView =null;
    private RelativeLayout relativeLayout=null;
    private MyOnTouchListener onTouchListenernew=null;
    //##################################################################################################
//                              DATI PASSATI
//##################################################################################################
    private GestioneFileECartelleGalleria galleria   = null;
    private String folder = null;
    private String file = null;
    private Bitmap foto = null;
//==================================================================================================
//                                  COSTRUTORI
//==================================================================================================
    public FotoGalleriaFragment(GestioneFileECartelleGalleria galleria,String folder,String file) {
        this.galleria   = galleria;
        this.folder = folder;
        this.file   = file;
    }
//==================================================================================================
//##################################################################################################


//##################################################################################################
//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
//==================================================================================================
//==================================================================================================
//                                  DEFINIZIONE DEL LAYOUT DEL FRAGMENT
//==================================================================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);//<<<-------------------------------------ATTEZIONE

        //Scelta del layout usato per fragment.
        viewGroupLayout = inflater.inflate(R.layout.fragment_foto_galleria, container, false);

        relativeLayout = viewGroupLayout.findViewById(R.id.rly_PageFoto);

        foto = galleria.getBitmapFoto(folder,file);
        imageView = ((ImageView)viewGroupLayout.findViewById(R.id.img_Foto));
        imageView.setImageBitmap(foto);

        //Metodi per lo Zoom e Move
        onTouchListenernew = new MyOnTouchListener(this.getContext(),relativeLayout,imageView);
        imageView.setOnTouchListener(onTouchListenernew);


        return viewGroupLayout;
    }

//==================================================================================================
//##################################################################################################
    private int xDelta;
    private int yDelta;


//##################################################################################################
//                                  COMUNICAZIONE CON L'ACTIVITY
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY
//==================================================================================================
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Crea il link con l'activity che contiene il fragment.
        listener = (OnClickFotoGalleriaListener) activity;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  ROTAZIONE IMMAGINE
//##################################################################################################
//==================================================================================================
//                                  ROTAZIONE A DESTRA DEL IMMAGINE
//==================================================================================================
    public void ruotaImmagineADestra(){
        ruotaImmagine(90);
    }
//==================================================================================================
//==================================================================================================
//                                  ROTAZIONE A SINISTRA DEL IMMAGINE
//==================================================================================================
    public void ruotaImmagineASinistra(){
        ruotaImmagine(270);
    }
//==================================================================================================
//==================================================================================================
//                                  ROTAZIONE DEL IMMAGINE
//==================================================================================================
    private void ruotaImmagine(int degree ){
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(foto, foto.getWidth(), foto.getHeight(), true);
        foto = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        imageView.setImageBitmap(foto);
        onTouchListenernew.reset();
    }
//==================================================================================================
//##################################################################################################
}