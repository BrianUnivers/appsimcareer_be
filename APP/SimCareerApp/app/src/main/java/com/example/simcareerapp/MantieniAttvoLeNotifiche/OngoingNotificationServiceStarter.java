package com.example.simcareerapp.MantieniAttvoLeNotifiche;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.simcareerapp.AppCompatActivityWithMenu.MyServiceDiNotifiche.MyJobServiceNotificheUtente;

/**
 * Questa classe è un servizio che viene avviato automaticamete alla chiusura dell'app così che le
 * le notifiche rimangono aperte anche dopo.
 */
public class OngoingNotificationServiceStarter extends BroadcastReceiver {
//==================================================================================================
//                                  MANTIENI ATTIVE LE NOTIFICHE DOPO LA CHIUSURA DELL'APP
//==================================================================================================
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, MyJobServiceNotificheUtente.class);
        context.startService(i);
    }
//==================================================================================================
}