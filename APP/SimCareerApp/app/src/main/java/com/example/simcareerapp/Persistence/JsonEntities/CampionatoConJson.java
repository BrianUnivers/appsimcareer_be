package com.example.simcareerapp.Persistence.JsonEntities;

import android.util.Log;

import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.CircuitoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Consumi;
import com.example.simcareerapp.Persistence.Entities.Gara;
import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;
import com.example.simcareerapp.Persistence.Entities.Pilota;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class CampionatoConJson extends Campionato  {
//##################################################################################################
//                                  ETICHETTE PER LA GESTIONE DEL JSON
//##################################################################################################
    private final String LABLE_ID                 = "id";
    private final String LABLE_NOME               = "nome";
    private final String LABLE_LOGO               = "logo";
    private final String LABLE_CALENDARIO         = "calendario";
    private final String LABLE_IMPOSTAZIONI_GIOCO = "impostazioni-gioco";
    private final String LABLE_LISTA_AUTO         = "lista-auto";
    private final String LABLE_PILOTI_ISCRITTI    = "piloti-iscritti";
//----------------------------------IMPOSTAZIONI DI GIOCO NEL DETTAGLIO
    private final String LABLE_TIPO   = "tipo";
    private final String LABLE_VALORE = "valore";
//##################################################################################################
//##################################################################################################
//                                  VALORI DELLE IMPOSTAZIONI DI GIOCO
//##################################################################################################
//----------------------------------TIPO
    private final String TIPO_BANDIERE           = "Bandiere";
    private final String TIPO_CONSUMO_CARBURANTE = "Consumo carburante";
    private final String TIPO_CONSUMO_GOMME      = "Consumo gomme";
    private final String TIPO_AIUTI_ALLA_GUIDA   = "Aiuti alla guida";
//----------------------------------VALORE
    private final String VALORE_BASSI   = "Bassi";
    private final String VALORE_NORMALE = "Normale";
    private final String VALORE_ELEVATI = "Elevati";
//##################################################################################################
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
    public CampionatoConJson(Long id,
                             String name,
                             String logoURL,
                             List<Gara> listaGara,
                             String bandire,
                             String aiuti,
                             Consumi consumiCarburante,
                             Consumi consumiGomme,
                             List<ValoreConImmaginePerSpinner> listaAuto,
                             List<Pilota> listaPiloti) {
        super(  id,                 name,           logoURL,
                listaGara,          bandire,        aiuti,
                consumiCarburante,  consumiGomme,   listaAuto, listaPiloti);
    }
//--------------------------------------------------------------------------------------------------
    public CampionatoConJson(JSONObject jsonObject) {
        if(jsonObject!=null) {
            try {
                this.setId(      jsonObject.getLong(   LABLE_ID   ) );
                this.setName(    jsonObject.getString( LABLE_NOME ) );
                this.setLogoURL( jsonObject.getString( LABLE_LOGO ) );

                //GESTIONE ARRAY PER LE GARE
                JSONArray jsonArrayGare = jsonObject.getJSONArray(LABLE_CALENDARIO);
                List<Gara> listaGare= new LinkedList<Gara>();
                for(int i=0; i<jsonArrayGare.length(); i++){
                    Gara gara= new GaraConJson(jsonArrayGare.getJSONObject(i));
                    if(!gara.isNull()){
                        listaGare.add(gara);
                    }
                }
                this.setListaGara(listaGare);

                //GESTIONE DELLE IMPOSTAZIONI DI GIOCO
                JSONArray jsonArrayImpostazioni = jsonObject.getJSONArray(LABLE_IMPOSTAZIONI_GIOCO);
                for(int i=0; i<jsonArrayImpostazioni.length(); i++){
                    gestisciTipoDiImpostazione(jsonArrayImpostazioni.getJSONObject(i));
                }

                //GESTIONE DELLA LISTA AUTO
                String stringaListaAuto = jsonObject.getString(LABLE_LISTA_AUTO);
                if( (stringaListaAuto!=null) && (!stringaListaAuto.equals(""))){
                    String[] arrrayNomiAuto = stringaListaAuto.split(",");
                    LinkedList<String> listaNomiAutoSenzaSpazzi = new LinkedList<String>();
                    for(int i=0;i<arrrayNomiAuto.length;i++){
                        listaNomiAutoSenzaSpazzi.add(arrrayNomiAuto[i].trim());
                    }
                    this.setListaAuto( AutoConImmagini.getListaMacchinaDaListaNomi(listaNomiAutoSenzaSpazzi) );
                }

                //GESTIONE ARRAY PER I PILOTI
                JSONArray jsonArrayPiloti = jsonObject.getJSONArray(LABLE_PILOTI_ISCRITTI);
                List<Pilota> listaPiloti = new LinkedList<Pilota>();
                for(int i=0; i<jsonArrayPiloti.length(); i++){
                    Pilota pilota= new PilotaConJson(jsonArrayPiloti.getJSONObject(i));
                    if(!pilota.isNull()){
                        listaPiloti.add(pilota);
                    }
                }
                this.setListaPiloti(listaPiloti);

            } catch (JSONException e) {
                Log.d("ErrJson",e.getMessage());
            }
        }
    }
//==================================================================================================
//==================================================================================================
//                              GESTIONE DEL JSON DI UNA IPOSTAZIONE DI GIOCO
//==================================================================================================
    private void gestisciTipoDiImpostazione(JSONObject jsonObject){
        if(jsonObject!=null){
            try {
                String tipoImpostazione=jsonObject.getString(LABLE_TIPO);
                String valoreImpostazione=jsonObject.getString(LABLE_VALORE);
                //CAPIRE DI CHE TIPO DI IMPOSTAZIONI SI TRATTA
                switch (tipoImpostazione){
                    case TIPO_BANDIERE:
                        this.abilitaBandiereDaStringa(valoreImpostazione);
                        break;
                    case TIPO_AIUTI_ALLA_GUIDA:
                        this.abilitaAiutiDaStringa(valoreImpostazione);
                        break;
                    case TIPO_CONSUMO_CARBURANTE:
                        this.setConsumiCarburante(super.convetiStingaInConsumi(valoreImpostazione));
                        break;
                    case TIPO_CONSUMO_GOMME:
                        this.setConsumiGomme(super.convetiStingaInConsumi(valoreImpostazione));
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
//==================================================================================================

//##################################################################################################
//                              GENERA JSON
//##################################################################################################
//==================================================================================================
//                              GENERA JSON CAMPIONATO
//==================================================================================================
    public JSONObject getJSON(){
        JSONObject object = new JSONObject();
        try {
            object.put(LABLE_ID,        this.getId());
            object.put(LABLE_NOME,      this.getName());
            object.put(LABLE_LOGO,      this.getLogoURL());

            //GESTIONE ARRAY PER LE GARE
            JSONArray jsonArrayGare = new JSONArray();
            Iterator<Gara> garaIterator = this.getListaGara().iterator();
            int i=0;
            while (garaIterator.hasNext()){
                Gara gara = garaIterator.next();
                GaraConJson garaConJson = new GaraConJson(gara);
                JSONObject garaJSON = garaConJson.getJSON();
                if(garaJSON!=null){
                    jsonArrayGare.put(i,garaJSON);
                    i++;
                }
            }
            object.put(LABLE_CALENDARIO,jsonArrayGare);

            //GESTIONE DELLE IMPOSTAZIONI DI GIOCO
            JSONArray jsonArrayImpostazione = new JSONArray();
            jsonArrayImpostazione.put(0,this.getBandieraJSON());//SOLLEVA ECCEZIONE
            jsonArrayImpostazione.put(1,this.getConsumoCarburanteJSON());//SOLLEVA ECCEZIONE
            jsonArrayImpostazione.put(2,this.getConsumiGommeJSON());//SOLLEVA ECCEZIONE
            jsonArrayImpostazione.put(3,this.getAiutiAlleGuidaJSON());//SOLLEVA ECCEZIONE
            object.put(LABLE_IMPOSTAZIONI_GIOCO,jsonArrayImpostazione);

            //GESTIONE DELLA LISTA AUTO
            String listaNomiAuto =null;
            Iterator<ValoreConImmaginePerSpinner> autoIterator =this.getListaAuto().iterator();
            while (autoIterator.hasNext()){
                AutoConImmagini autoConImmagini = (AutoConImmagini) autoIterator.next();
                if(listaNomiAuto==null){
                    listaNomiAuto=""+autoConImmagini.getNome();
                }else{
                    listaNomiAuto+=", "+autoConImmagini.getNome();
                }
            }
            object.put(LABLE_LISTA_AUTO,listaNomiAuto);

            //GESTIONE ARRAY PER I PILOTI
            JSONArray jsonArrayPiloti = new JSONArray();
            Iterator<Pilota> pilotiIterator = this.getListaPiloti().iterator();
            i=0;
            while (pilotiIterator.hasNext()){
                PilotaConJson pilota = new PilotaConJson(pilotiIterator.next());
                JSONObject pilotaJSON = pilota.getJSON();
                if(pilotaJSON!=null){
                    jsonArrayPiloti.put(i,pilotaJSON);
                    i++;
                }
            }
            object.put(LABLE_PILOTI_ISCRITTI,jsonArrayPiloti);


        }catch (JSONException e) {
            object=null;
            e.printStackTrace();
        }
        return object;
    }
//==================================================================================================
//==================================================================================================
//                              JSON CARBURANTE
//==================================================================================================
private JSONObject getConsumoCarburanteJSON() throws JSONException {
    JSONObject consumoCarburanteJSON = new JSONObject();
    consumoCarburanteJSON.put(LABLE_TIPO, TIPO_CONSUMO_CARBURANTE);
    consumoCarburanteJSON.put(LABLE_VALORE,getStringConsumo(this.getConsumiCarburante()));
    return consumoCarburanteJSON;
}
//==================================================================================================
//==================================================================================================
//                              JSON GOMME
//==================================================================================================
    private JSONObject getConsumiGommeJSON() throws JSONException {
        JSONObject consumiGommeJSON = new JSONObject();
        consumiGommeJSON.put(LABLE_TIPO, TIPO_CONSUMO_GOMME);
        consumiGommeJSON.put(LABLE_VALORE,getStringConsumo(this.getConsumiGomme()));
        return consumiGommeJSON;
    }
//==================================================================================================
//==================================================================================================
//                              JSON BANDIERA
//==================================================================================================
    private JSONObject getBandieraJSON() throws JSONException{
        JSONObject bandiereJSON = new JSONObject();
        bandiereJSON.put(LABLE_TIPO,    TIPO_BANDIERE);
        String listaBandiera = "";
        if(this.bandiraBiancaDiscoArancio){
            listaBandiera=Campionato.BANDIERA_BIANCA_DISCO_ARANCIO;
        }
        if(this.bandiraBiancaENera){
            if(!listaBandiera.equals("")){
                listaBandiera+=", ";
            }
            listaBandiera+=Campionato.BANDIERA_BIANCA_NERA;
        }
        if(this.bandiraBlu){
            if(!listaBandiera.equals("")){
                listaBandiera+=", ";
            }
            listaBandiera+=Campionato.BANDIERA_BLU;
        }
        if(this.bandiraGialla){
            if(!listaBandiera.equals("")){
                listaBandiera+=", ";
            }
            listaBandiera+=Campionato.BANDIERA_GIALLA;
        }
        if(this.bandiraNera){
            if(!listaBandiera.equals("")){
                listaBandiera+=", ";
            }
            listaBandiera+=Campionato.BANDIERA_NERA;
        }
        if(this.bandiraRossa){
            if(!listaBandiera.equals("")){
                listaBandiera+=", ";
            }
            listaBandiera+=Campionato.BANDIERA_ROSSA;
        }
        if(this.bandiraStrisceGialleERosse){
            if(!listaBandiera.equals("")){
                listaBandiera+=", ";
            }
            listaBandiera+=Campionato.BANDIERA_STRISCE_GIALLE_ROSE;
        }
        if(this.bandiraVerde){
            if(!listaBandiera.equals("")){
                listaBandiera+=", ";
            }
            listaBandiera+=Campionato.BANDIERA_VERDE;
        }
        if(listaBandiera==null){
            listaBandiera="";
        }
        bandiereJSON.put(LABLE_VALORE,listaBandiera );
        return bandiereJSON;
    }
//==================================================================================================
//==================================================================================================
//                              JSON AIUTI ALLA GIUDA
//==================================================================================================
    private JSONObject getAiutiAlleGuidaJSON() throws JSONException{
        JSONObject aiutiAlleGuidaJSON = new JSONObject();
        aiutiAlleGuidaJSON.put(LABLE_TIPO,    TIPO_AIUTI_ALLA_GUIDA);
        String listaAiutiAlleGuida = "";
        if(this.statoAiutoAbs){
            listaAiutiAlleGuida=Campionato.AIUTO_ABS;
        }
        if(this.statoAiutoAutoretromarcia){
            if(!listaAiutiAlleGuida.equals("")){
                listaAiutiAlleGuida+=", ";
            }
            listaAiutiAlleGuida+=Campionato.AIUTO_AUTORETROMARCIA;
        }
        if(this.statoAiutoBloccagioOpposto){
            if(!listaAiutiAlleGuida.equals("")){
                listaAiutiAlleGuida+=", ";
            }
            listaAiutiAlleGuida+=Campionato.AIUTO_BLOCCAGGIO_OPPOSTO;
        }
        if(this.statoAiutoCambioAutomatico){
            if(!listaAiutiAlleGuida.equals("")){
                listaAiutiAlleGuida+=", ";
            }
            listaAiutiAlleGuida+=Campionato.AIUTO_CAMBIO_AUTOMATICO;
        }
        if(this.statoAiutoControlloStabilita){
            if(!listaAiutiAlleGuida.equals("")){
                listaAiutiAlleGuida+=", ";
            }
            listaAiutiAlleGuida+=Campionato.AIUTO_CONTROLLO_STABILITA;
        }
        if(this.statoAiutoControlloTrazione){
            if(!listaAiutiAlleGuida.equals("")){
                listaAiutiAlleGuida+=", ";
            }
            listaAiutiAlleGuida+=Campionato.AIUTO_CONTROLLO_TRAZIONE;
        }
        if(this.statoAiutoCorsiaBox){
            if(!listaAiutiAlleGuida.equals("")){
                listaAiutiAlleGuida+=", ";
            }
            listaAiutiAlleGuida+=Campionato.AIUTO_CORSIA_BOX;
        }
        if(this.statoAiutoFrenata){
            if(!listaAiutiAlleGuida.equals("")){
                listaAiutiAlleGuida+=", ";
            }
            listaAiutiAlleGuida+=Campionato.AIUTO_FRENATA;
        }
        if(this.statoAiutoFrizioneAutomatica){
            if(!listaAiutiAlleGuida.equals("")){
                listaAiutiAlleGuida+=", ";
            }
            listaAiutiAlleGuida+=Campionato.AIUTO_FRIZIONE_AUTOMATICA;
        }
        if(this.statoAiutoInvulnerabilita){
            if(!listaAiutiAlleGuida.equals("")){
                listaAiutiAlleGuida+=", ";
            }
            listaAiutiAlleGuida+=Campionato.AIUTO_INVULNERABILITA;
        }
        if(this.statoAiutoRipresaSpin){
            if(!listaAiutiAlleGuida.equals("")){
                listaAiutiAlleGuida+=", ";
            }
            listaAiutiAlleGuida+=Campionato.AIUTO_RIPRESA_SPIN;
        }
        if(this.statoAiutoSterzata){
            if(!listaAiutiAlleGuida.equals("")){
                listaAiutiAlleGuida+=", ";
            }
            listaAiutiAlleGuida+=Campionato.AIUTO_STERZATA;
        }

        if(listaAiutiAlleGuida==null){
            listaAiutiAlleGuida="";
        }
        aiutiAlleGuidaJSON.put(LABLE_VALORE,listaAiutiAlleGuida );
        return aiutiAlleGuidaJSON;
    }
//==================================================================================================
//##################################################################################################
//==================================================================================================
//                              JSON APPOGIO
//==================================================================================================
    private String getStringConsumo(Consumi consumi){
        String ris;
        switch (consumi){
            case BASSI:
                ris = VALORE_BASSI;
                break;
            case NORMALI:
                ris = VALORE_NORMALE;
                break;
            case ELEVATI:
                ris = VALORE_ELEVATI;
                break;
            default:
                ris = VALORE_NORMALE;
        }
        return ris;
    }
//==================================================================================================
}







