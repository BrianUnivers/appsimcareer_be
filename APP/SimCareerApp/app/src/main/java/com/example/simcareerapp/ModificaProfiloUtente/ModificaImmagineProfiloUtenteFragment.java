package com.example.simcareerapp.ModificaProfiloUtente;


import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.GestioneVariazioneAPI.GestioneVariazioneAPI;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.simcareerapp.R;
import com.example.simcareerapp.ModificaProfiloUtente.Interface.OnFragmentImmagineProfiloUtenteEventListener;



/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ModificaImmagineProfiloUtenteFragment} factory method to
 * create an instance of this fragment.
 */
public class ModificaImmagineProfiloUtenteFragment extends Fragment {
    /**
     * listener è la variabile che permette di comunicare con l'Activity
     * View     è la variabile che permette di ricavaare le view dai id
     */
    private OnFragmentImmagineProfiloUtenteEventListener listener        = null;
    private View                                         viewGroupLayout = null;

    //Cambiamenti immagini stato dei pulsnti
    private boolean selezionaCambiaFoto=false;
    FloatingActionButton btnCambiaFotoProfilo;
    FloatingActionButton btnCambiaConFotocamera;
    FloatingActionButton btnCambiaConGalleria;
    //Bottoni animati
    Animation animOpen;
    Animation animClose;
    //Immagine utente gestitta
    private ImageView imageUser;


    private static final int PERMESSI_FOTOCAMERE_CODICE = 101;
    private static final int FOTOCAMERE_REQUEST_CODE    = 102;
    private static final int GALLERY_REQUEST_CODE       = 105;
    private static final int WRITE_REQUEST_CODE         = 112;

    String currentPhotoPath;

    private static final int REQUEST_CODE = 123;
//==================================================================================================
//                                  COSTRUTORI
//==================================================================================================
    public ModificaImmagineProfiloUtenteFragment() {
        // Required empty public constructor
    }

//==================================================================================================


//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
//==================================================================================================

//==================================================================================================
//                                  DEFINIZIONE DEL LAYOUT DEL FRAGMENT
//==================================================================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);//<<<-------------------------------------ATTEZIONE

        //Scelta del layout usato per fragment.
        viewGroupLayout = inflater.inflate(R.layout.fragment_modifica_immagine_profilo_utente, container, false);

        //Inizializzazione ImageView del profilo utente.
        imageUser= (ImageView) viewGroupLayout.findViewById(R.id.img_User);

        //Imposta stato pulsanti cambia foto
        selezionaCambiaFoto=false; //Non stò mostrando i vari metodi per cambiare immagine
        btnCambiaFotoProfilo = (FloatingActionButton ) viewGroupLayout.findViewById(R.id.btn_CambiaFotoProfilo);
        btnCambiaConFotocamera = (FloatingActionButton ) viewGroupLayout.findViewById(R.id.btn_CambiaConFotocamera);
        btnCambiaConGalleria = (FloatingActionButton ) viewGroupLayout.findViewById(R.id.btn_CambiaConGalleria);

        //Animazioni
        animOpen = AnimationUtils.loadAnimation(getActivity(),R.anim.fab_open);
        animClose = AnimationUtils.loadAnimation(getActivity(),R.anim.fab_close);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  LISTENER DEI BOTTONI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Listener del pulsante apri/chiudi il menu di cambiamento foto
        btnCambiaFotoProfilo.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                chiediTuttiPermessi();
            }
        });
//--------------------------------------------------------------------------------------------------
        // Listener del pulsante che cambia foto tramite la fotocamera
        btnCambiaConFotocamera.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                buttonCambiaFotoProfiloConFotocamera(v);
                buttonCambiaFotoProfilo();//Ciude i pulsanti apparsi
            }
        });
//--------------------------------------------------------------------------------------------------
        // Listener del pulsante che cambia foto tramite la galleria
        btnCambiaConGalleria.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                buttonCambiaFotoProfiloConGalleria(v);
                buttonCambiaFotoProfilo();//Ciude i pulsanti apparsi
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //inizializazione immagine utente
        inizializazioneImmagineUtente();
        return viewGroupLayout;
    }
//==================================================================================================


//==================================================================================================
//                                  RICHIEDI TUTTI I PERMESSI CHE POSSONO SERVIRE PER CAMBIARE LA FOTO
//==================================================================================================
    private void chiediTuttiPermessi(){
        if( ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA) +
                ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) +
                ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){

            //Quanfo i permessi non sono garantiti
            if( (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) ||
                    (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) ||
                    (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE)) ){

                // Creazione del AlertDialog per richedere i permessi
                AlertDialog.Builder builder = new AlertDialog.Builder( getActivity(),R.style.AlertDialog );
                builder.setTitle(R.string.dlg_ttl_PermessiCambioImmagione);
                builder.setMessage(R.string.dlg_txt_PermessiCambioImmagione);
                builder.setPositiveButton(R.string.dlg_btn_Acceto, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(
                                getActivity(),new String[] {
                                        Manifest.permission.CAMERA,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                        Manifest.permission.READ_EXTERNAL_STORAGE
                                },REQUEST_CODE);
                    }
                });
                builder.setNegativeButton(R.string.dlg_btn_Nega,null);
                AlertDialog alertDialog = builder.create();

                alertDialog.show();

            }else{
                ActivityCompat.requestPermissions(
                        getActivity(),new String[] {
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        },REQUEST_CODE);
            }
        }else{
            //Quando i permessi sono ativi
            buttonCambiaFotoProfilo();
        }
    }
//==================================================================================================
//==================================================================================================

    //                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY
//==================================================================================================
    @Override
    public void onAttach(Activity activity)    {
        super.onAttach(activity);
        //Crea il link con l'activity che contiene il fragment.
        listener=(OnFragmentImmagineProfiloUtenteEventListener) activity;
    }
//==================================================================================================

//==================================================================================================
//                                  METODI CHIAMATI ALLA PRESIONE DEI BOTTONI
//==================================================================================================
    private void buttonCambiaFotoProfilo(){
        selezionaCambiaFoto=!selezionaCambiaFoto;
        if(!selezionaCambiaFoto){//é vero se FALSO !!
            // Ho gia premuto il tasto modifica foto e voglio rìchiudere il menu di scelta
            btnCambiaFotoProfilo.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));//Colori Bordi
            btnCambiaFotoProfilo.setImageResource(R.drawable.ic_baseline_create_24_white);//Icona
            btnCambiaConFotocamera.startAnimation(animClose);
            btnCambiaConGalleria.startAnimation(animClose);
            btnCambiaConFotocamera.setClickable(false);
            btnCambiaConGalleria.setClickable(false);
            //btnCambiaConFotocamera.setVisibility(View.INVISIBLE);
            //btnCambiaConGalleria.setVisibility(View.INVISIBLE);
        }else{
            // Voglio aprire il menu di scelta
            btnCambiaFotoProfilo.setBackgroundTintList(getResources().getColorStateList(R.color.colorSecondary));//Colori Bordi
            btnCambiaFotoProfilo.setImageResource(R.drawable.ic_baseline_close_24_primary);//Icona
            btnCambiaConFotocamera.startAnimation(animOpen);
            btnCambiaConGalleria.startAnimation(animOpen);
            btnCambiaConFotocamera.setClickable(true);
            btnCambiaConGalleria.setClickable(true);
            //btnCambiaConFotocamera.setVisibility(View.VISIBLE);
            //btnCambiaConGalleria.setVisibility(View.VISIBLE);
        }
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante cambia foto con cotocamera che
     * scatta una foto e la cambia nella view ed la passa all'Acttivity che la salvera, dopo
     * averne salvata una copia nella galleria.
     */
    private void buttonCambiaFotoProfiloConFotocamera(View v){
        //Controllo se mi ha già dato il permeso in precedenza altrimenti lo chiedo
        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
            //Se ho il permesso apro la macchina fotografica
            dispatchTakePictureIntent();
        }
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante cambia foto che prendera una foto
     * e la cambia nella view ed la passa all'Acttivity che la salvera.
     */
    private void buttonCambiaFotoProfiloConGalleria(View v){
        MyIntent galleryIntent = new MyIntent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY_REQUEST_CODE);
    }
//==================================================================================================


//==================================================================================================
//                                  METODI DI PER RENDERE L'IMMAGINE ROTONA
//==================================================================================================
    private void inizializazioneImmagineUtente(){
        File flieImg = listener.inizializazioneImmagineTramiteId();
        if(flieImg!=null){
            impostaImmagineCircolareDaFile(flieImg);
        }else{
            int resId = R.drawable.userdefault;// Immagine di default.
            impostaImmagineCircolareDaID(resId);
        }
    }
    //--------------------------------------------------------------------------------------------------
    private void  impostaImmagineCircolareDaID(int id){
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),id);
        impostaImmagineCircolareDaBitmap(bitmap);
    }
    //--------------------------------------------------------------------------------------------------
    private void  impostaImmagineCircolareDaFile(File f){
        String filePath = f.getPath();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        impostaImmagineCircolareDaBitmap(bitmap);
    }
    //--------------------------------------------------------------------------------------------------
    private void impostaImmagineCircolareDaBitmap(Bitmap imgBitmap){
        //Ruota solo se la foto non è verticale
        if(imgBitmap.getHeight()<imgBitmap.getWidth()){
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            imgBitmap = Bitmap.createBitmap(imgBitmap, 0, 0, imgBitmap.getWidth(), imgBitmap.getHeight(), matrix, true);
        }

        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(),imgBitmap);
        roundedBitmapDrawable.setCircular(true);
        imageUser.setImageDrawable(roundedBitmapDrawable);
    }
//==================================================================================================



//==================================================================================================
//                                  METODI DI PER L'USO DELLA FOTOCAMERA
//==================================================================================================
    /**
     * Effetua la foto
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent

        /**
         * ATTENZIONE!!!!!
         * ALCUNI DISPOSITIVI NON HANNO LA FOTOCAMERA O NON HANNO UN APP PER LE FOTO CHE
         * SIA VALIDA PER MediaStore.ACTION_IMAGE_CAPTURE
         */
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {//<------CONTROLLO#######################################
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = generaImmagineFile();
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = GestioneVariazioneAPI.getURIFromFile(getContext(), photoFile);
                    //Errore
                    /*Uri photoURI = FileProvider.getUriForFile(getActivity(),
                            "com.example.android.fileprovider",
                            photoFile);*/
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, FOTOCAMERE_REQUEST_CODE);
                }
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.d("MyTAG ", ex.getMessage());
                Toast.makeText(getActivity(), "Non è stata generata l'immagine", Toast.LENGTH_SHORT).show();

            } catch (Exception ex) {
                // Error occurred while creating the File
                Log.d("MyTAG ", ex.getMessage());
                Toast.makeText(getActivity(), "Non è stata rilevata nessuna App per effetuare foto.", Toast.LENGTH_SHORT).show();
            }
        }else {//<----------------------------------------------------------------------------------------CONTROLLO#######################################
            Toast.makeText(getActivity(), "Non è stata rilevata nessuna App per effetuare foto.", Toast.LENGTH_SHORT).show();
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Salvo una copia della foto appena fatta nella galleria ed è pubblica a tutti
     **/
    private File generaImmagineFile() throws IOException {
        // Crea un file dellimmagine
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);// FOTO PRIVATE  //#######################################
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES); // FOTO PUBBLICHE

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",   /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        Log.d("Risult ", "path da avere: "+currentPhotoPath);
        return image;
    }

//==================================================================================================


//==================================================================================================
//                                  METODI DI PER L'USO DELLA GALLERIA
//==================================================================================================
    private String getFileExt(Uri contentUri) {
        ContentResolver c = getActivity().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(c.getType(contentUri));
    }
//==================================================================================================


//==================================================================================================
//                                  METODI DI PER LA GESTIONE DELL'IMMAGINE RITORNATATA
//                                  (sia fotocamera che galleria)
//==================================================================================================
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    /**
     * Metodo che riveve in uno dei due metodi la foto e in etrambi i casi la imposta alla imageView
     * e passa all'acctivity principale la foto in un oggetto File, che sarà usata per gestire il
     * il DATA BASE e un succesivo recupero.
     * */
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        File nuovaFoto = null;
        // FOTOCAMERA
        if(requestCode == FOTOCAMERE_REQUEST_CODE){
            if(resultCode == Activity.RESULT_OK){
                File f = new File(currentPhotoPath);
                nuovaFoto=f;
                //impostaImmagineCircolareDaFile(f);
                Log.d("MyTag" , "onActivityResult : Url assoluto dellimmagine è il seguente: "+Uri.fromFile(f));
                //GALLERY Salva in gallery
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                getActivity().sendBroadcast(mediaScanIntent);
                //GALLERY END
            }
        }
        // GALLERY
        if(requestCode == GALLERY_REQUEST_CODE){
            if(resultCode == Activity.RESULT_OK){
                Uri contentUri = data.getData();
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageFileName = "JPEG_" + timeStamp + "_"+getFileExt(contentUri);
                Log.d("MyTag" , "onActivityResult: Url del file nella Gallery è: "+ contentUri.getPath()+"/"+imageFileName);

                //imageUser.setImageURI(contentUri);
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentUri);
                    // impostaImmagineCircolareDaBitmap(bitmap);
                    File imageFile = persistImage(bitmap,imageFileName);
                    nuovaFoto = imageFile;
                } catch (IOException e) {
                    Log.d("MyTag" , "ERROR :  "+e.getMessage());
                    e.printStackTrace();
                }
            }
        }

        if(nuovaFoto!=null) {
            if (listener.cambioImmagine(nuovaFoto)) {
                impostaImmagineCircolareDaFile(nuovaFoto);
            }else{
                Log.d("MyTag" , "La fotografia scattata non è stata salvata nel area privata del'app.");
                Toast.makeText(getContext(),"La fotografia non è accetata.",Toast.LENGTH_LONG).show();
            }
        }
    }
//==================================================================================================
//==================================================================================================
    private  File persistImage(Bitmap bitmap, String name) {
        File filesDir =  getActivity().getApplicationContext().getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return imageFile;
    }
//==================================================================================================

}