package com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface;

import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Gara;
//##################################################################################################
//                                  INTERFACCIA PER COMUNICARE
//##################################################################################################
/**
 * Interfacca per interagire con questo GareDeiCampionatiRVAdapter che deve essere implementato dall'Activity con
 * cui si vule collabolrare. Per sapere cosa fare quando premo sul tasto del classifica.
 */
public interface OnClickClassificaDellaGaraListener {
    void visualizzaClassificaDelCampionato(Campionato campionato, Gara gara);
}
//##################################################################################################
//##################################################################################################