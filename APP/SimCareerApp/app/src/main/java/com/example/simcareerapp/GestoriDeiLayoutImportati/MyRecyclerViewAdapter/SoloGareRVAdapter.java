package com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MyChipAdapter.MyChipAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyIconMeteoAdapter.MyIconMeteoAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface.OnClickClassificaDellaGaraListener;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyView.MyGridView;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.CircuitoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Gara;
import com.example.simcareerapp.Persistence.Entities.Meteo;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Persistence.JsonEntities.MeteoConJson;
import com.example.simcareerapp.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class SoloGareRVAdapter extends RecyclerView.Adapter<SoloGareRVAdapter.MyViewHoder>  {
//##################################################################################################
//                              CLASSE INTERNA
//##################################################################################################
    /**
     * Questa classe è quella che permette alla RecyclingView Adapter di definire un layout custom
     * del singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public View v_mainLayout;
        //Informazioni generali
        public TextView  v_titoloView;
        public TextView  v_dataView;
        public TextView  v_temperaturaView;
        public TextView  v_umiditaView;
        //Immagine
        public ImageView v_ImagineGaraView;
        //Icone Meteo
        public GridView  v_meteoView;
        //Bottone
        public FloatingActionButton v_btn_classificaView;
//==================================================================================================
//                              COSTRUTTORI
//==================================================================================================
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout           = itemView.findViewById(R.id.lly_BloccoGara);
            //Informazioni generali
            v_titoloView         = itemView.findViewById(R.id.txt_TitoloBlocco);
            v_dataView           = itemView.findViewById(R.id.txt_Value_Giorno);
            v_temperaturaView    = itemView.findViewById(R.id.txt_Value_Temperatura);
            v_umiditaView        = itemView.findViewById(R.id.txt_Value_Umidita);
            //Immagine
            v_ImagineGaraView    = itemView.findViewById(R.id.img_ImmagineBlocco);
            //Icone Meteo
            v_meteoView          = itemView.findViewById(R.id.grd_IconeMeteo);
            //Bottone
            v_btn_classificaView = itemView.findViewById(R.id.btn_ClassificaParziale);
        }
//==================================================================================================
    }
//##################################################################################################



//##################################################################################################
//                              CLASSE PRINCIPALE SoloGareRVAdapter
//##################################################################################################
    private Campionato campionato;
    private Context comtext=null;
    private Activity activity=null;
    private List<Gara> listaGare=null;
    private Utente utenteAttivo=null;
    private OnClickClassificaDellaGaraListener actionOnClick = null;
    private HashMap<String, MeteoConJson> previsioniMeteo=null;
    //Formattazione
    private static String FORMATTAZIONE_DATE = "dd/MM/yyyy";
    private static String FORMATTAZIONE_VAL = "#0";

//==================================================================================================
//                              COSTRUTTORI
//==================================================================================================
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public SoloGareRVAdapter(Context comtext, Activity activity, OnClickClassificaDellaGaraListener actionOnClick, Campionato campionato, Utente utenteAttivo, HashMap<String, MeteoConJson> previsioniMeteo) {
        this.comtext = comtext;
        this.activity = activity;
        this.actionOnClick=actionOnClick;
        this.listaGare = campionato.getListaGara();
        this.campionato = campionato;
        this.utenteAttivo = utenteAttivo;
        this.previsioniMeteo = previsioniMeteo;
    }
//==================================================================================================

//==================================================================================================
//                              CERAZIONE DEL SINGOLO ELEMETOTO
//==================================================================================================
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public SoloGareRVAdapter.MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View bloccoElemeto = inflater.inflate(R.layout.row_gara, parent,false);
        return new SoloGareRVAdapter.MyViewHoder(bloccoElemeto);
    }
//==================================================================================================

//==================================================================================================
//                              INSERIRE L'ELEMETO NEL LAYOUT
//==================================================================================================
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore SoloGareRVAdapter.
     */
    @SuppressLint("RestrictedApi")
    @Override
    public void onBindViewHolder(@NonNull SoloGareRVAdapter.MyViewHoder holder, int position) {
        final Gara gara =listaGare.get(position);
        final CircuitoConImmagini circuitoConImmagini = gara.getCircuito();
        //Inserisci i dati nel layout
        holder.v_titoloView.setText(circuitoConImmagini.getNome());
        holder.v_ImagineGaraView.setImageResource(circuitoConImmagini.getIdImmagine());
        // Formattzazione delle date.
        SimpleDateFormat sdf = new SimpleDateFormat(FORMATTAZIONE_DATE);
        //Imposta data
        Date giorno = gara.getData();
        if (giorno!=null){
            String giornoStr = sdf.format(giorno);
            holder.v_dataView.setText(giornoStr);
        }else{
            holder.v_dataView.setText("");
        }

        Meteo meteo = previsioniMeteo.get(circuitoConImmagini.getNome().toLowerCase());
        //Imposta Temperatura
        holder.v_temperaturaView.setText("");
        if(meteo!=null){
            NumberFormat formatter = new DecimalFormat(FORMATTAZIONE_VAL);
            String temperatura = formatter.format(meteo.getMinTemperatura())+" - "+formatter.format(meteo.getMaxTemperatura())+"°C";
            holder.v_temperaturaView.setText(temperatura);
        }
        //Imposta Umidita
        holder.v_umiditaView.setText("");
        if(meteo!=null){
            NumberFormat formatter = new DecimalFormat(FORMATTAZIONE_VAL);
            String temperatura = formatter.format(meteo.getUmidita())+"%";
            holder.v_umiditaView.setText(temperatura);
        }
        //Imposta icone meteo
        List<Integer> listaIdIconeMeteo=generaraListaIcone(meteo);
        MyIconMeteoAdapter adapter =new MyIconMeteoAdapter(comtext,listaIdIconeMeteo);
        holder.v_meteoView.setAdapter(adapter);
        //Imposta visibilità pulsante classifica
        if(gara.getData().before(new Date())){
            holder.v_btn_classificaView.setVisibility(View.VISIBLE);
//--------------------------------------------------------------------------------------------------
//                          BOTTONE DELLA CLASSIFICA DELLA GARA
//--------------------------------------------------------------------------------------------------
            holder.v_btn_classificaView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            actionOnClick.visualizzaClassificaDelCampionato(campionato,gara);
                        }
                    }
            );
//--------------------------------------------------------------------------------------------------
        }
    }
//==================================================================================================
//                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE
//==================================================================================================
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaGare.size();
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                              APPOGIO PER INSERIMETO DELLE ICONE DEL METEO
//##################################################################################################
//==================================================================================================
//                              LISCTA ID DELLE ICONE METEO
//==================================================================================================
    private List<Integer> generaraListaIcone(Meteo meteo){
        List<Integer> ris = new ArrayList<Integer>();
        if(meteo.getNebbia()){
            ris.add(R.drawable.ic_baseline_smog_solid_primary);
        }
        if(meteo.getNuvoloso()){
            ris.add(R.drawable.ic_baseline_cloud_sun_solid_primary);
        }
        if(meteo.getPiovoso()){
            ris.add(R.drawable.ic_baseline_cloud_rain_solid_primary);
        }
        if(meteo.getSoleggiato()){
            ris.add(R.drawable.ic_baseline_sun_solid_primary);
        }
        if(meteo.getTemporale()){
            ris.add(R.drawable.ic_baseline_bolt_solid_primary);
        }
        if(meteo.getVentoso()){
            ris.add(R.drawable.ic_baseline_wind_solid_primary);
        }
        return ris;
    }
//##################################################################################################
}
