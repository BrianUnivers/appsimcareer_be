package com.example.simcareerapp.DataBase.ThreadAction;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;

import com.example.simcareerapp.DataBase.Management.SimpleDbHelper;
import com.example.simcareerapp.DataBase.Query.FotoQuery;
import com.example.simcareerapp.DataBase.Query.UtenteQuery;
import com.example.simcareerapp.Persistence.Entities.Foto;
import com.example.simcareerapp.Persistence.Entities.Utente;

public class ThreadUpdateUtente extends AsyncTask<Pair<Utente,Long>,Integer, Boolean> {
    private SimpleDbHelper database;
    private Context context;
//==================================================================================================
//                              COSTRUTORE                                                          <<Processo Principale>>
//==================================================================================================
    /**
     * Per avere la posibilità di accedere alla Accivity e instauro un collegamento col server.
     */
    public ThreadUpdateUtente(Context context) {
        //Crazione DataBase o Associazione con uno già creato
        this.database = new SimpleDbHelper(context);
        this.context = context;
    }
    //==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE PRIMA DELLA CHAMATA DEL THREAD                     <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
    //==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DOPO DELLA CHAMATA DEL THREAD                      <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
    }
    //==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DURANTE DELLA CHAMATA DEL THREAD                   <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }
//==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DAL THREAD                                         <<Processo Seconario>>
//==================================================================================================
    /**
     * Effetua l'aggiornameto di un utente con i dati completi passati, se prero non è stato modificato
     * un solo utente tele operazione restiuira false poiche non è stoto modificato nessuno, altrimeti
     * se viene modificato ritornerà vero.
     */
    @Override
    protected Boolean doInBackground(Pair<Utente,Long>... item) {
        Utente utente       = item[0].first;
        Long idUtente       = item[0].second;
        int id = UtenteQuery.updateUtente(database,utente,idUtente);
        Boolean  ris=false;
        if (id==1){
            ris =true;
        }
        return ris;
    }
//==================================================================================================
}
