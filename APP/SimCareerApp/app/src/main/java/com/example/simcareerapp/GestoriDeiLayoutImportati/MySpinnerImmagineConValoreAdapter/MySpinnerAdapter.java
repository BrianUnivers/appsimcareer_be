package com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinnerImmagineConValoreAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;
import com.example.simcareerapp.R;

import java.util.ArrayList;

public class MySpinnerAdapter extends ArrayAdapter<ValoreConImmaginePerSpinner> {
//==================================================================================================
//                                  COSTRUTTRI
//==================================================================================================
    public MySpinnerAdapter(Context context, ArrayList<ValoreConImmaginePerSpinner> lista){
        super(context,0,lista);
    }
//==================================================================================================

//==================================================================================================
//                                  IMPOSTA VISUALIZZAZIONE DEL ELEMENTO SELEZIONATO
//==================================================================================================
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }
//==================================================================================================

//==================================================================================================
//                                  IMPOSTA VISUALIZZAZIONE DEI ELEMENTI ELEMENTO
//==================================================================================================
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }
//==================================================================================================

//==================================================================================================
//                                  IMPOSTA IL LAYOUT PER LE TIGHE
//==================================================================================================
    /**
     * Questo metodo restituisce la view da usare per la singola voce sia selezionata sia per la lista
     * dello spinner.
     */
    private View initView(int position, View convertView,  ViewGroup parent){
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.row_spinner_layout,
                    parent,false);
        }
        TextView viewTesto      = convertView.findViewById(R.id.txt_TestoValoreSpinner);
        //ImageView viewImmagine  = convertView.findViewById(R.id.img_RowSpinner);

        ValoreConImmaginePerSpinner elemento = getItem(position);
        if(elemento!=null) {
            viewTesto.setText(elemento.getNome());
            //viewImmagine.setImageResource(elemento.getIdImmagine());
        }
        return convertView;
    }
//==================================================================================================

}
