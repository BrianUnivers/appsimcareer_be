package com.example.simcareerapp.GestioneVariazioneAPI;

import android.content.Context;
import android.net.Uri;
import android.os.Build;

import androidx.core.content.FileProvider;

import com.example.simcareerapp.BuildConfig;

import java.io.File;

/***
 * Questa classe contiene tutti i metidi che contengono operazioni che variano in base alla variazione
 * delle versioni delle API.
 */
public class GestioneVariazioneAPI {


//##################################################################################################
//                              VERIVICA VERSIONE
//##################################################################################################
    private static int API25 = 25;
//==================================================================================================
//                              VERIVICA VERSIONE SE MAGGIORE O UGUALE
//==================================================================================================
    public static boolean upperOrEqualVersionThen(int version){
        boolean ris =false;
        if (Build.VERSION.SDK_INT >= version) {
            ris =true;
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                              VERIVICA VERSIONE SE MINORE O UGUALE
//==================================================================================================
    public static boolean lowerOrEqualVersionThen(int version){
        boolean ris =false;
        if (Build.VERSION.SDK_INT <= version) {
            ris =true;
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                              VERIVICA VERSIONE SE MAGGIORE
//==================================================================================================
    public static boolean upperVersionThen(int version){
        boolean ris =false;
        if (Build.VERSION.SDK_INT > version) {
            ris =true;
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                              VERIVICA VERSIONE SE MINORE
//==================================================================================================
    public static boolean lowerVersionThen(int version){
        boolean ris =false;
        if (Build.VERSION.SDK_INT < version) {
            ris =true;
        }
        return ris;
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                              GET URI
//##################################################################################################
//==================================================================================================
//                              GET URI FROM FILE
//==================================================================================================
    public static Uri getURIFromFile(Context context,File file){
        Uri uri;
        if(upperOrEqualVersionThen(API25)) {
            uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileprovider", file);//Da 26 e succesive (va anche su 25)
        }else{
            uri = Uri.fromFile(file); // Da 25 e precedenti
        }
        return uri;
    }
//==================================================================================================
//##################################################################################################

}