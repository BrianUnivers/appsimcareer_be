package com.example.simcareerapp.Persistence.Entities;

import android.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Meteo {
    static private final String NUVOLOSO   = "Nuvoloso";
    static private final String NEBBIA     = "Nebbia";
    static private final String SOLEGGIATO = "Soleggiato";
    static private final String PIOGGIA    = "Pioggia";
    static private final String VENTOSO    = "Ventoso";
    static private final String TEMPORALE  = "Temporale";
//##################################################################################################
//                                  INFORMAZIONI PILOTI
//##################################################################################################
    protected String  luogo="";
    protected Double  minTemperatura =0.0;
    protected Double  maxTemperatura =40.0;
    protected Double  umidita    =0.0;
    protected Boolean nuvoloso   = false;
    protected Boolean nebbia     = false;
    protected Boolean soleggiato = false;
    protected Boolean piovoso    = false;
    protected Boolean ventoso    = false;
    protected Boolean temporale  = false;
//##################################################################################################
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
    protected Meteo() {
    }
//--------------------------------------------------------------------------------------------------
    public Meteo(String luogo, Double minTemperatura, Double maxTemperatura, Double umidita, Boolean nuvoloso, Boolean nebbia, Boolean soleggiato, Boolean piovoso, Boolean ventoso, Boolean temporale) {
        this.luogo = luogo;
        this.minTemperatura = minTemperatura;
        this.maxTemperatura = maxTemperatura;
        this.umidita = umidita;
        this.nuvoloso = nuvoloso;
        this.nebbia = nebbia;
        this.soleggiato = soleggiato;
        this.piovoso = piovoso;
        this.ventoso = ventoso;
        this.temporale = temporale;
    }
//==================================================================================================
//==================================================================================================
//                              GETTER E SETTER
//==================================================================================================
    public String getLuogo() {
        return luogo;
    }
//------------------------------
    public void setLuogo(String luogo) {
        this.luogo = luogo;
    }
//--------------------------------------------------------------------------------------------------
    public Double getMinTemperatura() {
        return minTemperatura;
    }
//------------------------------
    public void setMinTemperatura(Double minTemperatura) {
        this.minTemperatura = minTemperatura;
    }
//--------------------------------------------------------------------------------------------------
    public Double getMaxTemperatura() {
        return maxTemperatura;
    }
//------------------------------
    public void setMaxTemperatura(Double maxTemperatura) {
        this.maxTemperatura = maxTemperatura;
    }
//--------------------------------------------------------------------------------------------------
    public Double getUmidita() {
        return umidita;
    }
//------------------------------
    public void setUmidita(Double umidita) {
        this.umidita = umidita;
    }
//--------------------------------------------------------------------------------------------------
    public Boolean getNuvoloso() {
        return nuvoloso;
    }
//------------------------------
    public void setNuvoloso(Boolean nuvoloso) {
        this.nuvoloso = false;
        if(nuvoloso!=null){
            this.nuvoloso = nuvoloso;
        }
    }
//--------------------------------------------------------------------------------------------------
    public Boolean getNebbia() {
        return nebbia;
    }
//------------------------------
    public void setNebbia(Boolean nebbia) {
        this.nebbia = false;
        if(nebbia!=null){
            this.nebbia = nebbia;
        }
    }
//--------------------------------------------------------------------------------------------------
    public Boolean getSoleggiato() {
        return soleggiato;
    }
//------------------------------
    public void setSoleggiato(Boolean soleggiato) {
        this.soleggiato = false;
        if(soleggiato!=null){
            this.soleggiato = soleggiato;
        }
    }
//--------------------------------------------------------------------------------------------------
    public Boolean getPiovoso() {
        return piovoso;
    }
//------------------------------
    public void setPiovoso(Boolean piovoso) {
        this.piovoso = false;
        if(piovoso!=null){
            this.piovoso = piovoso;
        }
    }
//--------------------------------------------------------------------------------------------------
    public Boolean getVentoso() {
        return ventoso;
    }
//------------------------------
    public void setVentoso(Boolean ventoso) {
        this.ventoso = false;
        if(ventoso!=null){
            this.ventoso = ventoso;
        }
    }
//--------------------------------------------------------------------------------------------------
    public Boolean getTemporale() {
        return temporale;
    }
//------------------------------
    public void setTemporale(Boolean temporale) {
        this.temporale = false;
        if(temporale!=null){
            this.temporale = temporale;
        }
    }
//==================================================================================================
//==================================================================================================
//                              VERIFICA CHE NON SIA UN OGGETO CON PARAMETRI NULL
//==================================================================================================
public boolean isNull(){
    return (  (getLuogo()==null) || (getLuogo().equals("")) ||
            (getMaxTemperatura()==null) || (getMinTemperatura()==null) || (getUmidita()==null) );
}
//==================================================================================================

//==================================================================================================
//                              GET LABLE E STATO DEL METEO
//==================================================================================================
    public List<Pair<String, Boolean>> getStatoMeteo(){
        List<Pair<String, Boolean>> ris = new ArrayList<Pair<String, Boolean>>();
        ris.add(new Pair<String, Boolean>(Meteo.NEBBIA,    nebbia));
        ris.add(new Pair<String, Boolean>(Meteo.NUVOLOSO,  nuvoloso));
        ris.add(new Pair<String, Boolean>(Meteo.PIOGGIA,   piovoso));
        ris.add(new Pair<String, Boolean>(Meteo.SOLEGGIATO,soleggiato));
        ris.add(new Pair<String, Boolean>(Meteo.TEMPORALE, temporale));
        ris.add(new Pair<String, Boolean>(Meteo.VENTOSO,   ventoso));
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                              SET METEO DA LABLE E STATO
//==================================================================================================
    public void setStatoMeteo(Pair<String, Boolean> coppiaMeteoState){
        switch (coppiaMeteoState.first){
            case Meteo.NEBBIA:
                this.nebbia = coppiaMeteoState.second;
                break;
            case Meteo.NUVOLOSO:
                this.nuvoloso = coppiaMeteoState.second;
                break;
            case Meteo.PIOGGIA:
                this.piovoso = coppiaMeteoState.second;
                break;
            case Meteo.SOLEGGIATO:
                this.soleggiato = coppiaMeteoState.second;
                break;
            case Meteo.TEMPORALE:
                this.temporale = coppiaMeteoState.second;
                break;
            case Meteo.VENTOSO:
                this.ventoso = coppiaMeteoState.second;
                break;
        }
    }
//==================================================================================================
//==================================================================================================
//                              SET METEO DA LABLE E STATO
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Meteo)) return false;
        Meteo meteo = (Meteo) o;
        return Objects.equals(getLuogo(), meteo.getLuogo()) &&
                Objects.equals(getMinTemperatura(), meteo.getMinTemperatura()) &&
                Objects.equals(getMaxTemperatura(), meteo.getMaxTemperatura()) &&
                Objects.equals(getUmidita(), meteo.getUmidita()) &&
                Objects.equals(getNuvoloso(), meteo.getNuvoloso()) &&
                Objects.equals(getNebbia(), meteo.getNebbia()) &&
                Objects.equals(getSoleggiato(), meteo.getSoleggiato()) &&
                Objects.equals(getPiovoso(), meteo.getPiovoso()) &&
                Objects.equals(getVentoso(), meteo.getVentoso()) &&
                Objects.equals(getTemporale(), meteo.getTemporale());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(getLuogo(), getMinTemperatura(), getMaxTemperatura(), getUmidita(), getNuvoloso(), getNebbia(), getSoleggiato(), getPiovoso(), getVentoso(), getTemporale());
    }

//==================================================================================================
}
