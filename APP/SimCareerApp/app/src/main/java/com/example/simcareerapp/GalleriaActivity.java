package com.example.simcareerapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.simcareerapp.AppCompatActivityWithMenu.AppCompatActivityWithMyPrimaryMenu;
import com.example.simcareerapp.GestioneAssetPerGalleria.GestioneFileECartelleGalleria;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.CartelleGalleriaRVAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface.OnClickCartellaGalleriaListener;
import com.example.simcareerapp.Persistence.Entities.Utente;

public class GalleriaActivity extends AppCompatActivityWithMyPrimaryMenu
                                implements OnClickCartellaGalleriaListener {
//##################################################################################################
//                              DATI PASSATI TRAMITE UN INTENT
//##################################################################################################
    private Utente utenteAttivo         = null;
    private Boolean mantieniAttivoUtente= null;
//==================================================================================================
//                              METODO DI RECUPERO DI TALI VALORI DAL INTET CHE HA CHAMATO LA PAGINA
//==================================================================================================
    /**
     * Questo metodo serve per prendere dall'Activity chiamante le informazioni della sessione che
     * di obbligatori sono <<utenteAttivo>> e <<mantieniAttivoUtente>>.
     */
    private void initValoriDiSessioneDellAPP(){
        Intent i =getIntent();
        utenteAttivo            = MyIntent.getUtenteExtra(i,MyIntent.I_UTENTE_ATTIVO);
        mantieniAttivoUtente    = i.getBooleanExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,false);
    }
//==================================================================================================
//==================================================================================================
//                              IMPOSTAZIONI PARAMETRI DA PASSARE PER GLI INTENT DEL MENU LATERALE PRINCIPALE
//==================================================================================================
    @Override
    protected void setParamiter(MyIntent intent) {
        intent.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utenteAttivo);
        intent.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
    }
//==================================================================================================
//==================================================================================================
//                              APRI ACTIVITY LISTA FOTO DI UNA CARTELLA
//==================================================================================================
    protected void apriListaFotoGalleriaActivity(String folder) {
        MyIntent intent = new MyIntent(GalleriaActivity.this, ListaFotoGalleriaActivity.class);
        setParamiter(intent);
        intent.putExtra(MyIntent.I_FOLDER, folder);
        startActivity(intent);
    }
//==================================================================================================
//##################################################################################################


//##################################################################################################
//                                  CREAZIONE DEL LAYOUT
//##################################################################################################
    private RecyclerView RecyclerViewCartelle;
//==================================================================================================
//                                  CREAZIONE DELL'ACTITVITY
//==================================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  GET DATI DAL INTENT
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        initValoriDiSessioneDellAPP();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        setContentView(R.layout.activity_galleria);
        super.initToolBar(R.menu.bar_menu_vuoto);
        super.impostaPaginaComeHome();
        GestioneFileECartelleGalleria galleria= new GestioneFileECartelleGalleria(this,GestioneFileECartelleGalleria.PATH_GALLERIA);

        RecyclerViewCartelle = findViewById(R.id.rcl_CartelleGalleria);
        CartelleGalleriaRVAdapter adapter = new CartelleGalleriaRVAdapter(this,this, galleria.getListaCartelle());
        RecyclerViewCartelle.setAdapter(adapter);
        RecyclerViewCartelle.setLayoutManager(new GridLayoutManager(this,3));
    }
//##################################################################################################

//##################################################################################################
//                                  METODI PER INTERAGIRE CON  OnClickCartellaGalleriaListener
//##################################################################################################
    public void apriCartelleGalleria(String folder){
        apriListaFotoGalleriaActivity(folder);
    }
//##################################################################################################
}