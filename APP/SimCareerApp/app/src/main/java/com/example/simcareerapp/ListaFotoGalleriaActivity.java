package com.example.simcareerapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.simcareerapp.FotoGalleria.FotoGalleriaFragment;
import com.example.simcareerapp.FotoGalleria.Interface.OnFragmentListaFotoGalleriaFragmentEventListener;
import com.example.simcareerapp.FotoGalleria.ListaFotoGalleriaFragment;
import com.example.simcareerapp.GestioneAssetPerGalleria.GestioneFileECartelleGalleria;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.GestioneVariazioneAPI.GestioneVariazioneAPI;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface.OnClickFotoGalleriaListener;
import com.example.simcareerapp.Persistence.Entities.Utente;

import java.io.File;

public class ListaFotoGalleriaActivity extends AppCompatActivity
                                       implements OnFragmentListaFotoGalleriaFragmentEventListener,
                                                  OnClickFotoGalleriaListener {

//##################################################################################################
//                              DATI PASSATI TRAMITE UN INTENT
//##################################################################################################
    private Utente  utenteAttivo         = null;
    private Boolean mantieniAttivoUtente = null;
    private String  folder               = null;
    private String  fileImmagine         = null;
//==================================================================================================
//                              METODO DI RECUPERO DI TALI VALORI DAL INTET CHE HA CHAMATO LA PAGINA
//==================================================================================================
    /**
     * Questo metodo serve per prendere dall'Activity chiamante le informazioni della sessione che
     * di obbligatori sono <<utenteAttivo>>, <<mantieniAttivoUtente>> e <<folder>>.
     * Quest'ultima permette di ricavato la lista dei file
     *
     * Se non dovesse trovare il campionato con quel'id si ritorna alla pagina dei campionati.
     */
    private void initValoriDiSessioneDellAPP(){
        Intent i =getIntent();
        utenteAttivo            = MyIntent.getUtenteExtra(i,MyIntent.I_UTENTE_ATTIVO);
        mantieniAttivoUtente    = i.getBooleanExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,false);
        folder                  = i.getStringExtra(MyIntent.I_FOLDER);
        fileImmagine            = null;
    }
//==================================================================================================
//==================================================================================================
//                              APERTURA DELLA PAGINA DI LISTA CAMPIONATI PASSANDO I VALORI DI SESSIONE
//==================================================================================================
    /**
     * Questo metodo serve per inviare le informazioni della sessione che sono <<utenteAttivo>> e
     * <<mantieniAttivoUtente>> quelli obbligatori.
     * N.B.
     * Da questà activiti è possibile solo andare alla Activity GalleriaActivity senza apportare
     * modifiche.
     */
    private void apriPaginaPrecedete(){
        if(page!=null){
            if(page.getClass().equals(FotoGalleriaFragment.class)){
                apriSezioneSelezioneFoto();
            }else if(page.getClass().equals(ListaFotoGalleriaFragment.class)){
                super.onBackPressed();
            }
        }
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Sovrascrive il tasto indietro fisico
     */
    @Override
    public void onBackPressed() {
        apriPaginaPrecedete();
    }
//==================================================================================================
//##################################################################################################



//##################################################################################################
//                              DATI PER GESTIRE IL LAYOUT
//##################################################################################################
    //Toolbar da implemetare
    private Toolbar  toolbar     = null;
    private Fragment page        = null;
    GestioneFileECartelleGalleria galleria=null;
//##################################################################################################
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  GET DATI DAL INTENT
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        initValoriDiSessioneDellAPP();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        setContentView(R.layout.activity_lista_foto_galleria);
        //Implemetazione della toolbar
        toolbar =  findViewById(R.id.include_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24_white);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  LISTENER DEI BOTTONI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Listener del pulsante Annula modifiche
        if(toolbar!=null)
            toolbar.setNavigationOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    apriPaginaPrecedete();
                }
            });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //ListaFotoGalleriaFragment
        galleria= new GestioneFileECartelleGalleria(this,GestioneFileECartelleGalleria.PATH_GALLERIA);
        apriSezioneSelezioneFoto();
    }
//##################################################################################################


//##################################################################################################
//                                  APRI SEZIONI NEI FRAGMENT
//##################################################################################################
//==================================================================================================
//                                  APRI FRAGMENT DI SELEZIONE FOTO
//==================================================================================================
    private void apriSezioneSelezioneFoto(){
        fileImmagine = null;
        Menu menu = toolbar.getMenu();
        menu.clear();
        getMenuInflater().inflate(R.menu.bar_menu_vuoto, menu);
        page = new ListaFotoGalleriaFragment(galleria,folder);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fly_Page, page);
        ft.commit();
    }
//==================================================================================================
//==================================================================================================
//                                  APRI FRAGMENT DELLA FOTO
//==================================================================================================
    private void apriSezioneFoto(String fileNome){
        fileImmagine = fileNome;
        Menu menu = toolbar.getMenu();
        menu.clear();
        getMenuInflater().inflate(R.menu.bar_menu_foto, menu);
        page = new FotoGalleriaFragment(galleria,folder,fileNome);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fly_Page, page);
        ft.commit();
    }
//==================================================================================================

//##################################################################################################

//##################################################################################################
//                                  GESTIONE DEL MENU' SULLA TOOLBAR
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE DEL MENU' (senza il tasto a sinistra)
//==================================================================================================
    /**
     * Metodo per definire il layout della toolbar definedo quali sono le sue voci.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bar_menu_vuoto,menu);
        return true;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  METODI PER INTERAGIRE CON  OnClickFotoGalleriaListener
//##################################################################################################
//==================================================================================================
//                                  APRI FOTO
//==================================================================================================
    @Override
    public void apriFotoGalleria(String fileNome) {
        apriSezioneFoto(fileNome);
    }
//==================================================================================================
//##################################################################################################


//##################################################################################################
//                                  GESTIONE DEL MENU' SULLA TOOLBAR
//##################################################################################################
//==================================================================================================
//                                  GESTIONE DEI EVENTI DI CLICK SULLE VOCI DEL MENU'
//==================================================================================================
    /**
     * Metodo per distingere i tasti della toolbar e sapere si deve fare.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(page!=null){
            if(page.getClass().equals(FotoGalleriaFragment.class)){
                switch (id){
                    case R.id.btn_bar_RuotaADestra:
                        ((FotoGalleriaFragment)page).ruotaImmagineADestra();
                        break;
                    case R.id.btn_bar_RuotaASinistra:
                        ((FotoGalleriaFragment)page).ruotaImmagineASinistra();
                        break;
                    case R.id.btn_bar_Condividi:
                        condividiImmagine( folder, fileImmagine);
                        break;
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  CONDIVIDI IMMAGINE
//##################################################################################################
//==================================================================================================
//                                  CONDIVIDI DEL IMMAGINE
//==================================================================================================
    private void condividiImmagine( String folder,String fileImmagine){
        File file = galleria.getFileFoto(folder,fileImmagine);
        Intent shareIntent;
        if(file!=null){
            try {
                shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("image/*");
                // Questo metodo per perendere Uri gestisce le varie versioni di API.
                shareIntent.putExtra(Intent.EXTRA_STREAM, GestioneVariazioneAPI.getURIFromFile(this,file));
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.lbl_Condividi)));
            }catch (Exception e){
                Toast.makeText(this, "Immpossibile condividere la foto.", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this, "Immpossibile condividere la foto.", Toast.LENGTH_SHORT).show();
        }
    }
//==================================================================================================
//##################################################################################################
}
