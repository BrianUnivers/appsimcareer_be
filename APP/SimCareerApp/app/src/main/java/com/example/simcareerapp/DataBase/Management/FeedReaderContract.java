package com.example.simcareerapp.DataBase.Management;

import android.provider.BaseColumns;

/**
 * Questa classe contiene le costani per gestire il data base sqlite locale, assieme alle sue query.
 *
 * @author Brian Emanuel
 */
public class FeedReaderContract {
    private FeedReaderContract(){}
//##################################################################################################
//                              BOOLEANO DATABASE
//##################################################################################################
    /**
     * La costante "DB_TRUE" contiene il valore che utiliziamo nel data base come valore booleano per true.
     * ( Poichè il database sqlite non contiene come tipo di dato il booleano )
     */
    public static final Integer DB_TRUE     = 1;
    /**
     * La costante "DB_FALSE" contiene il valore che utiliziamo nel data base come valore booleano per false.
     * ( Poichè il database sqlite non contiene come tipo di dato il booleano )
     */
    public static final Integer DB_FALSE    = 0;
//##################################################################################################
//                              DEFINIZIONI ATTRIIBUTI E NOMI DI TABELLE DEL DATA BASE
//##################################################################################################
    /**
     * La costante "SQL_CREATE_DB" contiene un array di stringe, le quali contengono le isreuzioni SQL
     * per creare il data base locale.
     */
    public static final String[] SQL_CREATE_DB = new String[] { FeedUtente.SQL_CREATE_TABLE,
                                                                FeedFoto.SQL_CREATE_TABLE};
    /**
     * La costante "SQL_DELETE_DB" contiene un array di stringe, le quali contengono le isreuzioni SQL
     * per distruggere il data base locale.
     */
    public static final String[] SQL_DELETE_DB = new String[] { FeedUtente.SQL_DELETE_TABLE,
                                                                FeedFoto.SQL_DELETE_TABLE};
//==================================================================================================
//                              TABELLA UTENTE
//==================================================================================================
    /**
     * Questa classe contiene le costani per gestire il data base sqlite locale, assieme alle sue query,
     * riferenndosi solo alla tabella Utente.
     *
     * @author Brian Emanuel
     */
    public static class FeedUtente implements BaseColumns{
        public static final String TABLE_NAME                                 = "Unente";
        public static final String COLUMN_NAME_NOME                           = "nome";
        public static final String COLUMN_NAME_COGNOME                        = "cognome";
        public static final String COLUMN_NAME_DATA_NASCITA                   = "dataNascita";
        public static final String COLUMN_NAME_EMAIL                          = "email";
        public static final String COLUMN_NAME_USERNAME                       = "username";
        public static final String COLUMN_NAME_PASSWORD                       = "password";
        public static final String COLUMN_NAME_SALT                           = "salt";
        public static final String COLUMN_NAME_RESIDENZA_STATO                = "residenza_stato";
        public static final String COLUMN_NAME_RESIDENZA_REGIONE              = "residenza_regione";
        public static final String COLUMN_NAME_RESIDENZA_CITTA                = "residenza_citta";
        public static final String COLUMN_NAME_RESIDENZA_VIA_PIAZZA           = "residenza_viaPiazza";
        public static final String COLUMN_NAME_NUMERO_DI_GARA                 = "numeroGara";
        public static final String COLUMN_NAME_AUTO_PREFERITA_NOME            = "autoPref_nome";
        public static final String COLUMN_NAME_AUTO_PREFERITA_ID_IMMAGINE     = "autoPref_id_Immagine";
        public static final String COLUMN_NAME_CIRCUITO_PREFERITA_NOME        = "circuitoPref_nome";
        public static final String COLUMN_NAME_CIRCUITO_PREFERITA_ID_IMMAGINE = "circuitoPref_id_Immagine";
        public static final String COLUMN_NAME_CIRCUITO_ODIATO_NOME           = "circuitoOdiato_nome";
        public static final String COLUMN_NAME_CIRCUITO_ODIATO_ID_IMMAGINE    = "circuitoOdiato_id_Immagine";
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                              QUERY
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        /**
         * La costante "SQL_CREATE_TABLE" contiene l'isreuzione SQL per creare la tabella Utenti.
         */
        public static final String SQL_CREATE_TABLE =
                " CREATE TABLE "+ FeedUtente.TABLE_NAME + " ( " +
                    FeedUtente._ID                                        + " INTEGER PRIMARY KEY, "  +
                    FeedUtente.COLUMN_NAME_NOME                           + " VARCHAR(60) NOT NULL, " +
                    FeedUtente.COLUMN_NAME_COGNOME                        + " VARCHAR(60) NOT NULL, " +
                    FeedUtente.COLUMN_NAME_DATA_NASCITA                   + " BIGINT NOT NULL, " +
                    FeedUtente.COLUMN_NAME_EMAIL                          + " VARCHAR(150) NOT NULL UNIQUE, " +
                    FeedUtente.COLUMN_NAME_USERNAME                       + " VARCHAR(60) NOT NULL UNIQUE, " +
                    FeedUtente.COLUMN_NAME_PASSWORD                       + " VARCHAR(64) NOT NULL, " +
                    FeedUtente.COLUMN_NAME_SALT                           + " VARCHAR(64) NOT NULL, " +
                    FeedUtente.COLUMN_NAME_RESIDENZA_STATO                + " VARCHAR(80), " +
                    FeedUtente.COLUMN_NAME_RESIDENZA_REGIONE              + " VARCHAR(80), " +
                    FeedUtente.COLUMN_NAME_RESIDENZA_CITTA                + " VARCHAR(80), " +
                    FeedUtente.COLUMN_NAME_RESIDENZA_VIA_PIAZZA           + " VARCHAR(80), " +
                    FeedUtente.COLUMN_NAME_NUMERO_DI_GARA                 + " INTEGER"+
                                            " CHECK("+ FeedUtente.COLUMN_NAME_NUMERO_DI_GARA+" > -1) " +
                                            " CHECK("+ FeedUtente.COLUMN_NAME_NUMERO_DI_GARA+" < 100), "+
                    FeedUtente.COLUMN_NAME_AUTO_PREFERITA_NOME            + " VARCHAR(150), " +
                    FeedUtente.COLUMN_NAME_AUTO_PREFERITA_ID_IMMAGINE     + " INTEGER, " +
                    FeedUtente.COLUMN_NAME_CIRCUITO_PREFERITA_NOME        + " VARCHAR(150), " +
                    FeedUtente.COLUMN_NAME_CIRCUITO_PREFERITA_ID_IMMAGINE + " INTEGER, " +
                    FeedUtente.COLUMN_NAME_CIRCUITO_ODIATO_NOME           + " VARCHAR(150), " +
                    FeedUtente.COLUMN_NAME_CIRCUITO_ODIATO_ID_IMMAGINE    + " INTEGER"+" ); " ;
    //----------------------------------------------------------------------------------------------
    /**
     * La costante "SQL_DELETE_TABLE" contiene l'isreuzione SQL per distruggere la tabella Utenti.
     */
    public static final String SQL_DELETE_TABLE =
                " DROP TABLE IF EXISTS "+ FeedUtente.TABLE_NAME+" ; ";
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    }
//==================================================================================================
//==================================================================================================
//                              TABELLA FOTO
//==================================================================================================
    /**
     * Questa classe contiene le costani per gestire il data base sqlite locale, assieme alle sue query,
     * riferenndosi solo alla tabella Foto.
     *
     * @author Brian Emanuel
     */
    public static class FeedFoto implements BaseColumns{
        public static final String TABLE_NAME                                 = "Foto";
        public static final String COLUMN_NAME_ID_UTENTE                      = "id_utente";
        public static final String COLUMN_NAME_TESTO_ALTERNATIVO              = "cognome";
        public static final String COLUMN_NAME_STATO_ATTIVO                   = "stato_attvo";
        public static final String COLUMN_NAME_URL                            = "url";
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                              QUERY
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        /**
         * La costante "SQL_CREATE_TABLE" contiene l'isreuzione SQL per creare la tabella Foto.
         */
        public static final String SQL_CREATE_TABLE =
                " CREATE TABLE "+ FeedFoto.TABLE_NAME + " ( " +
                    FeedFoto._ID                                        + " INTEGER PRIMARY KEY, "  +
                    FeedFoto.COLUMN_NAME_ID_UTENTE                      + " INTEGER NOT NULL, " +
                    FeedFoto.COLUMN_NAME_TESTO_ALTERNATIVO              + " VARCHAR(200) NOT NULL, " +
                    FeedFoto.COLUMN_NAME_STATO_ATTIVO                   + " INTEGER NOT NULL " +
                                    " CHECK("+ FeedFoto.COLUMN_NAME_STATO_ATTIVO+" >= "+ DB_FALSE +" AND "+
                                               FeedFoto.COLUMN_NAME_STATO_ATTIVO+" <= "+ DB_TRUE  + " ), "+
                    FeedFoto.COLUMN_NAME_URL                            + " VARCHAR(400) NOT NULL," +
                " FOREIGN KEY ( "+ FeedFoto.COLUMN_NAME_ID_UTENTE+" ) " +
                    " REFERENCES "+ FeedUtente.TABLE_NAME + " ( " + FeedUtente._ID +" ) " +
                        " ); " ;
    //----------------------------------------------------------------------------------------------
    /**
     * La costante "SQL_DELETE_TABLE" contiene l'isreuzione SQL per distruggere la tabella Foto.
     */
        public static final String SQL_DELETE_TABLE =
                " DROP TABLE IF EXISTS "+ FeedFoto.TABLE_NAME+" ; ";
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    }
//==================================================================================================
//##################################################################################################
}
