package com.example.simcareerapp.Register.Interface;

import java.util.Calendar;

//##################################################################################################
//                                  INTERFACCIA PER COMUNICARE
//##################################################################################################

/**
 * Interfacca per interagire con questo fragment che deve essere implementato dall'Activity con
 * cui si vule collabolrare.
 */
public interface OnFragmentRegisterEventListener {
    void    anullaIscrizione();
    boolean usernameValido(String username);
    boolean emailValido(String email);
    void    effetuaRegistrazione(String nome, String cognome, Calendar dataNascita, String Email, String Username, String password);
}
//##################################################################################################
//##################################################################################################