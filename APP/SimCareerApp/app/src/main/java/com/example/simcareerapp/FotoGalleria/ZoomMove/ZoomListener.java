package com.example.simcareerapp.FotoGalleria.ZoomMove;

import android.view.ScaleGestureDetector;
import android.view.View;

public class ZoomListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
    private View view;
    private float zoom = 1.0f;
    private float zoomIniziale = 1.0f;
//==================================================================================================
//                                      COSTRUTTORE
//==================================================================================================
    public ZoomListener(View view) {
        this.view = view;
    }
//--------------------------------------------------------------------------------------------------
    public ZoomListener(View view, float zoom) {
        this.view = view;
        this.zoom = zoom;
        this.zoomIniziale=zoom;
    }
//==================================================================================================
//==================================================================================================
//                                      MODIFICA ZOOM SUL IMMAGINE
//==================================================================================================
    @Override
    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        zoom *= scaleGestureDetector.getScaleFactor();
        zoom = Math.max(0.1f, Math.min(zoom, 10.0f));
        view.setScaleX(zoom);
        view.setScaleY(zoom);
        return true;
    }
//==================================================================================================
//==================================================================================================
//                                      RESET ZOOM SUL IMMAGINE
//==================================================================================================
    public void resetZoom() {
        zoom = this.zoomIniziale;
        zoom = Math.max(0.1f, Math.min(zoom, 10.0f));
        view.setScaleX(zoom);
        view.setScaleY(zoom);
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER
//==================================================================================================
    public float getZoom() {
        return zoom;
    }
//==================================================================================================
}
