package com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface.OnClickCartellaGalleriaListener;
import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Pilota;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.R;

import java.util.List;

public class CartelleGalleriaRVAdapter extends RecyclerView.Adapter<CartelleGalleriaRVAdapter.MyViewHoder>  {
//##################################################################################################
//                              CLASSE INTERNA
//##################################################################################################
    /**
     * Questa classe è quella che permette alla RecyclingView Adapter di definire un layout custom
     * del singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public View v_mainLayout;
        //Informazioni generali
        public TextView  v_titoloView;
        public ImageView v_imagineView;
//==================================================================================================
//                              COSTRUTTORI
//==================================================================================================
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout    = itemView.findViewById(R.id.cly_CartellaGalleria);
            //Informazioni generali
            v_titoloView    = itemView.findViewById(R.id.txt_PreviewTitle);
            v_imagineView   = itemView.findViewById(R.id.img_PreviewFoto);
        }
//==================================================================================================
    }
//##################################################################################################



//##################################################################################################
//                              CLASSE PRINCIPALE CartelleGalleriaRVAdapter
//##################################################################################################
    private Context comtext=null;
    private OnClickCartellaGalleriaListener action=null;
    private List<Pair<String, Bitmap>> listaCartellaGalleria=null;

//==================================================================================================
//                              COSTRUTTORI
//==================================================================================================
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public CartelleGalleriaRVAdapter(Context comtext, OnClickCartellaGalleriaListener action, List<Pair<String, Bitmap>> listaCartellaGalleria) {
        this.comtext = comtext;
        this.action = action;
        this.listaCartellaGalleria = listaCartellaGalleria;
    }
//==================================================================================================

//==================================================================================================
//                              CERAZIONE DEL SINGOLO ELEMETOTO
//==================================================================================================
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public CartelleGalleriaRVAdapter.MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View bloccoElemeto = inflater.inflate(R.layout.row_cartella_foto, parent,false);
        return new CartelleGalleriaRVAdapter.MyViewHoder(bloccoElemeto);
    }
//==================================================================================================

//==================================================================================================
//                              INSERIRE L'ELEMETO NEL LAYOUT
//==================================================================================================
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore CartelleGalleriaRVAdapter.
     */
    @Override
    public void onBindViewHolder(CartelleGalleriaRVAdapter.MyViewHoder holder, int position) {
        final Pair<String, Bitmap> cartella =listaCartellaGalleria.get(position);
        //Informazioni generali
        holder.v_titoloView.setText(cartella.first);
        holder.v_imagineView.setImageBitmap(cartella.second);
        holder.v_mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                action.apriCartelleGalleria(cartella.first);
            }
        });
    }
//==================================================================================================
//                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE
//==================================================================================================
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaCartellaGalleria.size();
    }
//==================================================================================================
//##################################################################################################

}
