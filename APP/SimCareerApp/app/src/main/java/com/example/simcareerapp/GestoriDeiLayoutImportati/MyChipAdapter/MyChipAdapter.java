package com.example.simcareerapp.GestoriDeiLayoutImportati.MyChipAdapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;


import androidx.recyclerview.widget.RecyclerView;

import com.example.simcareerapp.R;
import com.google.android.material.chip.Chip;

import java.util.ArrayList;
import java.util.List;


public class MyChipAdapter extends ArrayAdapter<Pair<String,Boolean>> {
    private List<Pair<String,Boolean>> listaChip = null;
    private static int resourceLayout = R.layout.row_chips;
    private Context context = null;
    private Boolean isClickAble =false;
//==================================================================================================
//                                  COSTRUTTRI
//==================================================================================================
    public MyChipAdapter(Context context, List<Pair<String,Boolean>> listaChip){
        super(context, resourceLayout, listaChip);
        this.listaChip=listaChip;
        this.context = context;
        this.isClickAble = false;
    }
//--------------------------------------------------------------------------------------------------
    public MyChipAdapter(Context context, List<Pair<String,Boolean>> listaChip,Boolean isClickAble){
            super(context, resourceLayout, listaChip);
            this.listaChip=listaChip;
            this.context = context;
            this.isClickAble = isClickAble;
        }
//==================================================================================================
//==================================================================================================
//                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE
//==================================================================================================
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getCount() {return listaChip.size();}
//==================================================================================================
//==================================================================================================
//                              RITORNA UN ELEMETO DELLA LISTA
//==================================================================================================
    /**
     * Questa funzione deve ritorna un elemto della lista data una posizione.
     */
    @Override
    public Pair<String,Boolean> getItem(int position) {return listaChip.get(position);}
//==================================================================================================
//==================================================================================================
//                              RITORNA UN IDENTIFICATORE DI UN ELEMETO
//==================================================================================================
    /**
     * Questa funzione deve ritorna un identificativo di un elemento della lista data una posizione.
     */
    @Override
    public long getItemId(int position) {return getItem(position).hashCode();}
//==================================================================================================
//==================================================================================================
//                              IMPOSTA IL LAYOUT
//==================================================================================================
@Override
    public View getView(int position, View v, ViewGroup vg){
        if (v == null){
            v = LayoutInflater.from(context).inflate(resourceLayout, null);
        }
        Pair<String,Boolean> elemento = (Pair<String,Boolean>) getItem(position);
        TextView chip = v.findViewById(R.id.chp_Elemeto);
        chip.setText(elemento.first);
        if(elemento.second){
            if(isClickAble){
                setStyleChipSelezionato(chip);
            }else{
                setStyleChipAttivo(chip);
            }
        }else{
            setStyleChipDisattivo(chip);
        }
        chip.setClickable(false);
        return v;
    }
//==================================================================================================
//##################################################################################################
//                              IMPOSTA STILI
//##################################################################################################
//==================================================================================================
//                               IMPOSTA STILI CHIP ATTIVO                                          (neutro)
//==================================================================================================
/**
 <style name="ChipAttivo" parent="Chip">
 <item name="chipStrokeWidth">3dp</item>
 <item name="chipStrokeColor">@color/colorPrimary</item>
 <item name="android:textColor">@color/colorPrimary</item>
 <item name="chipBackgroundColor">@color/colorBackgraund</item>
 </style>
 */
    private void setStyleChipAttivo(TextView chip){
        chip.setTextColor(context.getResources().getColor(R.color.colorNormalText));
        chip.setBackgroundResource(R.drawable.chip_attivo);
        chip.setTypeface(null, Typeface.BOLD);
    }
//==================================================================================================
//==================================================================================================
//                               IMPOSTA STILI CHIP SELEZIONATO                                     (pulsante)
//==================================================================================================
/*
<style name="ChipSelezionato" parent="Chip">
<item name="android:textColor">@color/colorBackgraund</item>
<item name="chipBackgroundColor">@color/colorPrimary</item>
</style>
 */
    private void setStyleChipSelezionato(TextView chip){
        chip.setTextColor(context.getResources().getColor(R.color.colorBackgraund));
        chip.setBackgroundResource(R.drawable.chip_selezionato);
        chip.setTypeface(null, Typeface.BOLD);
    }
//==================================================================================================
//==================================================================================================
//                               IMPOSTA STILI CHIP DISATTIVO                                       (pulsante e neutro)
//==================================================================================================
/*
<style name="ChipDisattivo" parent="Chip">
<item name="android:textColor">@color/colorNormalTextLite</item>
<item name="chipBackgroundColor">@color/colorSecondary</item>
</style>
 */
    private void setStyleChipDisattivo(TextView chip){
        chip.setTextColor(context.getResources().getColor(R.color.colorNormalTextLite));
        chip.setBackgroundResource(R.drawable.chip_disattivo);
        chip.setTypeface(null, Typeface.BOLD);
    }
//==================================================================================================
//##################################################################################################
}
