package com.example.simcareerapp.TestingApp;

import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.GareDeiCampionatiRVAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface.OnClickClassificaDellaGaraListener;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinnerAdapter.CampionatoSpinnerAdapter;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Persistence.JsonEntities.CampionatoConJson;
import com.example.simcareerapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TestAppModifcaCampionatoFragment#} factory method to
 * create an instance of this fragment.
 */
public class TestAppModifcaCampionatoFragment extends Fragment {

    //private OnFragmentInfoCampionatoFragmentEventListener listener = null;
//##################################################################################################
//                              DATI PASSATI
//##################################################################################################
    private List<Campionato> listaCampionati   = null;
//==================================================================================================
//                                  COSTRUTORI
//==================================================================================================
    public TestAppModifcaCampionatoFragment(List<Campionato> listaCampionati) {
        this.listaCampionati   = listaCampionati;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  FRAGMENT
//##################################################################################################
    private View    viewGroupLayout = null;
    private Spinner viewSpinnerCampionato = null;
//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
//==================================================================================================
//==================================================================================================
//                                  DEFINIZIONE DEL LAYOUT DEL FRAGMENT
//==================================================================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);//<<<-------------------------------------ATTEZIONE
        //Scelta del layout usato per fragment.
        viewGroupLayout = inflater.inflate(R.layout.fragment_test_app_modifca_campionato, container, false);

        //Imposta lo spinner per la selezione del Campionato
        viewSpinnerCampionato  = viewGroupLayout.findViewById(R.id.spn_InputCampionato);
        ArrayList<Campionato> listaCampionatiApp = new ArrayList<>();
        listaCampionatiApp.addAll(listaCampionati);
        ArrayAdapter adapter = new CampionatoSpinnerAdapter(getContext(),listaCampionatiApp);
        viewSpinnerCampionato.setAdapter(adapter);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  METODO CHIAMATO SULLA SELEZIONE DEL CAMPIONATO
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        viewSpinnerCampionato.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Campionato campionato = (Campionato) adapterView.getItemAtPosition(i);
                visualizaImpostazioniCampionato(campionato);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return viewGroupLayout;
    }

//==================================================================================================
//##################################################################################################



//##################################################################################################
//                                  COMUNICAZIONE CON L'ACTIVITY
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY
//==================================================================================================
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Crea il link con l'activity che contiene il fragment.
        //listener = (OnFragmentInfoCampionatoFragmentEventListener) activity;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  SELEZIONE DI UN NUOVO CAMPIONATO
//##################################################################################################
    private void visualizaImpostazioniCampionato(Campionato campionato){
        Fragment frag = new  ModificaCampionatoFragment(campionato);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fly_ModificheAlCampionato, frag);
        ft.commit();
    }
//##################################################################################################
}