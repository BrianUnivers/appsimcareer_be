package com.example.simcareerapp.Register;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.example.simcareerapp.R;
import com.example.simcareerapp.Register.Interface.OnFragmentRegisterEventListener;



/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegisterFragment} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment {
    /**
     * listener è la variabile che permette di comunicare con l'Activity
     * View     è la variabile che permette di ricavaare le view dai id
     */
    private OnFragmentRegisterEventListener listener = null;
    private View viewGroupLayout = null;

    //Get Data
    private TextView txtInputNascita;
    private DatePickerDialog.OnDateSetListener dataDiNascitaListener;
    private Calendar dataDiNascita= Calendar.getInstance();
    private int maggioreEta=18;
    private int etaMassima=100;
    //Get Email
    private static final String regex = "^(.+)@(.+)\\.(.+)$";
//==================================================================================================
//                                  COSTRUTORI
//==================================================================================================
    public RegisterFragment() {
        // Required empty public constructor
    }
//==================================================================================================


//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
//==================================================================================================


//==================================================================================================
//                                  DEFINIZIONE DEL LAYOUT DEL FRAGMENT
//==================================================================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);//<<<-------------------------------------ATTEZIONE

        //Scelta del layout usato per fragment.
        viewGroupLayout = inflater.inflate(R.layout.fragment_register, container, false);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  LISTENER DEI BOTTONI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Listener del pulsante Annula
        Button btnAnnula = (Button) viewGroupLayout.findViewById(R.id.btn_Annula);
        btnAnnula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonAnnula(v);
            }
        });
//--------------------------------------------------------------------------------------------------
        // Listener del pulsante conferma
        Button btnConferma = (Button) viewGroupLayout.findViewById(R.id.btn_Conferma);
        btnConferma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonConferma(v);
            }
        });
//--------------------------------------------------------------------------------------------------
        //Abilita il pulsante solo se si sono viste tutte le condizioni .
        txtInputNascita = (TextView) viewGroupLayout.findViewById(R.id.txt_InputNascita);
        txtInputNascita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNascita(v);
            }
        });
        dataDiNascitaListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                dataDiNascita.set(Calendar.YEAR,year);
                dataDiNascita.set(Calendar.MONTH,month);
                dataDiNascita.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                txtInputNascita.setText(""+dayOfMonth+" / "+ (1+month)+" / "+year);
            }
        };

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return viewGroupLayout;
    }
//==================================================================================================


//==================================================================================================
//                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY
//==================================================================================================
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Crea il link con l'activity che contiene il fragment.
        listener = (OnFragmentRegisterEventListener) activity;
    }
//==================================================================================================


//==================================================================================================
//                                  METODI CHIAMATI ALLA PRESIONE DEI BOTTONI
//==================================================================================================
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante annula che passera il
     * controllo all' activity che attiverà la procedura di anullamento
     */
    public void buttonAnnula(View v) {
        listener.anullaIscrizione();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante di accetazione dei termini che
     * passera il controllo all' activity che concluderà la prima fase di registazione
     */
    public void buttonConferma(View v) {
        boolean datiCorreti=true;
        //Get View
        EditText txtInputNome           =   (EditText) viewGroupLayout.findViewById(R.id.txt_InputNome);
        EditText txtInputCognome        =   (EditText) viewGroupLayout.findViewById(R.id.txt_InputCognome);
        View dvdSottolinetatura         =   (View) viewGroupLayout.findViewById(R.id.dvd_sottolinetatura);
        EditText txtInputEmail          =   (EditText) viewGroupLayout.findViewById(R.id.txt_InputEmail);
        EditText txtInputUsername       =   (EditText) viewGroupLayout.findViewById(R.id.txt_InputUsername);
        EditText txtInputPassword       =   (EditText) viewGroupLayout.findViewById(R.id.txt_InputPassword);
        EditText txtInputRipetiPassword =   (EditText) viewGroupLayout.findViewById(R.id.txt_InputRipetiPassword);
        // Get ErrorMessage
        TextView errNome            =   (TextView) viewGroupLayout.findViewById(R.id.err_Nome);
        TextView errCognome         =   (TextView) viewGroupLayout.findViewById(R.id.err_Cognome);
        TextView errNascita         =   (TextView) viewGroupLayout.findViewById(R.id.err_Nascita);
        TextView errEmail           =   (TextView) viewGroupLayout.findViewById(R.id.err_Email);
        TextView errUsername        =   (TextView) viewGroupLayout.findViewById(R.id.err_Username);
        TextView errPassword        =   (TextView) viewGroupLayout.findViewById(R.id.err_Password);
        TextView errRipetiPassword  =   (TextView) viewGroupLayout.findViewById(R.id.err_RipetiPassword);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Controllo nome
        String nome = txtInputNome.getText().toString();
        if((nome.equals("")) || nome==null || ( nome.length()>60 ) ){
            datiCorreti= false;
            txtInputNome.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorError), PorterDuff.Mode.SRC_ATOP);
            errNome.setTextColor(this.getResources().getColor(R.color.colorError));
        }else{
            txtInputNome.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            errNome.setTextColor(this.getResources().getColor(R.color.colorBackgraund));
        }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Controllo cognome
        String cognome = txtInputCognome.getText().toString();
        if((cognome.equals("")) || cognome==null || ( cognome.length()>60 ) ){
            datiCorreti= false;
            txtInputCognome.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorError), PorterDuff.Mode.SRC_ATOP);
            errCognome.setTextColor(this.getResources().getColor(R.color.colorError));
        }else{
            txtInputCognome.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            errCognome.setTextColor(this.getResources().getColor(R.color.colorBackgraund));
        }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Controllo Data Valida
        Calendar maggiorenneOggi = Calendar.getInstance();
        maggiorenneOggi.set(Calendar.YEAR,(maggiorenneOggi.get(Calendar.YEAR)-maggioreEta));
        maggiorenneOggi.set(Calendar.DAY_OF_MONTH,1);
        Calendar etaMassimaDaOggi = Calendar.getInstance();
        etaMassimaDaOggi.set(Calendar.YEAR,(etaMassimaDaOggi.get(Calendar.YEAR)-etaMassima));
        etaMassimaDaOggi.set(Calendar.DAY_OF_MONTH,1);
        if( (dataDiNascita.after(maggiorenneOggi)) || (dataDiNascita.before(etaMassimaDaOggi)) ){
            datiCorreti= false;
            dvdSottolinetatura.setBackgroundResource( R.color.colorError);
            errNascita.setTextColor(this.getResources().getColor(R.color.colorError));
        }else{
            dvdSottolinetatura.setBackgroundResource( R.color.colorPrimary);
            errNascita.setTextColor(this.getResources().getColor(R.color.colorBackgraund));
        }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Controllo username
        String username = txtInputUsername.getText().toString();
        if((username.equals("")) || username==null || (!listener.usernameValido(username)) || ( username.length()>60 ) ){
            datiCorreti= false;
            txtInputUsername.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorError), PorterDuff.Mode.SRC_ATOP);
            errUsername.setTextColor(this.getResources().getColor(R.color.colorError));
        }else{
            txtInputUsername.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            errUsername.setTextColor(this.getResources().getColor(R.color.colorBackgraund));
        }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Controllo email
        String email = txtInputEmail.getText().toString();

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);

        if((email.equals("")) || email==null || (!listener.emailValido(email)) || (!matcher.matches()) || ( email.length()>150 ) ){
            datiCorreti= false;
            txtInputEmail.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorError), PorterDuff.Mode.SRC_ATOP);
            errEmail.setTextColor(this.getResources().getColor(R.color.colorError));
        }else{
            txtInputEmail.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            errEmail.setTextColor(this.getResources().getColor(R.color.colorBackgraund));
        }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Controllo password
        String password = txtInputPassword.getText().toString();
        if((password.equals("")) || password==null  || ( password.length()>60 )){
            datiCorreti= false;
            txtInputPassword.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorError), PorterDuff.Mode.SRC_ATOP);
            errPassword.setTextColor(this.getResources().getColor(R.color.colorError));
        }else{
            txtInputPassword.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            errPassword.setTextColor(this.getResources().getColor(R.color.colorBackgraund));
        }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Controllo password ripetuta
        String ripetiPassword = txtInputRipetiPassword.getText().toString();
        if( (!password.equals(ripetiPassword)) || (ripetiPassword.equals("")) || ripetiPassword==null  || ( ripetiPassword.length()>60 ) ){
            datiCorreti= false;
            txtInputRipetiPassword.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorError), PorterDuff.Mode.SRC_ATOP);
            errRipetiPassword.setTextColor(this.getResources().getColor(R.color.colorError));
        }else{
            txtInputRipetiPassword.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            errRipetiPassword.setTextColor(this.getResources().getColor(R.color.colorBackgraund));
        }

        if(datiCorreti) {
            listener.effetuaRegistrazione( nome,  cognome,  dataDiNascita,  email,  username,  password);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Il metodo chiamato dal Lisener quando si preme sul campo data di nascità per aprire una
     * finestra di dialogo dove poter inserire la data
     **/
    public void inputNascita(View v){
        int anno = dataDiNascita.get(Calendar.YEAR);
        int mese = dataDiNascita.get(Calendar.MONTH);
        int giorno = dataDiNascita.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(
                getActivity(),
                R.style.MyDatePickerStyle,
                dataDiNascitaListener,
                anno,mese,giorno);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.show();
    }
//==================================================================================================
}