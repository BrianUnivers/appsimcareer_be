package com.example.simcareerapp.Persistence.JsonEntities;

import android.util.Log;

import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Gara;
import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;
import com.example.simcareerapp.Persistence.Entities.Pilota;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class PilotaConJson extends Pilota {
//##################################################################################################
//                                  LABEL PILOTI PER JSON
//##################################################################################################
    protected final String LABLE_NOME = "nome";
    protected final String LABLE_TEAM = "team";
    protected final String LABLE_AUTO = "auto";
//##################################################################################################
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
    public PilotaConJson(String nomeCognome, String nomeTeam, String nomeAuto) {
        super(nomeCognome, nomeTeam, nomeAuto);
    }
//-------------------------------------------------------------------------------------------------
    public PilotaConJson(Pilota pilota) {
        super(pilota.getNomeCognome(), pilota.getNomeTeam(), pilota.getAuto().getNome());
    }
//--------------------------------------------------------------------------------------------------
    public PilotaConJson(JSONObject jsonObject){
        if(jsonObject!=null) {
            try {
                this.setNomeCognome( jsonObject.getString(LABLE_NOME) );
                this.setNomeTeam(    jsonObject.getString(LABLE_TEAM) );

                this.setAuto(null);
                String nomeAuto =    jsonObject.getString(LABLE_AUTO);
                if( (nomeAuto!=null) && (!nomeAuto.equals("")) ){
                    this.setAuto(new AutoConImmagini(nomeAuto));
                }
            } catch (JSONException e) {
                Log.d("ErrJson",e.getMessage());
            }
        }
    }
//==================================================================================================
//##################################################################################################
//                              GENERA JSON
//##################################################################################################
//==================================================================================================
//                              GENERA JSON PILOTA
//==================================================================================================
    public JSONObject getJSON(){
        JSONObject object = new JSONObject();
        try {
            object.put(LABLE_NOME,      this.getNomeCognome());
            object.put(LABLE_TEAM,      this.getNomeTeam());
            object.put(LABLE_AUTO,      this.getAuto().getNome());

        }catch (JSONException e) {
            object=null;
            e.printStackTrace();
        }
        return object;
    }
//==================================================================================================
//##################################################################################################
}
