package com.example.simcareerapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


import com.example.simcareerapp.GestioneJson.GestioneListeCampionati;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp.AlertDialogIscrizioneAlCampionato;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp.AlertDialogRitiratiDalCampionato;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp.Interface.OnAletDialogIscrizioneAlCampionatoEventListener;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp.Interface.OnAletDialogRitiratiDalCampionatoEventListener;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface.OnClickClassificaDellaGaraListener;
import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Gara;
import com.example.simcareerapp.Persistence.Entities.Pilota;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Persistence.JsonEntities.CampionatoConJson;
import com.example.simcareerapp.Persistence.JsonEntities.PilotaConJson;
import com.example.simcareerapp.SingoloCampionato.GareCampionatoFragment;
import com.example.simcareerapp.SingoloCampionato.InfoCampionatoFragment;
import com.example.simcareerapp.SingoloCampionato.Interface.OnFragmentInfoCampionatoFragmentEventListener;
import com.example.simcareerapp.SingoloCampionato.PartecipantiFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

public class SingoloCampionatoActivity
                        extends AppCompatActivity
                        implements OnFragmentInfoCampionatoFragmentEventListener,
                                   OnAletDialogIscrizioneAlCampionatoEventListener,
                                   OnAletDialogRitiratiDalCampionatoEventListener,
                                   OnClickClassificaDellaGaraListener {

//##################################################################################################
//                              DATI PASSATI TRAMITE UN INTENT
//##################################################################################################
    private Utente utenteAttivo             = null;
    private Boolean mantieniAttivoUtente    = null;
    private CampionatoConJson campionato    = null; // Non passato ma ricavato
    private List<CampionatoConJson> listaCampionati = null; // Serve per salvare le modifiche.
//==================================================================================================
//                              METODO DI RECUPERO DI TALI VALORI DAL INTET CHE HA CHAMATO LA PAGINA
//==================================================================================================
    /**
     * Questo metodo serve per prendere dall'Activity chiamante le informazioni della sessione che
     * di obbligatori sono <<utenteAttivo>>, <<mantieniAttivoUtente>> e <<Campionato>>.
     * Quest'ultimo però viene ricavato dall'id che viene passato
     *
     * Se non dovesse trovare il campionato con quel'id si ritorna alla pagina dei campionati.
     */
    private void initValoriDiSessioneDellAPP(){
        Intent i =getIntent();
        utenteAttivo            = MyIntent.getUtenteExtra(i,MyIntent.I_UTENTE_ATTIVO);
        mantieniAttivoUtente    = i.getBooleanExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,false);
        Long idCampionato       = i.getLongExtra(MyIntent.I_ID_CAMPIONATO_SELEZIONATO,-1);
        //Verifico se nel dataBase non ci sono valori
        if(idCampionato==-1){
            apriPaginaListaCampionati();
        }else{
            CampionatoConJson tempCampionato=null;
            listaCampionati = GestioneListeCampionati.letturaECreazioneListaCampionati(this,R.raw.campionati);
            if( (listaCampionati!=null) && (listaCampionati.size()!=0)){
                int j=-1;
                boolean campionatoTrovato=false;
                do {
                    j++;
                    tempCampionato = listaCampionati.get(j);
                    if(tempCampionato.getId()==idCampionato){
                        campionato = tempCampionato;
                        campionatoTrovato = true;
                    }else{
                        if(j==listaCampionati.size()-1){
                            campionatoTrovato = true;
                        }
                    }
                }while (!campionatoTrovato);
            }
        }
        if(campionato==null){
            apriPaginaListaCampionati();
        }
    }
//==================================================================================================
//==================================================================================================
//                              APERTURA DELLA PAGINA DI LISTA CAMPIONATI PASSANDO I VALORI DI SESSIONE
//==================================================================================================
    /**
     * Questo metodo serve per inviare le informazioni della sessione che sono <<utenteAttivo>> e
     * <<mantieniAttivoUtente>> quelli obbligatori.
     * N.B.
     * Da questà activiti è possibile solo andare alla Activity CampionatiActivity senza apportare
     * modifiche.
     */
    private void apriPaginaListaCampionati(){
        MyIntent i = new MyIntent(SingoloCampionatoActivity.this, CampionatiActivity.class);
        i.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utenteAttivo);
        i.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
        startActivity(i);
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Sovrascrive il tasto indietro fisico
     */
    @Override
    public void onBackPressed() {
        apriPaginaListaCampionati();
    }
//==================================================================================================
//==================================================================================================
//                              APERTURA DELLA PAGINA DELLE CLASSIFICHE I VALORI DI SESSIONE
//==================================================================================================
    /**
     * Questo metodo serve per inviare le informazioni della sessione che sono <<utenteAttivo>> e
     * <<mantieniAttivoUtente>> quelli obbligatori e  <<idCampiojnato>> così da visualizarlo.
     * N.B.
     * Da questà activiti è possibile solo andare alla Activity ClassificaActivity senza apportare
     * modifiche.
     */
    private void apriPaginaClassificaCampionato(Long id){
        MyIntent i = new MyIntent(SingoloCampionatoActivity.this, ClassificaActivity.class);
        i.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utenteAttivo);
        i.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
        i.putExtra(MyIntent.I_ID_CAMPIONATO_SELEZIONATO,id);
        startActivity(i);
    }
//==================================================================================================
//##################################################################################################



//##################################################################################################
//                              DATI PER GESTIRE IL LAYOUT
//##################################################################################################
    //Toolbar da implemetare
    private Toolbar     toolbar     = null;
    private BottomNavigationView bottomTabarView = null;
//##################################################################################################
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  GET DATI DAL INTENT
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        initValoriDiSessioneDellAPP();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        setContentView(R.layout.activity_singolo_campionato);
        //Implemetazione della toolbar
        toolbar =  findViewById(R.id.include_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24_white);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  LISTENER DEI BOTTONI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Listener del pulsante Annula modifiche
        if(toolbar!=null)
            toolbar.setNavigationOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    apriPaginaListaCampionati();
                }
            });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  BOTTONI DELLA TABAR IN BASSO
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        bottomTabarView = findViewById(R.id.tbb_TabCampionato);
        bottomTabarView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener(){
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return cambiaSchedaDaVisualizare(menuItem);
            }
        } );
        bottomTabarView.setSelectedItemId(R.id.btn_tab_InfoCampionato);//Selezione della prima vita
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
//##################################################################################################




//##################################################################################################
//                                  GESTIONE DEL MENU' SULLA TOOLBAR
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE DEL MENU' (senza il tasto a sinistra)
//==================================================================================================
    /**
     * Metodo per definire il layout della toolbar definedo quali sono le sue voci.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bar_menu_vuoto,menu);
        return true;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  OPERAZIONI FATTE QUANDO SI PREME SU UNA TAB
//##################################################################################################
//==================================================================================================
//                                  CAMBIA LA SCHEDA DA VISUALIZATA DELE CAMPIONATO
//==================================================================================================
    /**
     * Questo metodo vinene chiamato tramite il lissener,della tabbar quando viene premuto un dei quatro
     * tab viene cambiato il contenuto del FrameLayout
     */
    private boolean  cambiaSchedaDaVisualizare(@NonNull MenuItem menuItem){
        Fragment frag = null;

        boolean ris = true;
        switch (menuItem.getItemId()){
            case R.id.btn_tab_InfoCampionato:
                frag = new InfoCampionatoFragment(campionato,utenteAttivo);
                break;
            case R.id.btn_tab_GaraCampionato:
                frag = new GareCampionatoFragment(campionato,utenteAttivo,this);
                break;
            case R.id.btn_tab_PartecipantiCampionato:
                frag = new PartecipantiFragment(campionato,utenteAttivo);;
                break;
            case R.id.btn_tab_ForumCampionato:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.simcareer.org/forum/"));
                startActivity(browserIntent);
                frag = null;
                break;
            default:
                ris=false;
        }
        if( (ris) && (frag!=null) ){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fly_InformazioniCampionato, frag);
            ft.commit();
        }
        return ris;
    }
//==================================================================================================
//##################################################################################################




//##################################################################################################
//                                  METODI PER INTERAGIRE CON  FragmentInfoCampionatoFragment
//##################################################################################################
//==================================================================================================
//                                  ISTRIZIONE AL CAMPIONATO
//==================================================================================================
    /**
     * Metodo per l'iscrizione al campionato. Cioè apre l'AletDilog per prendere le informazioni
     * sull'iscrizione per poi modificare il file json dei campionati.
     */
    @Override
    public void creaNuovaIscrizioneAlCampionato() {
        AlertDialogIscrizioneAlCampionato alertDialog= new AlertDialogIscrizioneAlCampionato(
                this,campionato,utenteAttivo,
                R.layout.alert_iscrizione_al_campionato,
                R.drawable.ic_baseline_person_add_24_white,
                R.string.ttl_alert_IscrizioneCampinato,
                R.string.lbl_ButtonConferma,
                R.string.lbl_ButtonAnnula);
        alertDialog.create();
        alertDialog.show();
    }
//==================================================================================================
//==================================================================================================
//                                  CANCELLAZIONE  DEL'ISCRIZIONE AL CAMPIONATO
//==================================================================================================
    /**
     * Metodo per la cancellazione dell'iscrizione al campionato. Cioè apre l'AletDilog per una conferma
     * per poi modificare il file json dei campionati.
     */
    @Override
    public void cancellaIscrizioneAlCampionato() {
        AlertDialogRitiratiDalCampionato alertDialog= new AlertDialogRitiratiDalCampionato(
                this,campionato,utenteAttivo,
                R.layout.alert_ritirati_dal_campionato,
                R.drawable.ic_baseline_person_minus_24_white,
                R.string.ttl_alert_Attenzione,
                R.string.lbl_ButtonRitirati,
                R.string.lbl_ButtonAnnula);
        alertDialog.create();
        alertDialog.show();
    }
//==================================================================================================
//==================================================================================================
//                                  VISUALIZZA CLASSIFICA
//==================================================================================================
    /**
     * Metodo per chiamere l'activity per la visualizzazione della classifca del campionato.
     */
    @Override
    public void classificaCampionato() {
        apriPaginaClassificaCampionato(campionato.getId());
    }
//==================================================================================================
//##################################################################################################


//##################################################################################################
//                                  METODI PER INTERAGIRE CON  AletDialogIscrizioneAlCampionato
//##################################################################################################
    @Override
    public boolean informazioniPerNuovaIscrizioneAlCampionato(String nomeTeam, AutoConImmagini auto) {
        Boolean ris;
        if( (nomeTeam.equals("")) || (nomeTeam==null) || (auto==null)  ){
            ris= false;
        }else{
            String nomeCognomePilota = utenteAttivo.getNome()+" "+utenteAttivo.getCognome();
            Pilota pilota =new PilotaConJson(nomeCognomePilota,nomeTeam,auto.getNome());
            ris = true;
            campionato.getListaPiloti().add(pilota);
            bottomTabarView.setSelectedItemId(R.id.btn_tab_InfoCampionato);//Aggiorna fragment
            GestioneListeCampionati.aggiornaListaCampionati(this,R.raw.campionati,listaCampionati);
        }
        return ris;
    }
//##################################################################################################

//##################################################################################################
//                                  METODI PER INTERAGIRE CON  AletDialogRitiratiDalCampionato
//##################################################################################################
    @Override
    public boolean informazioniPerAnnulareIscrizioneAlCampionato(Pilota pilota) {
        Boolean ris= false;;
        if( pilota==null ){
            ris= false;
        }else{
            if(campionato.getListaPiloti().remove(pilota)){
                ris = true;
                GestioneListeCampionati.aggiornaListaCampionati(this,R.raw.campionati,listaCampionati);
            }
            bottomTabarView.setSelectedItemId(R.id.btn_tab_InfoCampionato);//Aggiorna fragment
        }
        return ris;
    }
//##################################################################################################

//##################################################################################################
//                                  METODI PER INTERAGIRE CON  OnClickClassificaDellaGaraListener
//##################################################################################################
    @Override
    public void visualizzaClassificaDelCampionato(Campionato campionato, Gara gara) {
        apriPaginaClassificaCampionato(campionato.getId());
    }
//##################################################################################################
}