package com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinner;



import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinnerImmagineConValoreAdapter.MySpinnerAdapter;
import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;
import com.example.simcareerapp.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class SpinnerConImmagine  implements  AdapterView.OnItemSelectedListener {//RecyclerView.Adapter
    private Context context;
    private ViewGroup bloccoSpinner;
    private Spinner spinner;
    private ImageView imageSpinnerView;
    private FloatingActionButton btnBack;
    private FloatingActionButton btnNext;
    private ArrayList<ValoreConImmaginePerSpinner> lista=null;
//##################################################################################################
//                                  COSTRUTTORI IMPOSTA LO STILE DELLE PARTE STATICHE
//##################################################################################################
    /**
     * Costruisce e gestice uno spinnere che modifica in base ai suoi valoi un immagine e ha due
     * bottoni che permettono di passare al valor succesiovo e o precendete cambiando immagine.
     */
//==================================================================================================
//                                  COSTRUTORE PER ACTIVITY
//==================================================================================================
    public SpinnerConImmagine(final AppCompatActivity context, int idImpost, int idText) {
        this.context = context;
        //Impostazione grafica di base
        bloccoSpinner = (ViewGroup) context.findViewById(idImpost);
        init(idImpost,idText);
    }
//==================================================================================================
//==================================================================================================
//                                  COSTRUTORE PER ALERTDILOG o TRAMITE VIEW
//==================================================================================================
    public SpinnerConImmagine( Context context,View view, int idImpost, int idText) {
        this.context = context;
        //Impostazione grafica di base
        bloccoSpinner = (ViewGroup) view.findViewById(idImpost);
        init(idImpost,idText);
    }
//==================================================================================================
//==================================================================================================
//                                  INIZIALIZZAZIONE COMUNE
//==================================================================================================
    private void init(int idImpost, int idText){
        TextView label = (TextView) bloccoSpinner.findViewById(R.id.txt_EtichettaDelValore);
        label.setText(idText);
        //Impostazione dello spinner
        spinner          = bloccoSpinner.findViewById(R.id.spn_SpinnerDeiValori);
        imageSpinnerView = bloccoSpinner.findViewById(R.id.img_ImmagineDelValore);
        //pilsanti
        btnBack = bloccoSpinner.findViewById(R.id.btn_BackValue);
        btnNext = bloccoSpinner.findViewById(R.id.btn_NextValue);
//--------------------------------------------------------------------------------------------------
//                                      BOTTONE ELEMETO PRECEDENTE
//--------------------------------------------------------------------------------------------------
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                precedente();
            }
        });
//--------------------------------------------------------------------------------------------------
//                                      BOTTONE ELEMETO SUCCESIVO
//--------------------------------------------------------------------------------------------------
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void  onClick(View v) {
                prossmo();
            }
        });
//--------------------------------------------------------------------------------------------------
    }
//==================================================================================================
//##################################################################################################
//==================================================================================================
//                                      METODI BLOCCANTI CHIAMATI DAL BOTTONE
//==================================================================================================
    /**
     * ATTENZIONE!
     * Prossimo elemeto dello spinner, questo metodo è definito synchronized poiche se si cambiava molto
     * velocemete le immagini portava l'applicazione ad essere uccisa dal Sistema Operativo.
     */
    synchronized private void prossmo(){
        int pos=spinner.getSelectedItemPosition();
        if((lista!=null)&&pos<lista.size()-1){
            pos++;
        }else{
            //Fatto un ciclo completo riparto dall'inizio
            pos=0;
        }
        spinner.setSelection(pos);
    }
//--------------------------------------------------------------------------------------------------
    /**
     * ATTENZIONE!
     * Precedente elemeto dello spinner, questo metodo è definito synchronized poiche se si cambiava molto
     * velocemete le immagini portava l'applicazione ad essere uccisa dal Sistema Operativo.
     */
    synchronized private void precedente(){
        int pos=spinner.getSelectedItemPosition();
        if((lista!=null)&&pos>0 ){
            pos--;
        }else{
            //Fatto un ciclo completo riparto dall'inizio
            pos=lista.size()-1;
        }
        spinner.setSelection(pos);
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Questo metodo permette di trovare e impostare il valore un valore qualsiasi all'interno dello ù
     * spinner.
     * ATTENZIONE!
     * Precedente elemeto dello spinner, questo metodo è definito synchronized poiche se si cambiava molto
     * velocemete le immagini portava l'applicazione ad essere uccisa dal Sistema Operativo.
     */
    synchronized public void impostaElemetoSePresente(ValoreConImmaginePerSpinner elemeto){
        if( lista != null){
            int pos = lista.indexOf(elemeto);
            if(pos>=0){
                spinner.setSelection(pos);
            }
        }
    }
//==================================================================================================

//==================================================================================================
//                                  IMPOSTAZIONI DELLE VARIANTI DI BLOCHI INCLUSI
//==================================================================================================
    /**
     * Associa allo spinne una lista di oggeti di tipo ValoreConImmaginePerSpinner che veranno selezionati
     * dallo spinner.
     */
    public void setSpinnerConImmagineGenerico(ArrayList<ValoreConImmaginePerSpinner> lista) {
        this.lista=lista;
        MySpinnerAdapter adapter = new MySpinnerAdapter(context,lista);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }
//==================================================================================================

//==================================================================================================
//                                  SELEZIONE E CAMBIA IMMAGINE DI UN ELEMETO
//==================================================================================================
    /**
     * Questo metodo permette di cambiare l'immagine che mostra la voce selezionata.
     */
    @Override
    public void onItemSelected(AdapterView<?> parent , View view, int posizione, long id) {
        ValoreConImmaginePerSpinner elemento = (ValoreConImmaginePerSpinner) parent.getItemAtPosition(posizione);
        imageSpinnerView.setImageResource(elemento.getIdImmagine());

        ImageView imageView = bloccoSpinner.findViewById(R.id.img_ImmagineDelValore);
        imageView.setImageResource(elemento.getIdImmagine());

        //Se si vulole aggiungere
        onItemSelected(elemento);
    }
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }
//==================================================================================================

//==================================================================================================
//                                  AZIONI AGGIUNTIVE
//==================================================================================================
    /**
     * Questo metodo permette di aggiungere delle azzioni dopo aver selezionato un nuovo elemeto
     * così che le classi succesive non devono riscrivere tutto il codice di cambiameto del blocco
     */
    protected void onItemSelected(ValoreConImmaginePerSpinner elemento) {
    }
//==================================================================================================
}
