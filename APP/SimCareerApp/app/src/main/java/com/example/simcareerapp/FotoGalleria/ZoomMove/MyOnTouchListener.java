package com.example.simcareerapp.FotoGalleria.ZoomMove;

import android.content.Context;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;

import com.example.simcareerapp.R;

import java.util.Date;

public class MyOnTouchListener implements View.OnTouchListener {
    private View view;
    private View fatherView;
    private ScaleGestureDetector scaleGestureDetector;
    private ZoomListener zoomListener;
//==================================================================================================
//                                      COSTRUTTORE
//==================================================================================================
    public MyOnTouchListener(Context context, ViewGroup fatherView ,View view) {
        this.view = view;
        this.fatherView = fatherView;
        zoomListener=new ZoomListener(view);
        scaleGestureDetector = new ScaleGestureDetector(context, zoomListener);
    }
//--------------------------------------------------------------------------------------------------
    public MyOnTouchListener(Context context,ViewGroup fatherView ,View view, float zoom) {
        this.view = view;
        this.fatherView = fatherView;
        zoomListener=new ZoomListener(view,zoom);
        scaleGestureDetector = new ScaleGestureDetector(context, zoomListener);
    }
//==================================================================================================
//##################################################################################################
//                              GESTIONE ZOOM E MOVE
//##################################################################################################
//==================================================================================================
//                              STATI POSSIBILI IN QUI POSSO TROVARMI PER FARE LE DUE AZIONI
//==================================================================================================
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;//Inozioalizazione
//==================================================================================================
//==================================================================================================
//                              VARIABILI CHE SERVONO SOLO PER LA MOVE
//==================================================================================================
    private Float xOld = null;
    private Float yOld = null;
    private Float x = null;
    private Float y = null;
//==================================================================================================
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        /*
        Questo switch case serve per capire se l'utente vuole zoomare
        o muovere l'immagine
         */
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN://-----|
                  /*                            V
                  Indico che la azione che si deve eseguire è di tipo DRAG
                 */
                mode = DRAG;
                break;
            case MotionEvent.ACTION_POINTER_DOWN://-----|
                  /*                                    V
                  Indico che la azione che si deve eseguire è di tipo ZOOM
                 */
                mode = ZOOM;
                break;
            case MotionEvent.ACTION_UP://-------------|
            case MotionEvent.ACTION_POINTER_UP://-----|
                /*                                    V
                Al termine di una qualsiasi operazione fatta viene chamato
                questo metodo con una di queste due azioni possibili così
                le si usa per imostare la prossima zaione di tipo NONE
                e resettare i valori per la MOVE così che anchè i succesivi
                movimeti siano fluidi e senza teletraporti di luncheze dipendenti
                da dove si è alzato il dito.
                */
                mode = NONE;
                xOld = x=null;
                yOld = y=null;
                break;
            case MotionEvent.ACTION_MOVE:

                if (mode == DRAG) {
                    if((x!=null)&&(y!=null)){
                        //Metti posizioneditopecedente in quella vechia
                        xOld = x;
                        yOld = y;
                    }
                    //Prendi pusizione attuale del dito
                    x = new Float((int) motionEvent.getRawX());
                    y = new Float((int) motionEvent.getRawY());

                    if((xOld!=null)&&(yOld!=null)){//Aggiuna tranzazione immagine
                        //TODO: METTERE DEI BLOCCHI PER NON USCRIE DALLA PAGINA
                        if ( ((view.getWidth()*zoomListener.getZoom()))>(fatherView.getWidth()) ){
                            view.setTranslationX(view.getTranslationX() - (xOld-x));
                        }
                        if( ((view.getHeight()*zoomListener.getZoom()))>(fatherView.getHeight()) ){
                            view.setTranslationY(view.getTranslationY() - (yOld-y));
                        }
                    }
                }else if (mode == ZOOM) {
                    scaleGestureDetector.onTouchEvent(motionEvent);
                }
                break;
        }
        fatherView.invalidate();
        return true;
    }
//==================================================================================================
//#####################################################################################################
//==================================================================================================
//                                  RESET ZOOM e MOVE IMMAGINE
//==================================================================================================
    public void reset(){
        this.zoomListener.resetZoom();
        this.view.setTranslationX(0);
        this.view.setTranslationY(0);
    }
//==================================================================================================
//#####################################################################################################
}

