package com.example.simcareerapp.TestingApp;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MyChipAdapter.MyChipAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinner.Interface.OnItemSelectedSpinnerConImmagineListener;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinner.SpinnerConImmagineCustom;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinnerAdapter.ConsumiSpinnerAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyView.MyGridView;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySeparatoriPagine.SeparatorePaginaConTesto;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinner.SpinnerConImmagine;
import com.example.simcareerapp.Persistence.Entities.CircuitoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Gara;
import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;
import com.example.simcareerapp.Persistence.Entities.Meteo;
import com.example.simcareerapp.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ModificaGaraFragment#} factory method to
 * create an instance of this fragment.
 */
public class ModificaGaraFragment extends Fragment implements OnItemSelectedSpinnerConImmagineListener {

    //private OnFragmentInfoCampionatoFragmentEventListener listener = null;
//##################################################################################################
//                              DATI PASSATI
//##################################################################################################
    private Gara  gara    = null;
    private Meteo meteo   = null;
    private DatePickerDialog.OnDateSetListener dataGaraListener;
    private Calendar data= Calendar.getInstance();
//==================================================================================================
//                                  COSTRUTORI
//==================================================================================================
    public ModificaGaraFragment(Gara gara, Meteo meteo) {
        this.gara   = gara;
        this.meteo  = meteo;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  FRAGMENT
//##################################################################################################
    private View       viewGroupLayout        = null;
    private MyGridView gridViewAiutiAllaGuida = null;
    private MyGridView gridViewBandiere       = null;
    private Spinner    viewSpinnerCarburante  = null;
    private Spinner    viewSpinnerGomme       = null;
    private SpinnerConImmagine spinnerCircuito= null;
    private EditText   editTextTemperaturaMinima  = null;
    private EditText   editTextTemperaturaMassima = null;
    private EditText   editTextUmidita            = null;
    private TextView   editTextGiorno             = null;
    private MyGridView gridViewMeteo          = null;
//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
//==================================================================================================
//==================================================================================================
//                                  DEFINIZIONE DEL LAYOUT DEL FRAGMENT
//==================================================================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);//<<<-------------------------------------ATTEZIONE
        //Scelta del layout usato per fragment.
        viewGroupLayout = inflater.inflate(R.layout.fragment_modifica_gara, container, false);


        editTextGiorno                 = viewGroupLayout.findViewById(R.id.txt_InputGiorno);
        editTextTemperaturaMinima      = viewGroupLayout.findViewById(R.id.txt_InputTemperaturaMinima);
        editTextTemperaturaMassima     = viewGroupLayout.findViewById(R.id.txt_InputTemperaturaMassima);
        editTextUmidita                = viewGroupLayout.findViewById(R.id.txt_InputUmidita);
        gridViewMeteo                  = (MyGridView) viewGroupLayout.findViewById(R.id.grd_Meteo);
        //Inizializazione contenuti impitatati
        setLayoutDeiInclude(viewGroupLayout);
        //Inizializza layout con i dati
        setLayoutPerGara();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  PEREMERE SUI GRIDVIEW
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Meteo
        gridViewMeteo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,int position, long id) {
                Pair<String, Boolean> elemeto=(Pair<String, Boolean>)gridViewMeteo.getItemAtPosition(position);
                Pair<String, Boolean>  nuovoElemeto = new Pair<>(elemeto.first,!elemeto.second);
                meteo.setStatoMeteo(nuovoElemeto);
                MyChipAdapter adapterBandiere=new MyChipAdapter(getActivity(),meteo.getStatoMeteo(),true);
                gridViewMeteo.setAdapter(adapterBandiere);
            }
        });
//--------------------------------------------------------------------------------------------------
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  CAMBIAMETI VALORI TESTUALI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        editTextTemperaturaMinima.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            public void afterTextChanged(Editable s) {
                Double valore = null;
                try {
                    valore = Double.parseDouble(s.toString());
                }catch (Exception e){}
                if( (valore!=null) && (valore<meteo.getMaxTemperatura()) && (valore>-60) && (valore<60)){
                    meteo.setMinTemperatura(valore);
                }else if((valore!=null) && (valore>=meteo.getMaxTemperatura())){
                    editTextTemperaturaMinima.setError("Temperatura minima è maggiore di quella massima");
                }else{
                    editTextTemperaturaMinima.setError("Temperatura minima deve essere compresa tra -60°C e 60°C");
                }
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        editTextTemperaturaMassima.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            public void afterTextChanged(Editable s) {
                Double valore = null;
                try {
                    valore = Double.parseDouble(s.toString());
                }catch (Exception e){}
                if( (valore!=null) && (valore>meteo.getMinTemperatura()) && (valore>-60) && (valore<60)){
                    meteo.setMaxTemperatura(valore);
                }else if((valore!=null) && (valore<=meteo.getMinTemperatura())){
                    editTextTemperaturaMassima.setError("Temperatura massima è minore di quella minima");
                }else{
                    editTextTemperaturaMassima.setError("Temperatura massima deve essere compresa tra -60°C e 60°C");
                }
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        editTextUmidita.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            public void afterTextChanged(Editable s) {
                Double valore = null;
                try {
                    valore = Double.parseDouble(s.toString());
                }catch (Exception e){}
                if( (valore!=null)  && (valore>0) && (valore<100)){
                    meteo.setUmidita(valore);
                }else{
                    editTextUmidita.setError("Umididita deve essere compresa tra 0°C e 100°C");
                }
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        editTextGiorno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputData(v);
            }
        });
        dataGaraListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Date oggi = new Date();
                data.set(Calendar.YEAR, year);
                data.set(Calendar.MONTH, month);
                data.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                if(oggi.before(data.getTime())) {
                    gara.setData(data.getTime());
                    editTextGiorno.setText("" + dayOfMonth + " / " + (1+month) + " / " + year);
                    editTextGiorno.setError(null);
                }else{
                    editTextGiorno.setError("Il giono selezionato è nel passato.");
                }
            }
        };
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return viewGroupLayout;
    }
//==================================================================================================
//                                 LISTENER SELEZIONE DATA
//==================================================================================================
    public void inputData(View v){
        data.setTime(gara.getData());
        int anno = data.get(Calendar.YEAR);
        int mese = data.get(Calendar.MONTH);
        int giorno = data.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(
                getActivity(),
                R.style.MyDatePickerStyle,
                dataGaraListener,
                anno,mese,giorno);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.show();
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                                 ADATTA IL CONTENUTO DEL LAYOUT
//##################################################################################################
//==================================================================================================
//                                 IMPOSTAZIONI DELLE VARIANTI DI BLOCHI INCLUSI
//==================================================================================================
    /**
     * Questo metodo imposta il layout dei bolochi neutri che vengono impotati per il loro riutilizzo.
     */
    private void setLayoutDeiInclude(View viewGroupLayout) {
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude((ViewGroup) viewGroupLayout,
                R.id.include_dvd_DivisoreMeteo,
                R.string.lbl_Meteo,
                R.drawable.ic_baseline_satellite_solid_normal_text);

        ArrayList<ValoreConImmaginePerSpinner> listaTotaleCircuiti  = new ArrayList<ValoreConImmaginePerSpinner>();
        listaTotaleCircuiti.add(gara.getCircuito());
        listaTotaleCircuiti.addAll(CircuitoConImmagini.getAllListaCircuito());
        spinnerCircuito = new SpinnerConImmagineCustom(getContext(),this,viewGroupLayout,R.id.include_Circuito,R.string.lbl_Crircuito);
        spinnerCircuito.setSpinnerConImmagineGenerico(listaTotaleCircuiti);

    }
//==================================================================================================
//==================================================================================================
//                                 IMPOSTAZIONI IL LAYOUTO IN BASE AL CAMPIONATO
//==================================================================================================
    /**
     * Questo metodo imposta il layout in base alla gara scelto
     */
    //Formattazione
    private static String FORMATTAZIONE_DATE = "dd/MM/yyyy";
    private void setLayoutPerGara(){
        if(meteo!=null){
            editTextTemperaturaMassima.setText(meteo.getMaxTemperatura().toString());
            editTextTemperaturaMinima.setText(meteo.getMinTemperatura().toString());
            editTextUmidita.setText(meteo.getUmidita().toString());

            //Imposta Tempo
            MyChipAdapter adapterMeteo=new MyChipAdapter(getActivity(),meteo.getStatoMeteo(),true);
            gridViewMeteo.setAdapter(adapterMeteo);
        }
        if(gara!=null){
            SimpleDateFormat sdf = new SimpleDateFormat(FORMATTAZIONE_DATE);
            String oggi = sdf.format(gara.getData());
            editTextGiorno.setText(oggi);
        }
    }
//==================================================================================================
//##################################################################################################



//##################################################################################################
//                                  COMUNICAZIONE CON L'ACTIVITY
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY
//==================================================================================================
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Crea il link con l'activity che contiene il fragment.
        //listener = (OnFragmentInfoCampionatoFragmentEventListener) activity;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  COMUNICAZIONE CON BLOCCHI
//##################################################################################################
//==================================================================================================
//                                  IMPLEMETAZIONI DEL SPINNER OnItemSelectedSpinnerConImmagineListener
//==================================================================================================
    @Override
    public void onItemSelectedSpinnerConImmagineCustom(ValoreConImmaginePerSpinner elemento){
        gara.setCircuito((CircuitoConImmagini) elemento);
    }
//==================================================================================================
//##################################################################################################
}