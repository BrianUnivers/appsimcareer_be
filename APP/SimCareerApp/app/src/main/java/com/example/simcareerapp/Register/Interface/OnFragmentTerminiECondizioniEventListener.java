package com.example.simcareerapp.Register.Interface;

//##################################################################################################
//                                  INTERFACCIA PER COMUNICARE
//##################################################################################################

/**
 * Interfacca per interagire con questo fragment che deve essere implementato dall'Activity con
 * cui si vule collabolrare.
 */
public interface OnFragmentTerminiECondizioniEventListener {
    void anullaIscrizione();
    void accettaTerminiECondizioni();
}
//##################################################################################################
//##################################################################################################