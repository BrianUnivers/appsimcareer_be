package com.example.simcareerapp.Persistence.Entities;

import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;

public class Pilota {
//##################################################################################################
//                                  INFORMAZIONI PILOTI
//##################################################################################################
    protected String nomeCognome      = null;
    protected String nomeTeam         = null;
    protected AutoConImmagini auto    = null;
//##################################################################################################
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
    protected Pilota() {
    }
//--------------------------------------------------------------------------------------------------
    public Pilota(String nomeCognome, String nomeTeam, String nomeAuto) {
        this.nomeCognome = nomeCognome;
        this.nomeTeam = nomeTeam;
        this.auto = new AutoConImmagini(nomeAuto);
    }
//==================================================================================================
//==================================================================================================
//                              GETTER E SETTER
//==================================================================================================
    public String getNomeCognome() {
        return nomeCognome;
    }
//------------------------------
    public void setNomeCognome(String nomeCognome) {
        this.nomeCognome = nomeCognome;
    }
//--------------------------------------------------------------------------------------------------
    public String getNomeTeam() {
        return nomeTeam;
    }
//------------------------------
    public void setNomeTeam(String nomeTeam) {
        this.nomeTeam = nomeTeam;
    }
//--------------------------------------------------------------------------------------------------
    public AutoConImmagini getAuto() {
        return auto;
    }
//------------------------------
    public void setAuto(AutoConImmagini auto) {
        this.auto = auto;
    }
//==================================================================================================
//==================================================================================================
//                              VERIFICA CHE NON SIA UN OGGETO CON PARAMETRI NULL
//==================================================================================================
public boolean isNull(){
    return (  (getNomeCognome()==null) || (getNomeCognome().equals("")) ||
              (getNomeTeam().equals("")) || (getNomeTeam()==null) || (getAuto()==null) );
}
//==================================================================================================
}
