package com.example.simcareerapp.Persistence.Entities;

public class Foto {
    private Long    id               = null;
    private Long    idUtente        = null;
    private String  testoAlternativo = null;
    private Boolean statoAttivo     = false;
    private String  urlFile          = null;
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
    public Foto() {
    }
//--------------------------------------------------------------------------------------------------
    public Foto(Long idUtente, String testoAlternativo, Boolean statoAttivo, String urlFile) {
        this.idUtente = idUtente;
        this.testoAlternativo = testoAlternativo;
        this.statoAttivo = statoAttivo;
        this.urlFile = urlFile;
    }
//--------------------------------------------------------------------------------------------------
    public Foto(Long id, Long idUtente, String testoAlternativo, Boolean statoAttivo, String urlFile) {
        this.id = id;
        this.idUtente = idUtente;
        this.testoAlternativo = testoAlternativo;
        this.statoAttivo = statoAttivo;
        this.urlFile = urlFile;
    }
//==================================================================================================
//==================================================================================================
//                              GETTER E SETTER
//==================================================================================================
    public Long getId() {
        return id;
    }
//------------------------------
    public void setId(Long id) {
        this.id = id;
    }
//--------------------------------------------------------------------------------------------------
    public Long getIdUtente() {
        return idUtente;
    }
//------------------------------
    public void setIdUtente(Long idUtente) {
        this.idUtente = idUtente;
    }
//--------------------------------------------------------------------------------------------------
    public String getTestoAlternativo() {
        return testoAlternativo;
    }
//------------------------------
    public void setTestoAlternativo(String testoAlternativo) {
        this.testoAlternativo = testoAlternativo;
    }
//--------------------------------------------------------------------------------------------------
    public Boolean getStatoAttivo() {
        return statoAttivo;
    }
//------------------------------
    public void setStatoAttivo(Boolean statoAttivo) {
        this.statoAttivo = statoAttivo;
    }
//--------------------------------------------------------------------------------------------------
    public String getUrlFile() {
        return urlFile;
    }
//------------------------------
    public void setUrlFile(String urlFile) {
        this.urlFile = urlFile;
    }
//==================================================================================================
}
