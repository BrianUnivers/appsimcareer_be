package com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinnerAdapter;

import android.content.Context;
import android.renderscript.Sampler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Consumi;
import com.example.simcareerapp.R;

import java.util.ArrayList;

public class ConsumiSpinnerAdapter extends ArrayAdapter<String> {
    private ArrayList<String> lista = null;

//##################################################################################################
//                                  VALORI DELLE IMPOSTAZIONI DI GIOCO
//##################################################################################################
    private static final String VALORE_BASSI   = "Bassi";
    private static final String VALORE_NORMALE = "Normale";
    private static final String VALORE_ELEVATI = "Elevati";
//##################################################################################################
//==================================================================================================
//                                  COSTRUTTRI
//==================================================================================================
    public ConsumiSpinnerAdapter(Context context, ArrayList<String> lista) {
        super(context, 0, lista);
        this.lista = lista;
    }
//==================================================================================================

//==================================================================================================
//                                  IMPOSTA VISUALIZZAZIONE DEL ELEMENTO SELEZIONATO
//==================================================================================================
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }
//==================================================================================================

//==================================================================================================
//                                  IMPOSTA VISUALIZZAZIONE DEI ELEMENTI ELEMENTO
//==================================================================================================
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }
//==================================================================================================

//==================================================================================================
//                                  IMPOSTA IL LAYOUT PER LE TIGHE
//==================================================================================================

    /**
     * Questo metodo restituisce la view da usare per la singola voce sia selezionata sia per la lista
     * dello spinner.
     */
    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.row_spinner,
                    parent, false);
        }
        TextView viewTesto = convertView.findViewById(R.id.txt_Testo);

        String elemento = getItem(position);
        if (elemento != null) {
            viewTesto.setText(elemento);
        }
        return convertView;
    }
//==================================================================================================

//##################################################################################################
//                                  GENERA LISTA CONSUMI
//##################################################################################################
//==================================================================================================
//                                  GENERA LISTA
//==================================================================================================
    public static ArrayList<String> generareListaConsumi(){
        ArrayList<String> ris = new ArrayList<String>();
        ris.add(ConsumiSpinnerAdapter.VALORE_ELEVATI);
        ris.add(ConsumiSpinnerAdapter.VALORE_NORMALE);
        ris.add(ConsumiSpinnerAdapter.VALORE_BASSI);
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                                  GENERA LISTA CON CONSUMI DEFAULT
//==================================================================================================
    public static ArrayList<String> generareListaConsumiConDefault(Consumi value){
        ArrayList<String> ris = new ArrayList<String>();
        if(value == Consumi.BASSI){
            ris.add(ConsumiSpinnerAdapter.VALORE_BASSI);
            ris.add(ConsumiSpinnerAdapter.VALORE_NORMALE);
            ris.add(ConsumiSpinnerAdapter.VALORE_ELEVATI);
        }
        if(value == Consumi.NORMALI){
            ris.add(ConsumiSpinnerAdapter.VALORE_NORMALE);
            ris.add(ConsumiSpinnerAdapter.VALORE_ELEVATI);
            ris.add(ConsumiSpinnerAdapter.VALORE_BASSI);
        }
        if(value == Consumi.ELEVATI){
            ris.add(ConsumiSpinnerAdapter.VALORE_ELEVATI);
            ris.add(ConsumiSpinnerAdapter.VALORE_BASSI);
            ris.add(ConsumiSpinnerAdapter.VALORE_NORMALE);
        }
        return ris;
    }
//==================================================================================================
//##################################################################################################
}