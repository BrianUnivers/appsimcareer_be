package com.example.simcareerapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import com.example.simcareerapp.AppCompatActivityWithMenu.AppCompatActivityWithMyPrimaryMenu;
import com.example.simcareerapp.DataBase.ThreadAction.ThreadRichiediFotoAttivaUtente;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySeparatoriPagine.SeparatorePaginaConTesto;
import com.example.simcareerapp.Persistence.Entities.Foto;
import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;
import com.example.simcareerapp.Persistence.Entities.Utente;

import java.io.File;
import java.util.concurrent.ExecutionException;

public class ProfiloUtenteActivity extends AppCompatActivityWithMyPrimaryMenu {
//##################################################################################################
//                              DATI PASSATI TRAMITE UN INTENT
//##################################################################################################
    private Utente utenteAttivo         = null;
    private Boolean mantieniAttivoUtente= null;
    private Foto fotoAttivaUtente       = null; // FACOLTATIVO
//==================================================================================================
//                              METODO DI RECUPERO DI TALI VALORI DAL INTET CHE HA CHAMATO LA PAGINA
//==================================================================================================
    /**
     * Questo metodo serve per prendere dall'Activity chiamante le informazioni della sessione che
     * di obbligatori sono <<utenteAttivo>> e <<mantieniAttivoUtente>> mentre se non è presente il
     * valore di <<fotoAttivaUtente>> è  pari a null.
     */
    private void initValoriDiSessioneDellAPP(){
        Intent i =getIntent();
        utenteAttivo            = MyIntent.getUtenteExtra(i,MyIntent.I_UTENTE_ATTIVO);
        mantieniAttivoUtente    = i.getBooleanExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,false);
        fotoAttivaUtente        = MyIntent.getFotoExtra(i,MyIntent.I_FOTO_ATTIVA_UTENTE);
        //Verifico se nel dataBase non ci sono valori
        if(fotoAttivaUtente==null){
            ThreadRichiediFotoAttivaUtente threadRichiediFotoAttivaUtente = new ThreadRichiediFotoAttivaUtente(this);
            threadRichiediFotoAttivaUtente.execute(utenteAttivo.getId());
            try {
                fotoAttivaUtente =threadRichiediFotoAttivaUtente.get();
            } catch (ExecutionException e) {
                fotoAttivaUtente =null;
                e.printStackTrace();
            } catch (InterruptedException e) {
                fotoAttivaUtente =null;
                e.printStackTrace();
            }
        }
    }
//==================================================================================================
//==================================================================================================
//                              APERTURA DELLA PAGINA DI PROFILO UTENTE PASSANDO I VALORI DI SESSIONE
//==================================================================================================
    /**
     * Questo metodo serve per inviare le informazioni della sessione che sono <<utenteAttivo>> e
     * <<mantieniAttivoUtente>> quelli obbligatori e <<fotoAttivaUtente>> se presente.
     * N.B.
     * Da questà activiti è possibile solo andare alla Activity ModificaProfiloUtenteActivity.
     */
    private void apriModificaPaginaProfiloUtente(){
        MyIntent i = new MyIntent(ProfiloUtenteActivity.this, ModificaProfiloUtenteActivity.class);
        i.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utenteAttivo);
        i.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
        if(fotoAttivaUtente!=null) {
            i.putExtraFoto(MyIntent.I_FOTO_ATTIVA_UTENTE, fotoAttivaUtente);
        }
        startActivity(i);
    }
//==================================================================================================
//==================================================================================================
//                              IMPOSTAZIONI PARAMETRI DA PASSARE PER GLI INTENT DEL MENU LATERALE PRINCIPALE
//==================================================================================================
@Override
protected void setParamiter(MyIntent intent) {
    intent.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utenteAttivo);
    intent.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
}
//==================================================================================================
//##################################################################################################



//##################################################################################################
//                                  CREAZIONE DEL LAYOUT
//##################################################################################################
//==================================================================================================
//                                  CREAZIONE DELL'ACTITVITY
//==================================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  GET DATI DAL INTENT
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        initValoriDiSessioneDellAPP();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        setContentView(R.layout.activity_profilo_utente);
        super.initToolBar(R.menu.bar_menu_profilo_utente);
        super.impostaPaginaComeHome();
        setLayoutDeiInclude();
        setLayoutPerUtente();
    }
//==================================================================================================
//##################################################################################################



//##################################################################################################
//                                  GESTIONE DEL MENU' SULLA TOOLBAR
//##################################################################################################
//==================================================================================================
//                                  GESTIONE DEI EVENTI DI CLICK SULLE VOCI DEL MENU'
//==================================================================================================
    /**
     * Metodo per distingere i tasti della toolbar e sapere si deve fare.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.btn_bar_Modifica:
                apriModificaPaginaProfiloUtente();
        }
        return super.onOptionsItemSelected(item);
    }
//==================================================================================================
//##################################################################################################



//##################################################################################################
//                                  APPORTARE LE MODIFICE RUNTIME AL LAYOUT
//##################################################################################################
//==================================================================================================
//                                  IMPOSTAZIONI DELLE VARIANTI DI BLOCHI INCLUSI
//==================================================================================================
    /**
     * Questo metodo imposta il layout dei bolochi neutri che vengono impotati per il loro riutilizzo.
     */
    private void setLayoutDeiInclude() {
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiIncludeLable(this,R.id.include_dvd_DivisorePreferitiPage,R.string.lbl_Preferito);
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude(this,R.id.include_dvd_DivisoreOdioPage,R.string.lbl_Odiato,R.drawable.ic_baseline_odiato_normal_text);
    }
//==================================================================================================
//==================================================================================================
//                                  IMPOSTAZIONI DELLE VARIANTI DATE DAI DATI UTENTE
//==================================================================================================
    /**
     * Questo metodo imposta il layout in base ai dati utente.
     */
    private void setLayoutPerUtente() {
        //Imposta foto
        inizializazioneImmagineUtente(fotoAttivaUtente);
        //Imposta nome e cognome
        TextView txtNomeCognomeUtente = (TextView) findViewById(R.id.txt_NomeCognomeUtente);
        txtNomeCognomeUtente.setText(utenteAttivo.getNome()+" "+utenteAttivo.getCognome());
        //Imposta email
        TextView txtEmailUtente = (TextView) findViewById(R.id.txt_EmailUtente);
        txtEmailUtente.setText(utenteAttivo.getEmail());
        //Imposta residenza
        TextView txtValueIndirizzo = (TextView) findViewById(R.id.txt_ValueIndirizzo);
        txtValueIndirizzo.setText(utenteAttivo.getResidenza().getIndirizzoCompleto());
        //Imposta numero di cara
        TextView txtValueNumeroDiGara = (TextView) findViewById(R.id.txt_ValueNumeroDiGara);
        txtValueNumeroDiGara.setText(utenteAttivo.getNumeroDiGara().toString());
        //Imposta auto preferita
        setLayoutValueDeiBlocchiSpinner(R.id.include_MachinaPreferita,R.string.lbl_Auto,utenteAttivo.getAutoPreferita());
        //Imposta circuito preferita
        setLayoutValueDeiBlocchiSpinner(R.id.include_CircuitoPreferita,R.string.lbl_Crircuito,utenteAttivo.getCircuitoPreferito());
        //Imposta circuito odiato
        setLayoutValueDeiBlocchiSpinner(R.id.include_CircuitoOdiato,R.string.lbl_Crircuito,utenteAttivo.getCircuitoOdiato());
    }
//==================================================================================================
//==================================================================================================
//                                  IMPOSTAZIONI DELLE VARIANTI DEI BLOCCHI DI SPINNER
//==================================================================================================
    private void setLayoutValueDeiBlocchiSpinner(int idBlocco,int idTextLable,ValoreConImmaginePerSpinner el){
        ViewGroup blocco = findViewById(idBlocco);
        //Imposta lable
        TextView txtEtichettaDelValore = blocco.findViewById(R.id.txt_EtichettaDelValore);
        txtEtichettaDelValore.setText(idTextLable);
        //Imposta immagine
        ImageView imgImmagineDelValore =blocco.findViewById(R.id.img_ImmagineDelValore);
        imgImmagineDelValore.setImageResource(el.getIdImmagine());
        //Imposta valore
        TextView txtValueValore = blocco.findViewById(R.id.txt_ValueValore);
        txtValueValore.setText(el.getNome());
    }
//==================================================================================================
//##################################################################################################



//##################################################################################################
//                                  IMPOSTA IMMAGINE
//##################################################################################################
//==================================================================================================
//                                  IMPOSTA IMMAGINE IN MANIERA ROTONA
//==================================================================================================
    private void inizializazioneImmagineUtente(Foto foto ){
        File flieImg = null;
        //Se c'è un oggeto foto
        if(fotoAttivaUtente!=null){
            flieImg = new File(foto.getUrlFile());
        }

        if(flieImg!=null){
            impostaImmagineCircolareDaFile(flieImg);
        }else{
            int resId = R.drawable.userdefault;// Immagine di default.
            impostaImmagineCircolareDaID(resId);
        }
    }
    //--------------------------------------------------------------------------------------------------
    private void  impostaImmagineCircolareDaID(int id){
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),id);
        impostaImmagineCircolareDaBitmap(bitmap);
    }
    //--------------------------------------------------------------------------------------------------
    private void  impostaImmagineCircolareDaFile(File f){
        String filePath = f.getPath();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        impostaImmagineCircolareDaBitmap(bitmap);
    }
    //--------------------------------------------------------------------------------------------------
    private void impostaImmagineCircolareDaBitmap(Bitmap imgBitmap){
        //Ruota solo se la foto non è verticale
        if(imgBitmap.getHeight()<imgBitmap.getWidth()){
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            imgBitmap = Bitmap.createBitmap(imgBitmap, 0, 0, imgBitmap.getWidth(), imgBitmap.getHeight(), matrix, true);
        }

        ViewGroup bloccoImmagine = findViewById(R.id.include_BloccoFotoUtente);
        ImageView imageUser = bloccoImmagine.findViewById(R.id.img_User);

        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(),imgBitmap);
        roundedBitmapDrawable.setCircular(true);
        imageUser.setImageDrawable(roundedBitmapDrawable);
    }
//==================================================================================================
//##################################################################################################
}