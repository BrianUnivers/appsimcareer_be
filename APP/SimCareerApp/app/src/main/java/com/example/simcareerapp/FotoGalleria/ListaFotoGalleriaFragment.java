package com.example.simcareerapp.FotoGalleria;

import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.simcareerapp.FotoGalleria.Interface.OnFragmentListaFotoGalleriaFragmentEventListener;
import com.example.simcareerapp.GestioneAssetPerGalleria.GestioneFileECartelleGalleria;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyChipAdapter.MyChipAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.CartelleGalleriaRVAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.FotoGalleriaRVAdapter;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface.OnClickCartellaGalleriaListener;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyRecyclerViewAdapter.Interface.OnClickFotoGalleriaListener;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySeparatoriPagine.SeparatorePaginaConTesto;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyView.MyGridView;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.Persistence.JsonEntities.CampionatoConJson;
import com.example.simcareerapp.R;
import com.example.simcareerapp.SingoloCampionato.Interface.OnFragmentInfoCampionatoFragmentEventListener;

import java.text.SimpleDateFormat;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ListaFotoGalleriaFragment} factory method to
 * create an instance of this fragment.
 */
public class ListaFotoGalleriaFragment extends Fragment {
    /**
     * listener è la variabile che permette di comunicare con l'Activity
     * View     è la variabile che permette di ricavaare le view dai id
     */
    private OnClickFotoGalleriaListener listener = null;
    private View viewGroupLayout = null;
    private RecyclerView RecyclerViewFoto = null;
//##################################################################################################
//                              DATI PASSATI
//##################################################################################################
    private GestioneFileECartelleGalleria galleria   = null;
    private String folder = null;
//==================================================================================================
//                                  COSTRUTORI
//==================================================================================================
    public ListaFotoGalleriaFragment(GestioneFileECartelleGalleria galleria,String folder) {
        this.galleria   = galleria;
        this.folder = folder;
    }
//==================================================================================================
//##################################################################################################


//##################################################################################################
//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
//==================================================================================================
//==================================================================================================
//                                  DEFINIZIONE DEL LAYOUT DEL FRAGMENT
//==================================================================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);//<<<-------------------------------------ATTEZIONE

        //Scelta del layout usato per fragment.
        viewGroupLayout = inflater.inflate(R.layout.fragment_lista_foto_galleria, container, false);
        //Imposta Titolo
        Toolbar subtitle = viewGroupLayout.findViewById(R.id.toolbar_subtitle);
        subtitle.setTitle(folder);

        RecyclerViewFoto = viewGroupLayout.findViewById(R.id.rcl_ListaFotoGalleria);
        FotoGalleriaRVAdapter adapter = new FotoGalleriaRVAdapter(getActivity(),listener, galleria.getListaFoto(folder));
        RecyclerViewFoto.setAdapter(adapter);
        RecyclerViewFoto.setLayoutManager(new GridLayoutManager(getActivity(),3));
        //Imposta lista foto
        return viewGroupLayout;
    }

//==================================================================================================
//##################################################################################################



//##################################################################################################
//                                  COMUNICAZIONE CON L'ACTIVITY
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY
//==================================================================================================
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Crea il link con l'activity che contiene il fragment.
        listener = (OnClickFotoGalleriaListener) activity;
    }
//==================================================================================================
//##################################################################################################

}