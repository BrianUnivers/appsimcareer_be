package com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinner.Interface;

import com.example.simcareerapp.Persistence.Entities.Indirizzo;
import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;

//##################################################################################################
//                                  INTERFACCIA PER COMUNICARE
//##################################################################################################
public interface OnItemSelectedSpinnerConImmagineListener {
    void onItemSelectedSpinnerConImmagineCustom(ValoreConImmaginePerSpinner elemento);
}
//##################################################################################################
//##################################################################################################