package com.example.simcareerapp.DataBase.ThreadAction;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;

import com.example.simcareerapp.DataBase.Management.SimpleDbHelper;
import com.example.simcareerapp.DataBase.Query.UtenteQuery;
import com.example.simcareerapp.Persistence.Entities.Utente;

public class ThreadVerificaEmail extends AsyncTask<String,Integer, Boolean> {
    private SimpleDbHelper database;
    private Context context;
//==================================================================================================
//                              COSTRUTORE                                                          <<Processo Principale>>
//==================================================================================================
    /**
     * Per avere la posibilità di accedere alla Accivity e instauro un collegamento col server.
     */
    public ThreadVerificaEmail(Context context) {
        //Crazione DataBase o Associazione con uno già creato
        this.database = new SimpleDbHelper(context);
        this.context = context;
    }
    //==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE PRIMA DELLA CHAMATA DEL THREAD                     <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
    //==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DOPO DELLA CHAMATA DEL THREAD                      <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
    }
    //==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DURANTE DELLA CHAMATA DEL THREAD                   <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }
//==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DAL THREAD                                         <<Processo Seconario>>
//==================================================================================================
    /**
     * Effetua la verifica dell'email passate sottoforma  <String>  cosi facendo controllo se c'è un
     * altro utente con lo stesso email se è presente il risultato
     * sara false, poichè tale valore non è valido per un nuovo utente, mentre ritornera vero se tale
     * email non è stato ancora usato.
     */
    @Override
    protected Boolean doInBackground(String... item) {
        String email  = item[0];
        Boolean ris = true;

        String esiste = UtenteQuery.getSaltByEmail(database,email);
        if(esiste!= null){
            //Esiste un utente e qiondi non è possibile usare tale email.
            ris = false;
        }
        return ris;
    }
//==================================================================================================
}
