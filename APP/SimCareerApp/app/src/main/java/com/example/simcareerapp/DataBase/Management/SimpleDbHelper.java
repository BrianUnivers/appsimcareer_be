package com.example.simcareerapp.DataBase.Management;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Questa classe contiene i metodi per creare, aggiornare, ripristinare e distruggere la struttura
 * del database locale.
 *
 * @author Brian Emanuel
 */
public class SimpleDbHelper extends SQLiteOpenHelper {
    /**
     * La costante "DATABASE_VERSION" contiene il numeto della versione del database che si vuole creare
     * se è presente un'altro sul dispositivo con tale numero diverso viene ricreato con questo seguendo 
     * le istuzioni in base alla differenza di versioni.
     */
    //N.B. Se si cambia il DB si deve incremetare il numero di versione del DB
    public static final int DATABASE_VERSION = 1;
    /**
     * La costante "DATABASE_NAME" contiene il nome del database locale, così da riconiscerlo ne se 
     * sono presenti degli altri. 
     */
    public static final String DATABASE_NAME = "SimpleDBSimCareer.db";
//==================================================================================================
//                              COSTRUTTORE
//=================================================================================================
    /**
     * Questo è il metodo costruttore che genera il database che viene specificato in questa classe. 
     * 
     * @param context
     *          Il parametro passsato contiene il contesto della applicazione a qui si fa riferimento.
     * @see SQLiteOpenHelper#SQLiteOpenHelper(Context, String, SQLiteDatabase.CursorFactory, int) 
     */
    public SimpleDbHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }
//==================================================================================================
//==================================================================================================
//                              CREAZIONE DEL DATABASE
//==================================================================================================
    /**
     * Metodo usato per la creazione del database, che viene eseguito una volta sola.
     * 
     * @param db 
     *      Il parametro passato contiene il riferimento al database al quale si vuole aggiungere le
     *      tabelle.
     * @see SQLiteOpenHelper#onCreate(SQLiteDatabase) 
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
            for (int i=0; i < FeedReaderContract.SQL_CREATE_DB.length; i++){
                db.execSQL(FeedReaderContract.SQL_CREATE_DB[i]);
            }
        }catch (Exception e){
            String err = e.toString();
            Log.d("DB_ERROR",err);
        }
    }
//==================================================================================================
//==================================================================================================
//                              AGGIORNAMETO DEL DATABASE
//==================================================================================================
    /**
     * Metodo usato per l'aggiornameto della struttura del database già esistente.
     * In questo caso se il database ha una versione differente da quella che viene indicata alla 
     * proma esequzine,il database viene canellato e sostituito con la versone indicata.
     * 
     * @param db
     *      Il parametro passato contiene il riferimento al database al quale si vuole aggiornare la 
     *      struttura delle tabelle.
     * @param oldVersion
     *      Il parametro passato contiene il numero di versione del database locale già presente.
     * @param newVersion 
     *      Il parametro passato contiene il numero della versione del database locale che si vule avere.
     * @see SQLiteOpenHelper#onUpgrade(SQLiteDatabase, int, int) 
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try{
            onDelete(db);
            onCreate(db);
        }catch (Exception e){
            String err = e.toString();
            Log.d("DB_ERROR",err);
        }
    }
//==================================================================================================
//==================================================================================================
//                              ROLLBACK DEL DATABASE A UNA VERSIONE PRECEDENTE
//==================================================================================================
    /**
     * Metodo usato per l'rollback alla versione precedente della struttura del database già esistente.
     * In questo caso se il database ha una versione differente da quella che viene indicata alla
     * proma esequzine,il database viene canellato e sostituito con la versone indicata.
     *
     * @param db
     *      Il parametro passato contiene il riferimento al database al quale si vuole aggiornare la
     *      struttura delle tabelle.
     * @param oldVersion
     *      Il parametro passato contiene il numero di versione del database locale già presente.
     * @param newVersion
     *      Il parametro passato contiene il numero della versione del database locale che si vule avere.
     * @see SQLiteOpenHelper#onDowngrade(SQLiteDatabase, int, int)
     */
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
//==================================================================================================
//==================================================================================================
//                              DELETE DEL DATABASE
//==================================================================================================
    /**
     * Metodo usato per distruggere la struttura del database locale.
     *
     * @param db
     *     Il parametro passato contiene il riferimento al database del quale si vuole sistruggere la
     *     struttura delle tabelle.
     */
    public void onDelete(SQLiteDatabase db) {
        for (int i=0; i < FeedReaderContract.SQL_DELETE_DB.length; i++){
            try{
                db.execSQL(FeedReaderContract.SQL_DELETE_DB[i]);
            }catch (Exception e){
                String err = e.toString();
                Log.d("DB_ERROR",err);
            }
        }
    }
//==================================================================================================
}


