package com.example.simcareerapp.Persistence.Entities;

import com.example.simcareerapp.DataBase.Management.HashPasswordWithSalt;

import java.sql.Date;

public class Utente {
    private Long                id                = null;
    private String              nome              = null;
    private String              cognome           = null;
    private Date                dataNascita       = null;
    private String              email             = null;
    private String              username          = null;
    private String              password          = null;
    private String              salt              = null;
    private Indirizzo           residenza         = null;
    private Integer             numeroDiGara      = null;
    private AutoConImmagini     autoPreferita     = null;
    private CircuitoConImmagini circuitoPreferito = null;
    private CircuitoConImmagini circuitoOdiato    = null;
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
//--------------------------------------------------------------------------------------------------
//                              VOUTO
//--------------------------------------------------------------------------------------------------
    public Utente() {
        this.salt = HashPasswordWithSalt.genraSalt();
    }
//--------------------------------------------------------------------------------------------------
//==================================================================================================
//==================================================================================================
//                              GETTER E SETTER
//==================================================================================================
    public Long getId() {
        return id;
    }
//------------------------------
    public void setId(Long id) {
        this.id = id;
    }
//--------------------------------------------------------------------------------------------------
    public String getNome() {
        return nome;
    }
//------------------------------
    public void setNome(String nome) {
        this.nome = nome;
    }
//--------------------------------------------------------------------------------------------------
    public String getCognome() {
        return cognome;
    }
//------------------------------
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
//--------------------------------------------------------------------------------------------------
    public Date getDataNascita() {
        return dataNascita;
    }
//------------------------------
    public void setDataNascita(Date dataNascita) {
        this.dataNascita = dataNascita;
    }
//--------------------------------------------------------------------------------------------------
    public String getEmail() {
        return email;
    }
//------------------------------
    public void setEmail(String email) {
        this.email = email;
    }
//--------------------------------------------------------------------------------------------------
    public String getUsername() {
        return username;
    }
//------------------------------
    public void setUsername(String username) {
        this.username = username;
    }
//--------------------------------------------------------------------------------------------------
    public String getPassword() {
        return password;
    }
//------------------------------
    public void setPasswordHashed(String password) {
        this.password = password;
    }
    //------------------------------
    public void setPassword(String password) {
        this.password = HashPasswordWithSalt.genraPasswordHashWithSalt(password,this.salt);
    }
//--------------------------------------------------------------------------------------------------
    public String getSalt() {
        return salt;
    }
//------------------------------
    public void setSalt(String salt) {
        this.salt = salt;
    }
//--------------------------------------------------------------------------------------------------
    public Indirizzo getResidenza() {
        return residenza;
    }
//------------------------------
    public void setResidenza(Indirizzo residenza) {
        this.residenza = residenza;
    }
//--------------------------------------------------------------------------------------------------
    public Integer getNumeroDiGara() {
        return numeroDiGara;
    }
//------------------------------
    public void setNumeroDiGara(Integer numeroDiGara) {
        this.numeroDiGara = numeroDiGara;
    }
//--------------------------------------------------------------------------------------------------
    public AutoConImmagini getAutoPreferita() {
        return autoPreferita;
    }
//------------------------------
    public void setAutoPreferita(AutoConImmagini autoPreferita) {
        this.autoPreferita = autoPreferita;
    }
//--------------------------------------------------------------------------------------------------
    public CircuitoConImmagini getCircuitoPreferito() {
        return circuitoPreferito;
    }
//------------------------------
    public void setCircuitoPreferito(CircuitoConImmagini circuitoPreferito) {
        this.circuitoPreferito = circuitoPreferito;
    }
//--------------------------------------------------------------------------------------------------
    public CircuitoConImmagini getCircuitoOdiato() {
        return circuitoOdiato;
    }
//------------------------------
    public void setCircuitoOdiato(CircuitoConImmagini circuitoOdiato) {
        this.circuitoOdiato = circuitoOdiato;
    }
//==================================================================================================
//==================================================================================================
//                              UGUALIANZA CON I PILOTI
//==================================================================================================
    public boolean equalsToPiloti(Pilota piloti){
        return (piloti.getNomeCognome().equals(this.getNome()+" "+this.getCognome()) );
    }
//==================================================================================================

}
