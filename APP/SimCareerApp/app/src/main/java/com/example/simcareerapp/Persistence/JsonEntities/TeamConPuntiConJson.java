package com.example.simcareerapp.Persistence.JsonEntities;

import android.util.Log;

import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.PilotaConPunti;
import com.example.simcareerapp.Persistence.Entities.TeamConPunti;

import org.json.JSONException;
import org.json.JSONObject;

public class TeamConPuntiConJson extends TeamConPunti {
//##################################################################################################
//                                  LABEL TEAM PER JSON
//##################################################################################################
    protected final String LABLE_NOME_TEAM = "team";
    protected final String LABLE_AUTO      = "auto";
    protected final String LABLE_PUNTI     = "punti";
//##################################################################################################
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
    public TeamConPuntiConJson(String nomeTeam, String nomeAuto, Integer punti) {
        super(nomeTeam, nomeAuto, punti);
    }
//-------------------------------------------------------------------------------------------------
    public TeamConPuntiConJson(TeamConPunti team) {
        super(team.getNomeTeam(),team.getAuto().getNome(),team.getPunti());
    }
//--------------------------------------------------------------------------------------------------
    public TeamConPuntiConJson(JSONObject jsonObject){
        if(jsonObject!=null) {
            try {
                this.setNomeTeam(    jsonObject.getString(LABLE_NOME_TEAM) );
                this.setPunti(       jsonObject.getInt(LABLE_PUNTI) );

                this.setAuto(null);
                String nomeAuto =    jsonObject.getString(LABLE_AUTO);
                if( (nomeAuto!=null) && (!nomeAuto.equals("")) ){
                    this.setAuto(new AutoConImmagini(nomeAuto));
                }
            } catch (JSONException e) {
                Log.d("ErrJson",e.getMessage());
            }
        }
    }
//==================================================================================================
}
