package com.example.simcareerapp.DataBase.ThreadAction;

import android.content.Context;
import android.os.AsyncTask;

import com.example.simcareerapp.DataBase.Management.SimpleDbHelper;
import com.example.simcareerapp.DataBase.Query.UtenteQuery;
import com.example.simcareerapp.Persistence.Entities.Utente;

public class ThreadVerificaUsername extends AsyncTask<String,Integer, Boolean> {
    private SimpleDbHelper database;
    private Context context;
//==================================================================================================
//                              COSTRUTORE                                                          <<Processo Principale>>
//==================================================================================================
    /**
     * Per avere la posibilità di accedere alla Accivity e instauro un collegamento col server.
     */
    public ThreadVerificaUsername(Context context) {
        //Crazione DataBase o Associazione con uno già creato
        this.database = new SimpleDbHelper(context);
        this.context = context;
    }
    //==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE PRIMA DELLA CHAMATA DEL THREAD                     <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
    //==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DOPO DELLA CHAMATA DEL THREAD                      <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
    }
    //==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DURANTE DELLA CHAMATA DEL THREAD                   <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }
//==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DAL THREAD                                         <<Processo Seconario>>
//==================================================================================================
    /**
     * Effetua la verifica dell'username passate sottoforma  <String>
     * cosi facendo controllo se c'è un altro utente con lo stesso Username se è presente il risultato
     * sara false, poichè tale valore non è valido per un nuovo utente, mentre ritornera vero se tale
     * username non è stato ancora usato.
     */
    @Override
    protected Boolean doInBackground(String... item) {
        String username  = item[0];
        Boolean ris = true;

        String esiste = UtenteQuery.getSaltByUsername(database,username);
        if(esiste!= null){
            //Esiste un utente e qiondi non è possibile usare tale username.
            ris = false;
        }
        return ris;
    }
//==================================================================================================
}
