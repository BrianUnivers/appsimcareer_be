package com.example.simcareerapp;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import com.example.simcareerapp.DataBase.ThreadAction.ThreadInsertFoto;
import com.example.simcareerapp.DataBase.ThreadAction.ThreadRichiediFotoAttivaUtente;
import com.example.simcareerapp.DataBase.ThreadAction.ThreadUpdateUtente;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.Interface.CostantiInserireEscrarreDatiDaiInternt;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp.Interface.OnAletDialogResidenzaEventListener;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySeparatoriPagine.SeparatorePaginaConTesto;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinner.SpinnerConImmagine;
import com.example.simcareerapp.Persistence.Entities.*;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp.*;
import com.example.simcareerapp.ModificaProfiloUtente.Interface.OnFragmentImmagineProfiloUtenteEventListener;
import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;


public class ModificaProfiloUtenteActivity
        extends     AppCompatActivity
        implements  OnFragmentImmagineProfiloUtenteEventListener,
                    OnAletDialogResidenzaEventListener,
                    CostantiInserireEscrarreDatiDaiInternt{
//==================================================================================================
//                                  METODO PER IMPEDIRE DI RAGGIUNGERE ACTIVITY PRECEDENTI
//==================================================================================================
    /**
     * Questo metodo impedisce di ritornare tramite il tasto "Fisico" indietro dello smartphone alle
     * activity precedenti così da rendere la activity attuale quella di root per la navigazione.
     */
    protected void impostaPaginaComeHome(){
        this.getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startMain);
            }
        });
    }
    //==================================================================================================
//##################################################################################################
//                              DATI PASSATI TRAMITE UN INTENT
//##################################################################################################
    private Utente  utenteAttivo        = null;
    private Boolean mantieniAttivoUtente= null;
    private Foto    fotoAttivaUtente    = null; // FACOLTATIVO
//==================================================================================================
//                              METODO DI RECUPERO DI TALI VALORI DAL INTET CHE HA CHAMATO LA PAGINA
//==================================================================================================
    /**
     * Questo metodo serve per prendere dall'Activity chiamante le informazioni della sessione che
     * di obbligatori sono <<utenteAttivo>> e <<mantieniAttivoUtente>> mentre se non è presente il
     * valore di <<fotoAttivaUtente>> è  pari a null.
     */
    private void initValoriDiSessioneDellAPP(){
        Intent i =getIntent();
        utenteAttivo            = MyIntent.getUtenteExtra(i,MyIntent.I_UTENTE_ATTIVO);
        mantieniAttivoUtente    = i.getBooleanExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,false);
        fotoAttivaUtente        = MyIntent.getFotoExtra(i,MyIntent.I_FOTO_ATTIVA_UTENTE);
        //Verifico se nel dataBase non ci sono valori
        if(fotoAttivaUtente==null){
            ThreadRichiediFotoAttivaUtente threadRichiediFotoAttivaUtente = new ThreadRichiediFotoAttivaUtente(this);
            threadRichiediFotoAttivaUtente.execute(utenteAttivo.getId());
            try {
                fotoAttivaUtente =threadRichiediFotoAttivaUtente.get();
            } catch (ExecutionException e) {
                fotoAttivaUtente =null;
                e.printStackTrace();
            } catch (InterruptedException e) {
                fotoAttivaUtente =null;
                e.printStackTrace();
            }
        }
    }
//==================================================================================================
//==================================================================================================
//                              APERTURA DELLA PAGINA DI PROFILO UTENTE PASSANDO I VALORI DI SESSIONE
//==================================================================================================
    /**
     * Questo metodo serve per inviare le informazioni della sessione che sono <<utenteAttivo>> e
     * <<mantieniAttivoUtente>> quelli obbligatori e <<fotoAttivaUtente>> se presente.
     * N.B.
     * Da questà activiti è possibile solo andare alla Activity ProfiloUtenteActivity o non avendo
     * modificato i valori presenti o dopo aver salvato tali modifiche.
     */
    private void apriPaginaProfiloUtente(){
        MyIntent i = new MyIntent(ModificaProfiloUtenteActivity.this, ProfiloUtenteActivity.class);
        i.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utenteAttivo);
        i.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,mantieniAttivoUtente);
        if(fotoAttivaUtente!=null) {
            i.putExtraFoto(MyIntent.I_FOTO_ATTIVA_UTENTE, fotoAttivaUtente);
        }
        startActivity(i);
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                              DATI DELLE MODIFICHE NON SALVATE
//##################################################################################################
    private File                fotoCambiata        = null; //Contiene il file del'ultima immagine scelta dall'utente
    private Indirizzo           indirizzoResidenza  = null; //Contiene l'indirizzo inserito ma non salvato
    private Integer             numeroDiGara        = null; //Contiene il numero della gara scelto dall'utente
    private AutoConImmagini     autoPref            = null; //Contiene l'auto preferita scelta
    private CircuitoConImmagini circuitoPref        = null; //Contiene il circuito preferita scelta
    private CircuitoConImmagini circuitoodiato      = null; //Contiene il circuito odiato scelta
//##################################################################################################

//##################################################################################################
//                              DATI PER GESTIRE IL LAYOUT
//##################################################################################################
    //Toolbar da implemetare
    private Toolbar toolbar=null;
//##################################################################################################

//##################################################################################################
//                              Oggetti che contengono gli spinner per la loro gestione
//##################################################################################################
    SpinnerConImmagine machinaPreferita;
    SpinnerConImmagine circuitoPreferita;
    SpinnerConImmagine circuitoOdiato;
//##################################################################################################


//##################################################################################################
//                                  CREAZIONE DEL LAYOUT
//##################################################################################################
//==================================================================================================
//                                  CREAZIONE DEL FRAGMENT
//==================================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  GET DATI DAL INTENT
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        initValoriDiSessioneDellAPP();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        setContentView(R.layout.activity_modifica_profilo_utente);
        //Implemetazione della toolbar
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Tasto a sinistra non inseristo al primo accaso
        if(!verificaSeNuovoUtente(utenteAttivo)){
            toolbar.setTitle(R.string.ttl_ConcludiProfiloUtente);
            toolbar.setNavigationIcon(R.drawable.ic_baseline_close_24_white);
        }else{
            //Impedisce di tornare indietro alla registrazione iniziale
            impostaPaginaComeHome();
        }
        setLayoutDeiInclude();
        personalizzaPaginaInBaseAiDatiUrente();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  LISTENER DEI BOTTONI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Listener del pulsante Annula modifiche
        if(toolbar!=null)
            toolbar.setNavigationOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    apriPaginaProfiloUtente();
                }
            });
//--------------------------------------------------------------------------------------------------
        TextView txtInputIndirizzo = findViewById(R.id.txt_InputIndirizzo);
        txtInputIndirizzo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                avviaAlertDialogResidenza();

            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
//==================================================================================================
//==================================================================================================
//                                  METODO CHIAMATO ALLA PRESIONE DEI BOTTONE
//==================================================================================================
    /**
     * Il metodo chiamato dal Lisener quando si preme il per inserire la residenza / indirizzo e
     * attiva i AlertDialog per inserire i vari campi della residenza
     */
    public void avviaAlertDialogResidenza(){
        AlertDialogResidenza alertDialog= new AlertDialogResidenza(
                this,indirizzoResidenza,
                R.layout.alert_residenza,
                R.drawable.ic_baseline_place_24_white,
                R.string.ttl_alert_Residenza,
                R.string.lbl_ButtonModifica,
                R.string.lbl_ButtonAnnula);
        alertDialog.create();
        alertDialog.show();
    }
//--------------------------------------------------------------------------------------------------
//==================================================================================================
//==================================================================================================
//                                  VERIFICA SE L'UTENTE E' NUOVO O STO PER MODIFICARE UN NUTENTE
//==================================================================================================
    /**
     * Verifica se l'utente è appena stato creato o ha completato almeno una volta la registazione.
     */
    private Boolean verificaSeNuovoUtente(Utente utente){
        return utente.getResidenza() == null;
    }
//==================================================================================================
//##################################################################################################





//##################################################################################################
//                                  GESTIONE DEL MENU' SULLA TOOLBAR
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE DEL MENU' (senza il tasto a sinistra)
//==================================================================================================
    /**
     * Metodo per definire il layout della toolbar definedo quali sono le sue voci.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bar_menu_modifica_profilo_utente,menu);
        return true;
    }
//==================================================================================================

//==================================================================================================
//                                  GESTIONE DEI EVENTI DI CLICK SULLE VOCI DEL MENU'
//==================================================================================================
    /**
     * Metodo per distingere i tasti della toolbar e sapere si deve fare.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.btn_bar_Salva:
                controllaESalvaCambiamenti();
                // N.B. La funzione è l'ultima del file che controla se tutti i dati sono correti per
                // poi chiamare la segune funzione che salva le modifiche.
        }
        return true;
    }
//==================================================================================================

//==================================================================================================
//                                  SALVA MODIFICHE E AGGIORNA IL DATABASE E APRI ProfiloUtenteActivity
//==================================================================================================
    /**
     * Metodo che aggiorna il database e passa alla activity del profilo utente completo.
     */
    private void salvaDati(File fotoCambiata,
                           Indirizzo indirizzoResidenza,
                           Integer numeroDiGaraPreferito,
                           AutoConImmagini autoPreferita,
                           CircuitoConImmagini circuitoPreferita,
                           CircuitoConImmagini circuitoOdiato) {


        Toast.makeText(this,"Salvatagio dei dati.",Toast.LENGTH_LONG);


        //AGGIORNAMETO UTENTE
        utenteAttivo.setNumeroDiGara(numeroDiGaraPreferito);
        utenteAttivo.setResidenza(indirizzoResidenza);
        utenteAttivo.setAutoPreferita(autoPreferita);
        utenteAttivo.setCircuitoPreferito(circuitoPreferita);
        utenteAttivo.setCircuitoOdiato(circuitoOdiato);

        ThreadUpdateUtente threadUpdateUtente = new ThreadUpdateUtente(this);
        threadUpdateUtente.execute(new Pair<Utente, Long>(utenteAttivo,utenteAttivo.getId()));
        Boolean modificatoUtente= false;
        try {
            modificatoUtente =threadUpdateUtente.get();
        } catch (ExecutionException e) {
            modificatoUtente= false;
            e.printStackTrace();
        } catch (InterruptedException e) {
            modificatoUtente= false;
            e.printStackTrace();
        }
        if( modificatoUtente){
            if (fotoCambiata!=null){
                //AGGIORNAMETO FOTO
            Foto foto = new Foto();
            foto.setIdUtente(utenteAttivo.getId());
            foto.setStatoAttivo(true);
            foto.setTestoAlternativo("Profilo di utnete "+utenteAttivo.getId());
            foto.setUrlFile(fotoCambiata.getAbsolutePath());
            ThreadInsertFoto threadInsertFoto = new ThreadInsertFoto(this);
            threadInsertFoto.execute(new Pair<Foto, Long>(foto,utenteAttivo.getId()));
            try {
                fotoAttivaUtente =threadInsertFoto.get();
            } catch (ExecutionException e) {
                fotoAttivaUtente =null;
                e.printStackTrace();
            } catch (InterruptedException e) {
                fotoAttivaUtente =null;
                e.printStackTrace();
            }
            }
            apriPaginaProfiloUtente();
        }
    }
//==================================================================================================
//##################################################################################################





//##################################################################################################
//                                  APPORTARE LE MODIFICE RUNTIME AL LAYOUT
//##################################################################################################
//==================================================================================================
//                                  IMPOSTAZIONI DELLE VARIANTI DI BLOCHI INCLUSI
//==================================================================================================
    /**
     * Questo metodo imposta il layout dei bolochi neutri che vengono impotati per il loro riutilizzo.
     */
    private void setLayoutDeiInclude() {
        ArrayList<ValoreConImmaginePerSpinner> listaTotaleAuto      = AutoConImmagini.getAllListaMacchina();
        ArrayList<ValoreConImmaginePerSpinner> listaTotaleCircuiti  = CircuitoConImmagini.getAllListaCircuito();
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiIncludeLable(this,R.id.include_dvd_DivisorePreferitiPage,R.string.lbl_Preferito);
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude(this,R.id.include_dvd_DivisoreOdioPage,R.string.lbl_Odiato,R.drawable.ic_baseline_odiato_normal_text);

        machinaPreferita  = new SpinnerConImmagine(this,R.id.include_MachinaPreferita,R.string.lbl_Auto);
        circuitoPreferita = new SpinnerConImmagine(this,R.id.include_CircuitoPreferita,R.string.lbl_Crircuito);
        circuitoOdiato    = new SpinnerConImmagine(this,R.id.include_CircuitoOdiato,R.string.lbl_Crircuito);

        machinaPreferita.setSpinnerConImmagineGenerico(listaTotaleAuto);
        circuitoPreferita.setSpinnerConImmagineGenerico(listaTotaleCircuiti);
        circuitoOdiato.setSpinnerConImmagineGenerico(listaTotaleCircuiti);
    }
//==================================================================================================

//==================================================================================================
//                                  ADATTA LA PAGINA IN BASE AL UTENTE CHE E' ATTIVO
//==================================================================================================
    /**
     * Inserisce il nome, il congnome, e l'email all'interno della pagina.
     */
    private void personalizzaPaginaInBaseAiDatiUrente(){
        //Dati di base
        TextView textNomeCognome = findViewById(R.id.txt_NomeCognomeUtente);
        textNomeCognome.setText(utenteAttivo.getNome()+" "+utenteAttivo.getCognome());
        TextView textEmail= findViewById(R.id.txt_EmailUtente);
        textEmail.setText(utenteAttivo.getEmail());

        //Imposta residenza
        if(utenteAttivo.getResidenza()!=null){
            impostaIndirizzoDiResidenza(utenteAttivo.getResidenza());
        }
        //Imposta numero di gara
        if( (utenteAttivo.getNumeroDiGara()!=null) && (utenteAttivo.getNumeroDiGara()>=0)){
            EditText inputViewNumGara =(EditText) findViewById(R.id.txt_InputNumeroDiGara);
            inputViewNumGara.setText(utenteAttivo.getNumeroDiGara().toString(),TextView.BufferType.EDITABLE);
        }
        //Imposta spinner auto preferita
        if(utenteAttivo.getAutoPreferita()!=null){
            machinaPreferita.impostaElemetoSePresente(utenteAttivo.getAutoPreferita());
        }
        //Imposta spinner circuito preferita
        if(utenteAttivo.getCircuitoPreferito()!=null){
            circuitoPreferita.impostaElemetoSePresente(utenteAttivo.getCircuitoPreferito());
        }
        //Imposta spinner circuito odiato
        if(utenteAttivo.getCircuitoOdiato()!=null){
            circuitoOdiato.impostaElemetoSePresente(utenteAttivo.getCircuitoOdiato());
        }


    }
//==================================================================================================
//##################################################################################################





//##################################################################################################
//                                  METODI PER INTERAGIRE CON  AletDialogResidenza
//##################################################################################################
//==================================================================================================
//                                  IMPLEMETAZIONI DEL ALTERDILOG OnAletDialogResidenzaEventListener
//==================================================================================================
    /**
     * Questa funzione permete di ricevvere dal <<AletDialogResidenza>> l'indirizzo inserito, con tale
     * indirizzo se ritenuto valio lo si inserisce nella pagina.
     *
     * N.B.
     *      Se il risultato di tale funzione sarà TRUE allpora si stà dicendo all'<<AletDialogResidenza>>
     *      che ha concluso il lavoro e potrà chiudersi mente se è FALSE, vorrà dire che i dati inseriti
     *      non sono ritenuti correti e quindi non permettera la chusura dell'<<AletDialogResidenza>>
     *      mantendo tali dati.
     */
    public boolean impostaIndirizzoDiResidenza(Indirizzo indirizzo){
        Boolean ris= false;
        if(     (indirizzo!=null) &&
                ( ( indirizzo.getStato()!=null )     && ( !indirizzo.getStato().equals("") ) )   &&
                ( ( indirizzo.getRegione()!=null )   && ( !indirizzo.getRegione().equals("") ) ) &&
                ( ( indirizzo.getCitta()!=null )     && ( !indirizzo.getCitta().equals("") ) )   &&
                ( ( indirizzo.getViaPiazza()!=null ) && ( !indirizzo.getViaPiazza().equals("") ) )){
            TextView viewIndirizzo = findViewById(R.id.txt_InputIndirizzo);
            indirizzoResidenza = indirizzo;
            viewIndirizzo.setText(indirizzo.getIndirizzoCompleto());
            ris= true;
        }
        return ris;
    }
//==================================================================================================
//##################################################################################################





//##################################################################################################
//                                  METODI PER INTERAGIRE CON  FragmentImmagineProfiloUtente
//                                              e
//                                  INTERZAZIONI CON IL DATABASE
//##################################################################################################
//==================================================================================================
//                                  IMPLEMETAZIONI DEL FRAGMENT OnFragmentImmagineProfiloUtenteEventListener
//==================================================================================================
    /**
     * Questa funzione non fa altro che:
     * - Se non è presente l'oggetto <<fotoAttivaUtente>> chiedere al database di fornire la foto attiva
     *      del utente attivo.
     * - Dal oggeto foto estrarre l'URL da qui estrarre il file, da passare come risultato.
     * - Se non è possibile recuperare il file o non c'è nessun file si ritorna null, e poi sarà il
     *      chiamante a decide quale immagine mostrare( che sarà quella di default cioè
     *      <<R.drawable.userdefault>> )
     */
    public File inizializazioneImmagineTramiteId(){
        File fileFotoProfilo = null;
        //Se c'è un oggeto foto
        if(fotoAttivaUtente!=null){
            fileFotoProfilo = new File(fotoAttivaUtente.getUrlFile());
        }
        return fileFotoProfilo;// ;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Questa funzione non fa altro che creare un file foto nell'area privata della applicazione
     * in cui nessuna applicazione possa cancellare tale file, e poi copiarci la foto che viene passata
     * dal fragment.
     */
    public boolean cambioImmagine(File immagine){
        Boolean risultato = false;
        cancellaFile(fotoCambiata);
        try {
            Pair<File, String> coppiaFileUrl = generaImmagineFile();
            fotoCambiata     = coppiaFileUrl.first;
            String currentPhotoPath = coppiaFileUrl.second;//Solo se serve
            copy(immagine,fotoCambiata);


            risultato =true;

        }catch (IOException e ){
            Log.d("Err_CeazioneFilePrivato",e.getMessage());
            risultato =false;
        }finally {
            return risultato;
        }
    }
//==================================================================================================
//==================================================================================================
//                              CREAZIONE DI UNA COPIA PRIVATA DELLA FOTO
//==================================================================================================
    /**
     * Salvo un file di tipo  foto nell'area privata della applicazione.
     **/
    private Pair<File,String> generaImmagineFile() throws IOException {
        // Crea un file dellimmagine
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);// FOTO PRIVATE
        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES); // FOTO PUBBLICHE

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",   /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        String currentPhotoPath = image.getAbsolutePath();
        Log.d("Risult ", "path da avere: "+currentPhotoPath);
        return new Pair<File,String>(image,currentPhotoPath);
    }
//==================================================================================================
//==================================================================================================
//                              COPIO IL CONTENUTO DI UN FILE IN UN ALTRO           (API 19+)
//==================================================================================================
    public static void copy(File src, File dst) throws IOException {
        try (InputStream in = new FileInputStream(src)) {
            try (OutputStream out = new FileOutputStream(dst)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }
//==================================================================================================
//==================================================================================================
//                             CANCELLA FILE TEMPORANEO
//==================================================================================================
    private void cancellaFile(File file){
        if(file!=null){
            if (file.exists()) {
                if (file.delete()) {
                    Log.d("MyTat","Ho cancellato il file della foto precetente");
                } else {
                    Log.d("MyTat","Non ho cancellato il file della foto precetente");
                }
            }
        }
    }
//==================================================================================================
//##################################################################################################





//==================================================================================================
//                                  CONTROLLA I DATI E SALVA LE MODIFICHE
//==================================================================================================
    /**
     * Metodo chiamato quando si prema il tato salva le modifice dei dati utente
     */
    private void controllaESalvaCambiamenti(){
        TextView errIndirizzo  = (TextView) findViewById(R.id.err_Indirizzo);
        View dvdSottolinetatura= (View)     findViewById(R.id.dvd_sottolinetatura);
        TextView errNumeroDiGara = (TextView) findViewById(R.id.err_NumeroDiGara);

        //Recumero oggeti
        TextView viewNumeroDiGaraPreferito = (TextView) findViewById(R.id.txt_InputNumeroDiGara);
        //Blochi di layout
        ViewGroup viewBloccoAutoPreferita       = (ViewGroup) findViewById(R.id.include_MachinaPreferita);
        ViewGroup viewBloccoCircuitoPreferita   = (ViewGroup) findViewById(R.id.include_CircuitoPreferita);
        ViewGroup viewBloccoCircuitoOdiato      = (ViewGroup) findViewById(R.id.include_CircuitoOdiato);
        //Spinner dei blochi
        Spinner spinnerBloccoAutoPreferita      = (Spinner) viewBloccoAutoPreferita.findViewById(R.id.spn_SpinnerDeiValori);
        Spinner spinnerBloccoCircuitoPreferita  = (Spinner) viewBloccoCircuitoPreferita.findViewById(R.id.spn_SpinnerDeiValori);
        Spinner spinnerBloccoCircuitoOdiato     = (Spinner) viewBloccoCircuitoOdiato.findViewById(R.id.spn_SpinnerDeiValori);

        //Dati effetivi
        //this.fotoCambiata;
        //this.indirizzoResidenza;
        String textNumeroDiGaraPreferito = viewNumeroDiGaraPreferito.getText().toString();
        Integer numeroDiGaraPreferito = null;
        AutoConImmagini     autoPreferita     = (AutoConImmagini) spinnerBloccoAutoPreferita.getSelectedItem();
        CircuitoConImmagini circuitoPreferita = (CircuitoConImmagini) spinnerBloccoCircuitoPreferita.getSelectedItem();
        CircuitoConImmagini circuitoOdiato    = (CircuitoConImmagini) spinnerBloccoCircuitoOdiato.getSelectedItem();


        boolean datiValidi=true;
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Verifica indirizzo
        if(     (indirizzoResidenza==null)                   || ( indirizzoResidenza.getStato().length()    >80 ) ||
                (indirizzoResidenza.getStato().equals(""))   || ( indirizzoResidenza.getRegione().length()  >80 ) ||
                (indirizzoResidenza.getRegione().equals("")) || ( indirizzoResidenza.getCitta().length()    >80 ) ||
                (indirizzoResidenza.getCitta().equals(""))   || ( indirizzoResidenza.getViaPiazza().length()>80 ) ||
                (indirizzoResidenza.getViaPiazza().equals("")) ){
            datiValidi = false;
            dvdSottolinetatura.setBackgroundResource( R.color.colorError);
            errIndirizzo.setTextColor(this.getResources().getColor(R.color.colorError));
        }else{
            dvdSottolinetatura.setBackgroundResource( R.color.colorPrimary);
            errIndirizzo.setTextColor(this.getResources().getColor(R.color.colorBackgraund));
        }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Verifica numero di gara
        if(     (textNumeroDiGaraPreferito!=null) ||
                (!textNumeroDiGaraPreferito.equals(""))){
            try{
                numeroDiGaraPreferito = Integer.parseInt(textNumeroDiGaraPreferito);
                if((numeroDiGaraPreferito<0)||(numeroDiGaraPreferito>=100)){
                    numeroDiGaraPreferito =null;
                }
            }catch(Exception e){
                numeroDiGaraPreferito =null;
            }
        }
        if(numeroDiGaraPreferito!=null){
            viewNumeroDiGaraPreferito.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            errNumeroDiGara.setTextColor(this.getResources().getColor(R.color.colorBackgraund));
        }else{
            datiValidi = false;
            viewNumeroDiGaraPreferito.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorError), PorterDuff.Mode.SRC_ATOP);
            errNumeroDiGara.setTextColor(this.getResources().getColor(R.color.colorError));
        }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if(datiValidi){
            salvaDati(fotoCambiata,indirizzoResidenza,numeroDiGaraPreferito,autoPreferita,circuitoPreferita,circuitoOdiato);
        }
    }
//==================================================================================================

}