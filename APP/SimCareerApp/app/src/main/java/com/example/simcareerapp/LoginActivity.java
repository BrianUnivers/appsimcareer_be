package com.example.simcareerapp;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.Interface.CostantiInserireEscrarreDatiDaiInternt;
import com.example.simcareerapp.DataBase.ThreadAction.ThreadLogin;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.GestioneRicordaSessione.GestioneSessione;
import com.example.simcareerapp.Persistence.Entities.Utente;

import java.util.concurrent.ExecutionException;


public class LoginActivity extends AppCompatActivity implements CostantiInserireEscrarreDatiDaiInternt{
//##################################################################################################
//                                  RIDUCE APPLICAZIONE
//##################################################################################################
//==================================================================================================
//                                  METODO PER IMPEDIRE DI RAGGIUNGERE ACTIVITY PRECEDENTI
//==================================================================================================
    /**
     * Questo metodo impedisce di ritornare tramite il tasto "Fisico" indietro dello smartphone alle
     * activity precedenti così da rendere la activity attuale quella di root per la navigazione.
     */
    protected void impostaPaginaComeHome(){
        this.getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startMain);
            }
        });
    }
//==================================================================================================

//##################################################################################################
//==================================================================================================
//                                  VIEW CON CUI SI INTERAGISCE
//==================================================================================================
    private EditText inputUsername;
    private EditText inputPassword;
    private Button   btnRegistrati;
    private Button   btnAccedi;
    private TextView lnkPasswordDimenticata;
    private CheckBox ckbRicordami;
//==================================================================================================
//                                  CREAZIONE DEL ACTIVITY
//==================================================================================================
    /**
     *  Metodo principale per l'activity che gestisce il login.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GestioneSessione.loginTramiteSessioneSePresente(this);//LOGIN SESSIONE
        impostaPaginaComeHome();//ELIMINA IL TASTO BACK FISICO
        setContentView(R.layout.activity_login);
//--------------------------------------------------------------------------------------------------
//                                  Imposta view di interazione
//--------------------------------------------------------------------------------------------------
        //Input
        inputUsername   = (EditText) findViewById(R.id.txt_InputUsername);
        inputPassword   = (EditText) findViewById(R.id.psw_InputPassword);
        //Bottoni
        btnRegistrati   = (Button)   findViewById(R.id.btn_Registrati);
        btnAccedi       = (Button)   findViewById(R.id.btn_Accedi);
        //Link
        lnkPasswordDimenticata = (TextView) findViewById(R.id.lnk_PasswordDimenticata);
        //CheckBox
        ckbRicordami   = (CheckBox) findViewById(R.id.ckb_Ricordami);
//--------------------------------------------------------------------------------------------------
//                                  Imposta Azioni
//--------------------------------------------------------------------------------------------------
        btnRegistrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                azioneBottoneRegistrazione();
            }
        });
//-----------------------------------
        btnAccedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                azioneBottoneLoginAccount();
            }
        });
//-----------------------------------
        lnkPasswordDimenticata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                azioneBottonePasswordDimenticata();
            }
        });
    }
//==================================================================================================


//==================================================================================================
//                                  METODI CHIAMATI ALLA PRESIONE DEI BOTTONI
//==================================================================================================
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante Registrazione che effetuerare il
     * passagio all'activity che effetuera la registrazione
     */
    public void azioneBottoneRegistrazione(){
        Log.d("MyTag","Ho premuto sul pulsate Registrati");
        try {
            Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(i);
        }catch (Exception e){
            Log.d("MyTag-Errore",e.toString());
            System.out.println(e);
        }
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante login che recupera il nome utente
     * la password e la spunta ricocordami, per poi fare l'autenticazione e passare eventualmete all'
     * activity succesiva.
     */
    private void azioneBottoneLoginAccount(){
        Log.d("MyTag","Ho premuto sul pulsate Accedi");
        String  username  = inputUsername.getText().toString();
        String  password  = inputPassword.getText().toString();
        Boolean ricordami = ckbRicordami.isChecked();

        Log.d("MyTag","Inizio controllo credenziali utente.");
        ThreadLogin threadLogin = new ThreadLogin(this);
        threadLogin.execute(new Pair<String, String>(username,password));
        Utente utente=null;
        try {
            utente =threadLogin.get();
        } catch (ExecutionException e) {
            utente=null;
            e.printStackTrace();
        } catch (InterruptedException e) {
            utente=null;
            e.printStackTrace();
        }
        if(utente != null){
            Log.d("MyTag","Utente valido");
            GestioneSessione.creaSessione(this,username,password,ricordami);//SESSIONE
            if(utente.getResidenza()!=null){
                //Utente che ha concluso la registazione
                apriPaginaCampionati(utente,ricordami);
            }else{
                //Utente che NON ha concluso la registazione
                apriPaginaConcludiRegistrazione(utente,ricordami);
            }
        }else{
            Log.d("MyTag","Utente non valido");
            visualizzaMessagioDiErroreLogin();
        }
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Il metodo chiamato dal Lisener quando si preme il link Registrazione inviera la richesta di
     * recupero password al server.
     */
    public void azioneBottonePasswordDimenticata(){
        Log.d("MyTag","Ho premuto sul link Password Dimenticata");
        //Toast.makeText(this,"EFFETUARE RICHIESTA DI RECUPERO PASSWORD",Toast.LENGTH_LONG).show();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.simcareer.org/forum/ucp.php?mode=sendpassword"));
        startActivity(browserIntent);
    }
//==================================================================================================
    /**
     * Il metodo che viene chamato per impostare il messagio di errore quando i dati inseriti non
     * corrispondono a nessun utente.
     */
    private void visualizzaMessagioDiErroreLogin(){
        TextView errLogin   = (TextView) findViewById(R.id.err_PasswordLogin);
        inputPassword.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorError), PorterDuff.Mode.SRC_ATOP);
        inputUsername.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorError), PorterDuff.Mode.SRC_ATOP);
        errLogin.setTextColor(this.getResources().getColor(R.color.colorError));
    }
//==================================================================================================
    /**
     * Il metodo che viene chamato aprire la pagina dei campinati passando come valori l'utente registrato
     * e il valore che serve per sapre se devo ricordarmi del utente.
     */
    private void apriPaginaCampionati(Utente utente,Boolean ricordami){
        MyIntent i = new MyIntent(LoginActivity.this, CampionatiActivity.class);
        i.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utente);
        i.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,ricordami);
        startActivity(i);
    }
//==================================================================================================
//==================================================================================================
    /**
     * Il metodo che viene chamato aprire la pagina dei per concludere la registrazione che è rismata
     * interrotta passando come valori l'utente registrato e il valore che serve per sapre se devo
     * ricordarmi del utente.
     */
    private void apriPaginaConcludiRegistrazione(Utente utente,Boolean ricordami){
        MyIntent i = new MyIntent(LoginActivity.this, ModificaProfiloUtenteActivity.class);
        i.putExtraUtente(MyIntent.I_UTENTE_ATTIVO, utente);
        i.putExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,ricordami);
        startActivity(i);
    }
//==================================================================================================
}