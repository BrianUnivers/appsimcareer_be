package com.example.simcareerapp.DataBase.ThreadAction;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.simcareerapp.DataBase.Management.HashPasswordWithSalt;
import com.example.simcareerapp.DataBase.Management.SimpleDbHelper;
import com.example.simcareerapp.DataBase.Query.FotoQuery;
import com.example.simcareerapp.DataBase.Query.UtenteQuery;
import com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp.AlertDialogResidenza;
import com.example.simcareerapp.Persistence.Entities.Foto;
import com.example.simcareerapp.Persistence.Entities.Indirizzo;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.R;

import java.sql.Date;
import java.util.List;


public class ThreadLogin extends AsyncTask<Pair<String,String>,Integer,Utente> {
    private SimpleDbHelper database;
    private Context context;
//==================================================================================================
//                              COSTRUTORE                                                          <<Processo Principale>>
//==================================================================================================
    /**
     * Per avere la posibilità di accedere alla Accivity e instauro un collegamento col server.
     */
    public ThreadLogin(Context context) {
        //Crazione DataBase o Associazione con uno già creato
        this.database = new SimpleDbHelper(context);
        this.context = context;
    }
//==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE PRIMA DELLA CHAMATA DEL THREAD                     <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
//==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DOPO DELLA CHAMATA DEL THREAD                      <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onPostExecute(Utente utente) {
        super.onPostExecute(utente);
    }
//==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DURANTE DELLA CHAMATA DEL THREAD                   <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }
//==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DAL THREAD                                         <<Processo Seconario>>
//==================================================================================================
    /**
     * Effetua la verifica delle credeziali passate sottoforma di coppia <Username/Email , Password>
     * cosi facendo controllo se c'è un utente con Username o Email per recupreare il Salt che permettera
     * di generare la password hash con il salt per poi estrare l'utente solo se il suo nome e la
     * password sono correti.
     * Quindi tale funzione/thread restituisce o l'utente richesto o null se i dati non sono validi.
     */
    @Override
    protected Utente doInBackground(Pair<String, String>... pairs) {
        Utente utente = null;
        Pair<String, String> pair =  pairs[0];
        String nomeIdentificativo = pair.first;
        String password = pair.second;

        if( (nomeIdentificativo!=null) && (!nomeIdentificativo.equals("")) &&
                (password!=null) && (!password.equals(""))){
            String salt=null;
            if(salt==null){
                salt = UtenteQuery.getSaltByEmail(database,nomeIdentificativo);
            }
            if(salt==null){
                salt = UtenteQuery.getSaltByUsername(database,nomeIdentificativo);
            }

            if(salt!=null){//Verifica la password e recupero utente
                String passwordHashed = HashPasswordWithSalt.genraPasswordHashWithSalt(password,salt);
                if(utente==null){
                    utente = UtenteQuery.getUtenteAutenticatoByEmailPassword(database,nomeIdentificativo,passwordHashed);
                }
                if(utente==null){
                    utente = UtenteQuery.getUtenteAutenticatoByUsernamePassword(database,nomeIdentificativo,passwordHashed);
                }
            }
        }
        return utente;
    }
//==================================================================================================
}
