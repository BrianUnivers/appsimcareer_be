package com.example.simcareerapp.Persistence.JsonEntities;

import android.util.Log;

import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.ClassificaCampionati;
import com.example.simcareerapp.Persistence.Entities.Pilota;
import com.example.simcareerapp.Persistence.Entities.PilotaConPunti;
import com.example.simcareerapp.Persistence.Entities.TeamConPunti;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class ClassificaCampionatiConJson extends ClassificaCampionati {
//##################################################################################################
//                                  LABEL CLASSIFICA PER JSON
//##################################################################################################
    protected final String LABLE_ID                 = "id";
    protected final String LABLE_NOME               = "nome";
    protected final String LABLE_LOGO_URL           = "logo";
    protected final String LABLE_CLASSIFICA_PILOTI  = "classifica-piloti";
    protected final String LABLE_CLASSIFICA_TEAM    = "classifica-team";
//##################################################################################################
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
    public ClassificaCampionatiConJson(Long id,
                                       String name,
                                       String logoURL,
                                       List<PilotaConPunti> classificaPiloti,
                                       List<TeamConPunti> classificaTeam) {
        super(id, name, logoURL, classificaPiloti, classificaTeam);
    }
//--------------------------------------------------------------------------------------------------
    public ClassificaCampionatiConJson(JSONObject jsonObject){
        if(jsonObject!=null) {
            try {
                this.setId(     jsonObject.getLong(  LABLE_ID) );
                this.setName(   jsonObject.getString(LABLE_NOME) );
                this.setLogoURL(jsonObject.getString(LABLE_LOGO_URL) );

                //GESTIONE ARRAY PER LA CLASSIFICA DEI PILOTI
                JSONArray jsonArrayPiloti = jsonObject.getJSONArray(LABLE_CLASSIFICA_PILOTI);
                List<PilotaConPunti> listaPiloti = new LinkedList<PilotaConPunti>();
                for(int i=0; i<jsonArrayPiloti.length(); i++){
                    PilotaConPunti pilota= new PilotaConPuntiConJson(jsonArrayPiloti.getJSONObject(i));
                    if(!pilota.isNull()){
                        listaPiloti.add(pilota);
                    }
                }
                this.setClassificaPiloti(listaPiloti);

                //GESTIONE ARRAY PER LA CLASSIFICA DEI TEAM
                JSONArray jsonArrayTeam = jsonObject.getJSONArray(LABLE_CLASSIFICA_TEAM);
                List<TeamConPunti> listaTeam = new LinkedList<TeamConPunti>();
                for(int i=0; i<jsonArrayTeam.length(); i++){
                    TeamConPunti team= new TeamConPuntiConJson(jsonArrayTeam.getJSONObject(i));
                    if(!team.isNull()){
                        listaTeam.add(team);
                    }
                }
                this.setClassificaTeam(listaTeam);

            } catch (JSONException e) {
                Log.d("ErrJson",e.getMessage());
            }
        }
    }
//==================================================================================================
}
