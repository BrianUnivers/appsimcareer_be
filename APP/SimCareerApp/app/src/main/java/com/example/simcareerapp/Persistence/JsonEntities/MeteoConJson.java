package com.example.simcareerapp.Persistence.JsonEntities;

import android.util.Log;

import com.example.simcareerapp.Persistence.Entities.AutoConImmagini;
import com.example.simcareerapp.Persistence.Entities.Meteo;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

public class MeteoConJson extends Meteo {
//##################################################################################################
//                                  LABEL PILOTI PER JSON
//##################################################################################################
    protected final String LABLE_LUOGO      = "luogo";
    protected final String LABLE_MIN_TEMP   = "minTemperatura";
    protected final String LABLE_MAX_TEMP   = "maxTemperatura";
    protected final String LABLE_UMIDITA    = "umidita";
    protected final String LABLE_NUVOLOSO   = "nuvoloso";
    protected final String LABLE_NEBBIA     = "nebbia";
    protected final String LABLE_SOLEGGIATO = "soleggiato";
    protected final String LABLE_PIOVOSO    = "piovoso";
    protected final String LABLE_VENTOSO    = "ventoso";
    protected final String LABLE_TEMPORALE  = "temporale";
//##################################################################################################
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
    public MeteoConJson(String luogo, Double minTemperatura, Double maxTemperatura, Double umidita, Boolean nuvoloso, Boolean nebbia, Boolean soleggiato, Boolean piovoso, Boolean ventoso, Boolean temporale) {
        super(luogo, minTemperatura, maxTemperatura, umidita, nuvoloso, nebbia, soleggiato, piovoso, ventoso, temporale);
    }
//--------------------------------------------------------------------------------------------------
    public MeteoConJson(JSONObject jsonObject){
        if(jsonObject!=null) {
            try {
                this.setLuogo(          jsonObject.getString(LABLE_LUOGO) );
                this.setMaxTemperatura( jsonObject.getDouble(LABLE_MAX_TEMP) );
                this.setMinTemperatura( jsonObject.getDouble(LABLE_MIN_TEMP) );
                this.setUmidita(        jsonObject.getDouble(LABLE_UMIDITA) );
                this.setNuvoloso(       jsonObject.getBoolean(LABLE_NUVOLOSO) );
                this.setNebbia(         jsonObject.getBoolean(LABLE_NEBBIA) );
                this.setSoleggiato(     jsonObject.getBoolean(LABLE_SOLEGGIATO) );
                this.setPiovoso(        jsonObject.getBoolean(LABLE_PIOVOSO) );
                this.setVentoso(        jsonObject.getBoolean(LABLE_VENTOSO) );
                this.setTemporale(      jsonObject.getBoolean(LABLE_TEMPORALE) );
            } catch (JSONException e) {
                Log.d("ErrJson",e.getMessage());
            }
        }
}
//==================================================================================================
//##################################################################################################
//                              GENERA JSON
//##################################################################################################
//==================================================================================================
//                              GENERA JSON METEO
//==================================================================================================
    public JSONObject getJSON(){
        JSONObject object = new JSONObject();
        try {
            object.put(LABLE_LUOGO,      this.luogo);
            object.put(LABLE_MAX_TEMP,   this.maxTemperatura);
            object.put(LABLE_MIN_TEMP,   this.minTemperatura);
            object.put(LABLE_UMIDITA,    this.umidita);
            object.put(LABLE_NUVOLOSO,   this.nuvoloso);
            object.put(LABLE_NEBBIA,     this.nebbia);
            object.put(LABLE_SOLEGGIATO, this.soleggiato);
            object.put(LABLE_PIOVOSO,    this.piovoso);
            object.put(LABLE_VENTOSO,    this.ventoso);
            object.put(LABLE_TEMPORALE,  this.temporale);
        }catch (JSONException e) {
            object=null;
            e.printStackTrace();
        }
        return object;
    }
//==================================================================================================
//##################################################################################################
}
