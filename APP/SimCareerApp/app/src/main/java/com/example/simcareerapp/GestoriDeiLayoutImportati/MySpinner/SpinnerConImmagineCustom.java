package com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinner;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MySpinner.Interface.OnItemSelectedSpinnerConImmagineListener;
import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;

public class SpinnerConImmagineCustom extends SpinnerConImmagine{
    private  OnItemSelectedSpinnerConImmagineListener selectedAction=null;
//##################################################################################################
//                                  COSTRUTTORI IMPOSTA LO STILE DELLE PARTE STATICHE
//##################################################################################################
    /**
     * Costruisce e gestice uno spinnere che modifica in base ai suoi valoi un immagine e ha due
     * bottoni che permettono di passare al valor succesiovo e o precendete cambiando immagine.
     */
//==================================================================================================
//                                  COSTRUTORE PER ACTIVITY
//==================================================================================================
    public SpinnerConImmagineCustom(AppCompatActivity context, OnItemSelectedSpinnerConImmagineListener selectedAction, int idImpost, int idText) {
        super(context, idImpost, idText);
        this.selectedAction = selectedAction;
    }
//--------------------------------------------------------------------------------------------------
    public SpinnerConImmagineCustom(Context context, OnItemSelectedSpinnerConImmagineListener selectedAction,  View view, int idImpost, int idText) {
        super(context, view, idImpost, idText);
        this.selectedAction = selectedAction;
    }
//==================================================================================================
//##################################################################################################


//==================================================================================================
//                                  AZIONI AGGIUNTIVE
//==================================================================================================
    /**
     * Questo metodo permette di aggiungere delle azzioni dopo aver selezionato un nuovo elemeto
     * così che le classi succesive non devono riscrivere tutto il codice di cambiameto del blocco
     */
    @Override
    protected void onItemSelected(ValoreConImmaginePerSpinner elemento) {
        this.selectedAction.onItemSelectedSpinnerConImmagineCustom(elemento);
    }
//==================================================================================================
}
