package com.example.simcareerapp.DataBase.Management;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 * Questa classe contiene i metodi per poteggere le password dei utenti e per verificare, se un testo
 * corrisponde ad una password hashed con l'utilizzo di un specifico salt.
 *
 * @author Brian Emanuel
 */
public class HashPasswordWithSalt {
    /**
     * La costante "NUM_HASHED_PASSWORD" contiene il numeto di volte che viene calcolata l'hash della
     * password a cui si aggiunge il salt.
     */
    private static int    NUM_HASHED_PASSWORD = 100;
    /**
     * La costante "TYPE_OF_HASH" contiene il tipo di algoritmo usato per calcolare l'hash.
     */
    private static String TYPE_OF_HASH        = "SHA-256";
//==================================================================================================
//                              CONVERTE UN ARRAY BYTE A STRING
//==================================================================================================
    /**
     * La costante "HEX_ARRAY" contiene in un array di caratteri i valori esadecipali,in modo tale
     * da avere una rappresentazione di un byte.
     */
    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    /**
     * Questa funzione serve per convertire un array binario in una stringa di caratteri non spciali
     * @param bytes
     *      Il parametro che passi è un array di byte.
     * @return
     *      Il risutltato è una stringa che è la rappresentazione in caratte non speciali del array
     *      che si è passato come parametro.
     */
    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
//==================================================================================================
//==================================================================================================
//                              GENERA HASH FROM STRING
//==================================================================================================
    /**
     * Questa funzione dato una stringa effetua l'operazione di hash e ne restituisce una corrispondente
     * al array binario del hash
     *
     * @param text
     *      Il parametro passsato sarà il dato sorgente sul quale verrà applicato la funzione di hash.
     * @return
     *      Il rusultato di questo metodo è una stringa che contiene la rapresentazione in esadecimale
     *      della sequenza di byte denerata dal hash del parametro iniziale.
     *      Se non fosse possibile calcolare l'hash, il risultato di tale funzione è null.
     */
    public static String genraHashFromString(String text){
        MessageDigest digest = null;
        String ris = null;
        try {
            digest = MessageDigest.getInstance(TYPE_OF_HASH);
            byte[] encodedhash = digest.digest(text.getBytes(StandardCharsets.UTF_8));
            ris = bytesToHex(encodedhash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                              GENERA SALT
//==================================================================================================
    /**
     * Qusta funzione genrara un salt sepre diverso a partire dai millesecondi attuali.
     *
     * @return
     *      Il risultato di questo metodo è una stringa contenete una rappresentazione in esadecimale
     *      della sequenza di byte che viene generato. Se non fosse possibile calcolare un salt il
     *      risultata retiuirà null.
     */
    public static String genraSalt(){
        Date today = new Date();
        Long seedNum = today.getTime();
        String seedString = seedNum.toString();
        return genraHashFromString(seedString);
    }
//==================================================================================================
//==================================================================================================
//                              GENERA PASSWORD WITH SALT
//==================================================================================================
    /**
     * Qusta funzione genrara la password hashed partendo da quella in chiaro e il salt, applicando
     * per "NUM_HASHED_PASSWORD" la procedura di hash sulla coppia salt e password.
     *
     * @param password
     *      Questo parametro conterra la password, solitamente in chiaro, per otenerne una sua versione
     *      privata da usare.
     * @param salt
     *      Questo parametro permette, se per ogni password è sempre diverso, di generare una versione
     *      privata della password in modo tale che la stessa password in chiaro non generi la stessa
     *      versione privata.
     * @return
     *      Il risultato è la rapreasentazione sicura della password.
     *      Se ci dovesse essere un problema con il calcolo del hash il risultato è null.
     */
    public static String genraPasswordHashWithSalt(String password,String salt){
        String ris= null;
        if((password!=null)&&(!password.equals(""))){
            ris=password;
            for (int i =0; i<NUM_HASHED_PASSWORD; i++){
                ris=genraHashFromString((salt+ris+salt));
            }
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                              GENERA PASSWORD WITH SALT
//==================================================================================================
    /**
     * Qusta funzione controlla se una password in chiaro con un determinato salt è uguale a una
     * già hashed.
     *
     * @param password
     *      Questo parametro conterrà la password in chiaro allca quale si aggiunge il salt per il
     *      confronto.
     * @param salt
     *      Questo parametro conterrà il salt della password di cui si ha la versione hashed così da
     *      poterla generare una versione a partire dalla password in chiaro.
     * @param passwordHashed
     *      Questo parametro conterrà la password già associata al salt a cui è già stata applicata
     *      l'operazine di hash.
     * @return
     *      Il risultato booleano di questo metodo ci dice si l'a password e il salt generano la stessa
     *      passwordHashed nel caso di una riposta affermativa (true) altrimenti il risultato è false.
     */
    public static Boolean verifcaUguaglianzaPasswordHash(String password,String salt,String passwordHashed){
        return genraPasswordHashWithSalt(password,salt).equals(passwordHashed);
    }
//==================================================================================================
}
