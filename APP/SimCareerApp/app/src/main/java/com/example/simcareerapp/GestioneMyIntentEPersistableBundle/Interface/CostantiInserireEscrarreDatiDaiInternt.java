package com.example.simcareerapp.GestioneMyIntentEPersistableBundle.Interface;

/**
 * Costanti usater per passare i dati da una Activity all'altra tramite gli Intent.
 */
public interface CostantiInserireEscrarreDatiDaiInternt {
//##################################################################################################
//                              DATI PASSATI CON GLI INTENT
//##################################################################################################
//==================================================================================================
//                              PARAMETRI PASSATI IN TUTTE LE ACTIVITY (Eccezione LOGIN e REGISTER ACTIVITY)
//==================================================================================================
    public static String I_UTENTE_ATTIVO                  = "UtenteAttivo";         //UTENTE
    public static String I_MANTIENI_ATTIVO_UTENTE         = "MantieniAttivoUtente"; //REMEMBER ME
    // SOLO Per le Activity MdificaProfiloUtente e ProfiloUtente.
    public static String I_FOTO_ATTIVA_UTENTE             = "FotoAttivaDellUtente"; //FOTO
    // SOLO Per la Activiti SingoloCampionato
    public static String I_ID_CAMPIONATO_SELEZIONATO      = "IdDelCampionatoSelezionato";
    // SOLO Per la Activiti ListaFotoGalleria
    public static String I_FOLDER                         = "Folder";
//==================================================================================================
//##################################################################################################
//==================================================================================================
//                              PER I DATI DEL UTENTE
//==================================================================================================
    public static String I_UTENTE_ID                    = "I_Utente_Id";
    public static String I_UTENTE_NOME                  = "I_Utente_Nome";
    public static String I_UTENTE_COGNOME               = "I_Utente_Cognome";
    public static String I_UTENTE_DATA_NASCITA          = "I_Utente_Data_Nascita";
    public static String I_UTENTE_USERNAME              = "I_Utente_Data_Username";
    public static String I_UTENTE_EMAIL                 = "I_Utente_Email";
    public static String I_UTENTE_SALT                  = "I_Utente_Salt";
    public static String I_UTENTE_PASSWORD              = "I_Utente_Password";
//--------------------------------------------------------------------------------------------------
    public static String I_UTENTE_RESIDENZA_STATO       = "I_Utente_Resideza_Stato";
    public static String I_UTENTE_RESIDENZA_REGIONE     = "I_Utente_Resideza_Regione";
    public static String I_UTENTE_RESIDENZA_CITTA       = "I_Utente_Resideza_Citta";
    public static String I_UTENTE_RESIDENZA_VIA_PIAZZA  = "I_Utente_Resideza_ViaPiazza";
//--------------------------------------------------------------------------------------------------
    public static String I_UTENTE_NUMERO_GARA           = "I_Utente_NumeroGara";
//--------------------------------------------------------------------------------------------------
    public static String I_UTENTE_AUTO_PREF_NOME        = "I_Utente_Auto_Pref_Nome";
    public static String I_UTENTE_AUTO_PREF_ID_IMG      = "I_Utente_Auto_Pref_IdImg";
//--------------------------------------------------------------------------------------------------
    public static String I_UTENTE_CIRCUITO_PREF_NOME    = "I_Utente_Circuito_Pref_Nome";
    public static String I_UTENTE_CIRCUITO_PREF_ID_IMG  = "I_Utente_Circuito_Pref_IdImg";
//--------------------------------------------------------------------------------------------------
    public static String I_UTENTE_CIRCUITO_ODIATO_NOME  = "I_Utente_Circuito_Odiato_Nome";
    public static String I_UTENTE_CIRCUITO_ODIATO_ID_IMG= "I_Utente_Circuito_Odiato_IdImg";
//==================================================================================================
//##################################################################################################
//==================================================================================================
//                              PER I DATI DEL UTENTE
//==================================================================================================
    public static String I_FOTO_ID                  = "I_Foto_Id";
    public static String I_FOTO_ID_UTENTE           = "I_Foto_IdUtente";
//--------------------------------------------------------------------------------------------------
    public static String I_FOTO_TESTO_ALTERNATIVO   = "I_Foto_TestoAlternativo";
    public static String I_FOTO_STATO_ATTIVO        = "I_Foto_StatoAttivo";
    public static String I_FOTO_URL_FILE            = "I_Foto_UrlFile";
//==================================================================================================
}
