package com.example.simcareerapp.Persistence.Entities;

import android.util.Pair;

import com.example.simcareerapp.Persistence.Entities.Interface.CotantiValoriCampionato;
import com.example.simcareerapp.Persistence.Entities.Interface.ValoreConImmaginePerSpinner;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Campionato implements CotantiValoriCampionato  {
    public Integer MAX_NUM_ISCRITTI = 32;
//##################################################################################################
//                                  INFORMAZIONI CAMPIONATO
//##################################################################################################
    protected Long    id      = null;
    protected String  name    = null;
    protected String  logoURL = null;
//##################################################################################################
//##################################################################################################
//                                  INFORMAZIONI GARE
//##################################################################################################
    protected List<Gara> listaGara = new LinkedList<Gara>();
//##################################################################################################
//##################################################################################################
//                                  BANDIERE USATE
//##################################################################################################
    protected boolean bandiraNera                 = false;
    protected boolean bandiraBlu                  = false;
    protected boolean bandiraGialla               = false;
    protected boolean bandiraRossa                = false;
    protected boolean bandiraVerde                = false;
    protected boolean bandiraBiancaENera          = false;
    protected boolean bandiraBiancaDiscoArancio   = false;
    protected boolean bandiraStrisceGialleERosse  = false;
//##################################################################################################
//##################################################################################################
//                                  AIUTI ALLA GUIDA
//##################################################################################################
    protected boolean statoAiutoAbs                 = false;
    protected boolean statoAiutoAutoretromarcia     = false;
    protected boolean statoAiutoControlloTrazione   = false;
    protected boolean statoAiutoCorsiaBox           = false;
    protected boolean statoAiutoBloccagioOpposto    = false;
    protected boolean statoAiutoFrizioneAutomatica  = false;
    protected boolean statoAiutoFrenata             = false;
    protected boolean statoAiutoCambioAutomatico    = false;
    protected boolean statoAiutoInvulnerabilita     = false;
    protected boolean statoAiutoSterzata            = false;
    protected boolean statoAiutoControlloStabilita  = false;
    protected boolean statoAiutoRipresaSpin         = false;
//##################################################################################################
//##################################################################################################
//                                  CONSUMI
//##################################################################################################
    protected Consumi consumiCarburante   = null;
    protected Consumi consumiGomme        = null;
//##################################################################################################
//##################################################################################################
//                                  LISTA VEICOLI POSSIBILI
//##################################################################################################
    protected List<ValoreConImmaginePerSpinner> listaAuto = new LinkedList<ValoreConImmaginePerSpinner>();
//##################################################################################################
//##################################################################################################
//                                  LISTA VEICOLI POSSIBILI
//##################################################################################################
    protected List<Pilota> listaPiloti = new LinkedList<Pilota>();
//##################################################################################################
//==================================================================================================
//                              COSTRUTTORE
//==================================================================================================
    public Campionato() {
    }
//--------------------------------------------------------------------------------------------------
    public Campionato(  Long id,
                        String name,
                        String logoURL,
                        List<Gara> liastaGara,
                        String bandire,
                        String aiuti,
                        Consumi consumiCarburante,
                        Consumi consumiGomme,
                        List<ValoreConImmaginePerSpinner> listaAuto,
                        List<Pilota> listaPiloti) {
        this.id = id;
        this.name = name;
        this.logoURL = logoURL;
        this.listaGara = listaGara;
        abilitaBandiereDaStringa(bandire);
        abilitaAiutiDaStringa(aiuti);
        this.consumiCarburante = consumiCarburante;
        this.consumiGomme = consumiGomme;
        this.listaAuto = listaAuto;
        this.listaPiloti = listaPiloti;
    }
//==================================================================================================



//==================================================================================================
//                              GETTER E SETTER
//==================================================================================================
    public Long getId() {
        return id;
    }
//------------------------------
    public void setId(Long id) {
        this.id = id;
    }
//--------------------------------------------------------------------------------------------------
    public String getName() {
        return name;
    }
//------------------------------
    public void setName(String name) {
        this.name = name;
    }
//--------------------------------------------------------------------------------------------------
    public String getLogoURL() {
        return logoURL;
    }
//------------------------------
    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }
//--------------------------------------------------------------------------------------------------
    public List<Gara> getListaGara() {
        return listaGara;
    }
//------------------------------
    public void setListaGara(List<Gara> listaGara) {
        this.listaGara = listaGara;
    }
//--------------------------------------------------------------------------------------------------
    public Consumi getConsumiCarburante() {
        return consumiCarburante;
    }
//------------------------------
    public void setConsumiCarburante(Consumi consumiCarburante) {
        this.consumiCarburante = consumiCarburante;
    }
    public void setConsumiCarburante(String consumiCarburante) {
        this.consumiCarburante = convetiStingaInConsumi(consumiCarburante);
    }
//--------------------------------------------------------------------------------------------------
    public Consumi getConsumiGomme() {
        return consumiGomme;
    }
//------------------------------
    public void setConsumiGomme(Consumi consumiGomme) {
        this.consumiGomme = consumiGomme;
    }
    public void setConsumiGomme(String consumiGomme) {
        this.consumiGomme = convetiStingaInConsumi(consumiGomme);
    }
//--------------------------------------------------------------------------------------------------
    public List<ValoreConImmaginePerSpinner> getListaAuto() {
        return listaAuto;
    }
//------------------------------
    public void setListaAuto(List<ValoreConImmaginePerSpinner> listaAuto) {
        this.listaAuto = listaAuto;
    }
//--------------------------------------------------------------------------------------------------
    public List<Pilota> getListaPiloti() {
        return listaPiloti;
    }
//------------------------------
    public void setListaPiloti(List<Pilota> listaPiloti) {
        this.listaPiloti = listaPiloti;
    }
//--------------------------------------------------------------------------------------------------
    public String getConsumiCarburanteString() {
        return converitiConsumiToString(consumiCarburante);
    }
//------------------------------
    public String getConsumiGommeString() {
        return converitiConsumiToString(consumiGomme);
    }
//==================================================================================================
//                              CONVERTI STINGA IN CONSUMI
//==================================================================================================
    protected String converitiConsumiToString(Consumi consumi){
        String ris ="";
        switch (consumi){
            case BASSI:
                ris=Campionato.CONSUMI_BASSI;
                break;
            case NORMALI:
                ris=Campionato.CONSUMI_NORMALI;
                break;
            case ELEVATI:
                ris=Campionato.CONSUMI_ELEVATI;
                break;
            default:
                ris="";
        }
        return ris;
    }
//--------------------------------------------------------------------------------------------------
    protected Consumi convetiStingaInConsumi(String string){
        Consumi ris;
        switch (string){
            case Campionato.CONSUMI_BASSI:
                ris = Consumi.BASSI;
                break;
            case Campionato.CONSUMI_NORMALI:
                ris = Consumi.NORMALI;
                break;
            case Campionato.CONSUMI_ELEVATI:
                ris = Consumi.ELEVATI;
                break;
            default:
                ris=Consumi.NORMALI;
        }
        return ris;
    }
//==================================================================================================



//==================================================================================================
//                              CONVERTI STRINGA A BANDIERE ATTIVE
//==================================================================================================
    protected void abilitaBandiereDaStringa(String bandire) {
        this.bandiraNera                = false;
        this.bandiraBlu                 = false;
        this.bandiraGialla              = false;
        this.bandiraRossa               = false;
        this.bandiraVerde               = false;
        this.bandiraBiancaENera         = false;
        this.bandiraBiancaDiscoArancio  = false;
        this.bandiraStrisceGialleERosse = false;
        if( (bandire!=null) && (!bandire.equals(""))  ){
            String[] arrayBandiereAttive = bandire.split(",");
            for (int i=0; i<arrayBandiereAttive.length;i++){
                String bandiera = arrayBandiereAttive[i].trim();
                impostaBandieraDaStringa(bandiera);
            }
        }
    }
//--------------------------------------------------------------------------------------------------
    private void impostaBandieraDaStringa(String bandira){
        switch (bandira){
            case BANDIERA_NERA:
                this.bandiraNera = true;
                break;
            case BANDIERA_BLU:
                this.bandiraBlu = true;
                break;
            case BANDIERA_GIALLA:
                this.bandiraGialla = true;
                break;
            case BANDIERA_ROSSA:
                this.bandiraRossa = true;
                break;
            case BANDIERA_VERDE:
                this.bandiraVerde = true;
                break;
            case BANDIERA_BIANCA_NERA:
                this.bandiraBiancaENera = true;
                break;
            case BANDIERA_BIANCA_DISCO_ARANCIO:
                this.bandiraBiancaDiscoArancio = true;
                break;
            case BANDIERA_STRISCE_GIALLE_ROSE:
                this.bandiraStrisceGialleERosse = true;
                break;
        }
    }
//==================================================================================================
//==================================================================================================
//                              CONVERTI STRINGA A AIUTI ALLA GUIDA
//==================================================================================================
    protected void abilitaAiutiDaStringa(String aiuti) {
        this.statoAiutoAbs                 = false;
        this.statoAiutoAutoretromarcia     = false;
        this.statoAiutoControlloTrazione   = false;
        this.statoAiutoCorsiaBox           = false;
        this.statoAiutoBloccagioOpposto    = false;
        this.statoAiutoFrizioneAutomatica  = false;
        this.statoAiutoFrenata             = false;
        this.statoAiutoCambioAutomatico    = false;
        this.statoAiutoInvulnerabilita     = false;
        this.statoAiutoSterzata            = false;
        this.statoAiutoControlloStabilita  = false;
        this.statoAiutoRipresaSpin         = false;
        if( (aiuti!=null) && (!aiuti.equals(""))  ){
            String[] arrayAiutiAttive = aiuti.split(",");
            for (int i=0; i<arrayAiutiAttive.length;i++){
                String aiuto = arrayAiutiAttive[i].trim();
                impostaAiutiDaStringa(aiuto);
            }
        }
    }
//--------------------------------------------------------------------------------------------------
    private void impostaAiutiDaStringa(String aiuto){
        switch (aiuto){
            case AIUTO_ABS:
                this.statoAiutoAbs = true;
                break;
            case AIUTO_AUTORETROMARCIA:
                this.statoAiutoAutoretromarcia = true;
                break;
            case AIUTO_CONTROLLO_TRAZIONE:
                this.statoAiutoControlloTrazione = true;
                break;
            case AIUTO_CORSIA_BOX:
                this.statoAiutoCorsiaBox = true;
                break;
            case AIUTO_BLOCCAGGIO_OPPOSTO:
                this.statoAiutoBloccagioOpposto = true;
                break;
            case AIUTO_FRIZIONE_AUTOMATICA:
                this.statoAiutoFrizioneAutomatica = true;
                break;
            case AIUTO_FRENATA:
                this.statoAiutoFrenata = true;
                break;
            case AIUTO_CAMBIO_AUTOMATICO:
                this.statoAiutoCambioAutomatico = true;
                break;
            case AIUTO_INVULNERABILITA:
                this.statoAiutoInvulnerabilita = true;
                break;
            case AIUTO_STERZATA:
                this.statoAiutoSterzata = true;
                break;
            case AIUTO_CONTROLLO_STABILITA:
                this.statoAiutoControlloStabilita = true;
                break;
            case AIUTO_RIPRESA_SPIN:
                this.statoAiutoRipresaSpin = true;
                break;
        }
    }
//==================================================================================================
//==================================================================================================
//                              VERIFICA CHE NON SIA UN OGGETO CON PARAMETRI NULL
//==================================================================================================
    public boolean isNull(){
        return !(   (getId()!=null) &&
                    (getName()!=null) &&
                    (getLogoURL()!=null) &&
                    (getListaGara()!=null) &&
                    (getListaGara().size()!=0)&&
                    (getListaAuto()!=null) &&
                    (getListaAuto().size()!=0) &&
                    (getListaPiloti()!=null)   );
    }
//==================================================================================================



//==================================================================================================
//                              CALCOLA LA DATA DI INIZIO E FINE CAMPIONATO
//==================================================================================================
    public Date getDataInizioCampionato(){
        List<Gara> gare = this.getListaGara();
        Date date=null;
        if( (gare!=null) && (gare.size()!=0)){
            for (int i=0; i<gare.size(); i++){
                 if( (date==null) || (date.after(gare.get(i).getData()))  ){
                     date = gare.get(i).getData();
                 }
            }
        }
        return date;
    }
//--------------------------------------------------------------------------------------------------
    public Date getDataFineCampionato(){
        List<Gara> gare = this.getListaGara();
        Date date=null;
        if( (gare!=null) && (gare.size()!=0)){
            for (int i=0; i<gare.size(); i++){
                if( (date==null) || (date.before(gare.get(i).getData()))  ){
                    date = gare.get(i).getData();
                }
            }
        }
        return date;
    }
//==================================================================================================


//==================================================================================================
//                              STATO ISCRIZIONI AL CAMPIONATO
//==================================================================================================
    public boolean iscrizioneAperta(){
        Boolean ris = false;
        Date oggi = new Date();
        if(oggi.before(getDataInizioCampionato())){
            ris = true;
        }
        return ris;
    }
//--------------------------------------------------------------------------------------------------
    public boolean campionatoConcluso(){
        Boolean ris = true;
        Date oggi = new Date();
        if(oggi.before(getDataFineCampionato())){
            ris = false;
        }
        return ris;
    }
//--------------------------------------------------------------------------------------------------
    public boolean postiDisponibili(){
        Boolean ris = false;
        List<Pilota> pilotaList = getListaPiloti();
        if( (pilotaList==null) || (pilotaList.size()<MAX_NUM_ISCRITTI)){
            ris = true;
        }
        return ris;
    }
//==================================================================================================

//==================================================================================================
//                              VERIFICA SE UTENTE PRESENTE NEI PARTECIPANTI
//==================================================================================================
    public boolean utenteIscritto(Utente utente){
        Boolean ris = false;
        List<Pilota> pilotaList = getListaPiloti();
        if((pilotaList!=null) || (pilotaList.size()!=0)){
            int i=-1;
            boolean pilotaTrovato = false;
            do{
                i++;
                if(utente.equalsToPiloti(pilotaList.get(i) )){
                    ris = true;
                    pilotaTrovato=true;
                }else{
                    if((pilotaList.size()-1)==i){
                        ris = false;
                        pilotaTrovato=true;
                    }
                }
            }while (!pilotaTrovato);
        }
        return  ris;
    }
//==================================================================================================

//==================================================================================================
//                              GET LABLE E STATO DEI AIUTI ALLA GUIDA
//==================================================================================================
    public List<Pair<String, Boolean>> getStatoDegliAiutiAllaGuida(){
        List<Pair<String, Boolean>> ris = new ArrayList<Pair<String, Boolean>>();
        ris.add(new Pair<String, Boolean>(Campionato.AIUTO_ABS,                 statoAiutoAbs));
        ris.add(new Pair<String, Boolean>(Campionato.AIUTO_AUTORETROMARCIA,     statoAiutoAutoretromarcia));
        ris.add(new Pair<String, Boolean>(Campionato.AIUTO_CONTROLLO_TRAZIONE,  statoAiutoControlloTrazione));
        ris.add(new Pair<String, Boolean>(Campionato.AIUTO_CORSIA_BOX,          statoAiutoCorsiaBox));
        ris.add(new Pair<String, Boolean>(Campionato.AIUTO_BLOCCAGGIO_OPPOSTO,  statoAiutoBloccagioOpposto));
        ris.add(new Pair<String, Boolean>(Campionato.AIUTO_FRIZIONE_AUTOMATICA, statoAiutoFrizioneAutomatica));
        ris.add(new Pair<String, Boolean>(Campionato.AIUTO_FRENATA,             statoAiutoFrenata));
        ris.add(new Pair<String, Boolean>(Campionato.AIUTO_CAMBIO_AUTOMATICO,   statoAiutoCambioAutomatico));
        ris.add(new Pair<String, Boolean>(Campionato.AIUTO_INVULNERABILITA,     statoAiutoInvulnerabilita));
        ris.add(new Pair<String, Boolean>(Campionato.AIUTO_STERZATA,            statoAiutoSterzata));
        ris.add(new Pair<String, Boolean>(Campionato.AIUTO_CONTROLLO_STABILITA, statoAiutoControlloStabilita));
        ris.add(new Pair<String, Boolean>(Campionato.AIUTO_RIPRESA_SPIN,        statoAiutoRipresaSpin));
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                              SET AIUTI ALLA GUIDA DA LABLE E STATO
//==================================================================================================
public void setStatoDellAiutiAllaGuida(Pair<String, Boolean> coppiaAiutiAllaGuidaState){
    switch (coppiaAiutiAllaGuidaState.first){
        case Campionato.AIUTO_ABS:
            this.statoAiutoAbs = coppiaAiutiAllaGuidaState.second;
            break;
        case Campionato.AIUTO_AUTORETROMARCIA:
            this.statoAiutoAutoretromarcia = coppiaAiutiAllaGuidaState.second;
            break;
        case Campionato.AIUTO_CONTROLLO_TRAZIONE:
            this.statoAiutoControlloTrazione = coppiaAiutiAllaGuidaState.second;
            break;
        case Campionato.AIUTO_CORSIA_BOX:
            this.statoAiutoCorsiaBox = coppiaAiutiAllaGuidaState.second;
            break;
        case Campionato.AIUTO_BLOCCAGGIO_OPPOSTO:
            this.statoAiutoBloccagioOpposto = coppiaAiutiAllaGuidaState.second;
            break;
        case Campionato.AIUTO_FRIZIONE_AUTOMATICA:
            this.statoAiutoFrizioneAutomatica = coppiaAiutiAllaGuidaState.second;
            break;
        case Campionato.AIUTO_FRENATA:
            this.statoAiutoFrenata = coppiaAiutiAllaGuidaState.second;
            break;
        case Campionato.AIUTO_CAMBIO_AUTOMATICO:
            this.statoAiutoCambioAutomatico = coppiaAiutiAllaGuidaState.second;
            break;
        case Campionato.AIUTO_INVULNERABILITA:
            this.statoAiutoInvulnerabilita = coppiaAiutiAllaGuidaState.second;
            break;
        case Campionato.AIUTO_STERZATA:
            this.statoAiutoSterzata = coppiaAiutiAllaGuidaState.second;
            break;
        case Campionato.AIUTO_CONTROLLO_STABILITA:
            this.statoAiutoControlloStabilita = coppiaAiutiAllaGuidaState.second;
            break;
        case Campionato.AIUTO_RIPRESA_SPIN:
            this.statoAiutoRipresaSpin = coppiaAiutiAllaGuidaState.second;
            break;
    }
}
//==================================================================================================
//==================================================================================================
//                              GET LABLE E STATO DELLE BANDIERE
//==================================================================================================
    public List<Pair<String, Boolean>> getStatoDelleBandiere(){
        List<Pair<String, Boolean>> ris = new ArrayList<Pair<String, Boolean>>();
        ris.add(new Pair<String, Boolean>(Campionato.BANDIERA_NERA,                bandiraNera));
        ris.add(new Pair<String, Boolean>(Campionato.BANDIERA_BLU,                 bandiraBlu));
        ris.add(new Pair<String, Boolean>(Campionato.BANDIERA_GIALLA,              bandiraGialla));
        ris.add(new Pair<String, Boolean>(Campionato.BANDIERA_ROSSA,               bandiraRossa));
        ris.add(new Pair<String, Boolean>(Campionato.BANDIERA_VERDE,               bandiraVerde));
        ris.add(new Pair<String, Boolean>(Campionato.BANDIERA_BIANCA_NERA,         bandiraBiancaENera));
        ris.add(new Pair<String, Boolean>(Campionato.BANDIERA_BIANCA_DISCO_ARANCIO,bandiraBiancaDiscoArancio));
        ris.add(new Pair<String, Boolean>(Campionato.BANDIERA_STRISCE_GIALLE_ROSE, bandiraStrisceGialleERosse));
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                              SET BANDIERA DA LABLE E STATO
//==================================================================================================
    public void setStatoDelleBandiera(Pair<String, Boolean> coppiaBandieraState){
        switch (coppiaBandieraState.first){
            case Campionato.BANDIERA_NERA:
                this.bandiraNera = coppiaBandieraState.second;
                break;
            case Campionato.BANDIERA_BLU:
                this.bandiraBlu = coppiaBandieraState.second;
                break;
            case Campionato.BANDIERA_GIALLA:
                this.bandiraGialla = coppiaBandieraState.second;
                break;
            case Campionato.BANDIERA_ROSSA:
                this.bandiraRossa = coppiaBandieraState.second;
                break;
            case Campionato.BANDIERA_VERDE:
                this.bandiraVerde = coppiaBandieraState.second;
                break;
            case Campionato.BANDIERA_BIANCA_NERA:
                this.bandiraBiancaENera = coppiaBandieraState.second;
                break;
            case Campionato.BANDIERA_BIANCA_DISCO_ARANCIO:
                this.bandiraBiancaDiscoArancio = coppiaBandieraState.second;
                break;
            case Campionato.BANDIERA_STRISCE_GIALLE_ROSE:
                this.bandiraStrisceGialleERosse = coppiaBandieraState.second;
                break;
        }
    }
//==================================================================================================

//##################################################################################################
//                                  EQUALS
//##################################################################################################
//==================================================================================================
//                              EQUALS E HASHCODE GENERICI
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Campionato)) return false;
        Campionato that = (Campionato) o;
        return bandiraNera == that.bandiraNera &&
                bandiraBlu == that.bandiraBlu &&
                bandiraGialla == that.bandiraGialla &&
                bandiraRossa == that.bandiraRossa &&
                bandiraVerde == that.bandiraVerde &&
                bandiraBiancaENera == that.bandiraBiancaENera &&
                bandiraBiancaDiscoArancio == that.bandiraBiancaDiscoArancio &&
                bandiraStrisceGialleERosse == that.bandiraStrisceGialleERosse &&
                statoAiutoAbs == that.statoAiutoAbs &&
                statoAiutoAutoretromarcia == that.statoAiutoAutoretromarcia &&
                statoAiutoControlloTrazione == that.statoAiutoControlloTrazione &&
                statoAiutoCorsiaBox == that.statoAiutoCorsiaBox &&
                statoAiutoBloccagioOpposto == that.statoAiutoBloccagioOpposto &&
                statoAiutoFrizioneAutomatica == that.statoAiutoFrizioneAutomatica &&
                statoAiutoFrenata == that.statoAiutoFrenata &&
                statoAiutoCambioAutomatico == that.statoAiutoCambioAutomatico &&
                statoAiutoInvulnerabilita == that.statoAiutoInvulnerabilita &&
                statoAiutoSterzata == that.statoAiutoSterzata &&
                statoAiutoControlloStabilita == that.statoAiutoControlloStabilita &&
                statoAiutoRipresaSpin == that.statoAiutoRipresaSpin &&
                MAX_NUM_ISCRITTI.equals(that.MAX_NUM_ISCRITTI) &&
                getId().equals(that.getId()) &&
                getName().equals(that.getName()) &&
                getConsumiCarburante() == that.getConsumiCarburante() &&
                getConsumiGomme() == that.getConsumiGomme() &&

                getListaGara().equals(that.getListaGara()) &&
                getListaAuto().equals(that.getListaAuto());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
//==================================================================================================
//==================================================================================================
//                              EQUALS BANDIERE
//==================================================================================================
    public boolean equalsBandere(Campionato campionato){
        boolean ris = false;
        if(campionato!=null){
            ris = ( (this.bandiraBiancaDiscoArancio==campionato.bandiraBiancaDiscoArancio) &&
                    (this.bandiraBiancaENera==campionato.bandiraBiancaENera) &&
                    (this.bandiraBlu==campionato.bandiraBlu) &&
                    (this.bandiraGialla==campionato.bandiraGialla) &&
                    (this.bandiraNera==campionato.bandiraNera) &&
                    (this.bandiraRossa==campionato.bandiraRossa) &&
                    (this.bandiraStrisceGialleERosse==campionato.bandiraStrisceGialleERosse) &&
                    (this.bandiraVerde=campionato.bandiraVerde) );
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                              EQUALS AIUTI ALLA GUIDA
//==================================================================================================
public boolean equalsAiutiAllaGuida(Campionato campionato){
    boolean ris = false;
    if(campionato!=null){
        ris = ( (this.statoAiutoAbs==campionato.statoAiutoAbs) &&
                (this.statoAiutoAutoretromarcia==campionato.statoAiutoAutoretromarcia) &&
                (this.statoAiutoBloccagioOpposto==campionato.statoAiutoBloccagioOpposto) &&
                (this.statoAiutoCambioAutomatico==campionato.statoAiutoCambioAutomatico) &&
                (this.statoAiutoControlloStabilita==campionato.statoAiutoControlloStabilita) &&
                (this.statoAiutoControlloTrazione==campionato.statoAiutoControlloTrazione) &&
                (this.statoAiutoCorsiaBox==campionato.statoAiutoCorsiaBox) &&
                (this.statoAiutoFrenata==campionato.statoAiutoFrenata) &&
                (this.statoAiutoFrizioneAutomatica==campionato.statoAiutoFrizioneAutomatica) &&
                (this.statoAiutoInvulnerabilita==campionato.statoAiutoInvulnerabilita) &&
                (this.statoAiutoRipresaSpin==campionato.statoAiutoRipresaSpin) &&
                (this.statoAiutoSterzata==campionato.statoAiutoSterzata)  );
    }
    return ris;
}
//==================================================================================================
//==================================================================================================
//                              EQUALS CONSUMI
//==================================================================================================
    public boolean equalsConsumi(Campionato campionato){
        boolean ris = false;
        if(campionato!=null){
            ris = ( (this.getConsumiGomme()==campionato.getConsumiGomme()) &&
                    (this.getConsumiCarburante()==campionato.getConsumiCarburante()) );
        }
        return ris;
    }
//==================================================================================================
//##################################################################################################


}
