package com.example.simcareerapp.AppCompatActivityWithMenu;


import android.app.NotificationManager;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.simcareerapp.AppCompatActivityWithMenu.MyServiceDiNotifiche.MyServizioDiNotifiche;
import com.example.simcareerapp.GalleriaActivity;
import com.example.simcareerapp.CampionatiActivity;
import com.example.simcareerapp.GestioneMyIntentEPersistableBundle.MyIntent;
import com.example.simcareerapp.GestioneRicordaSessione.GestioneSessione;
import com.example.simcareerapp.LoginActivity;
import com.example.simcareerapp.MieProssimeGareActivity;
import com.example.simcareerapp.Persistence.Entities.Utente;
import com.example.simcareerapp.TestingAppActivity;
import com.google.android.material.navigation.NavigationView;

import com.example.simcareerapp.ProfiloUtenteActivity;
import com.example.simcareerapp.R;

//##################################################################################################
//                                  CLASSE PER LE ACTIVITY CON IL MENU' LATERALE PRINCIPLAE
//##################################################################################################
/**
 * Questa classe è stata crata estendendo la classe AppCompatActivity, per gestire in maniera univoca
 * e autonoma tutte le funzioni che devono essere eseguite per utilizzare il menù laterale principale
 * del app.
 * In oltre permette a qualunque tra le pagine che hanno il menù laterale di lanciare il servizzio
 * che si occupa di rilevare e notificare eventuali modifiche dei campionati.
 *
 * @author Brian Emanuel
 */
public class AppCompatActivityWithMyPrimaryMenu extends AppCompatActivity {
    /**
     * L'attributo "toolbar" contiene l'oggetto che connterrà la toolbar principale dell'activity.
     */
    private Toolbar toolbar=null;
    //Menu Laterale Principale
    /**
     * L'attributo "menuLateralePrincipale" contiene l'oggetto che connterrà il menù laterale principale.
     */
    private DrawerLayout menuLateralePrincipale;
    /**
     * L'attributo "myToggle" contiene l'oggetto che si occupa dell'operazione di toggle del menù laterale
     * principale, cioè si occupa della apertura (chiusura) del menù leterale se chiuso (aperto).
     */
    private ActionBarDrawerToggle myToggle;
    /**
     * L'attributo "menuLayout" contiene l'identificatore della serie di pulsanti da usare nella toolbar.
     */
    private int menuLayout;
//==================================================================================================
//                                  CREAZIONE DELLA TOOLBAR E DEL MENU PRINCIPALE LATERALE
//==================================================================================================
    /**
     * Metodo chiamato nel onCreate delle activity che devono, definire il layout della toolbar, cioè definire
     * i pulsanti presenti su di esso, ed avere del menu laterale principale.
     * In oltre tale metodo è responsabile di avviare il servizzio di notifiche.
     *
     * @param menuLayout
     *      Questo peramento contiene l'identificativo della lista dei comandi che devono essere
     *      presenti nel menu laterale principale.
     */
    protected void initToolBar(int menuLayout){

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  GESTIONE DEL AVVIO NOTIFICHE
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Intent i =getIntent();
        Utente utenteAttivo          = MyIntent.getUtenteExtra(i,MyIntent.I_UTENTE_ATTIVO);
        Boolean mantieniAttivoUtente = i.getBooleanExtra(MyIntent.I_MANTIENI_ATTIVO_UTENTE,false);
        //Avvio servizzio di notifiche.
        MyServizioDiNotifiche.avviaServizioDiNotifiche(this,utenteAttivo,mantieniAttivoUtente);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        this.menuLayout=menuLayout;
        //Implemetazione della toolbar
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Implemetazione del menu laterale principale
        menuLateralePrincipale = findViewById(R.id.dly_MenuLateralePrincipale);
        myToggle  = new ActionBarDrawerToggle(this,menuLateralePrincipale,R.string.nav_Open,R.string.nav_Close);

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  LISTENER DEI BOTTONI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Listener dell' apertura e chiusura del menu laterale principale
        menuLateralePrincipale.addDrawerListener(myToggle);
        myToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//--------------------------------------------------------------------------------------------------
        // Listener dei pulsanti del menu laterale principale
        NavigationView menuLateralePrincipale = (NavigationView) findViewById(R.id.nav_menu_laterale_principale);
        menuLateralePrincipale.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener(){
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return gestioneSelezionePulsanti(item);
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        setSelectedButton();
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  GESTIONE DEI PULSANTI MENU' LATERALE PRINCIPLAE
//##################################################################################################
//==================================================================================================
//                                  IMPOSTAZIONE PULSANTE SELEZIONATO
//==================================================================================================
    /**
     * Questo metodo serve per impostare il tasto selezionato del menu laterale in modo tale che cambi
     * in base alla pagina/activity stessa che ha tale menu.
     */
    private void setSelectedButton() {
        NavigationView manuLateralePrincipale = findViewById(R.id.nav_menu_laterale_principale);
        if( (this.getClass().equals(CampionatiActivity.class)     )){
            manuLateralePrincipale.getMenu().getItem(0).setChecked(true);
        }
        if( (this.getClass().equals(ProfiloUtenteActivity.class)  )){
            manuLateralePrincipale.getMenu().getItem(1).setChecked(true);
        }
        if( (this.getClass().equals(MieProssimeGareActivity.class)  )){//TODO: ??
            manuLateralePrincipale.getMenu().getItem(2).setChecked(true);
        }
        if( (this.getClass().equals(GalleriaActivity.class)  )){
            manuLateralePrincipale.getMenu().getItem(3).setChecked(true);
        }
        if( (this.getClass().equals(TestingAppActivity.class)  )){
            manuLateralePrincipale.getMenu().getItem(4).setChecked(true);
        }
        if(!(this  instanceof  Object)){//TODO: INSERIRE LA CLASSE A CUI APPATIENE LA ACTIVITY DI DESTINAZIOONE   ATTEZIONE TOGLIERE LA NEGAZIONE
            manuLateralePrincipale.getMenu().getItem(5).setChecked(true);
        }
    }
//==================================================================================================

//==================================================================================================
//                                  METODO PER LA GESTIONE DEI PULSANTI PREMUTI NEL MENU LATERALE
//                                  PRINCIPALE
//==================================================================================================
    /**
     * Questo metodo serve per impostare l'azzione che il pulsante del menu laterale deve compire in
     * base alla pagina/activity in cui si trova. Se il tasto va verso un altra pagina chiama un metodo
     * che genra l'intent per passare a quella nuova, altrimeti chiude solametre il menu latrale dato
     * che ci si trova già in posizione senza ricaricarla.
     *
     * @param item
     *      Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *      è stato premuto dall'utente.
     */
    public boolean gestioneSelezionePulsanti(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.btn_nav_Campionati: {
                    if(!(this  instanceof  CampionatiActivity))
                        //Non sono nella pagina scelta
                        buttonCampionati(item);
                    else
                        //Sono gia nella pagina scelta
                        menuLateralePrincipale.closeDrawers();
                    break;
                    }

            case R.id.btn_nav_Profilo:{
                if(!(this  instanceof  ProfiloUtenteActivity))
                    //Non sono nella pagina scelta
                    buttonProfilo(item);
                else
                    //Sono gia nella pagina scelta
                    menuLateralePrincipale.closeDrawers();
                break;
                }

            case R.id.btn_nav_MieProssimeGare: {
                if(!(this  instanceof  MieProssimeGareActivity))
                    buttonMieProssimeGare(item);
                else
                    menuLateralePrincipale.closeDrawers();
                break;
                }

            case R.id.btn_nav_Galleria: {
                if(!(this  instanceof GalleriaActivity))
                    //Non sono nella pagina scelta
                    buttonGalleria(item);
                else
                    //Sono gia nella pagina scelta
                    menuLateralePrincipale.closeDrawers();
                break;
                }

            case R.id.btn_nav_TestingApp: {
                if(!(this  instanceof  TestingAppActivity))
                    //Non sono nella pagina scelta
                    buttonTestingApp(item);
                else
                    //Sono gia nella pagina scelta
                    menuLateralePrincipale.closeDrawers();
                break;
                }

            case R.id.btn_nav_Logout: {
                    buttonLogout(item);
                break;
                }
        }
        return false;
    }
//==================================================================================================

//==================================================================================================
//                                  METODI CHIAMATI ALLA PRESIONE DEI BOTTONI
//==================================================================================================
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante Campionati. Questo metodo non fa
     * altro che passare i dati tramite Intent ed avviare la nuova activiti.
     * @param item
     *      Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *      è stato premuto dall'utente.
     */
    private void buttonCampionati(MenuItem  item){
        try {
            MyIntent i = new MyIntent(this, CampionatiActivity.class);
            setParamiter(i);//IMPOTANTE: SERVE PER POI FARE L'OVERRIDE DEL METODO PER INSERIRE I PARAMETRI CHE SERVONO.
            startActivity(i);
            //Toast.makeText(this, "this.getClass() = "+ this.getClass()+" CampionatiActivity.class = " +CampionatiActivity.class, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante Profilo. Questo metodo non fa
     * altro che passare i dati tramite Intent ed avviare la nuova activiti.
     * @param item
     *      Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *      è stato premuto dall'utente.
     */
    private void buttonProfilo(MenuItem  item){
        try {
            MyIntent i = new MyIntent(this, ProfiloUtenteActivity.class);
            setParamiter(i);//IMPOTANTE: SERVE PER POI FARE L'OVERRIDE DEL METODO PER INSERIRE I PARAMETRI CHE SERVONO.
            startActivity(i);
            //Toast.makeText(this, "this.getClass() = "+ this.getClass()+" ProfiloUtenteActivity.class = " +ProfiloUtenteActivity.class, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            System.out.println(e);
        }

    }
//--------------------------------------------------------------------------------------------------
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante MieProssimeGare. Questo metodo non fa
     * altro che passare i dati tramite Intent ed avviare la nuova activiti.
     * @param item
     *     Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *     è stato premuto dall'utente.
     */
    private void buttonMieProssimeGare(MenuItem  item){
        try {
            MyIntent i = new MyIntent(this, MieProssimeGareActivity.class);
            setParamiter(i);//IMPOTANTE: SERVE PER POI FARE L'OVERRIDE DEL METODO PER INSERIRE I PARAMETRI CHE SERVONO.
            startActivity(i);
            //Toast.makeText(this, "this.getClass() = "+ this.getClass()+" ProfiloUtenteActivity.class = " +MieProssimeGareActivity.class, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante Galleria. Questo metodo non fa
     * altro che passare i dati tramite Intent ed avviare la nuova activiti.
     * @param item
     *     Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *     è stato premuto dall'utente.
     */
    private void buttonGalleria(MenuItem  item){
        try {
            MyIntent i = new MyIntent(this, GalleriaActivity.class);
            setParamiter(i);//IMPOTANTE: SERVE PER POI FARE L'OVERRIDE DEL METODO PER INSERIRE I PARAMETRI CHE SERVONO.
            startActivity(i);
            //Toast.makeText(this, "this.getClass() = "+ this.getClass()+" ProfiloUtenteActivity.class = " +GalleriaActivity.class, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante TestingApp. Questo metodo non fa
     * altro che passare i dati tramite Intent ed avviare la nuova activiti.
     * @param item
     *     Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *     è stato premuto dall'utente.
     */
    private void buttonTestingApp(MenuItem  item){
        try {
            MyIntent i = new MyIntent(this, TestingAppActivity.class);
            setParamiter(i);//IMPOTANTE: SERVE PER POI FARE L'OVERRIDE DEL METODO PER INSERIRE I PARAMETRI CHE SERVONO.
            startActivity(i);
            //Toast.makeText(this, "this.getClass() = "+ this.getClass()+" ProfiloUtenteActivity.class = " +TestingAppActivity.class, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante Logout. Questo metodo non fa
     * altro che cancellare i dati di sessioe e le notififiche dell'utente che stà uscendo.
     * @param item
     *     Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *     è stato premuto dall'utente.
     */
    private void buttonLogout(MenuItem  item){
        //Toast.makeText(this, "AppCompatActivityWithMyPrimaryMenu: Logout", Toast.LENGTH_SHORT).show();
        try {
            //Cancella le notifiche fatte
            MyServizioDiNotifiche.chiusuraServizioDiNotificheENotifiche(this);
            //Vai alla pagina di login
            GestioneSessione.cancellaSessioneAperte(this);//CANCELLA SESSIONE
            MyIntent i = new MyIntent(this, LoginActivity.class);
            startActivity(i);

        } catch (Exception e) {
            System.out.println(e);
        }
    }
//==================================================================================================

//==================================================================================================
//                                  METODO PER PASSARE I PARAMETRI ALLE ATRE PAGINE
//==================================================================================================
    /*
     * Le classi che estendono questa super classe, protranno fare l'override di questo medodo
     * così da addiunggere al intent chiamato in automatioco i parametri in maniera dinamica.
     * Questo petodo infatti non ha senso essere chiamato da solo, ma viene chamato in maierà automatica
     * al momento che si pattiva l'azzione legata alla presione di un tasto tra quelli del menù laterale
     * principale.
     * @param intent
     *      Tale parametro contiene l'azione che verra fatta al termine di questà operazione, quindi
     *      permette l'inserimento in esso dei dati.
     **/
    protected void setParamiter(MyIntent intent){
    };
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  RIDUCE APPLICAZIONE
//##################################################################################################
//==================================================================================================
//                                  METODO PER IMPEDIRE DI RAGGIUNGERE ACTIVITY PRECEDENTI
//==================================================================================================
    /**
     * Questo metodo impedisce di ritornare tramite il tasto "Fisico/Generico" indietro dello smartphone alle
     * activity precedenti così da rendere la activity attuale quella di root per la navigazione.
     * Infatti se si preme tale pulsante l'applicazione verrà spostata tra quelle non attive mostrando
     * la pagina principale del dispositivo.
     */
    protected void impostaPaginaComeHome(){
        this.getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startMain);
            }
        });
    }
//==================================================================================================

//##################################################################################################

//##################################################################################################
//                                  GESTIONE APERTURA, CHIUSURA E INSERIMETO BOTTONE PER ACCDERE AL
//                                  MENU' LATERALE PRINCIPLAE
//##################################################################################################
//==================================================================================================
//                                  DEFINIZIONE DEL MENU' (senza il tasto a sinistra)
//==================================================================================================
    /**
     * Questo metodo serve per non avere il tasto sinistro.
     *
     * @see AppCompatActivity#onCreateOptionsMenu(Menu)
     * @return Restituisce solo true.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(menuLayout,menu);
        return true;
    }
//==================================================================================================

//==================================================================================================
//                                  METODO PER LA APERTURA E CHIUSURA DEL MENU LATERALE PRINCIPALE
//==================================================================================================
    /**
     * Questo metodo serve per aprire e chiudere il menu latrale
     *
     * @see AppCompatActivity#onOptionsItemSelected(MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(myToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
//==================================================================================================
//##################################################################################################
}
