package com.example.simcareerapp.Persistence.Entities.Interface;

import android.view.View;
import android.widget.ImageView;


import com.example.simcareerapp.R;

import java.util.Objects;

public class ValoreConImmaginePerSpinner {
    protected String nome= null;
    protected Integer idImmagine= null;
//==================================================================================================
//                                  Costrottore
//==================================================================================================
    public ValoreConImmaginePerSpinner(String nome) {
        this.nome = nome;
    }
//==================================================================================================

//==================================================================================================
//                                  Getter
//==================================================================================================
    public String getNome() {
        return nome;
    }
    //-------------------------------------------------------------------------------------------------
    public Integer getIdImmagine() {
        return idImmagine;
    }
//==================================================================================================

//==================================================================================================
//                                  Set immaine
//==================================================================================================
    public void impostaImmagine(View v, int id) {
        ImageView viewImage=v.findViewById(id);
        impostaImmagine(viewImage);
    }
    //-------------------------------------------------------------------------------------------------
    public void impostaImmagine(ImageView viewImage) {
        if(viewImage!=null){
            viewImage.setImageResource( idImmagine);
        }
    }
//==================================================================================================

//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ValoreConImmaginePerSpinner)) return false;
        ValoreConImmaginePerSpinner that = (ValoreConImmaginePerSpinner) o;
        boolean ris1 = ( getNome().equals(that.getNome()) );
        boolean ris2 = ( getIdImmagine().equals(that.getIdImmagine()));
        return ris1&&ris2;
    }
//==================================================================================================
    @Override
    public int hashCode() {
        return Objects.hash(getNome(), getIdImmagine());
    }
//==================================================================================================
}
