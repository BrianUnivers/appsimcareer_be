package com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp;

import android.content.Context;
import android.view.View;

import com.example.simcareerapp.GestoriDeiLayoutImportati.MyAlertDialogApp.Interface.OnAletDialogRitiratiDalCampionatoEventListener;
import com.example.simcareerapp.Persistence.Entities.Campionato;
import com.example.simcareerapp.Persistence.Entities.Pilota;
import com.example.simcareerapp.Persistence.Entities.Utente;

import java.util.Iterator;

public class AlertDialogRitiratiDalCampionato extends MyGenericAlertDialog {
    private Campionato campionato;
    private Utente utenteAttivo;
//==================================================================================================
//                                  CREAZIONE DEL ALERTDIALOG
//==================================================================================================
    /**
     * Questo è il cortutore a che serve per sapiere l'utente e il campionato a cui si stà per discrivere
     */
    public AlertDialogRitiratiDalCampionato(Context context, Campionato campionato, Utente utenteAttivo, int idAlertDialog, int idIcon, int idTitle, int idBtnPrimario, int idBtnSecondario) {
        super(context, idAlertDialog, idIcon, idTitle, idBtnPrimario, idBtnSecondario);
        this.campionato = campionato;
        this.utenteAttivo = utenteAttivo;
    }
//==================================================================================================


//==================================================================================================
//                                  METODI CHIAMATI ALLA PRESIONE DEI BOTTONI
//==================================================================================================
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante Conferma del AlertDialoge
     * prende dal campionato il pilota corrispondente al nome del utente attivo.
     * Se non è presente allora passera un oggetto null alla funzione
     * informazioniPerAnnulareIscrizioneAlCampionato
     */
    @Override
    protected void listenerButtonPrimario(View v){
        Pilota pilotaTrovato = null;

        Boolean ricercaPilotaTrovato = false;
        Iterator<Pilota> iterator = campionato.getListaPiloti().iterator();
        while ((iterator.hasNext()) && (!ricercaPilotaTrovato)){
            Pilota pilota = iterator.next();
            if(utenteAttivo.equalsToPiloti(pilota)){
                pilotaTrovato = pilota;
                ricercaPilotaTrovato = true;
            }
        }

        if( ((OnAletDialogRitiratiDalCampionatoEventListener)context).informazioniPerAnnulareIscrizioneAlCampionato(pilotaTrovato) ){
            this.cancel();
        }
    }
//==================================================================================================


//##################################################################################################

}
