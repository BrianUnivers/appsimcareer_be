package com.example.simcareerapp.DataBase.ThreadAction;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;

import com.example.simcareerapp.DataBase.Management.HashPasswordWithSalt;
import com.example.simcareerapp.DataBase.Management.SimpleDbHelper;
import com.example.simcareerapp.DataBase.Query.UtenteQuery;
import com.example.simcareerapp.Persistence.Entities.Utente;

import java.sql.Date;

public class ThreadRegistraUtenteParziale extends AsyncTask<Utente,Integer, Utente> {
    private SimpleDbHelper database;
    private Context context;
//==================================================================================================
//                              COSTRUTORE                                                          <<Processo Principale>>
//==================================================================================================
    /**
     * Per avere la posibilità di accedere alla Accivity e instauro un collegamento col server.
     */
    public ThreadRegistraUtenteParziale(Context context) {
        //Crazione DataBase o Associazione con uno già creato
        this.database = new SimpleDbHelper(context);
        this.context = context;
    }
//==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE PRIMA DELLA CHAMATA DEL THREAD                     <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
//==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DOPO DELLA CHAMATA DEL THREAD                      <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onPostExecute(Utente utente) {
        super.onPostExecute(utente);
    }
    //==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DURANTE DELLA CHAMATA DEL THREAD                   <<Processo Principale>>
//==================================================================================================
    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }
//==================================================================================================
//==================================================================================================
//                              OPERAZIONI FATTE DAL THREAD                                         <<Processo Seconario>>
//==================================================================================================
    /**
     * Inserisce il nuovo utente nel database e retituisce l'oggeto utente inserito col id del record
     * in cui è stato inserito se non è  stato possibile inserire l'utente verra ritornato null.
     */
    @Override
    protected Utente doInBackground(Utente... utenti) {
        Utente utente = utenti[0];

        Long id = UtenteQuery.insertUtenteDatiParziali(database,utente);
        if(id>=0){
            //L'utnete è inserito con successo
            utente.setId(id);
        }else{
            //L'utente non è stato inserito con succeso.
            utente=null;
        }
        return utente;
    }
//==================================================================================================
}
